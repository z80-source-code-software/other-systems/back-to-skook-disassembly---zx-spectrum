; Back to Skool disassembly
; https://skoolkit.ca
;
; Copyright 1985 Microsphere Computer Services Ltd (Back to Skool)
; Copyright 2001, 2008-2019 Richard Dymond (this disassembly)

  ORG 20631

  LD HL,FALLING2
  LD SP,HL
  LD C,L
  LD E,L
  JP COUNTDOWN1

; Clear character buffer 214
;
; Used by the routine at INPUTDEV. Clearing character buffer 214 serves no
; useful purpose; it is not used until the game has started, and is properly
; initialised before then anyway.
;
; C 0
CCBUF214:
  LD HL,CBUF214           ; Fill character buffer 214 with zeroes
  LD B,32                 ;
CCBUF214_0:
  LD (HL),C               ;
  INC L                   ;
  DJNZ CCBUF214_0         ;
  RET

; POKE table
;
; Used by the routine at GAMEPOKES.
POKETABLE:
  DEFW PRINTTILE_4        ; Change the instruction at PRINTTILE_4 from NOP
                          ; to...
  DEFB 2                  ; ...LD (BC),A
  DEFW PRINTTILE_9        ; Change the instruction at PRINTTILE_9 from XOR A
                          ; to...
  DEFB 26                 ; ...LD A,(DE)
  DEFW 27130              ; Change the instruction at CHR2BUF_3 from LD B,32
                          ; to...
  DEFB 8                  ; ...LD B,8
  DEFW 27428              ; Change the instruction at BUFSIZE from LD BC,0
                          ; to...
  DEFB 64                 ; ...LD BC,16384
  DEFW 27438              ; Change the instruction at TXTWIDTH from LD C,255
                          ; to...
  DEFB 64                 ; ...LD C,64

; Print a message centred across the entire screen
;
; Used by the routine at GETNAMES.
;
; A Message number
PRINTMSG:
  LD HL,20480             ; 20480=display file address
; This entry point is used by the routines at CHANGENAME, GETNAMES, INPUTDEV
; and INPUTDEV2 with HL holding the appropriate display file address.
PRINTMSG_0:
  PUSH HL                 ; Save the display file address
  LD L,A                  ; L=message number
  BIT 7,A                 ; Is the message number < 128?
  JR Z,PRINTMSG_1         ; Jump if so
  LD H,80                 ; Point HL at the appropriate entry in the message
                          ; address table at MSGADDRS
  LD E,(HL)               ; E=LSB of the message address
  INC HL                  ; Point HL at the MSB
  JR PRINTMSG_2
PRINTMSG_1:
  LD H,254                ; Use the regular message address table for message
                          ; numbers < 128
  LD E,(HL)               ; E=LSB of the message address
  INC H                   ; Point HL at the MSB
PRINTMSG_2:
  LD D,(HL)               ; Now DE=address of the message to print
  LD HL,MSGBUFFER         ; Prepare the message in screen-ready form in the
  PUSH HL                 ; buffer at MSGBUFFER
  CALL TXT2BUF            ;
  POP HL                  ;
  POP DE                  ; Restore the display file address to DE
PRINTMSG_3:
  PUSH DE                 ; Display the message on-screen
  LD C,32                 ;
  LDIR                    ;
  POP DE                  ;
  INC D                   ;
  BIT 0,H                 ;
  JR NZ,PRINTMSG_3        ;
  RET

; Unused
  DEFS 3

; Populate a row of the screen with machine code
;
; Used by the routine at 33204. Copies 256 bytes of machine code from the
; source (either the top row of the screen, or character buffers 183-190) to
; the destination (the second or third row from the bottom of the screen), in
; eight 32-byte blocks.
;
; HL Source (16384 or 46848)
; DE Destination display file address (20640 or 20672)
MC2SCREEN:
  LD BC,32                ; There are 32 bytes per row of pixels
  PUSH DE
  LDIR                    ; Copy 32 bytes to the display file
  LD L,B                  ; L=0
  POP DE
  INC D                   ; Point DE at the start of the next row of pixels
  INC H                   ; Point HL at the next batch of 32 bytes to copy
  BIT 3,D                 ; Have we copied 8 rows of bytes yet?
  JR Z,MC2SCREEN          ; Jump back if not
  RET

; Message address table
;
; Used by the routine at PRINTMSG.
MSGADDRS:
  DEFW MSG244             ; Message 244: '' (empty string)
  DEFW MSG246             ; Message 246: 'None/Cursor/Kempston/Int2? (N,C,K or
                          ; I)'
  DEFW MSG248             ; Message 248: '{THE }CHARACTERS'
  DEFW MSG250             ; Message 250: 'Press 'C' to change name'
  DEFW MSG252             ; Message 252: 'Enter new name'

  DEFS 162

; Change a character's name
;
; Used by the routine at GETNAMES. Waits for keypresses while a character's
; name is being changed, and prints them as they are typed. Returns when ENTER
; is pressed.
;
; A Message number of the character's name (21-31)
CHANGENAME:
  LD L,A                  ; Collect the start address of the message (the
  LD H,254                ; character's name) into HL
  LD E,(HL)               ;
  INC H                   ;
  LD D,(HL)               ;
  EX DE,HL                ;
  LD BC,3072              ; B=12, C=0
CHANGENAME_0:
  LD (HL),C               ; Blank out the current name (12 bytes)
  INC HL                  ;
  DJNZ CHANGENAME_0       ;
CHANGENAME_1:
  PUSH AF                 ; Save the message number
  LD HL,20544             ; 20544=display file address at which to print the
                          ; name
  CALL PRINTMSG_0         ; Print the name built up so far
CHANGENAME_2:
  CALL READKEY_1          ; Wait for a keypress
  JR Z,CHANGENAME_2       ;
  CP 13                   ; Set the zero flag if ENTER was pressed
  LD B,A                  ; B=keypress code
  JR NZ,CHANGENAME_3      ; Jump unless ENTER was pressed
  POP AF                  ; Restore the message number to A
  RET                     ; Return with the new name entered
; A key was pressed, and it wasn't ENTER. Place the ASCII code in the right
; slot of the message area corresponding to the character's name.
CHANGENAME_3:
  POP AF                  ; Restore the message number to A
  LD L,A                  ; Collect the start address of the message (the
  LD H,254                ; character's name) into DE
  LD E,(HL)               ;
  INC H                   ;
  LD D,(HL)               ;
  LD HL,11
  LD C,A                  ; Store the message number in C briefly
  ADD HL,DE               ; Point HL at the last byte in the message area for
                          ; the character's name
  LD A,(HL)               ; Pick up this last byte in A
  AND A                   ; Is the message area already full?
  JR NZ,CHANGENAME_5      ; Jump if so
  EX DE,HL                ; HL=start address of the message area
  DEC HL
CHANGENAME_4:
  INC HL                  ; Point HL at the first unused slot in the message
  LD A,(HL)               ; area
  AND A                   ;
  JR NZ,CHANGENAME_4      ;
  LD (HL),B               ; Place the letter just entered into this slot
CHANGENAME_5:
  LD A,C                  ; Restore the message number to A
  JR CHANGENAME_1         ; Print the letter and collect another

; Guide a character onto the catwalk or off it
;
; Used by the routine at GETNAMES. Makes a character walk in from the left to
; the middle of the screen (at which point his name may be changed), or from
; the middle of the screen and off to the right.
WALKONOFF:
  LD B,40                 ; The character will walk 40 paces
WALKONOFF_0:
  PUSH BC
  LD H,210                ; We use ERIC's buffer at page 210 to control the
                          ; character
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  INC A                   ; A=character's next animatory state
  AND 251                 ;
  BIT 0,A                 ; Is the character midstride now?
  JR NZ,WALKONOFF_1       ; Jump if so
  INC E                   ; Otherwise move forward a pace
WALKONOFF_1:
  CALL UPDATEAS           ; Update the character's animatory state and location
                          ; and update the SRB
  CALL UPDATESCR          ; Update the display
  CALL WALKSOUNDY         ; Make a walking sound effect
  LD C,100                ; Wait a bit
WALKONOFF_2:
  DJNZ WALKONOFF_2        ;
  DEC C                   ;
  JR NZ,WALKONOFF_2       ;
  POP BC
  DJNZ WALKONOFF_0        ; Jump back until all 40 paces have been walked
  RET

  DEFS 160

; Display the cast of characters and change their names
;
; Used by the routine at INPUTDEV2. For each member of the cast: displays his
; name and title, walks him onto the screen, allows the user to change the
; name, and then walks him off the screen.
GETNAMES:
  LD A,21                 ; Message 21: 'MR WACKER'
GETNAMES_0:
  PUSH AF                 ; Save the message number
  ADD A,80                ; A=101-111 (message number for the character's
                          ; title)
  LD HL,16512             ; 16512=display file address
  CALL PRINTMSG_0         ; Print the appropriate title (e.g. 'THE HEADMASTER')
  LD A,244                ; Message 244: '' (empty string)
  CALL PRINTMSG           ; Print this (thus removing 'Press 'C' to change
                          ; name' from the screen)
  POP AF                  ; Restore the message number for the character's name
                          ; to A
  PUSH AF                 ; Save it again
  LD HL,20544             ; 20544=display file address
  CALL PRINTMSG_0         ; Print the character's name there
  POP AF                  ; Restore the message number for the character's name
                          ; to A
  PUSH AF                 ; Save it again
  ADD A,224               ; Point HL at an entry in the table of animatory
  LD L,A                  ; states at ANIMSTATES
  LD H,82                 ;
  LD A,(HL)               ; A=animatory state of the character standing, facing
                          ; right
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC's buffer
  LD (HL),A               ; Fill in the animatory state
  INC L                   ; L=1
  LD (HL),124             ; Fill in the character's x-coordinate
  CALL WALKONOFF          ; Walk the character to the middle of the screen
GETNAMES_1:
  LD HL,23611             ; Reset bit 5 at 23611, ready for future keypresses
  RES 5,(HL)              ;
  LD A,250                ; Message 250: 'Press 'C' to change name'
  CALL PRINTMSG           ; Print this message
GETNAMES_2:
  CALL READKEY_1          ; Wait for a keypress
  JR Z,GETNAMES_2         ;
  OR 32                   ; Convert the keypress to lower case
  CP 99                   ; Was 'c' pressed?
  JR NZ,GETNAMES_3        ; Jump if not
; The user pressed 'c' to change the character's name.
  LD A,252                ; Message 252: 'Enter new name'
  CALL PRINTMSG           ; Print this message
  POP AF                  ; Restore the message number for the character's name
                          ; to A
  PUSH AF                 ; Save it again
  CALL CHANGENAME         ; Change the character's name
  JR GETNAMES_1
; The user pressed some key other than 'c'.
GETNAMES_3:
  CALL WALKONOFF          ; Walk the character off screen
  POP AF                  ; Restore the message number for the character's name
                          ; to A
  INC A                   ; Next character
  CP 32                   ; Have we done all the main game characters yet?
  JR NZ,GETNAMES_0        ; Jump back if not
  RET

; Table of animatory states of the main characters
;
; Used by the routine at GETNAMES.
ANIMSTATES:
  DEFB 208                ; 208=MR WACKER
  DEFB 216                ; 216=MR WITHIT
  DEFB 224                ; 224=MR ROCKITT
  DEFB 232                ; 232=MR CREAK
  DEFB 240                ; 240=MISS TAKE
  DEFB 248                ; 248=ALBERT
  DEFB 144                ; 144=BOY WANDER
  DEFB 160                ; 160=ANGELFACE
  DEFB 176                ; 176=EINSTEIN
  DEFB 184                ; 184=HAYLEY
  DEFB 128                ; 128=ERIC

  DEFS 160

; Do essential POKEs before the game starts
;
; Used by the routine at INPUTDEV2. Uses the table at POKETABLE to do some
; POKEs that are required before the game starts.
;
; HL POKETABLE
GAMEPOKES:
  BIT 0,(HL)              ; Have we reached the end of the POKE table?
  RET NZ                  ; Return if so
  LD E,(HL)               ; Pick up the address that needs to be POKEd in DE
  INC L                   ;
  LD D,(HL)               ;
  INC L                   ; Point HL at the POKE operand
  LDI                     ; Do the POKE
  JR GAMEPOKES            ; Jump back to do the next POKE

; Make a walking sound effect (yellow border)
;
; Used by the routines at INPUTDEV and WALKONOFF.
WALKSOUNDY:
  PUSH BC
  LD A,6                  ; The border will be yellow
  JP WALKSOUND_0          ; Make the sound effect

; Clear the screen and request the input method (keyboard, joystick, Int2)
;
; Used by the routine at COUNTDOWN.
INPUTDEV:
  LD BC,640               ; INK 0: PAPER 6 for the top 20 lines of screen
  LD DE,22529             ;
  LD HL,22528             ;
  LD (HL),48              ;
  LDIR                    ;
  LD C,128                ; INK 6: PAPER 6 for the bottom 4 lines of screen
  LD (HL),54              ;
  LDIR                    ;
  CALL WALKSOUNDY         ; Make a short beep
  LD H,64                 ; Clear the top two-thirds of the display file
  LD D,H                  ;
  LD (HL),L               ;
  LD B,16                 ;
  LDIR                    ;
INPUTDEV_0:
  LD B,160                ; Clear the remainder of the screen, except the
INPUTDEV_1:
  LD (HL),C               ; bottom 3 lines (which contain machine code)
  INC L                   ;
  DJNZ INPUTDEV_1         ;
  LD L,B                  ;
  INC H                   ;
  BIT 3,H                 ;
  JR Z,INPUTDEV_0         ;
  LD A,128                ; Set the column of the play area at the far left of
  LD (LEFTCOL),A          ; the screen to 128
  NOP
  CALL CCBUF214           ; Clear character buffer 214 (for no good reason)
  LD A,246                ; Message 246: 'None/Cursor/Kempston/Int2? (N,C,K or
                          ; I)'
  LD HL,16448             ; Print this message
  CALL PRINTMSG_0         ;
  LD HL,23611             ; Clear bit 5 at 23611, ready for future keypresses
  RES 5,(HL)              ;
  JP INPUTDEV2

; Unused
  DEFS 2

; Keypress vector table for Int2
;
; Used by the routine at INPUTDEV2. These 10 bytes are copied to KEYTABLE in
; the keypress offset table (corresponding to keys 0-9) if Int2 is selected.
INT2:
  DEFB 104                ; 0: Fire catapult
  DEFB 80                 ; 1: Left (fast)
  DEFB 82                 ; 2: Right (fast)
  DEFB 86                 ; 3: Down (fast)
  DEFB 84                 ; 4: Up (fast)
  DEFB 104                ; 5: Fire
  DEFB 80                 ; 6: Left (fast)
  DEFB 82                 ; 7: Right (fast)
  DEFB 86                 ; 8: Down (fast)
  DEFB 84                 ; 9: Up (fast)

  DEFS 160

; Set the input method, change the characters' names, and start the game
;
; Continues from INPUTDEV.
INPUTDEV2:
  CALL READKEY_1          ; Wait for a keypress, and collect its ASCII code in
  JR Z,INPUTDEV2          ; A
  OR 32                   ; Convert the keypress to lower case
  CP 99                   ; Was 'c' (Cursor) pressed?
  JR Z,INPUTDEV2_1        ; Jump if so
  CP 110                  ; Was 'n' (None) pressed?
  JR Z,INPUTDEV2_1        ; Jump if so
  CP 107                  ; Was 'k' (Kempston) pressed?
  JR NZ,INPUTDEV2_0       ; Jump if not
  LD A,1                  ; Set KEMPSTON to 1: using Kempston
  LD (KEMPSTON),A         ;
  JR INPUTDEV2_1
INPUTDEV2_0:
  CP 105                  ; Was 'i' (Int2) pressed?
  JR NZ,INPUTDEV2         ; Jump back to wait for another keypress if not
  LD HL,INT2              ; Copy the 10-byte table at INT2 into the keypress
  LD DE,KEYTABLE          ; offset table at KEYTABLE if Int2 is being used
  LD BC,10                ;
  LDIR                    ;
; The input method has been established. Now prepare the screen for the cast of
; characters.
INPUTDEV2_1:
  LD HL,46849             ; Draw a line (the catwalk) across the screen, and
  LD DE,18560             ; set byte 1 of each character's buffer (which holds
  LD B,32                 ; the character's x-coordinate) to a number between 1
  LD A,255                ; and 32 (so that they are off-screen: the column of
INPUTDEV2_2:
  LD (HL),B               ; the play area at the far left of the screen, stored
  LD (DE),A               ; at LEFTCOL, was set to 128 earlier on)
  INC H                   ;
  INC E                   ;
  DJNZ INPUTDEV2_2        ;
  LD HL,53762             ; Set byte 2 of ERIC's character buffer (which holds
  LD (HL),8               ; his y-coordinate) to 8; this will be the
                          ; y-coordinate of the characters as they proceed
                          ; along the catwalk
  LD A,248                ; Message 248: 'THE CHARACTERS'
  LD HL,16448             ; Print this message
  CALL PRINTMSG_0         ;
  CALL GETNAMES           ; Display the main characters and let the user change
                          ; their names
; This entry point is used by the routine at COUNTDOWN.
INPUTDEV2_3:
  LD HL,POKETABLE         ; Do the POKEs specified by the table at POKETABLE
  CALL GAMEPOKES          ;
  JP START                ; Start the game

; Prepare addresses 23808 to 24575
;
; Used by the routine at 33204. Copies 256 bytes from eight 32-byte segments.
; The routine is called three times with the values in DE and HL shown below.
; The 24 32-byte segments are used later on as character buffers.
;
; DE 23808, 24064 or 24320
; H 191, 199 or 207
; L 0
PREP93:
  LD A,8                  ; There are 8 chunks of 32 bytes to copy
PREP93_0:
  LD BC,32                ; Copy 32 bytes
  LDIR                    ;
  INC H                   ; Point HL at the next chunk of 32 bytes to copy
  LD L,B                  ;
  DEC A                   ; Next chunk
  JR NZ,PREP93_0          ; Jump back until all 8 chunks have been copied
  RET

  DEFS 161

; Count down from 9 to 0
;
; Continues from the routine at 33204. Counts down from 9 to 0, giving the user
; a chance to set the input method and change the characters' names before the
; game starts.
;
; BC 256
; DE 33024
; HL 23296
COUNTDOWN:
  LDIR                    ; Copy 256 bytes from 23296 to 33024
COUNTDOWN1:
  LD H,88                 ; INK 1: PAPER 1
  LD B,3                  ;
  LD D,H                  ;
  INC E                   ;
  LD (HL),9               ;
  LDIR                    ;
  LD HL,23611             ; Reset bit 5 at 23611, ready for future keypresses
  RES 5,(HL)              ;
  EXX                     ; Point HL' at the last byte of the graphic data for
  LD HL,15823             ; the digit '9' in the ROM
  EXX                     ;
COUNTDOWN_0:
  LD DE,1084              ; Wait approximately one second
COUNTDOWN_1:
  DJNZ COUNTDOWN_1        ;
  DEC DE                  ;
  LD A,D                  ;
  OR E                    ;
  JR NZ,COUNTDOWN_1       ;
  LD HL,23020             ; 23020 is the attribute file address where the
                          ; bottom row of 'big pixels' will be drawn
  LD DE,65496             ; DE=-40
  LD C,8                  ; There are 8 pixel rows per countdown digit
COUNTDOWN_2:
  EXX
  BIT 7,L                 ; Set the zero flag when HL' hits 15743
  LD A,(HL)               ; Pick up a digit pixel row byte
  DEC HL                  ; Move HL' to the next pixel row up
  EXX
  JP Z,INPUTDEV2_3        ; Start the game if we've already printed '0'
  LD B,8                  ; There are 8 bits per pixel row
COUNTDOWN_3:
  LD (HL),9               ; INK 1: PAPER 1 (blue) for a reset pixel
  RLCA                    ; Is this a reset pixel?
  JR NC,COUNTDOWN_4       ; Jump if so
  LD (HL),36              ; INK 4: PAPER 4 (green) for a set pixel
COUNTDOWN_4:
  INC HL                  ; Move HL along to the next attribute file address
  DJNZ COUNTDOWN_3        ; Jump back until all 8 'big pixels' have been drawn
  ADD HL,DE               ; Move HL back 8 spaces and up one row of the
                          ; attribute file
  DEC C                   ; Next pixel row
  JR NZ,COUNTDOWN_2       ; Jump back until all 8 pixel rows have been drawn
  CALL WALKSOUND          ; Make a ticking sound effect
  LD HL,23611
  BIT 5,(HL)              ; Reset the zero flag if a key was pressed
  RES 5,(HL)              ; Reset bit 5 at 23611, ready for future keypresses
  JP NZ,INPUTDEV          ; Jump if a key was pressed
  JR COUNTDOWN_0          ; Wait roughly one second, then print the next digit

; Messages 252 and 244: 'Enter new name' and ''
;
; Used by the routine at GETNAMES. The end marker at MSG244 also serves as
; message 244 ('': empty string), which is also used by the routine at
; GETNAMES.
MSG252:
  DEFM "Enter new name"
MSG244:
  DEFB 0                  ; End marker

  DEFS 161

; Message 246: 'None/Cursor/Kempston/Int2? (N,C,K or I)'
;
; Used by the routine at INPUTDEV.
MSG246:
  DEFM "None/Cursor/Kempston/Int2? (N,C,K or I)"
  DEFB 0                  ; End marker

; Message 108: '{THE }BULLY'
;
; Used by the routine at GETNAMES.
MSG108:
  DEFB 8                  ; 8: 'THE '
  DEFM "BULLY"
  DEFB 0                  ; End marker

; Message 248: '{THE }CHARACTERS'
;
; Used by the routine at INPUTDEV2.
MSG248:
  DEFB 8                  ; 8: 'THE '
  DEFM "CHARACTERS"
  DEFB 0                  ; End marker

; Message 250: 'Press 'C' to change name'
;
; Used by the routine at GETNAMES.
MSG250:
  DEFM "Press 'C' to change name"
  DEFB 0                  ; End marker

; Message 101: '{THE }HEADMASTER'
;
; Used by the routine at GETNAMES. 'MASTER' follows at MSG017.
MSG101:
  DEFB 8                  ; 8: 'THE '
  DEFM "HEAD"

; Message 17: 'MASTER'
;
; Used as a submessage of messages 102, 103 and 104. Message 101 also continues
; here.
MSG017:
  DEFM "MASTER"
  DEFB 0                  ; End marker

  DEFS 160

; Message 102: '{THE }GEOGRAPHY {MASTER}'
;
; Used by the routine at GETNAMES.
MSG102:
  DEFB 8                  ; 8: 'THE '
  DEFM "GEOGRAPHY "
  DEFB 17                 ; 17: 'MASTER'
  DEFB 0                  ; End marker

; Message 103: '{THE }SCIENCE {MASTER}'
;
; Used by the routine at GETNAMES.
MSG103:
  DEFB 8                  ; 8: 'THE '
  DEFM "SCIENCE "
  DEFB 17                 ; 17: 'MASTER'
  DEFB 0                  ; End marker

; Message 104: '{THE }HISTORY {MASTER}'
;
; Used by the routine at GETNAMES.
MSG104:
  DEFB 8                  ; 8: 'THE '
  DEFM "HISTORY "
  DEFB 17                 ; 17: 'MASTER'
  DEFB 0                  ; End marker

; Message 105: '{THE }HEADMISTRESS '
;
; Used by the routine at GETNAMES.
MSG105:
  DEFB 8                  ; 8: 'THE '
  DEFM "HEADMISTRESS "
  DEFB 0                  ; End marker

; Message 106: '{THE }CARETAKER'
;
; Used by the routine at GETNAMES.
MSG106:
  DEFB 8                  ; 8: 'THE '
  DEFM "CARETAKER"
  DEFB 0                  ; End marker

; Message 107: '{THE }TEARAWAY'
;
; Used by the routine at GETNAMES.
MSG107:
  DEFB 8                  ; 8: 'THE '
  DEFM "TEARAWAY"
  DEFB 0                  ; End marker

; Message 109: '{THE }SWOT'
;
; Used by the routine at GETNAMES.
MSG109:
  DEFB 8                  ; 8: 'THE '
  DEFM "SWOT"
  DEFB 0                  ; End marker

; Message 110: '{THE }HEROINE'
;
; Used by the routine at GETNAMES.
MSG110:
  DEFB 8                  ; 8: 'THE '
  DEFM "HEROINE"
  DEFB 0                  ; End marker

; Message 111: 'OUR HERO'
;
; Used by the routine at GETNAMES.
MSG111:
  DEFM "OUR HERO"
  DEFB 0                  ; End marker

; Attribute file
  DEFS 769

; Message graphic buffer
;
; Used by the routine at PRINTMSG to prepare message graphic data before
; copying it to the screen. The first 64 bytes of this buffer (23296-23359) are
; also used by the routine at SPEAK to prepare message graphic data for the
; speech bubble text window.
MSGBUFFER:
  DEFS 256

; System variables and BASIC program area
  DEFS 9

; System variables REPDEL and REPPER
  DEFB 35                 ; Set REPDEL to 35
  DEFB 5                  ; Set REPPER to 5

  DEFS 48

; System variable FLAGS
  DEFB 8                  ; Set bit 3 so that the keyboard is read in 'L' mode

  DEFS 196

; Deal with ERIC while he's falling (2)
;
; Continues from FALLING.
;
; A Descent table entry (see FALLING)
; B 0 if ERIC's facing left, 128 if he's facing right
; C Same as A
; D ERIC's y-coordinate
; E ERIC's x-coordinate
; H 210 (ERIC)
FALLING2:
  AND 15                  ; Retain only bits 0-3 (the animatory state)
  ADD A,B                 ; Now A=ERIC's new animatory state
  LD B,A                  ; Store this in B for now
  LD A,C                  ; A=descent table entry
  RLCA                    ; Is ERIC ascending (bit 7 of C set)?
  JR NC,FALLING2_0        ; Jump if not
  DEC D                   ; Move ERIC up a level
FALLING2_0:
  RLCA                    ; Is ERIC descending (bit 6 of C set)?
  JR NC,FALLING2_1        ; Jump if not
  INC D                   ; Move ERIC down a level
FALLING2_1:
  RLCA                    ; Should ERIC move forward one space (bit 5 of C
                          ; set)?
  JR NC,FALLING2_2        ; Jump if not
  BIT 7,B                 ; Is ERIC facing right?
  JR NZ,FALLING2_3        ; Jump if so
  DEC E
FALLING2_2:
  RLCA                    ; Should ERIC move backwards one space (bit 4 of C
                          ; set)?
  JR NC,FALLING2_4        ; Jump if not
  DEC E
  BIT 7,B                 ; Is ERIC facing right?
  JR NZ,FALLING2_4        ; Jump if so
  INC E
FALLING2_3:
  INC E                   ; Now E=ERIC's new x-coordinate, D=ERIC's new
                          ; y-coordinate
FALLING2_4:
  LD A,B                  ; A=ERIC's new animatory state
  JP MVERIC2_0            ; Update ERIC's animatory state and location, update
                          ; the SRB make a sound effect, and scroll the screen
                          ; if necessary

; Deal with ERIC's flight from the saddle of the bike
;
; Used by the routine at HANDLEERIC when bit 6 at STATUS2 is set on its own by
; the routine at WHEELBIKE because ERIC has hit a wall, a closed door, or the
; closed skool gate while standing on the saddle of the bike.
OFFSADDLE:
  LD A,(53761)            ; A=ERIC's x-coordinate
  SUB 128                 ; Did ERIC hit the closed skool gate?
  CP 16                   ;
  SBC A,A                 ; A=255 if so, 0 otherwise
  ADD A,254               ; L=253 if ERIC hit the skool gate, 254 otherwise
  LD L,A                  ;
; This entry point is used by the routine at BIGFALL with L=255.
OFFSADDLE_0:
  LD H,159                ; Now HL points at the appropriate descent table
  JP FALLING_3            ; Begin the descent

; Deal with ERIC's descent from the top-floor window
;
; Used by the routine at HANDLEERIC when bit 5 at STATUS2 is set by the routine
; at STEPPEDOFF because ERIC has jumped out of the top-floor window.
BIGFALL:
  LD L,255                ; Use descent table number 255
  JR OFFSADDLE_0          ; Begin the descent

; 'T' pressed - throw away the water pistol
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'T' is
; pressed.
THROW:
  LD A,(INVENTORY)        ; INVENTORY holds the inventory flags
  AND 24                  ; Has ERIC got the water pistol (bit 3 or 4 set)?
  RET Z                   ; Return if not
  LD A,39                 ; 39: animatory state of ERIC bending over
  LD HL,THROW_0           ; Place THROW_0 into ERICDATA1 to take care of ERIC
  JP CATCH_0              ; from this point on, update ERIC's animatory state,
                          ; and update the SRB
; This is where we deal with ERIC after 'T' has been pressed:
THROW_0:
  LD HL,INVENTORY         ; Reset bits 3 and 4 of the inventory flags at
  LD A,(HL)               ; INVENTORY, thus dropping the water pistol
  AND 231                 ;
  LD (HL),A               ;
  JP PRINTINV             ; Print the inventory

; 'J' pressed - jump
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'J' or
; 'L' is pressed.
JUMP:
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  RET C                   ; Return if so
  LD HL,STATUS            ; Set bit 0 of ERIC's status flags at STATUS,
  INC (HL)                ; triggering a jump to JUMPING next time we deal with
                          ; ERIC
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  AND 128                 ; A=7 or 135: animatory state of ERIC jumping
  ADD A,7                 ;
  DEC D                   ; ERIC will rise one level
  JR JUMPING_2            ; Update ERIC's animatory state and location and
                          ; update the SRB

; Deal with ERIC when he's jumping
;
; Called by the routine at HANDLEERIC when bit 0 at STATUS is set (by the
; routine at JUMP). Checks whether ERIC has jumped onto a plant pot, up to the
; safe, or up to the drinks cabinet.
JUMPING:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time for ERIC to land?
  RET NZ                  ; Return if not
  LD L,251                ; Reset all of ERIC's status flags at STATUS (ERIC is
  LD (HL),0               ; no longer doing anything special)
  LD H,210                ; 210=ERIC
  CALL CHECKPOT           ; Set the zero flag if ERIC is in the same position
                          ; as a plant pot
  LD A,2                  ; Bit 1 set at STATUS: make the routine at HANDLEERIC
                          ; inspect ERIC's other status flags at STATUS2; bit 1
                          ; set at STATUS2: ERIC is standing on a plant pot
  JR NZ,JUMPING_3         ; Jump if ERIC's not in the same position as a plant
                          ; pot
; This entry point is used by the routine at HIT with A=0.
JUMPING_0:
  LD DE,STATUS            ; Set or reset the appropriate status flags at STATUS
  LD (DE),A               ;
  LD E,237                ; Set or reset the appropriate status flags at
  LD (DE),A               ; STATUS2
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
JUMPING_1:
  AND 128                 ; Restore ERIC's animatory state to 0 (facing left)
                          ; or 128 (facing right) as appropriate
; This entry point is used by the routine at JUMP.
JUMPING_2:
  JP ERICSITLIE_4         ; Update ERIC's animatory state and update the SRB
; ERIC has jumped, but not onto a plant pot.
JUMPING_3:
  LD L,A                  ; L=2
  LD A,(HL)               ; A=ERIC's y-coordinate
  SUB L
  JR Z,JUMPING_5          ; Jump if ERIC jumped while on the top floor
JUMPING_4:
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  INC D                   ; Increment ERIC's y-coordinate, bringing him back to
                          ; the floor
  JR JUMPING_1            ; Restore ERIC's animatory state and update the SRB
; ERIC jumped while on the top floor. Up to the drinks cabinet?
JUMPING_5:
  LD L,A                  ; L=0
  LD A,(HL)               ; A=ERIC's animatory state
  INC L                   ; Now A=x-coordinate of ERIC's raised arm
  RLCA                    ;
  SBC A,A                 ;
  ADD A,A                 ;
  CPL                     ;
  INC A                   ;
  ADD A,(HL)              ;
  LD HL,DOORFLAGS         ; DOORFLAGS holds the door/window status flags
  CP 190                  ; Did ERIC jump up to the drinks cabinet?
  JR C,JUMPING_6          ; Jump if not
  BIT 5,(HL)              ; Is the drinks cabinet door open?
  JR Z,JUMPING_4          ; Just bring ERIC back to the floor if not
; ERIC has jumped up to the drinks cabinet, and it's open. But has he got the
; water pistol?
  LD L,235                ; HL=INVENTORY (which holds the inventory flags)
  BIT 4,(HL)              ; Has ERIC already got a sherry-filled water pistol?
  JR NZ,JUMPING_4         ; Jump if so
  BIT 3,(HL)              ; Has ERIC got a water-filled water pistol?
  JR Z,JUMPING_4          ; Jump if not
  SET 4,(HL)              ; Signal: ERIC now has a sherry-filled water pistol
  CALL PRINTINV           ; Print the inventory
  LD HL,534               ; Make a sound effect
  CALL ERICHIT_4          ;
  JR JUMPING_4            ; Bring ERIC back to the floor
; ERIC has jumped, but not onto a plant pot or up to the drinks cabinet. Up to
; the safe?
JUMPING_6:
  CP 81                   ; Did ERIC jump up to the safe in WACKER's study?
  JR NZ,JUMPING_4         ; Jump if not
  LD L,235                ; HL=INVENTORY (which holds the inventory flags)
  BIT 0,(HL)              ; Has ERIC got the safe key?
  JR Z,JUMPING_4          ; Just bring ERIC back to the floor if not
; ERIC has jumped up to the safe, and he has the safe key. Celebration time.
  LD L,222                ; Set GAMEMODE to 1 so the game will be restarted
  INC (HL)                ; instead of going into demo mode
  LD A,200                ; Add 2000 to the score and print it
  CALL ADDPTS             ;
  XOR A                   ; A=0: 'ONTO THE NEXT YEAR'
  CALL NEWLESSON2_1       ; Print this in the lesson box and ring the bell
  LD HL,56363             ; Play the up-a-year tune
  CALL PLAYTUNE_0         ;
  JP START                ; Restart the game

; Unused
  DEFB 0

; Compare blackboard contents with combinations
;
; Used by the routine at WRITING. Compares the bike and storeroom combinations
; with what ERIC has just written on a blackboard. Unchains the bike or gives
; ERIC the storeroom key if there is a match.
;
; H 127
CHKCOMBOS:
  LD D,H                  ; D=127
  LD L,251                ; Reset all of ERIC's status flags at STATUS now that
  LD (HL),0               ; he's no longer writing on the board
  LD L,156                ; HL=COMBOS (combinations)
  LD BC,2052              ; B=8 (4 numbers, 4 letters), C=4
CHKCOMBOS_0:
  RES 7,(HL)              ; Reset bit 7 of each combination number/letter; bit
  INC L                   ; 7 will be set later if there is a match with what
  DJNZ CHKCOMBOS_0        ; ERIC wrote on the board
  LD L,216                ; HL=ERICDATA2 (which holds the identifier of the
                          ; blackboard ERIC wrote on)
  LD L,(HL)               ; Point HL at byte 1 of the relevant blackboard
  INC L                   ; buffer (bytes 2-5 hold the first 4 characters ERIC
                          ; wrote)
  LD B,C                  ; B=4
CHKCOMBOS_1:
  INC L                   ; Return unless at least four letters were written on
  BIT 7,(HL)              ; the board by ERIC
  RET NZ                  ;
  DJNZ CHKCOMBOS_1        ;
  EX DE,HL                ; Point DE at the last byte of the blackboard buffer
CHKCOMBOS_2:
  LD L,156                ; HL=COMBOS (combinations)
  LD B,8                  ; 4 numbers, 4 letters
  LD A,(DE)               ; A=ASCII code of the character written on the board
  CP 96                   ; Is it upper case?
  JR C,CHKCOMBOS_3        ; Jump if so
  SUB 32                  ; Make lower case characters upper case
CHKCOMBOS_3:
  CP (HL)                 ; Does the character written on the board match the
                          ; combination number/letter?
  JR Z,CHKCOMBOS_4        ; Jump if so
  INC L                   ; Point to the next combination number/letter
  DJNZ CHKCOMBOS_3
  RET
CHKCOMBOS_4:
  DEC E                   ; Point to the next character written on the board
  SET 7,(HL)              ; Signal: matching character
  DEC C                   ; Next combination letter/number
  JR NZ,CHKCOMBOS_2       ; Jump back to check the remaining numbers/letters
  LD L,156                ; HL=COMBOS (combinations)
  LD B,4                  ; There are 4 digits in the bike combination
  LD A,(HL)               ; Pick up the first digit
CHKCOMBOS_5:
  AND (HL)                ; Bit 7 of A will remain set if all four written
  INC L                   ; characters matched up with the bike combination
  DJNZ CHKCOMBOS_5        ; digits
  RLCA                    ; Was there a match?
  JR NC,CHKCOMBOS_7       ; Jump if not
; ERIC has written the bike combination number on a blackboard. Free the bike
; if it's still chained up.
  LD HL,54017             ; H=211 (bike), L=1
  LD A,(HL)               ; A=bike's x-coordinate
  CP 224                  ; Is the bike already free?
  RET NZ                  ; Return if so
  LD (HL),100             ; Place the bike at x-coordinate 100
  LD HL,URTTREE           ; Alter UDG references in the play area to release
  CALL ALTERUDGS          ; the bike from the tree
CHKCOMBOS_6:
  LD A,100                ; Add 1000 to the score and print it
  CALL ADDPTS             ;
  JP ERICSITLIE_16        ; Print the inventory and make a sound effect
; The first four characters written on the board by ERIC didn't match the bike
; combination digits. What about the storeroom combination letters?
CHKCOMBOS_7:
  LD A,(HL)               ; Pick up the first combination letter
  LD B,4                  ; There are 4 letters in the storeroom combination
CHKCOMBOS_8:
  AND (HL)                ; Bit 7 of A will remain set if all four written
  INC L                   ; characters matched up with the storeroom
  DJNZ CHKCOMBOS_8        ; combination letters
  RLCA                    ; Was there a match?
  RET NC                  ; Return if not
  LD L,235                ; HL=INVENTORY (which holds the inventory flags)
  BIT 1,(HL)              ; Has ERIC already got the storeroom key?
  RET NZ                  ; Return if so
  SET 1,(HL)              ; Otherwise give ERIC the key to the storeroom
  JR CHKCOMBOS_6          ; Add 1000 to the score, print the inventory, and
                          ; make a sound effect

; Unused
  DEFB 0

; 'W' pressed - write on a blackboard
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'W' is
; pressed.
WRITE:
  LD H,210                ; 210=ERIC
  CALL BOARDID            ; Collect info about the board ERIC's standing at (if
                          ; any)
  RET C                   ; Return if ERIC's not standing at a blackboard
  LD HL,ERICDATA2         ; Copy the blackboard identifier (84, 90, 96, 102 or
  LD (HL),B               ; 108) from B to ERICDATA2
  DEC L                   ; While ERIC is writing on the board, ERICDATA1 will
  LD (HL),L               ; be used to store the ASCII code of the last
                          ; character written; initialise this with something
                          ; other than 13 (ENTER)
  LD L,B                  ; Point HL at the first byte of the blackboard's
                          ; buffer, which usually holds the number of the next
                          ; clean pixel column
  LD (HL),1               ; ERIC will start writing at pixel column 1
                          ; (regardless of what's already been written on the
                          ; board)
  INC L                   ; A=number of the character who last wrote on this
  LD A,(HL)               ; board, or 0 if it's clean
  LD (HL),210             ; Signal: ERIC wrote on this board
  LD B,4                  ; There are 4 digits or letters in the bike and
                          ; storeroom combinations
  CPL                     ; A=128 if the board is clean, 0 otherwise
  AND 128                 ;
WRITE_0:
  INC L                   ; Prepare 4 spaces in bytes 2-5 of the blackboard
  LD (HL),A               ; buffer for combination letters/numbers, with bit 7
  DJNZ WRITE_0            ; set if the board is clean
  LD L,251                ; HL=STATUS (ERIC's status flags)
  LD (HL),16              ; Set bit 4: ERIC is writing on a blackboard
; This entry point is used by the routine at WRITING.
WRITE_1:
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
  OR 7                    ; A=7/135: ERIC with his arm up (as if writing on a
                          ; board)
  JP ERICSITLIE_4         ; Update ERIC's animatory state and location and
                          ; update the SRB

; Deal with ERIC when he's writing on a blackboard
;
; This routine is called by the routine at HANDLEERIC when bit 4 at STATUS is
; set (by the routine at WRITE).
WRITING:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC's buffer
  LD A,(HL)               ; A=ERIC's animatory state
  RRCA                    ; Is ERIC's arm up?
  JR NC,WRITING_0         ; Jump if not
  CALL ERICSITLIE_3       ; Make a sound effect and update the SRB
  LD HL,ERICDATA1         ; Copy the code of the last key pressed from
  LD A,(HL)               ; ERICDATA1 to A
  CP 13                   ; Was ENTER pressed?
  JP Z,CHKCOMBOS          ; Compare ERIC's scribblings with the bike and
                          ; storeroom combinations if so
  LD A,86                 ; Message 86: DON'T TOUCH BLACKBOARDS
  JP CLOUD_0              ; Make any teacher within range give ERIC lines
WRITING_0:
  CALL READKEY            ; Get the ASCII code of the last keypress in A
  RET Z                   ; Return if no keys were pressed
  LD HL,ERICDATA1         ; Store this keypress code in ERICDATA1 (so it can be
  LD (HL),A               ; collected on the next pass through this routine)
  INC L                   ; L=identifier of the board ERIC is writing on (84,
  LD L,(HL)               ; 90, 96, 102 or 108)
  INC L                   ; Point HL at byte 1 of the blackboard buffer
  LD B,4                  ; There are 4 slots available in the blackboard
                          ; buffer
WRITING_1:
  INC L                   ; Point HL at the next slot
  BIT 7,(HL)              ; Is this slot in the blackboard buffer available to
                          ; store the keypress?
  JR NZ,WRITING_2         ; Jump if so
  DJNZ WRITING_1          ; Otherwise check the remaining slots
  JR WRITING_3            ; Jump if there were no remaining slots
WRITING_2:
  LD (HL),A               ; Store the keypress code in the blackboard buffer
WRITING_3:
  CP 13                   ; Set the zero flag if ENTER was pressed
  LD H,210                ; 210=ERIC
  CALL NZ,WRITECHR_0      ; Write a character on the board unless ENTER was
                          ; pressed
  JR WRITE_1              ; Raise ERIC's arm and update the SRB

; Unused
  DEFB 0

; Deal with ERIC when he's firing the catapult, hitting or kissing
;
; This routine is called by the routine at HANDLEERIC when bit 5 at STATUS is
; set (by the routine at STARTFHK).
MIDFHK:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  LD (HL),5               ; Reset the main action timer to 5
  JP BENDING_0            ; Jump to the routine address stored in ERICDATA1,
                          ; which will deal with the next phase of ERIC's
                          ; animation

; Signal that ERIC is firing the catapult, hitting, or kissing
;
; Used by the routines at HIT, FIRE and KISS.
;
; A ERIC's next animatory state (bit 7 reset)
STARTFHK:
  LD HL,ERICTIMER         ; Initialise ERIC's main action timer at ERICTIMER to
  LD (HL),5               ; 5
  LD L,251                ; HL=STATUS (ERIC's status flags)
  LD (HL),32              ; Set bit 5: ERIC is firing, hitting or kissing
  POP HL                  ; HL=address of the instruction after the CALL
  LD (ERICDATA1),HL       ; STARTFHK that got us here; store this at ERICDATA1
                          ; for collection later by the routine at MIDFHK
  JP CATCH_1              ; Begin the fire/hit/kiss action

; 'H' pressed - hit
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'H' is
; pressed.
HIT:
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  RET C                   ; Return if so
  LD A,8                  ; 8: ERIC raising his fist
  CALL STARTFHK           ; Adjust ERIC's animatory state, update the SRB, and
                          ; return to HIT_0 (below) when it's time to deal with
                          ; ERIC again
HIT_0:
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC's buffer
  LD A,(HL)               ; A=ERIC's animatory state
  RRCA                    ; Is ERIC raising his fist (A=8/136)?
  JP NC,HITTING_0         ; If so, raise ERIC's fist fully now and check for
                          ; victims
  LD A,97                 ; Message 97: DON'T HIT YOUR MATES
  CALL CLOUD_0            ; Make any nearby teachers give ERIC lines
  LD A,8                  ; 8: ERIC lowering his fist
; This is where the routine at FIRE ought to enter:
LOWERARM:
  CALL STARTFHK           ; Adjust ERIC's animatory state, update the SRB, and
                          ; return to HIT_1 (below) when it's time to deal with
                          ; ERIC again
; This entry point is used by the routine at KISS.
HIT_1:
  XOR A                   ; A=0 (prepare to clear all status flags)
  JP JUMPING_0            ; Clear all of ERIC's status flags now that he's
                          ; finished firing, hitting or kissing, update his
                          ; animatory state and update the SRB

; Unused
  DEFS 2

; 'F' pressed - fire catapult
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'F' or
; '0' (zero) is pressed.
FIRE:
  CALL CHECK214           ; Give up right now if the catapult pellet's buffer
                          ; is already in use
  LD A,10                 ; 10: ERIC raising the catapult
  CALL STARTFHK           ; Adjust ERIC's animatory state, update the SRB, and
                          ; return to FIRE_0 (below) when it's time to deal
                          ; with ERIC again
FIRE_0:
  LD A,2                  ; Message 2: NO CATAPULTS
  CALL CLOUD_0            ; Make any nearby teachers give ERIC lines
  LD A,11                 ; 11: ERIC firing the catapult
  CALL STARTFHK           ; Adjust ERIC's animatory state, update the SRB, and
                          ; return to FIRE_1 (below) when it's time to deal
                          ; with ERIC again
FIRE_1:
  LD B,214                ; ERIC's catapult pellet uses buffer 214
  CALL PREPCATTY          ; Prepare the pellet's buffer and make a catapult
                          ; sound effect
  LD A,11                 ; 11: ERIC (still) firing the catapult
  CALL STARTFHK           ; Adjust ERIC's animatory state, update the SRB, and
                          ; return to FIRE_2 (below) when it's time to deal
                          ; with ERIC again
FIRE_2:
  LD A,10                 ; 10: ERIC lowering the catapult
  JR 24289                ; This is a bug; it should be JR LOWERARM

; Make HAYLEY hit ERIC
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of HAYLEY's buffer by the routine at KISS when ERIC has tried to
; steal one kiss too many.
;
; A 95 (MSB of the address of this routine)
; H 209 (HAYLEY)
HITERIC:
  LD (ERICTIMER),A        ; Place a large value into ERIC's main action timer
                          ; at ERICTIMER to ensure that he will not have time
                          ; to escape HAYLEY's fist
  CALL UPDATESRB          ; Update the SRB for HAYLEY's current animatory state
  LD L,20                 ; Store HAYLEY's current animatory state in byte 20
  LD (HL),A               ; of her buffer for the time being
  OR 7                    ; A=63/191: HAYLEY with her arm up (as if hitting
                          ; ERIC)
  CALL SUBCMD             ; Update HAYLEY's animatory state, update the SRB,
                          ; and return to HITERIC_0 (below) next time
HITERIC_0:
  CALL OBJFALL_9          ; Knock ERIC over
  JP BWFIRING_1           ; Restore HAYLEY's original animatory state and
                          ; update the SRB

; 'K' pressed - kiss
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'K' is
; pressed.
;
; A ERIC's animatory state
; D ERIC's y-coordinate
; E ERIC's x-coordinate
KISS:
  LD HL,HAYLEYCBUF        ; Point HL at byte 0 of HAYLEY's buffer
  XOR (HL)                ; Are ERIC and HAYLEY facing the same way?
  RLCA                    ;
  JR NC,KISS_1            ; Jump if so
  BIT 2,(HL)              ; Is HAYLEY standing up?
  JR NZ,KISS_1            ; Jump if not
  LD A,(HL)               ; A=HAYLEY's animatory state
  RLCA                    ; A=x-coordinate of the position two spaces in front
  SBC A,A                 ; of HAYLEY
  ADD A,A                 ;
  CPL                     ;
  ADD A,A                 ;
  INC L                   ;
  ADD A,(HL)              ;
  CP E                    ; Does ERIC's x-coordinate match this?
  JR NZ,KISS_1            ; Jump if not
  INC L                   ; L=2
  LD A,(HL)               ; A=HAYLEY's y-coordinate
KISS_0:
  SUB D                   ; Jump if ERIC's y-coordinate does not match
  JR NZ,KISS_1            ; HAYLEY's, or ERIC has used up all his kisses
  LD L,18                 ; Set the zero flag if there is no uninterruptible
  CP (HL)                 ; subcommand routine address in HAYLEY's buffer (A=0)
; At this point the zero flag is set if HAYLEY is kissable (i.e. ERIC and
; HAYLEY are standing and facing each other at close proximity, and ERIC hasn't
; already used up all his kisses), and reset otherwise.
KISS_1:
  LD A,1                  ; 1: ERIC midstride
  LD HL,DONOWT            ; At DONOWT lies a RET instruction
  JP NZ,CATCH_0           ; Jump if HAYLEY is not kissable at this time
  LD A,(KISSCOUNT)        ; KISSCOUNT holds HAYLEY's kiss counter
  AND A                   ; Has ERIC used up all his kisses?
  JR NZ,KISS_2            ; Jump if not
  LD HL,HITERIC           ; HITERIC: hit ERIC
KISS_2:
  LD (53521),HL           ; Place the appropriate uninterruptible subcommand
                          ; routine address (HITERIC: hit ERIC, or DONOWT: do
                          ; nothing) into bytes 17 and 18 of HAYLEY's buffer
  JR Z,KISS_0             ; Make HAYLEY hit ERIC if he's used up all his kisses
; ERIC has scored a kiss.
  SUB 7                   ; Subtract 7 from the kiss counter
  JR NC,KISS_3            ; Jump if that doesn't take it below zero
  XOR A                   ; Set it to zero otherwise
KISS_3:
  LD (KISSCOUNT),A        ; Store the new kiss count at KISSCOUNT
  LD HL,(LINES)           ; LINES holds the lines total (divided by 10)
  LD BC,65436             ; BC=-100
  XOR A
  ADD HL,BC               ; Subtract 1000 from the lines total
  JR C,KISS_4             ; Jump if the lines total (in HL) is still positive
  LD H,A                  ; Otherwise set HL=0
  LD L,A                  ;
KISS_4:
  LD (LINES),HL           ; Decrease ERIC's lines total by 1000 (or to 0)
  CALL ADDLINES           ; Print the number of lines
  LD H,209                ; 209=HAYLEY
  CALL UPDATESRB          ; Update the SRB for HAYLEY's current animatory state
                          ; and location
  LD L,20                 ; Save HAYLEY's current animatory state in byte 20 of
  LD (HL),A               ; her buffer for later retrieval
  DEC L                   ; Save HAYLEY's current x-coordinate in byte 19 of
  LD (HL),E               ; her buffer for later retrieval
  LD L,1                  ; Set HAYLEY's x-coordinate to 224 (out of sight)
  LD (HL),224             ; briefly
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  LD L,1                  ; Point HL at byte 1 of ERIC's buffer
  RLCA                    ; Is ERIC facing right (bit 7 set)?
  JR C,KISS_5             ; Jump if so
  DEC (HL)
  DEC (HL)
KISS_5:
  INC (HL)                ; Move ERIC one space forward for the snog (which
                          ; pushes ERIC through the closed skool gate if he
                          ; kisses HAYLEY from behind it; this is a bug)
  LD A,15                 ; 15: ERIC and HAYLEY kissing
  CALL STARTFHK           ; Adjust ERIC's animatory state, update the SRB, and
                          ; return to KISS_6 (below) next time
KISS_6:
  CALL ERICSITLIE_17      ; Make a sound effect
  LD HL,53523             ; Point HL at byte 19 of HAYLEY's buffer
  LD E,(HL)               ; Pick up HAYLEY's pre-kiss x-coordinate in E
  LD L,2                  ; Point HL at byte 2 of HAYLEY's buffer
  LD D,(HL)               ; D=HAYLEY's y-coordinate
  CALL BWFIRING_2         ; Restore HAYLEY's pre-kiss coordinates and animatory
                          ; state and update the SRB
  LD H,210                ; 210=ERIC
  JP HIT_1                ; Detach ERIC from HAYLEY's embrace

; Unused
  DEFB 0

; Play a tune
;
; Used by the routine at PREPGAME. The main entry point is used to play the
; theme tune.
PLAYTUNE:
  LD HL,23611             ; Reset bit 5 at 23611, clearing the record of any
  RES 5,(HL)              ; keypresses before the tune starts
  LD HL,56384             ; The theme tune data starts at OTSEG1
; This entry point is used by the routine at JUMPING with HL=UAYTUNE1-1 (the
; up-a-year tune data starts at UAYTUNE1).
PLAYTUNE_0:
  DI                      ; Disable interrupts
PLAYTUNE_1:
  INC HL                  ; Point HL at the next tune datum
  LD A,(HL)               ; Pick it up
  INC A                   ; Is it an end-of-section marker?
  JR NZ,PLAYTUNE_2        ; Jump if not
  INC HL                  ; Point HL at the first datum in the next section
  LD A,(HL)               ;
  INC HL                  ;
  LD H,(HL)               ;
  LD L,A                  ;
PLAYTUNE_2:
  LD A,(HL)               ; A=tune datum
  AND A                   ; Have we reached the end of the tune?
  JP Z,ERICHIT_7          ; Jump if so
; A non-zero tune datum has been found, meaning the tune is not finished yet.
  PUSH HL                 ; Save the tune data table pointer
  RRCA                    ; Set the carry flag if we should pause briefly
  LD B,A                  ; Copy the right-rotated tune datum to B
  JR NC,PLAYTUNE_4        ; Jump unless we should pause briefly
  LD D,15                 ; Pause briefly
PLAYTUNE_3:
  DEC E                   ;
  JR NZ,PLAYTUNE_3        ;
  DEC D                   ;
  JR NZ,PLAYTUNE_3        ;
PLAYTUNE_4:
  AND 7                   ; 0<=A<=7
  LD C,A                  ; C=border colour for this note
  ADD A,A                 ; Point HL at an entry in the note duration/pitch
  ADD A,240               ; data table at PITCHDATA
  LD L,A                  ;
  LD H,95                 ;
  LD A,B                  ; A=original tune datum rotated right one bit
  RRCA                    ; Obtain the note frequency parameter in E and the
  RRCA                    ; note duration parameter in HL
  RRCA                    ;
  AND 15                  ;
  LD B,A                  ;
  LD E,(HL)               ;
  INC L                   ;
  LD A,(HL)               ;
  LD HL,0                 ;
PLAYTUNE_5:
  ADD HL,DE               ;
  DJNZ PLAYTUNE_5         ;
  RES 0,L                 ;
  LD E,A                  ;
PLAYTUNE_6:
  LD A,C                  ; Produce a note
  OUT (254),A             ;
  XOR 16                  ;
  LD C,A                  ;
  LD B,E                  ;
PLAYTUNE_7:
  DJNZ PLAYTUNE_7         ;
  DEC HL                  ;
  DEC L                   ;
  LD A,H                  ;
  OR L                    ;
  JR NZ,PLAYTUNE_6        ;
  POP HL                  ; Restore the tune data table pointer to HL
  JR PLAYTUNE_1           ; Pick up the next tune datum

; Note duration and pitch data for tunes
;
; Used by the routine at PLAYTUNE.
PITCHDATA:
  DEFB 47,196             ; 47*196=9212
  DEFB 53,174             ; 53*174=9222
  DEFB 60,154             ; 60*154=9240
  DEFB 63,145             ; 63*145=9135
  DEFB 71,129             ; 71*129=9159
  DEFB 80,114             ; 80*114=9120
  DEFB 90,101             ; 90*101=9090 (unused)
  DEFB 95,96              ; 95*96=9120

; Superimpose sprite tiles onto a tile of the play area
;
; Used by the routine at PRINTTILE. Checks through B characters, starting at
; character H. If any part of a character's sprite needs to be printed at the
; row and column specified by DE, the appropriate UDG is located and
; superimposed onto the contents of the buffer at BACKBUF, which on the first
; call to this routine contains the appropriate skool UDG (the background).
;
; B Number of characters to consider
; D Row of play area (0-20)
; E Column of play area (0-191)
; H Number of the first character to consider
GETTILE:
  LD L,1                  ; Point HL at byte 1 of the character's buffer
  LD A,E                  ; A=column of play area (0-191) under consideration
  SUB (HL)                ; Subtract the character's x-coordinate
  CP 3                    ; Sprites are three character squares wide
  JR NC,GETTILE_4         ; Jump if no part of this character's sprite is in
                          ; this column
  DEC L                   ; L=0
  BIT 7,(HL)              ; Is the character facing left?
  JR Z,GETTILE_0          ; Jump if so
  CPL                     ; 'Flip' the number in A, so 0 becomes 2, 1 remains
  ADD A,3                 ; 1, and 2 becomes 0 (i.e. A becomes 2-A)
GETTILE_0:
  ADD A,A                 ; Now C=0 if the front of the character is in this
  ADD A,A                 ; column, 4 if it's the middle, or 8 if it's the back
  LD C,A                  ;
  LD L,2                  ; Point HL at byte 2 of the character's buffer
  LD A,D                  ; A=row of play area (0-20) under consideration
  SUB (HL)                ; Subtract the character's y-coordinate
  CP 4                    ; Sprites are 4 character squares tall
  JR NC,GETTILE_4         ; Jump if no part of this character's sprite is in
                          ; this row
  ADD A,C                 ; 0<=A<=11 (index of the sprite tile at this row and
                          ; column, where 0=top-front, 4=top-middle,
                          ; 8=top-back, 11=bottom-back)
  ADD A,215               ; 215<=A<=226
  EXX                     ; Set H' to the number of the page containing the UDG
  LD H,A                  ; reference for the sprite tile
  EXX                     ;
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  LD A,(HL)               ; A=character's animatory state
  OR 128                  ; Set bit 7 to obtain the LSB of the address that
                          ; holds the sprite tile UDG reference
  EXX
  LD L,A                  ; 128<=L'<=255 (215<=H'<=226)
  LD A,(HL)               ; A=sprite tile UDG reference (73-255)
  AND A                   ; Is this tile the 'null' UDG (a blank square)?
  JR Z,GETTILE_3          ; Jump if so (no need to superimpose it)
  LD E,A                  ; E'=UDG reference (73-255)
  LD A,L                  ; A=(normalised) animatory state (128-255)
  CP 208                  ; 208=animatory state of MR WACKER, the first teacher
  EXX
  LD A,(HL)               ; A=character's (true) animatory state (0-255)
  EXX
  LD D,183                ; Point DE' at the graphic data at page 183 onwards
                          ; (for animatory states 0-79 and 128-207)
  JR C,GETTILE_1          ; Jump unless we're dealing with animatory states
                          ; 80-127 or 208-255 (teachers, ALBERT, and water
                          ; fired from the pistol)
  LD D,199                ; Point DE' at the graphic data at page 199 onwards
                          ; (for animatory states 80-127 and 208-255)
GETTILE_1:
  LD HL,BACKBUF           ; Point HL' at the UDG back buffer
  RLCA                    ; Push the character's 'direction' bit into the carry
                          ; flag
  JR C,GETTILE_5          ; Jump if the character is facing right
  EX DE,HL
GETTILE_2:
  LD A,(DE)               ; A=byte already in the UDG buffer
  OR (HL)                 ; Superimpose the sprite tile byte
  INC H                   ; Point HL' at the outline mask for this tile byte
  AND (HL)                ; AND with this mask
  INC H                   ; Point HL' at the next sprite tile byte
  LD (DE),A               ; Place the resultant byte back in the UDG buffer
  INC E                   ; Move DE' along the UDG buffer
  BIT 3,E                 ; Have we done all 8 bytes yet?
  JR Z,GETTILE_2          ; Jump back if not
GETTILE_3:
  EXX
GETTILE_4:
  INC H                   ; Next game character
  DJNZ GETTILE            ; Jump back until all characters have been done
  RET
; The character is facing right, so we have to produce a mirror image of the
; graphic data.
GETTILE_5:
  LD B,182                ; Page 182 contains mirror images of 0-255
GETTILE_6:
  LD A,(DE)               ; A=sprite tile byte
  LD C,A                  ; A=mirror image of this byte
  LD A,(BC)               ;
  OR (HL)                 ; Superimpose what's already in the UDG buffer
  LD (HL),A               ; Put the result so far back in the UDG buffer
  INC D                   ; A=outline mask for this sprite tile byte
  LD A,(DE)               ;
  LD C,A                  ; A=mirror image of this mask
  LD A,(BC)               ;
  AND (HL)                ; AND it with what's in the UDG buffer
  LD (HL),A               ; Place the result in the UDG buffer
  INC D                   ; Next sprite tile byte
  INC L                   ; Next byte of the UDG buffer
  BIT 3,L                 ; Have we done all 8 bytes yet?
  JR Z,GETTILE_6          ; Jump back if not
  JR GETTILE_3

; Unused
  DEFS 7

; Print a tile
;
; Used by the routines at LSCROLL8, RSCROLL8 and UPDATESCR. Copies a tile of
; the play area into the back buffer at BACKBUF, superimposes character sprite
; tiles as appropriate, and then copies the resultant tile to the screen. Also
; sets the corresponding attribute byte.
;
; The play area graphic data is laid out across pages 128 to 181:
;
; +---------+---------+-------------------------------------------------+
; | Page(s) | Bytes   | Contents                                        |
; +---------+---------+-------------------------------------------------+
; | 128-135 | 0-255   | Skool graphic data                              |
; | 136-143 | 0-255   | Skool graphic data                              |
; | 144-151 | 0-255   | Skool graphic data (except bytes 1, 11, 21, 30) |
; | 152-159 | 0-249   | Skool graphic data (except bytes 224-239)       |
; | 160-180 | 0-143   | LSBs of tile base addresses                     |
; |         | 144-179 | Tile MSB indicator bit-pairs (see below)        |
; |         | 180-251 | Tile BRIGHT/PAPER attribute nibbles             |
; | 181     | 0-191   | Q (0<=Q<=143; see below)                        |
; +---------+---------+-------------------------------------------------+
;
; For the tile at skool coordinates (X,Y) (0<=X<=191, 0<=Y<=20), the column
; pointer Q (0<=Q<=143) is collected from byte X of page 181. Then Q and Y
; together determine the location of the attribute byte, and where to look up
; the base address of the tile graphic data.
;
; The BRIGHT/PAPER bits for row Y are arranged in a set of 144 nibbles in bytes
; 180-251 of page Y+160, from bits 7-4 of byte 180 (nibble 0) to bits 3-0 of
; byte 251 (nibble 143). Q is the index of the nibble that corresponds to the
; tile at (X,Y). In other words, the BRIGHT/PAPER bits for the skool location
; (X,Y) can be found in byte 180+INT(Q/2) of page Y+160: bits 4-7 if Q is even,
; bits 0-3 if Q is odd.
;
; By default, the INK is 0 (black). However, if the BRIGHT/PAPER bits are all
; 0, this indicates a tile with non-black INK, of which there are normally
; four: the three shields in MR WACKER's study (which are red, blue, and red),
; and the top of the entrance to the girls' skool (which is blue). (However,
; when the left study door is open - which never happens in an unhacked game -
; there is a fifth tile with non-black INK: the top of the left study doorway,
; which is magenta.) When a tile has non-black INK, the BRIGHT/PAPER bits are
; taken from byte 180+INT(Q/2) of page Y+161 (i.e. the attribute byte
; corresponding to the skool location (X,Y+1)); then the INK is 2 (red) if Q is
; even, 1 (blue) if 4|Q-1, or 3 (magenta) if 4|Q-3.
;
; So much for the attributes. The LSB of the base address of the tile graphic
; data is found in byte Q of page Y+160. Information on which MSB (128, 136,
; 144 or 152) applies is arranged in a set of 144 bit-pairs in bytes 144-179 of
; page Y+160. Bit-pair 0 is bits 7 and 3 of byte 144; bit-pair 1 is bits 6 and
; 2; bit-pair 2 is bits 5 and 1; bit-pair 3 is bits 4 and 0; bit-pair 4 is bits
; 7 and 3 of byte 145; and so on up to bit-pair 143, which is bits 4 and 0 of
; byte 179. Q is the index of the bit-pair that corresponds to the tile at
; (X,Y). In other words, the tile MSB indicator for the skool location (X,Y)
; can be found in byte 144+INT(Q/4) of page Y+160: bits 7 and 3 if 4|Q, 6 and 2
; if 4|Q-1, 5 and 1 if 4|Q-2, 4 and 0 if 4|Q-3. The bit-pair forms a 2-digit
; binary number from 0 to 3; if we call this P, then the MSB is 128+8P.
;
; To summarise, then, the attributes and graphic data for the tile at skool
; location (X,Y) can be found thus, where Q=PEEK(QVALUES+X):
;
; +--------------+------------------------------------------------------+
; | Data         | Look at...                                           |
; +--------------+------------------------------------------------------+
; | BRIGHT/PAPER | Nibble Q of bytes 180-251 in page Y+160 (or Y+161 if |
; |              | nibble Q in page Y+160 is 0)                         |
; | INK          | 0 if nibble Q in page Y+160 is not 0; 1, 2 or 3 as   |
; |              | explained above otherwise                            |
; | Tile LSB     | Byte Q in page Y+160                                 |
; | Tile MSB     | Bit-pair Q of bytes 144-179 in page Y+160            |
; +--------------+------------------------------------------------------+
;
; H Screen row number (0-20)
; L Screen column number (0-31)
PRINTTILE:
  PUSH HL
  LD A,H                  ; Compute the appropriate attribute file address in
  AND 24                  ; BC and the appropriate display file address in DE
  LD D,A                  ;
  LD A,H                  ;
  RRCA                    ;
  RRCA                    ;
  RRCA                    ;
  LD H,A                  ;
  AND 224                 ;
  ADD A,L                 ;
  LD E,A                  ;
  LD C,A                  ;
  LD A,H                  ;
  AND 3                   ;
  ADD A,88                ;
  LD B,A                  ;
  SET 6,D                 ;
  EX DE,HL                ; Place the display file address on the stack and get
  EX (SP),HL              ; (row,col) back in HL
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
                          ; (0-160)
  ADD A,L                 ; L=X: column of the play area corresponding to the
  LD L,A                  ; character square under consideration (0-191)
  PUSH HL                 ; Push the skool coordinates (X,Y) onto the stack
  LD A,H                  ; A=Y (0-20)
  LD H,181                ; HL=QVALUES+X
  LD L,(HL)               ; L=Q (0<=Q<=143)
  ADD A,160               ; H=Y+160
  LD H,A                  ;
  LD E,L                  ; E=Q (0<=Q<=143)
  LD A,L                  ; A=Q
  RRA                     ; Is Q odd?
  JR C,PRINTTILE_2        ; Jump if so
; Q (the contents of (QVALUES+X)) is even, which means the PAPER colour and
; BRIGHT status can be found in bits 4-7 of byte 180+Q/2 in page Y+160. At this
; point A=Q/2.
  ADD A,180               ; 180<=A<=251
  LD L,A                  ; Point HL at the attribute byte
  LD A,(HL)               ; Pick up the attribute byte in A
  AND 240                 ; The PAPER colour and BRIGHT status are in bits 4-7
  RR A                    ; Shift them into bits 3-6
  JR NZ,PRINTTILE_4       ; Jump if this character square has INK 0
; We are dealing with one of the few character squares with non-black INK. In
; this case the PAPER colour and BRIGHT status can be found in bits 4-7 (since
; Q is even) of byte 180+Q/2 in page Y+161.
  INC H                   ; Point HL at the attribute byte in page Y+161
  LD A,(HL)               ; Pick up the attribute byte in A
  AND 240                 ; The PAPER colour and BRIGHT status are in bits 4-7
  RRA                     ; Shift them into bits 3-6
PRINTTILE_0:
  LD L,A                  ; L holds the PAPER colour and BRIGHT status
  LD A,E                  ; A=Q (0<=Q<=143)
  AND 3                   ; A=0, 1, 2 or 3
  JR NZ,PRINTTILE_1       ; Jump if the INK is blue, red, or magenta
  ADD A,2                 ; A=2: INK 2 (red)
PRINTTILE_1:
  OR L                    ; OR on the PAPER colour and BRIGHT status
  DEC H                   ; H=Y+160
  JR PRINTTILE_4          ; Jump forward to transfer the attribute byte in A
                          ; onto the screen
; Q (the contents of (QVALUES+X)) is odd, which means the PAPER colour and
; BRIGHT status can be found in bits 0-3 of byte 180+(Q-1)/2 in page Y+160. At
; this point A=(Q-1)/2.
PRINTTILE_2:
  ADD A,180               ; 180<=A<=251
  LD L,A                  ; Point HL at the attribute byte
  LD A,(HL)               ; Pick up the attribute byte in A
  AND 15                  ; The PAPER colour and BRIGHT status are in bits 0-3
  JR NZ,PRINTTILE_3       ; Jump if this character square has INK 0
; We are dealing with one of the few character squares with non-black INK. In
; this case the PAPER colour and BRIGHT status can be found in bits 0-3 (since
; Q is odd) of byte 180+(Q-1)/2 in page Y+161.
  INC H                   ; Point HL at the attribute byte in page Y+161
  LD A,(HL)               ; Pick up the attribute byte in A
  AND 15                  ; The PAPER colour and BRIGHT status are in bits 0-3
  ADD A,A                 ; Shift the PAPER and BRIGHT bits from bits 0-3 to
  ADD A,A                 ; bits 3-6
  ADD A,A                 ;
  JR PRINTTILE_0          ; Jump back to fill in the INK bits
PRINTTILE_3:
  ADD A,A                 ; Shift the PAPER and BRIGHT bits from bits 0-3 to
  ADD A,A                 ; bits 3-6, and set the INK to 0
  ADD A,A                 ;
PRINTTILE_4:
  NOP                     ; Do nothing (during the startup sequence), or poke
                          ; the attribute byte onto the screen (this
                          ; instruction is set to LD (BC),A before the game
                          ; starts; see GAMEPOKES)
; The appropriate attribute byte has been poked onto the screen. Now to find
; the appropriate tile graphic data.
  LD L,E                  ; L=Q (0<=Q<=143), H=Y+160
  LD E,(HL)               ; E=LSB of the base address of the tile graphic data
  LD A,136                ; A=10001000
  SRL L                   ; Shift A right (Q AND 3) times
  JR NC,PRINTTILE_5       ;
  RRCA                    ;
PRINTTILE_5:
  SRL L                   ;
  JR NC,PRINTTILE_6       ;
  RRCA                    ;
  RRCA                    ;
PRINTTILE_6:
  LD C,A                  ; C=10001000, 01000100, 00100010 or 00010001
  LD A,L                  ; Point HL at the byte containing the MSB indicator
  ADD A,144               ; bit-pair: H=Y+160, 144<=L=144+INT(Q/4)<=179
  LD L,A                  ;
  LD A,C                  ; A=10001000, 01000100, 00100010 or 00010001
  AND (HL)                ; Keep only the bits of the bit-pair
; The base page of the tile graphic data depends on the bits set in A.
  LD D,128
  CP 16                   ; Are any of bits 4-7 in A set?
  JR C,PRINTTILE_7        ; Jump if not
  LD D,144
PRINTTILE_7:
  AND 15                  ; Are any of bits 0-3 in A set?
  JR Z,PRINTTILE_8        ; Jump if not
  SET 3,D
; Now D=128, 136, 144 or 152, and DE points at the first byte of the graphic
; data for the tile at skool coordinates (X,Y).
PRINTTILE_8:
  LD HL,BACKBUF           ; We're going to copy the tile into the 8-byte back
  LD B,8                  ; buffer at BACKBUF
PRINTTILE_9:
  XOR A                   ; Use a blank graphic byte (during startup), or
                          ; collect the tile graphic byte (this instruction is
                          ; set to LD A,(DE) before the game starts; see
                          ; GAMEPOKES)
  INC D                   ; Point DE at the next graphic byte
  LD (HL),A               ; Copy the graphic byte into the back buffer
  INC L                   ; Move to the next slot in the back buffer
  DJNZ PRINTTILE_9        ; Jump back until the entire tile has been copied
; The appropriate play area tile has been copied into the back buffer at
; BACKBUF. Now it's time to superimpose game character sprite tiles (if any).
  LD H,183                ; 183=character number of little girl no. 1
  POP DE                  ; D=Y (0-20), E=X (0-191)
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
                          ; (0-160)
  CP 120                  ; This x-coordinate is roughly halfway between the
                          ; tree and the gate
  JR C,PRINTTILE_10       ; Jump if columns 144 onwards (girls' skool + half
                          ; the girls' playground) are off-screen
  LD B,7                  ; 7 little girls
  CALL GETTILE            ; Superimpose graphic bytes for the little girls if
                          ; necessary
  LD H,198                ; 198=character number of little boy no. 9
  LD B,17                 ; 2 little boys, 11 main characters, plus the objects
                          ; using character buffers 211-214
  JR PRINTTILE_12
PRINTTILE_10:
  CP 80                   ; This is the x-coordinate of the assembly hall stage
                          ; (or thereabouts)
  JR C,PRINTTILE_11       ; Jump if columns 104 onwards (everything to the
                          ; right of the tree, roughly) are off-screen
  LD B,3                  ; 3 little girls (183-185)
  CALL GETTILE            ; Superimpose graphic bytes for these little girls if
                          ; necessary
  LD H,193                ; 193=character number of little boy no. 4
  LD B,22                 ; 7 little boys, 11 main characters, plus the objects
                          ; using character buffers 211-214
  JR PRINTTILE_12
PRINTTILE_11:
  LD H,190                ; 190=character number of little boy no. 1
  LD B,25                 ; All 10 little boys, 11 main characters, plus the
                          ; objects using character buffers 211-214
PRINTTILE_12:
  CALL GETTILE            ; Superimpose graphic bytes for these characters if
                          ; necessary
  LD HL,BACKBUF           ; The 8-byte back buffer at BACKBUF now contains the
                          ; tile to be copied to the screen
  POP DE                  ; Retrieve the appropriate display file address in DE
; This entry point is used by the routine at BUBBLEUDG.
PRINTTILE_13:
  LD B,8                  ; There are 8 bytes per character square
PRINTTILE_14:
  LD A,(HL)               ; Transfer the graphic bytes to the screen
  LD (DE),A               ;
  INC L                   ;
  INC D                   ;
  DJNZ PRINTTILE_14       ;
  RET

; Unused
  DEFS 2

; Update a character's animatory state and location and update the SRB
;
; Used by many routines. Sets the new animatory state and location of a
; character, and updates the screen refresh buffer (SRB) accordingly.
;
; A New animatory state
; D New y-coordinate (3-17)
; E New x-coordinate (0-189)
; H Character number (183-214)
UPDATEAS:
  LD L,2                  ; Point HL at byte 2 of the character's buffer
  LD (HL),D               ; Fill in the new y-coordinate
  DEC L                   ; L=1
  LD (HL),E               ; Fill in the new x-coordinate
  DEC L                   ; L=0
  LD (HL),A               ; Fill in the new animatory state
  LD C,A                  ; C=new animatory state
; This entry point is used by the routine at UPDATESRB to update the SRB for
; the current animatory state and location of the character.
UPDATEAS_0:
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
                          ; (0-160)
  SUB 3
  JR C,UPDATEAS_1
  CP E
  RET NC                  ; Return if the character is entirely off-screen to
                          ; the left
UPDATEAS_1:
  ADD A,34
  CP E
  RET C                   ; Return if the character is entirely off-screen to
                          ; the right
  SUB 32                  ; A=E-(LEFTCOL): the character's relative
  CPL                     ; x-coordinate (-2 to 31)
  ADD A,E                 ;
  LD B,3                  ; The sprites are 3 tiles wide
  PUSH HL
  LD H,215
  CP 254                  ; Is the character either fully on-screen or walking
                          ; off it to the right?
  JR C,UPDATEAS_2         ; Jump if so
  LD H,219
  DEC B                   ; B=2
  INC A                   ; Is the character one-third off (two-thirds on) at
                          ; the left?
  JR Z,UPDATEAS_2         ; Jump if so
  LD H,223
  DEC B                   ; B=1 (the character is one-third on-screen at the
                          ; left)
  XOR A
UPDATEAS_2:
  LD E,A                  ; E=leftmost column of the screen occupied by the
                          ; character's sprite (0-31)
  SUB 30
  JR C,UPDATEAS_3         ; Jump if the character is one- or two-thirds
                          ; off-screen to the right
  CPL
  ADD A,B
  LD B,A
; Now B holds a value (1, 2 or 3) equal to the number of columns of the screen
; occupied by the character's sprite.
UPDATEAS_3:
  LD L,C                  ; L=animatory state
  LD C,0
  BIT 7,L                 ; Is the character facing left?
  JR Z,UPDATEAS_4         ; Jump if so
  LD C,248
  LD A,182
  SUB H
  LD H,A
; Now H holds 215, 219 or 223 - the page containing the UDG reference for the
; tile on the top row of the leftmost column of the sprite that is on-screen. C
; is 0 if the character is facing left, or -8 if he's facing right.
UPDATEAS_4:
  SET 7,L                 ; Point HL at the UDG reference for the sprite tile
  LD A,E                  ; A=leftmost column of the screen occupied by the
                          ; sprite (0-31)
  EXX
  AND 7
  ADD A,120
  LD L,A
  LD H,225
  LD C,(HL)               ; Pick up a byte from the table at SRBBITS
  EXX
; Now C' holds 1, 2, 4, 8, 16, 32, 64 or 128. The bit set in C' corresponds to
; the bit that needs to be set in the relevant byte of the screen refresh
; buffer (SRB).
  LD A,E                  ; A=leftmost column of the screen occupied by the
                          ; sprite
  RRCA                    ; Set A to the LSB of the first byte of the SRB that
  RRCA                    ; needs to be modified
  RRCA                    ;
  AND 3                   ;
  ADD A,D                 ;
  ADD A,D                 ;
  ADD A,D                 ;
  ADD A,D                 ;
  EXX
  LD B,A                  ; Copy this LSB to B'
  LD H,127                ; The SRB starts at page 127 (SRB)
; Here we enter a loop to set the appropriate bits in the SRB.
UPDATEAS_5:
  EXX
  LD E,0                  ; E will count up to 4 (the number of rows occupied
                          ; by a sprite)
UPDATEAS_6:
  LD A,(HL)               ; Pick up the sprite tile UDG reference in A
  AND A                   ; Is this the 'null' UDG (blank square)?
  JR Z,UPDATEAS_7         ; Jump if so
  LD A,E                  ; A=sprite tile row number (0-3)
  ADD A,A                 ; Multiply by 4 (the number of bytes of the SRB that
  ADD A,A                 ; correspond to one row of the screen)
  EXX
  ADD A,B                 ; Point HL' at the relevant byte of the SRB
  LD L,A                  ;
  LD A,C                  ; The bit set in A is the bit that needs setting in
                          ; the SRB byte
  OR (HL)                 ; Set the bit in the SRB
  LD (HL),A               ;
  EXX
UPDATEAS_7:
  INC H                   ; Point HL at the UDG reference for the next tile in
                          ; the sprite (one row down)
  INC E                   ; Next row down in this column of the sprite
  BIT 2,E                 ; Have we done all four rows yet?
  JR Z,UPDATEAS_6         ; Jump back if not
  DEC B                   ; Have we done all the columns occupied by the sprite
                          ; yet?
  JR Z,UPDATEAS_8         ; Jump if so
  LD A,H                  ; Point HL at the UDG reference for the tile in the
  ADD A,C                 ; top row of the next column of the sprite (C=0 if
  LD H,A                  ; the character's facing left, -8 if facing right)
  EXX
  RRC C                   ; Move the SRB marker bit one place to the right
                          ; (possibly wrapping round to bit 7)
  JR NC,UPDATEAS_5        ; Jump back if there are still bits to be set in the
                          ; current SRB byte
  INC B                   ; Otherwise move to the next SRB byte
  JR UPDATEAS_5
UPDATEAS_8:
  POP HL                  ; Restore the character number to H (L=0)
  RET

; Unused
  DEFB 0

; Update the SRB for a character's current animatory state and location
;
; Used by many routines. Updates the screen refresh buffer (SRB) for a
; character's current animatory state and location. Returns with the
; character's current coordinates in DE and animatory state in A.
;
; H Character number (183-214)
UPDATESRB:
  LD L,2                  ; Point HL at byte 2 of the character's buffer
  LD D,(HL)               ; D=character's y-coordinate (0-17)
  DEC L                   ; L=1
  LD E,(HL)               ; E=character's x-coordinate (0-189)
  DEC L                   ; L=0
  LD C,(HL)               ; C=character's animatory state
  PUSH DE                 ; Save the character's coordinates briefly
  CALL UPDATEAS_0         ; Set the appropriate bits in the SRB for the
                          ; character's current animatory state and location
  POP DE                  ; Restore the character's coordinates to DE
  LD A,(HL)               ; A=character's animatory state
  RET

; Scroll the screen left 8 columns
;
; Used by the routines at MVERIC2 and PREPGAME.
LSCROLL8:
  LD B,8                  ; 8 columns will be scrolled on
LSCROLL8_0:
  PUSH BC                 ; Save the column counter
  LD HL,22529             ; First shift the attributes one place to the left
  LD DE,22528             ;
  LD BC,671               ;
  LDIR                    ;
  CALL LSCROLL            ; Scroll the display file one character square to the
                          ; left
  INC A                   ; Adjust the leftmost column of the play area now on
  LD (LEFTCOL),A          ; screen (held at LEFTCOL)
  LD HL,5151              ; H=20 (bottom row of the screen), L=31 (column at
                          ; the far right)
LSCROLL8_1:
  PUSH HL
  CALL PRINTTILE          ; Print the play area character square at row H,
                          ; column L=31
  POP HL
  DEC H                   ; Next row up
  JP P,LSCROLL8_1         ; Jump back until all 21 squares in the column have
                          ; been printed
  POP BC                  ; Restore the column counter to B
  DJNZ LSCROLL8_0         ; Jump back until 8 columns have been scrolled on
; The screen has been scrolled left 8 columns. Check whether any of the minor
; characters may be teleported without us noticing.
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  LD B,5                  ; 5 little boys, starting with 193=little boy no. 4
  LD H,193                ;
  CP 120                  ; Is the skool gate in the middle of the screen?
  JR Z,RSCROLL8_3         ; Jump if so (to consider teleporting little boys
                          ; 4-8)
  CP 80                   ; Is the boys' skool door in the middle of the
                          ; screen?
  RET NZ                  ; Return if not
  LD H,190                ; 190=little boy no. 1
  JR RSCROLL8_2           ; Consider teleporting little boys 1-3

; Scroll the screen right 8 columns
;
; Used by the routine at MVERIC2.
RSCROLL8:
  LD B,8                  ; 8 columns will be scrolled on
RSCROLL8_0:
  PUSH BC                 ; Save the column counter
  LD HL,23198             ; First shift the attributes one place to the right
  LD DE,23199             ;
  LD BC,671               ;
  LDDR                    ;
  CALL RSCROLL            ; Scroll the display file one character square to the
                          ; right
  DEC A                   ; Adjust the leftmost column of the play area now on
  LD (LEFTCOL),A          ; screen (held at LEFTCOL)
  LD HL,5120              ; H=20 (bottom row of the screen), L=0 (column at the
                          ; far left)
RSCROLL8_1:
  PUSH HL
  CALL PRINTTILE          ; Print the play area character square at row H,
                          ; column L=0
  POP HL
  DEC H                   ; Next row up
  JP P,RSCROLL8_1         ; Jump back until all 21 squares in the column have
                          ; been printed
  POP BC                  ; Restore the column counter to B
  DJNZ RSCROLL8_0         ; Jump back until 8 columns have been scrolled on
; The screen has been scrolled right 8 columns. Check whether any of the minor
; characters may be teleported without us noticing.
  LD A,(LEFTCOL)          ; A=leftmost column of the play area now on screen
  LD B,4                  ; 4 little girls, starting with 186=little girl no. 4
  LD H,186                ;
  CP 112                  ; Was the skool gate in the middle of the screen
                          ; before scrolling?
  JR Z,RSCROLL8_3         ; Jump if so (to consider teleporting little girls
                          ; 4-7)
  CP 72                   ; Was the boys' skool door in the middle of the
                          ; screen before scrolling?
  RET NZ                  ; Return if not
  LD H,183                ; 183=little girl no. 1
; The next section of code moves certain off-screen minor characters straight
; to their destination. Sneaky! This entry point is used by the routine at
; LSCROLL8 with H=190 (little boy no. 1).
RSCROLL8_2:
  LD B,3
; This entry point is used by the routines at LSCROLL8 (with H=193, B=5) and
; NEWLESSON1.
RSCROLL8_3:
  LD L,29                 ; Bit 3 of byte 29 is set if the character should be
  BIT 3,(HL)              ; moved immediately to the next destination in his
                          ; command list
  JR Z,RSCROLL8_5         ; Jump if this character should not be 'teleported'
  RES 3,(HL)              ; Reset bit 3 now, so that this character is not
                          ; considered for teleportation again during this
                          ; lesson
  DEC L                   ; L=28
  LD D,(HL)               ; Collect the address reached in the command list
  DEC L                   ; into DE
  LD E,(HL)               ;
  LD A,(DE)               ; A=LSB of this address
  SUB 100                 ; Is this the address of the routine at GOTO?
  JR NZ,RSCROLL8_5        ; Jump if not
  DEC L                   ; L=26
  INC DE                  ; Point DE at the location coordinates (the
  INC DE                  ; parameters of GOTO in the command list)
  LD (HL),D               ; Store this address in bytes 25 and 26 of the
  DEC L                   ; character's buffer
  LD (HL),E               ;
  DEC L                   ; L=24
RSCROLL8_4:
  LD (HL),A               ; Fill bytes 1-24 of the character's buffer with
  DEC L                   ; zeroes
  JR NZ,RSCROLL8_4        ;
  LD A,(HL)               ; Set the character's animatory state to its base
  AND 248                 ; value (cancelling any midstride or knockout)
  LD (HL),A               ;
  INC L                   ; L=1
  CALL GETPARAMS          ; Bring the location coordinates from the command
                          ; list into bytes 1 and 2 of the buffer, effecting
                          ; the teleportation
  LD E,(HL)               ; Collect the character's new coordinates into DE
  INC L                   ;
  LD D,(HL)               ;
  LD L,5                  ; Copy these coordinates into bytes 5 and 6 of the
  LD (HL),E               ; buffer, which normally hold the character's
  INC L                   ; destination; this is to let the routine at GOTO
  LD (HL),D               ; know it has nothing left to do
  LD L,29                 ; Reset bit 0 of byte 29, cancelling any pending
  RES 0,(HL)              ; request to restart the command list
RSCROLL8_5:
  INC H                   ; Next character
  DJNZ RSCROLL8_3         ; Jump back until all characters have been done
  RET

; Unused
  DEFS 2

; Copy two bytes from a command list into a character's buffer
;
; Used by the routines at RSCROLL8, MVCHARS, GOTO, MOVEDOOR, WALKABOUT and
; ADDR2CBUF. Copies a routine address or pair of parameters from a character's
; current command list into his buffer.
;
; H Character number (183-214)
; L Destination for the copied bytes
GETPARAMS:
  PUSH DE                 ; Save DE
  PUSH HL                 ; Save the pointer to the character's buffer
  LD L,25                 ; Collect into DE the address of the point reached in
  LD E,(HL)               ; the command list (stored in bytes 25 and 26 of the
  INC L                   ; character's buffer)
  LD D,(HL)               ;
  POP HL                  ; Restore the pointer to the character's buffer
  LD A,(DE)               ; Collect the first byte from the command list and
  INC DE                  ; copy it to the character's buffer
  LD (HL),A               ;
  INC L                   ; Move along one byte in the character's buffer
  LD A,(DE)               ; Collect the second byte from the command list and
  INC DE                  ; copy it to the character's buffer
  LD (HL),A               ;
  DEC L                   ; Move back again in the character's buffer
  LD A,L                  ; Save L temporarily
  LD L,25                 ; Update the address reached in the command list
  LD (HL),E               ; (stored in bytes 25 and 26 of the character's
  INC L                   ; buffer)
  LD (HL),D               ;
  POP DE                  ; Restore DE
  LD L,A                  ; Restore L
  RET

; Collect one byte from a command list
;
; Used by the routines at MVTILL, BWWRITE, RSTTELLSIT and CLISTJR10, and also
; by the unused routines at XSIGRAISE and SIGLOWER. Collects a byte from a
; character's current command list and returns it in A.
;
; H Character number (183-214)
GETPARAM:
  PUSH HL                 ; Save the character number
  LD L,25                 ; Bytes 25 and 26 of the character's buffer hold the
                          ; address of the point reached in the command list
  LD A,(HL)               ; Pick up the LSB of this address
  INC (HL)                ; Increment the LSB
  INC HL                  ; L=26
  JR NZ,GETPARAM_0        ; Jump unless the LSB was incremented to 0
  INC (HL)                ; Increment the MSB
  LD H,(HL)               ; HL=address of the point reached in the command list
  DEC H                   ;
  LD L,A                  ;
  LD A,(HL)               ; Pick up the byte from the command list
  POP HL                  ; Restore the character number to H
  RET
GETPARAM_0:
  LD H,(HL)               ; HL=address of the point reached in the command list
  LD L,A                  ;
  LD A,(HL)               ; Pick up the byte from the command list
  POP HL                  ; Restore the character number to H
  RET

; Get a random number
;
; Used by the routines at MVCHARS, GOTORAND, WRITEBRD, GIVELINES, CATTY,
; VIOLENT, FROGFALL, MVMOUSE, PREPMICE, CATCHMORF, WALKCHECK, SWOTLINES,
; TELLCLASS, DOCLASS, DETENTION, ERICSITLIE and PREPGAME. Returns with a random
; number in A.
GETRANDOM:
  PUSH HL
  LD HL,(RANDSEED)        ; Pick up the current random number seed from
                          ; RANDSEED
  INC HL                  ; Update the seed (by adding 257)
  INC H                   ;
  LD (RANDSEED),HL        ;
  LD A,(23672)            ; Pick up the LSB of the system variable FRAMES in A
  XOR (HL)                ; Now A=a pseudo-random number
  POP HL
  RET

; Update the display
;
; Used by the routines at WALKONOFF, MVERIC2, ERICHIT and MAINLOOP. Goes
; through the screen refresh buffer (SRB) and for every set bit found, updates
; the corresponding character square on-screen.
UPDATESCR:
  CALL BUBBLESRB          ; Update the SRB so that speech bubbles are not
                          ; corrupted by moving characters
  LD B,84                 ; 21 screen rows, 4 bytes (32 bits) per row
UPDATESCR_0:
  LD A,(HL)               ; Pick up a byte from the screen refresh buffer
  AND A                   ; Anything need updating in this particular 8-tile
                          ; segment?
  JR Z,UPDATESCR_4        ; Jump if not
  PUSH BC                 ; Save the SRB byte counter
  LD A,L                  ; For this particular byte of the SRB, compute the
  AND 252                 ; corresponding screen row number (0-20) in D
  RRCA                    ;
  RRCA                    ;
  LD D,A                  ;
  LD A,L                  ; Also for this particular SRB byte, compute the
  AND 3                   ; column of the screen (0, 8, 16 or 24) corresponding
  ADD A,A                 ; to bit 7
  ADD A,A                 ;
  ADD A,A                 ;
  DEC A
  LD E,A
UPDATESCR_1:
  INC E
  SLA (HL)                ; Does a character square need printing?
  JR C,UPDATESCR_2        ; Jump if so
  JR NZ,UPDATESCR_1       ; Jump back if there are still non-zero bits left in
                          ; this SRB byte
  JR UPDATESCR_3          ; Jump forward to consider the next SRB byte
; We found a set bit in the current SRB byte. Print the corresponding character
; square.
UPDATESCR_2:
  PUSH HL                 ; Save the SRB pointer
  PUSH DE                 ; Save the screen (row,column) pointer
  EX DE,HL                ; Switch the screen (row,column) pointer to HL
  CALL PRINTTILE          ; Print the character square at this row and column
  POP DE                  ; Restore the screen (row,column) pointer to DE
  POP HL                  ; Restore the SRB pointer to HL
  JR UPDATESCR_1          ; Examine the next bit of the current SRB byte
; There are no set bits remaining in the current SRB byte. Move to the next SRB
; byte.
UPDATESCR_3:
  POP BC                  ; Restore the SRB byte counter to B
UPDATESCR_4:
  INC L                   ; Point HL at the next SRB byte
  DJNZ UPDATESCR_0        ; Jump back until all 84 SRB bytes have been dealt
                          ; with
  RET

; Move the characters
;
; Used by the routine at MAINLOOP2.
MVCHARS:
  CALL MVCHARS_0          ; Move one character
  CALL MVCHARS_0          ; Move the next character
MVCHARS_0:
  LD HL,LASTCHAR
  LD A,(HL)               ; A=number of the character last moved (183-209,
                          ; 211-214)
MVCHARS_1:
  INC A                   ; A=number of the next character to move
  CP 210                  ; Is it ERIC?
  JR Z,MVCHARS_1          ; Jump if so (we don't want to move ERIC!)
  CP 215                  ; Have we completed an entire cast movement cycle?
  JR C,MVCHARS_2          ; Jump if not
  LD A,183                ; Back to 183 (little girl no. 1) if so
MVCHARS_2:
  CP 198                  ; Are we dealing with little girls 1-7 or little boys
                          ; 1-8?
  JR NC,MVCHARS_5         ; Jump if not
; It's the turn of one of the little girls (183-189), or one of the first 8
; little boys (190-197). But if the part of the play area currently on-screen
; is somewhere this character never ventures into, we skip ahead to the next
; suitable candidate. Specifically, the following characters are skipped
; depending on the value of X (the leftmost column of the skool on-screen):
;
; +------------+----------------------------------+
; | X          | Skip                             |
; +------------+----------------------------------+
; | X<=72      | girls 1-7 (183-189)              |
; | 80<=X<=112 | girls 4-7 and boys 1-3 (187-192) |
; | X>=120     | boys 1-8 (190-197)               |
; +------------+----------------------------------+
  LD E,A                  ; E=character number (183-197)
  LD D,190                ; 190=little boy no. 1
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  CP 80                   ; This is the x-coordinate of the middle of the
                          ; assembly hall stage
  JR NC,MVCHARS_3         ; Jump if we can't see to the left of this
; We are deep into the boys' skool, where no girls ever set foot.
  LD A,E                  ; A=character number (183-197)
  CP D                    ; Is this a little boy?
  JR NC,MVCHARS_5         ; Move him if so
  LD A,D                  ; Otherwise bypass the little girls and skip ahead to
  JR MVCHARS_5            ; little boy no. 1 (190)
; The leftmost column of the play area on screen is at least 80.
MVCHARS_3:
  CP 120                  ; This x-coordinate is about halfway between the tree
                          ; and the gate
  LD A,E                  ; A=character number (183-197)
  JR C,MVCHARS_4          ; Jump if we can't see the first half of the girls'
                          ; playground
; The leftmost column of the play area on screen is at least 120. Little boys
; 1-8 never set foot here.
  CP D                    ; Is this a little girl?
  JR C,MVCHARS_5          ; Move her if so
  LD A,198                ; Otherwise bypass little boys 1-8 and skip ahead to
  JR MVCHARS_5            ; little boy no. 9 (198)
; The leftmost column of the play area on screen is between 80 and 112. Little
; girls 4-7 and little boys 1-3 never visit these parts.
MVCHARS_4:
  CP 186                  ; Is this little girl no. 1-3?
  JR C,MVCHARS_5          ; Move her if so
  CP 193                  ; Is this little boy no. 4-8?
  JR NC,MVCHARS_5         ; Move him if so
  LD A,193                ; Otherwise bypass little girls 4-7 and little boys
                          ; 1-3 and skip ahead to little boy no. 4 (193)
; We've now determined which character to move next. His character number is in
; A.
MVCHARS_5:
  LD (HL),A               ; Store the number of the character to be moved next
                          ; in LASTCHAR
  LD H,A                  ; Point HL at byte 30 of this character's buffer
  LD L,30                 ;
  DEC (HL)                ; Is it time to consider a change in walking speed?
  JR NZ,MVCHARS_9         ; Jump if not
; It's time to consider a change in walking speed. If the character is a boy or
; girl, decide whether they will walk or run for the next little while.
  CALL GETRANDOM          ; A=random number
  AND 31                  ; 0<=A<=31
  ADD A,32                ; 32<=A<=63
  RRA                     ; 16<=A<=31 (and the carry flag is set if the random
                          ; number was odd)
  LD (HL),A               ; Place this number into byte 30 of the character's
                          ; buffer
  SBC A,A                 ; A=0 if the random number was even, 255 otherwise
  DEC L                   ; L=29
  AND 128                 ; A=0 (character will walk) or 128 (character will
                          ; run if he's a kid)
  OR (HL)                 ; Superimpose the current contents of byte 29
  BIT 6,(HL)              ; Is this a human character (183<=H<=209)?
  JR NZ,MVCHARS_6         ; Jump if not
; We're dealing with a human character. Bit 4 of byte 29 of the character's
; buffer is never set, but if it were, it would make the character run until
; the current command list routine has finished. Check bit 4 of byte 29 now.
  BIT 4,(HL)              ; Bit 4 of byte 29 is unused and always reset
  JR Z,MVCHARS_7          ; Always make this jump
MVCHARS_6:
  OR 128                  ; Ensure that bit 7 of byte 29 is set for non-human
                          ; characters
  JR MVCHARS_8
MVCHARS_7:
  BIT 5,(HL)              ; Is this an adult character (200<=H<=205)?
  JR Z,MVCHARS_8          ; Jump if not
  AND 127                 ; Ensure that bit 7 of byte 29 is reset for adult
                          ; characters (to make them walk)
MVCHARS_8:
  LD (HL),A               ; Restore byte 29 with bit 7 set (run) or reset
                          ; (walk) as appropriate
  INC L                   ; L=30
; Now to determine whether to move the character this time round. The answer
; will be yes if byte 30 (which is decremented on each pass through this
; routine) is even, or bit 7 of byte 29 is set (indicating that the character
; is either running or non-human).
MVCHARS_9:
  BIT 0,(HL)              ; Is the counter at byte 30 currently even?
  JR Z,MVCHARS_10         ; Jump if so (half the time)
  DEC L                   ; L=29
  BIT 7,(HL)              ; Is the character both human and not running?
  RET Z                   ; Return if so
; The character shall be moved. From this point, the following steps are taken:
;
; +------+--------------------------------------------------------------------+
; | Step | Action                                                             |
; +------+--------------------------------------------------------------------+
; | 1    | If there is an uninterruptible subcommand routine address in bytes |
; |      | 17 and 18 of the character's buffer, jump to it                    |
; | 2    | If there is a continual subcommand routine address in bytes 23 and |
; |      | 24 of the character's buffer, call it (and then return to step 3)  |
; | 3    | If there is an interruptible subcommand routine address in bytes 9 |
; |      | and 10 of the character's buffer, jump to it                       |
; | 4    | Restart the command list if bit 0 of byte 29 of the character's    |
; |      | buffer is set (and carry on to step 5)                             |
; | 5    | If there is a primary command routine address in bytes 3 and 4 of  |
; |      | the character's buffer, jump to it                                 |
; | 6    | Remove any continual subcommand routine address from bytes 23 and  |
; |      | 24 of the character's buffer                                       |
; | 7    | Collect the next primary command routine address from the command  |
; |      | list, place it into bytes 3 and 4 of the character's buffer, and   |
; |      | jump to it                                                         |
; +------+--------------------------------------------------------------------+
;
; The address of one of the following uninterruptible subcommand routines (or
; an entry point thereof) may be present in bytes 17 and 18 of the character's
; buffer:
;
; +------------+------------------------------------------------------------+
; | Address    | Description                                                |
; +------------+------------------------------------------------------------+
; | HITERIC    | Make HAYLEY hit ERIC                                       |
; | UNSEAT1    | Deal with a character who's been dethroned (1)             |
; | UNSEAT2    | Deal with a character who's been dethroned (2)             |
; | FINDSEAT2  | Deal with a character who is looking for a seat            |
; | SITASSEM   | Make a boy sit still until assembly is finished            |
; | CHRMVDOOR  | Make a character open or close a door                      |
; | WHEELBIKE  | Control the bike when ERIC's not sitting on the saddle     |
; | OBJFALL    | Control the descent of the water, sherry or conker         |
; | KNOCKED    | Deal with a character who has been knocked over            |
; | PELLET     | Control the flight of a catapult pellet                    |
; | BWFIRING   | Deal with BOY WANDER when he is firing                     |
; | HITTING    | Deal with ANGELFACE when he is hitting                     |
; | FROGFALL   | Deal with the frog when it has been knocked out of a cup   |
; | MVFROG     | Control the frog                                           |
; | GIRLJPING1 | Control a female character while she's standing on a chair |
; |            | or jumping (1)                                             |
; | STARTJPING | Make a female character stand on a chair or start jumping  |
; | MVMOUSE    | Control a mouse                                            |
; | DONOWT     | Make a character do nothing                                |
; | DESKLID    | Control an open desk lid                                   |
; | SBCLOUD    | Control a stinkbomb cloud                                  |
; | HEAD2WIN   | Make MR WACKER find a window to open after smelling a      |
; |            | stinkbomb                                                  |
; | WATERFALL  | Control water fired from the pistol                        |
; | GROWPLANT  | Control a watered plant                                    |
; +------------+------------------------------------------------------------+
;
; The address of one of the following continual subcommand routines (or an
; entry point thereof) may be present in bytes 23 and 24 of the character's
; buffer:
;
; +-----------+-------------------------------------------------------+
; | Address   | Description                                           |
; +-----------+-------------------------------------------------------+
; | CATTY     | Make BOY WANDER fire his catapult now and then        |
; | WATCHERIC | Make ALBERT keep an eye out for ERIC during lessons   |
; | HITHAYLEY | Make ANGELFACE stalk HAYLEY                           |
; | HITFIRE   | Make ANGELFACE or BOY WANDER hit or fire now and then |
; +-----------+-------------------------------------------------------+
;
; The address of one of the following interruptible subcommand routines (or an
; entry point thereof) may be present in bytes 9 and 10 of the character's
; buffer:
;
; +------------+--------------------------------------------------+
; | Address    | Description                                      |
; +------------+--------------------------------------------------+
; | WALK       | Guide a character to an intermediate destination |
; | ASCEND     | Guide a character up a staircase                 |
; | DESCEND    | Guide a character down a staircase               |
; | WIPE       | Make a teacher wipe a blackboard                 |
; | WRITEBRD   | Make a character write on a blackboard           |
; | SPEAK      | Make a character speak                           |
; | HERDERIC2  | Make MISS TAKE chase ERIC (2)                    |
; | SWOTSPK    | Make EINSTEIN speak                              |
; | SEEKERIC   | Make a teacher find the truant ERIC              |
; | FINDTRUANT | Make MR WACKER find the truant ERIC              |
; +------------+--------------------------------------------------+
MVCHARS_10:
  LD L,18                 ; Jump if there is an uninterruptible subcommand
  LD A,(HL)               ; routine address in bytes 17 and 18 of the
  AND A                   ; character's buffer
  JR NZ,MVCHARS_16        ;
  LD L,24                 ; Jump if there is no continual subcommand routine
  LD A,(HL)               ; address in bytes 23 and 24 of the character's
  AND A                   ; buffer
  JR Z,MVCHARS_12         ;
  PUSH HL                 ; Save the character number
  LD BC,MVCHARS_11        ; Push MVCHARS_11 (the address of the entry point to
  PUSH BC                 ; return to) onto the stack
  LD B,A                  ; Collect the address of the continual subcommand
  DEC L                   ; routine from bytes 23 and 24 of the character's
  LD C,(HL)               ; buffer into BC and push it onto the stack
  PUSH BC                 ;
  RET                     ; JP (BC) and then return to MVCHARS_11 below
MVCHARS_11:
  POP HL                  ; Restore the character number to H
; This entry point is used by the routine at RMUSUBCMD.
MVCHARS_12:
  LD L,10                 ; Jump if there is an interruptible subcommand
  LD A,(HL)               ; routine address in bytes 9 and 10 of the
  AND A                   ; character's buffer
  JR NZ,MVCHARS_16        ;
; If we get here, that means there's no uninterruptible subcommand routine
; address in bytes 17 and 18 and no interruptible subcommand routine address in
; bytes 9 and 10 of the character's buffer at the moment. We take this
; opportunity to check whether a command list restart has been requested, by
; inspecting bit 0 of byte 29. This bit is set in the following routines on
; particular occasions:
;
; +------------+-------------------------------------------------------------+
; | Routine    | Occasion                                                    |
; +------------+-------------------------------------------------------------+
; | CHKERIC2   | MISS TAKE needs to chase ERIC out of the girls' skool       |
; | SEEKERIC   | A teacher must find the absent ERIC during class, dinner or |
; |            | assembly                                                    |
; | WATCHERIC  | MR WACKER must find the truant ERIC                         |
; | EXPEL      | MR WACKER must find and expel ERIC                          |
; | NEWLESSON1 | Lesson has ended                                            |
; +------------+-------------------------------------------------------------+
;
; In addition, a command in the command list may have requested a restart (see
; RSTTELLSIT, RESTART, RSTDROPEN and RSTORASSEM).
  LD L,29                 ; Check bit 0 of byte 29: do we need to restart the
  BIT 0,(HL)              ; command list?
  JR Z,MVCHARS_13         ; Jump if not
  RES 0,(HL)              ; Reset bit 0 of byte 29 now that we are going to
                          ; restart
  LD D,H                  ; D=character number
  LD L,27                 ; Point HL at where the address of the character's
                          ; current command list is held
  LD E,25                 ; Point DE at where the address of the place reached
                          ; in the character's current command list is held
  LDI                     ; Copy the former into the latter (thus returning to
  LDI                     ; the start of the command list)
  JR MVCHARS_14
MVCHARS_13:
  LD L,4                  ; Jump if there is a primary command routine address
  LD A,(HL)               ; in bytes 3 and 4 of the character's buffer
  AND A                   ;
  JR NZ,MVCHARS_16        ;
; There is no primary command routine address in bytes 3 and 4 of the
; character's buffer, which means it's time to move to the next command in the
; list.
  LD L,29                 ; Reset bit 3 of byte 29, indicating that this
  RES 3,(HL)              ; character may not be considered for teleportation
                          ; for the remainder of this lesson (see LSCROLL8)
MVCHARS_14:
  RES 4,(HL)              ; Reset bit 4 of byte 29, indicating that the
                          ; character should no longer run if he was doing so
                          ; (though this bit is unused and always reset)
  LD L,24                 ; Remove the address of any continual subcommand
  LD (HL),0               ; routine from bytes 23 and 24 of the character's
                          ; buffer
; This entry point is used by the routine at ADDR2CBUF.
MVCHARS_15:
  LD L,3                  ; Collect the routine address of the next command in
  CALL GETPARAMS          ; the character's current command list and place it
                          ; into bytes 3 and 4 of the buffer
  INC L                   ; L=4
MVCHARS_16:
  LD B,(HL)               ; Collect the command routine address from bytes 3
  DEC L                   ; and 4, bytes 9 and 10, or bytes 17 and 18 of the
  LD C,(HL)               ; character's buffer into BC
  PUSH BC                 ; Make an indirect jump to this address
  RET                     ;

; Terminate a command
;
; The main entry point is used by the routines at GOTO, MVTILL, WALKABOUT,
; BWWRITE, RSTTELLSIT, RESTART, DINDUTY, ASSEMDUTY, DETENTION, CLSTART,
; RSTDROPEN, CLISTJR10, WAITDOOR and RSTORASSEM. Removes the primary command
; routine address from bytes 3 and 4 of the character's buffer, which has the
; effect of making the routine at MVCHARS move immediately to the next command
; in the command list.
NEXTCMD:
  LD L,4                  ; Byte 4 of the buffer contains the MSB of the
                          ; primary command routine address
  JR NEXTCMD_0            ; Jump forward to replace it with 0
; This entry point is used by the routines at WALK, ASCEND, DESCEND, WIPE,
; WRITEBRD, SPEAK, HERDERIC2 and SWOTSPK. Removes the interruptible subcommand
; routine address from bytes 9 and 10 of the character's buffer, which has the
; effect of making the routine at MVCHARS immediately hand control of the
; character back to the primary command.
RMSUBCMD:
  LD L,10                 ; Byte 10 of the buffer contains the MSB of the
                          ; interruptible subcommand routine address
  JR NEXTCMD_0            ; Jump forward to replace it with 0
; This entry point is used by the routines at KNOCKED, BWFIRING and MVMOUSE.
; Removes the uninterruptible subcommand routine address from bytes 17 and 18
; of the character's buffer, which has the effect of making the routine at
; MVCHARS immediately hand control of the character back to the interruptible
; subcommand or the primary command.
RMUSUBCMD:
  LD L,18                 ; Byte 18 of the buffer contains the MSB of the
                          ; uninterruptible subcommand routine address
NEXTCMD_0:
  LD A,(LASTCHAR)         ; A=number of the character being worked on (183-214)
  LD H,A                  ; Zero out the relevant routine address MSB (in byte
  LD (HL),0               ; 4, 10 or 18) in the character's buffer
  JP MVCHARS_12           ; Re-enter the character-moving routine (MVCHARS) at
                          ; the checkpoint for the interruptible subcommand

; Call an interruptible subcommand
;
; Used by the routines at INASSEMBLY, MVTILL, WALKABOUT, BWWRITE, GRASSETC,
; TELLCLASS, RSTTELLSIT, DINDUTY, DOCLASS, DETENTION and FINDEXPEL. Drops the
; return address from the stack and copies it into bytes 3 and 4 of the
; character's buffer (where the address of the primary command routine is
; held), copies the address of the interruptible subcommand routine from BC
; into bytes 9 and 10 of the character's buffer, and then jumps to that
; routine.
;
; BC Interruptible subcommand routine address
; H Character number (183-214)
CALLSUBCMD:
  LD L,9                  ; Copy the routine address from BC into bytes 9 and
  LD (HL),C               ; 10 of the character's buffer
  INC L                   ;
  LD (HL),B               ;
  LD A,H                  ; A=character number
  POP HL                  ; Drop the return address from the stack into HL
  PUSH BC                 ; Push the routine address in BC onto the stack
  LD C,L
  LD L,3
; This entry point is used by the (unused) routine at XDOSUBCMD with L=9.
CALLSUBCMD_0:
  LD B,H                  ; Now BC=address of the instruction after the CALL
                          ; that got us here
  LD H,A                  ; H=character number
; This entry point is used by the routine at DOSUBCMD with L=9 and BC=WALK,
; ASCEND, DESCEND or FINDTRUANT.
CALLSUBCMD_1:
  LD (HL),C               ; Copy the routine address in BC into bytes 3 and 4
  INC L                   ; (or 9 and 10) of the character's buffer
  LD (HL),B               ;
  RET                     ; Make an indirect jump to the interruptible
                          ; subcommand routine

; Jump to an interruptible subcommand
;
; Used by the routines at GOTO and STALKERIC. Copies the routine address from
; BC into bytes 9 and 10 of the character's buffer (thus setting the
; interruptible subcommand), and then jumps to that routine.
;
; H Character number (183-214)
; BC WALK, ASCEND, DESCEND or FINDTRUANT
DOSUBCMD:
  PUSH BC                 ; Push the routine address in BC onto the stack
  LD L,9                  ; Point HL at byte 9 of the character's buffer
  JR CALLSUBCMD_1         ; Place the interruptible subcommand routine address
                          ; into bytes 9 and 10 of the character's buffer, then
                          ; jump to it

; Set the interruptible subcommand (unused)
;
; This routine is not used. It drops the return address from the stack and
; copies it into bytes 9 and 10 of the character's buffer (thus setting the
; interruptible subcommand), and then jumps to the routine whose address is in
; BC.
;
; BC Routine address
; H Character number (183-214)
XDOSUBCMD:
  LD A,H                  ; A=character number
  POP HL                  ; Drop the return address from the stack into HL
  PUSH BC                 ; Push the routine address in BC onto the stack
  LD C,L
  LD L,9
  JR CALLSUBCMD_0         ; Copy the return address into bytes 9 and 10 of the
                          ; character's buffer, and jump to the routine whose
                          ; address was in BC on entering this routine

; Unused
  DEFB 0

; Determine ERIC's y-coordinate
;
; Used by the routines at FINDERIC and ERICLOCID. Returns with D holding ERIC's
; y-coordinate if he has his feet on the floor. Otherwise returns with D=3, 10,
; 14 or 17, indicating the floor that ERIC is directly above.
GETERICY:
  LD DE,(53761)           ; Get ERIC's coordinates in DE
  LD A,(STATUS)           ; Copy ERIC's status flags from STATUS to A
  RRCA                    ; Is ERIC jumping?
  JR C,GETERICY_0         ; Jump if so
  RRCA                    ; Does ERIC have his feet on the floor?
  RET NC                  ; Return if so (with D holding ERIC's y-coordinate)
  LD A,(STATUS2)          ; Copy ERIC's other status flags from STATUS2 to A
  AND 6                   ; Set the zero flag unless bit 1 (ERIC is standing on
                          ; a plant) or bit 2 (ERIC's stepping off a plant or
                          ; the stage) is set
GETERICY_0:
  JP GETERICY2            ; Determine which floor ERIC's closest to

; Get the play area region identifier for a given location
;
; Used by the routines at GOTO, NEXTMV and HERDERIC2. The value returned in A
; (corresponding to the location in DE) is as follows:
;
; +-----+-----------------------------------------------------------------+
; | ID  | Region                                                          |
; +-----+-----------------------------------------------------------------+
; | 189 | Top floor, left of the left study door in the boys' skool       |
; | 190 | Middle floor, left of the far wall of the Science Lab storeroom |
; | 191 | Anywhere on the bottom floor (or in mid-air!)                   |
; | 192 | Assembly hall stage                                             |
; | 193 | Near the middle-floor window in the boys' skool                 |
; | 194 | Top floor, right of the left study door in the boys' skool      |
; | 195 | Middle floor, girls' skool                                      |
; | 196 | Top floor, girls' skool                                         |
; +-----+-----------------------------------------------------------------+
;
; Or if the entry point at GETREGION_0 is used (for ERIC's location):
;
; +----+-------------------------------------------------------+
; | ID | Region                                                |
; +----+-------------------------------------------------------+
; | 0  | None of the places below (ERIC is never allowed here) |
; | 1  | Playground                                            |
; | 2  | Various places in the boys' skool outside classrooms  |
; | 3  | Assembly hall                                         |
; | 4  | Dining hall                                           |
; | 5  | Revision Library, outside the Yellow Room door        |
; | 6  | Science Lab                                           |
; | 7  | Blue Room                                             |
; | 8  | Yellow Room                                           |
; +----+-------------------------------------------------------+
;
; The carry flag is set upon return to the caller of this routine if the
; coordinates in DE correspond to a location on a staircase.
;
; D y-coordinate of the play area (3-17)
; E x-coordinate of the play area (0-190)
GETREGION:
  CALL ONSTAGE            ; Divert back to the calling routine with A=192 and
                          ; the carry flag reset if the character is on the
                          ; assembly hall stage; otherwise return here with
                          ; B=182 and C=59
; This entry point is used by the routine at ERICLOCID with ERIC's coordinates
; in DE, B=185, and C=59.
GETREGION_0:
  LD A,D                  ; A=y-coordinate
GETREGION_1:
  INC B                   ; Set B to 183 (186) if D<=6 (top floor), 184 (187)
  SUB 7                   ; if 7<=D<=13 (middle floor), or 185 (188) if
  JR NC,GETREGION_1       ; 14<=D<=18 (bottom floor)
; Now BC will be used to index one of the skool region tables that contain the
; location identifiers:
;
; +-----------+---------------------+
; | Address   | Region              |
; +-----------+---------------------+
; | TOPFLOOR  | Top floor           |
; | MIDFLOOR  | Middle floor        |
; | BOTFLOOR  | Bottom floor        |
; | ERICLOCTF | Top floor (ERIC)    |
; | ERICLOCMF | Middle floor (ERIC) |
; | ERICLOCBF | Bottom floor (ERIC) |
; +-----------+---------------------+
  CP 252                  ; Does D=3, 10 or 17 (top, middle, bottom floor)?
  JR Z,GETREGION_2        ; Jump if so
  SCF                     ; Signal: D does not correspond exactly to the top,
                          ; middle or bottom floor
  NOP                     ;
  NOP                     ;
  NOP                     ;
GETREGION_2:
  PUSH AF                 ; Save the carry flag temporarily
GETREGION_3:
  INC C                   ; Point BC at the appropriate entry in one of the
  LD A,(BC)               ; region tables
  INC C                   ;
  CP E                    ;
  JR C,GETREGION_3        ;
  POP AF                  ; Restore the carry flag (reset if D=3, 10 or 17; set
                          ; otherwise)
  LD A,(BC)               ; A=region identifier (189-196 or 0-8; see the tables
                          ; above)
  RET

; Guide a character to an intermediate destination
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of the character's buffer by the routines at GOTO, INASSEMBLY,
; WALKCHECK and DOCLASS. It is used to make a character go straight to a
; destination (or intermediate destination) that is reachable without
; negotiating any staircases.
;
; H Character number (183-214)
WALK:
  LD L,12                 ; Point HL at byte 12 of the character's buffer
  LD (HL),8               ; Initialise this to 8
; Byte 12 is used to count the number of paces the character has taken while
; under the control of this routine. It is decremented on each pass (at WALK_3
; below). If it reaches 0, the address of this routine is removed from bytes 9
; and 10 of the character's buffer. This has the effect of allowing the
; character-moving routine at MVCHARS the chance to check whether a command
; list restart (bit 0 of byte 29 set) has been requested in the meantime. This
; ensures that the character does not proceed all the way to the (intermediate)
; destination before responding to some important event, such as the start of a
; new lesson (important for every character), ERIC playing truant (important
; for MR WACKER), or ERIC being present in the girls' skool when it's not
; playtime (important for MISS TAKE). If no command list restart has been
; requested in the meantime, the address of this routine will be placed into
; bytes 9 and 10 of the character's buffer again by the routine at GOTO, and
; the character will continue towards his destination.
  NOP                     ;
  NOP                     ;
  LD L,9                  ; Replace the address of this routine in bytes 9 and
  LD (HL),247             ; 10 of the character's buffer with WALK0 (below)
; The second and subsequent calls to this routine (from MVCHARS) enter here:
WALK0:
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  BIT 0,(HL)              ; Is the character midstride?
  JR Z,WALK_3             ; Jump if not
; The character is midstride. He'll have to finish his stride before we move
; him one step closer to his destination. This entry point is used by the
; routines at ASCEND, FINDSEAT2, CLOUD and TOWINDOW.
WALK_0:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
; This entry point is used by the routines at DESCEND and FINDERIC.
WALK_1:
  INC E                   ; E=1 + character's x-coordinate
  BIT 7,A                 ; A=character's animatory state
  JR NZ,WALK_2            ; Jump if the character is facing right
  DEC E
  DEC E                   ; E=character's x-coordinate - 1
WALK_2:
  AND 252                 ; Set B to the base animatory state of the character
  LD B,A                  ; by discarding bits 0 and 1
  LD A,E                  ; A=character's new x-coordinate (after finishing his
                          ; stride)
  AND 1                   ; A=0 if the x-coordinate is even, 2 if it's odd
  ADD A,A                 ;
  OR B                    ; A=character's new animatory state (standing/walking
                          ; phase 1 for even x-coordinates, phase 3 for odd)
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB
; The character is not midstride. If he hasn't reached his destination yet,
; make him put one foot forward (if he's facing the right way), or turn him
; round.
WALK_3:
  LD L,12                 ; Point HL at byte 12 of the character's buffer
  DEC (HL)                ; Is it time to give the character-moving routine at
                          ; MVCHARS a chance to check whether a command list
                          ; restart has been requested?
  JP Z,RMSUBCMD           ; Terminate this interruptible subcommand if so
  DEC L                   ; L=11
  LD A,(HL)               ; A=character's destination x-coordinate
  LD L,1                  ; Byte 1 of the character's buffer holds his current
                          ; x-coordinate
  CP (HL)                 ; Has the character reached his destination?
  JP Z,RMSUBCMD           ; Terminate this interruptible subcommand if so
; This entry point is used by the routine at TOWINDOW.
WALK_4:
  DEC HL                  ; Point HL at byte 0 of the character's buffer
  BIT 7,(HL)              ; Is the character facing left?
  JR Z,WALK_6             ; Jump if so
  JR C,WALK_7             ; Jump if the character will have to turn round first
; This entry point is used by the routine at DESCEND.
WALK_5:
  CALL DOOROPEN           ; Check for closed doors in the character's path
  INC A                   ; A=character's new (midstride) animatory state
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB
; This entry point is used by the routine at FINDSEAT2.
WALK_6:
  JR C,WALK_5             ; Jump if the character is facing the right way
; This entry point is used by the routines at GOTO, FINDERIC, INASSEMBLY,
; TURNERIC and WATCHERIC (to turn a character round), and CLOUD (to move the
; stinkbomb cloud).
WALK_7:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  XOR 128                 ; Flip bit 7 of the character's animatory state, thus
                          ; turning him round
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Guide a character up a staircase
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of a character's buffer by the routine at GOTO.
;
; H Character number (183-209)
ASCEND:
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  BIT 0,(HL)              ; Is the character midstride?
  JP NZ,WALK_0            ; Finish the stride if so
  LD L,11                 ; Byte 11 of the character's buffer holds the number
                          ; of steps left to be climbed
  DEC (HL)                ; One fewer now
  JP Z,RMSUBCMD           ; Terminate this interruptible subcommand if the top
                          ; of the staircase has been reached
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  DEC D                   ; Up a step
  INC A                   ; One foot forward
  JP UPDATEAS             ; Update the character's animatory state (to
                          ; midstride) and location and update the SRB

; Guide a character down a staircase
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of a character's buffer by the routine at GOTO.
;
; H Character number (183-209)
DESCEND:
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  BIT 0,(HL)              ; Is the character midstride?
  JR NZ,DESCEND_0         ; Jump if so
  LD L,11                 ; Byte 11 of the character's buffer holds the number
                          ; of steps left to be descended
  DEC (HL)                ; One fewer now
  JP Z,RMSUBCMD           ; Terminate this interruptible subcommand if the
                          ; bottom of the staircase has been reached
  JP WALK_5               ; One foot forward
DESCEND_0:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  INC D                   ; Down a step
  JP WALK_1               ; Legs together again and update the SRB

; Unused
  DEFB 0

; Make a character go to a location
;
; Used by command lists 0, 2, 4, 6, 8, 10, 12, 14, 16, 20, 22, 24, 26, 28, 30,
; 32, 34, 36, 38, 40, 44, 46, 48, 50, 52, 54, 56, 58, 60, 62, 64, 66, 70, 72,
; 74, 76, 78, 80, 82, 84, 86 and 88 to make a character go to a specified
; place.
;
; H Character number (183-209)
GOTO:
  LD L,5                  ; Copy the destination coordinates from the command
  CALL GETPARAMS          ; list into bytes 5 and 6 of the character's buffer
; This entry point is used by the routine at GOTORAND.
GOTO_0:
  LD L,3                  ; Replace the address of this routine in bytes 3 and
  LD (HL),126             ; 4 of the character's buffer with GOTO_1 (below)
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  LD A,(HL)               ; A=character's animatory state
  AND 7                   ; Is the character sitting on a chair (kids only)?
  CP 4                    ;
  JR NZ,GOTO_1            ; Jump if not
; The character is sitting on a chair. He'll have to stand up first.
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  AND 248                 ; A=animatory state of the character standing up
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB
; The second and subsequent calls to this routine (from MVCHARS) enter here:
GOTO_1:
  LD L,6                  ; Collect the destination coordinates into DE
  LD D,(HL)               ;
  DEC L                   ;
  LD E,(HL)               ;
  LD L,1                  ; Point HL at byte 1 of the character's buffer
  LD A,E                  ; A=destination x-coordinate
  CP (HL)                 ; Compare it with the character's current
                          ; x-coordinate
  INC HL                  ; L=2
  JR NZ,GOTO_2            ; Jump forward unless the character's current
                          ; x-coordinate matches that of the destination
  LD A,D                  ; A=destination y-coordinate
  CP (HL)                 ; Is the character already at his destination?
  JP Z,NEXTCMD            ; Move to the next command in the command list if so
; The character hasn't reached his destination yet.
GOTO_2:
  CALL GETREGION          ; Get the region identifier for the character's
                          ; destination
; Now A holds a region ID (189-196) corresponding to the character's
; destination:
;
; +-----+-----------------------------------------------------------------+
; | ID  | Region                                                          |
; +-----+-----------------------------------------------------------------+
; | 189 | Top floor, left of the left study door in the boys' skool       |
; | 190 | Middle floor, left of the far wall of the Science Lab storeroom |
; | 191 | Bottom floor (anywhere)                                         |
; | 192 | Assembly hall stage                                             |
; | 193 | Near the middle-floor window in the boys' skool                 |
; | 194 | Top floor, right of the left study door in the boys' skool      |
; | 195 | Middle floor, girls' skool                                      |
; | 196 | Top floor, girls' skool                                         |
; +-----+-----------------------------------------------------------------+
  LD D,(HL)               ; Collect the character's current coordinates into DE
  DEC L                   ;
  LD E,(HL)               ;
  LD L,A                  ; L=destination region ID (189-196)
  CALL GETREGION          ; A=region ID (189-196) for the character's current
                          ; location
  CP L                    ; Will the character have to negotiate any staircases
                          ; in order to reach his destination?
  JR Z,GOTO_4             ; Jump if not (he's in the same region as his
                          ; destination)
  LD D,A                  ; D=region ID of the character's current location
                          ; (189-196)
  LD A,L                  ; A=destination region ID (189-196)
  SUB 129                 ; E=destination region ID minus 129 (60-67)
  LD E,A                  ;
  LD A,(DE)               ; Collect the appropriate staircase endpoint
                          ; identifier
; The staircase endpoint identifiers are arranged in bytes 60-67 (corresponding
; to the destination) of pages 189-196 (corresponding to the current location)
; as follows:
;
;                    +-----+-----+-----+-----+-----+-----+-----+-----+
;                    | 189 | 190 | 191 | 192 | 193 | 194 | 195 | 196 |
; +------------+-----+-----+-----+-----+-----+-----+-----+-----+-----+
; | SCEPIDS189 | 189 |     | 189 | 189 | 189 | 189 | 189 | 189 | 189 |
; | SCEPIDS190 | 190 | 190 |     | 191 | 191 | 191 | 191 | 191 | 191 |
; | SCEPIDS191 | 191 | 192 | 192 |     | 193 | 193 | 193 | 194 | 194 |
; | SCEPIDS192 | 192 | 195 | 195 | 195 |     | 196 | 196 | 195 | 195 |
; | SCEPIDS193 | 193 | 197 | 197 | 197 | 197 |     | 198 | 197 | 197 |
; | SCEPIDS194 | 194 | 199 | 199 | 199 | 199 | 199 |     | 199 | 199 |
; | SCEPIDS195 | 195 | 200 | 200 | 200 | 200 | 200 | 200 |     | 206 |
; | SCEPIDS196 | 196 | 207 | 207 | 207 | 207 | 207 | 207 | 207 |
; +------------+-----+-----+-----+-----+-----+-----+-----+-----+
;
; The staircase endpoint indentifier now in A (189-200, 206 or 207) corresponds
; to the top or bottom of the first staircase the character will have to
; negotiate in order to reach his destination. The x-coordinate (x), direction
; indicator (d), appropriate routine address LSB (56 for ASCEND, 77 for
; DESCEND) and number of steps (n) for the staircase can be found in bytes
; 68-71 of pages 189-200 and 206-207, as follows:
;
; +-----+-----+-----+-----+---+-----------------------------------------------+
; | ID  | x   | d   | LSB | n | Staircase endpoint                            |
; +-----+-----+-----+-----+---+-----------------------------------------------+
; | 189 | 26  | 0   | 77  | 8 | Top of the staircase leading down from the    |
; |     |     |     |     |   | Revision Library                              |
; | 190 | 19  | 128 | 56  | 8 | Bottom of the staircase leading up to the     |
; |     |     |     |     |   | Revision Library                              |
; | 191 | 15  | 128 | 77  | 8 | Top of the staircase leading down to the      |
; |     |     |     |     |   | bottom floor on the far left of the boys'     |
; |     |     |     |     |   | skool                                         |
; | 192 | 22  | 0   | 56  | 8 | Bottom of the staircase leading up to the     |
; |     |     |     |     |   | middle floor on the far left of the boys'     |
; |     |     |     |     |   | skool                                         |
; | 193 | 83  | 0   | 56  | 4 | Bottom of the staircase leading up to the     |
; |     |     |     |     |   | assembly hall stage                           |
; | 194 | 176 | 128 | 56  | 8 | Bottom of the staircase leading up to the     |
; |     |     |     |     |   | middle floor in the girls' skool              |
; | 195 | 80  | 128 | 77  | 4 | Top of the staircase leading down from the    |
; |     |     |     |     |   | assembly hall stage                           |
; | 196 | 81  | 128 | 56  | 5 | Bottom of the staircase leading up from the   |
; |     |     |     |     |   | assembly hall stage                           |
; | 197 | 85  | 0   | 77  | 5 | Top of the staircase leading down to the      |
; |     |     |     |     |   | assembly hall stage                           |
; | 198 | 91  | 0   | 56  | 8 | Bottom of the staircase leading up to the     |
; |     |     |     |     |   | head's study                                  |
; | 199 | 84  | 128 | 77  | 8 | Top of the staircase leading down from the    |
; |     |     |     |     |   | head's study                                  |
; | 200 | 183 | 0   | 77  | 8 | Top of the staircase leading down to the      |
; |     |     |     |     |   | bottom floor in the girls' skool              |
; | 206 | 186 | 0   | 56  | 8 | Bottom of the staircase leading up to the top |
; |     |     |     |     |   | floor in the girls' skool                     |
; | 207 | 179 | 128 | 77  | 8 | Top of the staircase leading down to middle   |
; |     |     |     |     |   | floor in the girls' skool                     |
; +-----+-----+-----+-----+---+-----------------------------------------------+
  LD D,A                  ; Place the staircase endpoint identifier in D
  LD E,68                 ; Byte 68 holds the x-coordinate of the top or bottom
                          ; of the staircase identified by D
  LD L,1                  ; Point DE at byte 1 of the character's buffer, and
  EX DE,HL                ; HL at the staircase endpoint x-coordinate
  LD A,(DE)               ; A=character's current x-coordinate
  CP (HL)                 ; Is the character already at the top or bottom of
                          ; the appropriate staircase?
  JR NZ,GOTO_3            ; Jump forward if not
; At this stage the character is standing at the top or bottom of the first
; staircase he will have to negotiate in order to reach his destination.
  INC L                   ; (HL)=0 if the character has to face left to ascend
                          ; or descend the stairs, 128 otherwise
  DEC E                   ; E=0
  LD A,(DE)               ; A=character's animatory state
  XOR (HL)                ; Set the carry flag if the character has to turn
  RLCA                    ; round to negotiate the staircase
  EX DE,HL                ; Swap DE and HL so that H holds the character number
  JP C,WALK_7             ; Turn the character round if he's facing the wrong
                          ; way
  EX DE,HL                ; Swap DE and HL back again
  LD E,11                 ; Point DE at byte 11 of the character's buffer
  INC L                   ; L=70
  LD C,(HL)               ; BC=ASCEND if the stairs are to be ascended, DESCEND
  LD B,100                ; if descended
  NOP
  INC L                   ; L=71
  LD A,(HL)               ; A=1 + number of steps in the staircase
  LD (DE),A               ; Place this number of steps in byte 11 of the
                          ; character's buffer
  EX DE,HL                ; Swap DE and HL so that H holds the character number
  JP DOSUBCMD             ; Copy the interruptible subcommand routine address
                          ; from BC into bytes 9 and 10 of the character's
                          ; buffer and jump to it
; The character is not yet at the top or bottom of the appropriate staircase.
GOTO_3:
  LD A,(HL)               ; A=x-coordinate of the top or bottom of the
                          ; staircase
  EX DE,HL                ; H=character number
  JR GOTO_5               ; Send the character towards the staircase
; The character can reach his destination without negotiating any staircases.
GOTO_4:
  LD L,5                  ; Point HL at byte 5 of the character's buffer
  LD A,(HL)               ; A=x-coordinate of the character's destination
GOTO_5:
  LD L,11                 ; Place the x-coordinate of the top or bottom of the
  LD (HL),A               ; staircase, or the destination, into byte 11 of the
                          ; character's buffer
  LD BC,WALK              ; Place the address of the interruptible subcommand
  JP DOSUBCMD             ; routine at WALK into bytes 9 and 10 of the
                          ; character's buffer and jump to it, thus guiding the
                          ; character to this x-coordinate

; Unused
  DEFS 3

; Make a character go to a random location
;
; Used by command lists 18, 20, 28, 40, 42, 66, 68, 78 and 88 to make a
; character go to a place at random. The random locations are arranged in eight
; groups of four. The group of locations that is suitable for a particular
; character to choose from is determined by byte 36 of the character's buffer.
;
; H Character number (183-209)
GOTORAND:
  LD L,36                 ; Point HL at byte 36 of the character's buffer
  LD E,(HL)               ; Pick up the contents in E
  EX DE,HL
GOTORAND_0:
  CALL GETRANDOM          ; A=random number
  AND 3                   ; A=211, 212, 213 or 214
  ADD A,211               ;
  LD H,A                  ; Point HL at an entry in the table of random
                          ; locations
  LD E,1                  ; Point DE at byte 1 of the character's buffer
  LD A,(DE)               ; A=character's x-coordinate
  CP (HL)                 ; Is the character already at this x-coordinate?
  JR Z,GOTORAND_0         ; Get another random location if so
  LD E,5                  ; Copy the random location's coordinates into bytes 5
  LDI                     ; and 6 of the character's buffer, thus making it his
  LDI                     ; next destination
  EX DE,HL                ; Transfer the character number back to H
  JP GOTO_0               ; Send the character on his way
; The contents of byte 36 of each character's buffer are as follows:
;
; +-----+-------------------+------------+------+
; | H   | Character         | Address    | Byte |
; +-----+-------------------+------------+------+
; | 183 | Little girl no. 1 | CBUF183_36 | 36   |
; | 184 | Little girl no. 2 | CBUF184_36 | 36   |
; | 185 | Little girl no. 3 | CBUF185_36 | 36   |
; | 186 | Little girl no. 4 | CBUF186_36 | 38   |
; | 187 | Little girl no. 5 | CBUF187_36 | 38   |
; | 188 | Little girl no. 6 | CBUF188_36 | 38   |
; | 189 | Little girl no. 7 | CBUF189_36 | 38   |
; | 190 | Little boy no. 1  | CBUF190_36 | 40   |
; | 191 | Little boy no. 2  | CBUF191_36 | 40   |
; | 192 | Little boy no. 3  | CBUF192_36 | 40   |
; | 193 | Little boy no. 4  | CBUF193_36 | 42   |
; | 194 | Little boy no. 5  | CBUF194_36 | 42   |
; | 195 | Little boy no. 6  | CBUF195_36 | 42   |
; | 196 | Little boy no. 7  | CBUF196_36 | 42   |
; | 197 | Little boy no. 8  | CBUF197_36 | 42   |
; | 198 | Little boy no. 9  | CBUF198_36 | 44   |
; | 199 | Little boy no. 10 | CBUF199_36 | 44   |
; | 200 | MR WACKER         | CBUF200_36 | 48   |
; | 201 | MR WITHIT         | CBUF201_36 | 48   |
; | 202 | MR ROCKITT        | CBUF202_36 | 68   |
; | 203 | MR CREAK          | CBUF203_36 | 68   |
; | 204 | MISS TAKE         | CBUF204_36 | 38   |
; | 205 | ALBERT            | CBUF205_36 | 44   |
; | 206 | BOY WANDER        | CBUF206_36 | 46   |
; | 207 | ANGELFACE         | CBUF207_36 | 46   |
; | 208 | EINSTEIN          | CBUF208_36 | 44   |
; | 209 | HAYLEY            | CBUF209_36 | 36   |
; +-----+-------------------+------------+------+
;
; The eight groups of random locations used by this routine are:
;
; +----+--------+--------+--------+--------+
; | E  | D=211  | D=212  | D=213  | D=214  |
; +----+--------+--------+--------+--------+
; | 36 | 129,17 | 144,17 | 136,17 | 189,17 |
; | 38 | 189,17 | 189,10 | 170,17 | 182,3  |
; | 40 | 38,3   | 10,10  | 10,17  | 66,17  |
; | 42 | 38,3   | 10,17  | 109,17 | 75,17  |
; | 44 | 112,17 | 145,17 | 158,17 | 37,3   |
; | 46 | 189,10 | 183,17 | 137,17 | 72,17  |
; | 48 | 157,17 | 38,3   | 10,17  | 92,17  |
; | 68 | 38,3   | 90,3   | 10,17  | 90,17  |
; +----+--------+--------+--------+--------+

; Determine the next move of a character following another character
;
; Used by the routines at FINDERIC and GETINPUT. Returns with one of the
; following values in bits 0-2 of A depending on the relative locations of the
; follower and his target:
;
; +---+---------------------------------------------------+
; | A | Meaning                                           |
; +---+---------------------------------------------------+
; | 0 | Follower is at the same coordinates as the target |
; | 1 | Follower should go (or continue going) upstairs   |
; | 2 | Follower should go (or continue going) downstairs |
; | 3 | Follower should go left                           |
; | 4 | Follower should go right                          |
; +---+---------------------------------------------------+
;
; In addition, bit 3 of A will be set if the follower is facing a closed door,
; and C will hold the identifier of the closed door (see MVDOORWIN).
;
; DE Target character's coordinates
; H Follower's character number (200-204, 210)
NEXTMV:
  PUSH DE                 ; Save the target coordinates temporarily
  CALL ONSTAIRS_0         ; Is the follower on a staircase?
  JR NC,NEXTMV_0          ; Jump if not
; The follower is on a staircase. At this point, A=0 if the staircase goes up
; and to the left, 128 (bit 7 set) if it goes up and to the right.
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  XOR (HL)                ; Compare the staircase's direction with the
                          ; follower's
  RLCA                    ; Set the carry flag if the character is descending
                          ; the staircase
  LD A,1                  ; A=1 if the follower is ascending the staircase, 2
  ADC A,0                 ; if descending
  POP DE                  ; Restore the target coordinates to DE
  SCF                     ; Signal: follower is on a staircase (this is ignored
                          ; by both callers)
  RET
; The follower is not on a staircase.
NEXTMV_0:
  CALL GETREGION          ; Get the region identifier for the follower's
                          ; current location
  LD L,A                  ; Copy this to L
  POP DE                  ; Restore the target coordinates to DE
  CALL GETREGION          ; Get the region identifier for the target
                          ; coordinates
  CP L                    ; Compare the current and target regions
  LD D,L                  ; D=region ID for the follower's current location
  LD L,1                  ; Point HL at byte 1 of the follower's buffer
  JR NZ,NEXTMV_5          ; Jump if the follower is not in the same region as
                          ; his target
  LD A,E                  ; A=target x-coordinate
NEXTMV_1:
  SUB (HL)                ; Is the follower in exactly the same location as his
                          ; target?
  RET Z                   ; Return if so (with the carry flag reset)
; The follower is in the same region as his target, but still has some walking
; to do. At this point the carry flag is set if the follower is to the right of
; his target, and reset if he's to the left.
  SBC A,A                 ; A=3 if the follower must go left, 4 if right to
  ADD A,4                 ; reach the target
  PUSH AF                 ; Save this value briefly
  LD L,0                  ; Point HL at byte 0 of the follower's buffer
  RRCA                    ; Set bit 7 of A if the follower is facing the right
  XOR (HL)                ; way to reach his target
  RLCA                    ; Push bit 7 of A into the carry flag
  JR C,NEXTMV_3           ; Jump if the follower is facing the right way to
                          ; reach his target
NEXTMV_2:
  POP AF                  ; A=3 (go left) or 4 (go right)
  AND A                   ; Clear the carry flag (the follower is not on a
                          ; staircase)
  RET
; The follower is in the same region as his target, and is facing the right way
; to reach him. But are there any doors in the way?
NEXTMV_3:
  CALL CHKDOOR            ; Check for closed doors in front of the follower
  JR NC,NEXTMV_2          ; Jump if there are none
  LD A,H                  ; A=follower's character number
  CP 210                  ; Is this ERIC (in demo mode)?
  JR NZ,NEXTMV_4          ; Jump if not
  POP AF                  ; A=3 (go left) or 4 (go right)
  XOR 7                   ; 3 (go left) becomes 4 (go right) and vice versa for
                          ; ERIC
  RET                     ; Return with the carry flag reset and C holding the
                          ; door identifier
NEXTMV_4:
  POP AF                  ; A=3 (go left) or 4 (go right)
  ADD A,8                 ; Set bit 3 (A=11, 12): closed door ahead
  RET                     ; Return with the carry flag reset and C holding the
                          ; door identifier
; The follower is not in the same region as his target. This means at least one
; visit to a staircase first.
NEXTMV_5:
  SUB 129                 ; A=identifier of the top or bottom of the staircase
  LD E,A                  ; (see GOTO)
  LD A,(DE)               ;
  LD D,A                  ; A=x-coordinate of the top or bottom of the
  LD E,68                 ; staircase the follower will have to go to first in
  LD A,(DE)               ; order to reach his target
  CP (HL)                 ; Is the follower standing at this staircase already?
  JR NZ,NEXTMV_1          ; Jump if not
; The follower is at the top or bottom of the first staircase he must negotiate
; in order to reach his target. Is he facing the right way to begin his ascent
; or descent?
  INC E                   ; Point DE at the staircase direction indicator
  DEC L                   ; Point HL at byte 0 of the character's buffer
  LD A,(DE)               ; A=0 if the follower must face left to negotiate the
                          ; stairs, 128 (bit 7 set) if right
  XOR (HL)                ; Set the carry flag if the follower is facing the
  RLCA                    ; wrong way
  JR NC,NEXTMV_6          ; Jump if the follower is facing the right way to
                          ; negotiate the stairs
  LD A,(HL)               ; A=follower's animatory state
  RLCA                    ; A=3 if the follower's facing right, 4 if facing
  SBC A,A                 ; left
  ADD A,4                 ;
  AND A                   ; Clear the carry flag (the follower is not on a
                          ; staircase)
  RET
; The follower is facing the right way to begin his ascent or descent of the
; staircase.
NEXTMV_6:
  INC E                   ; A=LSB of the routine to make the follower ascend
  LD A,(DE)               ; (56/ASCEND) or descend (77/DESCEND) the stairs
  AND 1                   ; A=1 if the follower must ascend the stairs, 2 if he
  INC A                   ; must descend
  RET                     ; Return with the carry flag reset

; Unused
  DEFS 4

; Make a teacher find ERIC
;
; Used by the routines at HERDERIC2, SEEKERIC, FINDTRUANT and FINDEXPEL.
; Computes the teacher's next move in the search for ERIC.
;
; H Teacher's character number (200-204)
FINDERIC:
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  BIT 0,(HL)              ; Is the teacher midstride?
  JR Z,FINDERIC_1         ; Jump if not
; This entry point is used by the routines at HERDERIC2, SEEKERIC and
; FINDTRUANT to make the teacher who his chasing ERIC finish his stride.
FINDERIC_0:
  CALL UPDATESRB          ; Update the SRB for the teacher's current animatory
                          ; state and location
  LD B,A                  ; Save the animatory state in B temporarily
  LD L,15                 ; Byte 15 holds the y-coordinate adjustment to make
  LD A,(HL)               ; as the teacher finishes this stride: 0 normally, 1
                          ; if descending a staircase (see later in this
                          ; routine)
  ADD A,D                 ; Make the appropriate y-coordinate adjustment
  LD D,A                  ;
  LD A,B                  ; Restore the animatory state to A
  JP WALK_1               ; Make the teacher finish his stride
; The teacher is not midstride. Is he close enough to ERIC to stop the chase?
FINDERIC_1:
  CALL ONSTAIRS_0         ; Is the teacher on a staircase?
  JR C,FINDERIC_6         ; Jump if so
  LD DE,(53761)           ; Pick up ERIC's coordinates in DE
  LD L,0                  ; Byte 0 holds the teacher's animatory state
  BIT 7,(HL)              ; Is the teacher facing left?
  LD L,1
  JR Z,FINDERIC_2         ; Jump if so
  LD A,E                  ; A=ERIC's x-coordinate
  SUB (HL)                ; Subtract that of the teacher
  JR FINDERIC_3
FINDERIC_2:
  LD A,(HL)               ; A=teacher's x-coordinate
  SUB E                   ; Subtract that of ERIC
FINDERIC_3:
  CP 4                    ; Is ERIC less than 4 horizontal spaces in front of
                          ; the teacher?
  JR NC,FINDERIC_5        ; Jump if not
  INC L                   ; L=2
  LD A,(HL)               ; A=teacher's y-coordinate
  SUB 4
  JR C,FINDERIC_4         ; Jump if the teacher's on the top floor
  CP D                    ; Is ERIC at least 4 vertical spaces above the
                          ; teacher?
  JR NC,FINDERIC_5        ; Jump if so
FINDERIC_4:
  ADD A,7                 ; Set the carry flag if ERIC is at least 4 vertical
  CP D                    ; spaces below the teacher
  CCF                     ; Return if ERIC is within 3 vertical spaces either
  RET C                   ; side of the teacher
; The teacher is not close enough to ERIC yet to stop chasing him. Now to
; figure out the teacher's next move.
FINDERIC_5:
  CALL GETERICY           ; D=ERIC's y-coordinate (or the y-coordinate of the
                          ; floor he's closest to if his feet are not on the
                          ; floor)
FINDERIC_6:
  CALL NEXTMV             ; Work out how the teacher can reach ERIC from here
  BIT 3,A                 ; Is there a closed door in the way?
  JP NZ,DOOROPEN_1        ; Open it if so
; There is no closed door in the way. Is the teacher on a staircase?
  LD BC,0                 ; Initialise the staircase indicator to 0
  CP 2                    ; Is the teacher descending a staircase?
  JR NZ,FINDERIC_7        ; Jump if not
  INC B                   ; B=1 (teacher is descending a staircase)
FINDERIC_7:
  JR NC,FINDERIC_8        ; Jump if the teacher is not ascending a staircase
  DEC C                   ; C=-1 (teacher is ascending a staircase)
FINDERIC_8:
  CP 3                    ; Is the teacher on a staircase?
  JR C,FINDERIC_11        ; Jump if so
  LD L,0                  ; Byte 0 holds the teacher's animatory state
  CP 4                    ; Set the carry flag if the teacher should go left
  BIT 7,(HL)              ; Set the zero flag if the teacher is facing left
  JR C,FINDERIC_10        ; Jump if the teacher must go left to reach ERIC
  JR NZ,FINDERIC_11       ; Jump if the teacher is facing right
FINDERIC_9:
  JP WALK_7               ; Turn the teacher round
FINDERIC_10:
  JR NZ,FINDERIC_9        ; Turn the teacher round if he's facing right
; Now BC is still 0 if the teacher is not on a staircase. Otherwise, B=1 and
; C=0 if he's descending a staircase, or B=0 and C=-1 if he's ascending a
; staircase. The value in B is the y-coordinate adjustment to make when the
; teacher finishes this stride later, and the value in C is the y-coordinate
; adjustment to make as he goes midstride now.
FINDERIC_11:
  LD L,15                 ; Set byte 15 of teacher's buffer to 1 if the teacher
  LD (HL),B               ; is descending a staircase, or 0 if he's ascending a
                          ; staircase or not on one
  PUSH BC
  CALL UPDATESRB          ; Update the SRB for the teacher's current animatory
                          ; state and location
  POP BC
  BIT 0,C                 ; Is the teacher ascending a staircase?
  JR Z,FINDERIC_12        ; Jump if not
  DEC D                   ; Up a stair as the teacher goes midstride
FINDERIC_12:
  INC A                   ; One foot forward (midstride)
  JP UPDATEAS             ; Update the teacher's animatory state and location
                          ; and update the SRB

; Unused
  DEFS 6

; Check whether a character is beside a chair
;
; Used by the routines at FINDSEAT2, STARTJPING and SIT2. The value returned in
; A is as follows:
;
; +-----+------------------------------------------------------+
; | A   | Meaning                                              |
; +-----+------------------------------------------------------+
; | 0   | Character is facing left and standing beside a chair |
; | 3   | Character must walk to the next chair to the left    |
; | 4   | Character must walk to the rightmost chair           |
; | 255 | Character is not in a classroom                      |
; +-----+------------------------------------------------------+
;
; H Character number (183-210)
BYSEAT:
  LD L,2                  ; Point HL at byte 2 of the character's buffer
  LD A,(HL)               ; A=character's y-coordinate
  LD DE,TFRMSMINX         ; The table at TFRMSMINX contains the x-coordinates
                          ; of the left ends of the classrooms
  LD B,3                  ; There are three sets of chairs on the top floor
  CP B                    ; Is the character on the top floor?
  JR Z,BYSEAT_0           ; Jump if so
  DEC B                   ; B=2 (two sets of chairs on the middle floor)
  CP 10                   ; Is the character on the middle floor?
  JR NZ,BYSEAT_3          ; Jump if not
  LD E,53                 ; DE=MFRMSMINX
BYSEAT_0:
  DEC L
  LD A,(HL)               ; A=character's x-coordinate
  EX DE,HL                ; HL=TFRMSMINX if the character is on the top floor,
                          ; MFRMSMINX if he's on the middle floor
BYSEAT_1:
  CP (HL)                 ; Is the character to the left of this classroom?
  JR C,BYSEAT_2           ; Jump if so
  INC H                   ; Point HL at the x-coordinate of the right end of
                          ; the classroom (in the table at TFRMSMAXX)
  CP (HL)                 ; Is the character in this classroom?
  JR C,BYSEAT_4           ; Jump if so
  DEC H
BYSEAT_2:
  INC L
  DJNZ BYSEAT_1
  EX DE,HL
BYSEAT_3:
  LD A,255                ; Signal: the character is not in a classroom
  RET
; The character is in a classroom. But is he beside a chair?
BYSEAT_4:
  INC H                   ; Point HL at the x-coordinate of the leftmost seat
                          ; in the classroom (in the table at SEATSMINX)
  CP (HL)                 ; Is the character to the right of the leftmost
                          ; chair?
  JR NC,BYSEAT_6          ; Jump if so
BYSEAT_5:
  LD A,4                  ; Signal: the character must walk to the rightmost
                          ; chair
  EX DE,HL
  RET
BYSEAT_6:
  INC H                   ; Point HL at the x-coordinate of the rightmost seat
                          ; in the classroom (in the table at SEATSMAXX)
  CP (HL)                 ; Is the character to the left of the rightmost
                          ; chair?
  JR C,BYSEAT_8           ; Jump if so
BYSEAT_7:
  LD A,3                  ; Signal: the character must walk to the next chair
                          ; to the left
  EX DE,HL
  RET
; The character is between the leftmost and rightmost chairs in the classroom.
BYSEAT_8:
  DEC E                   ; E=0
  LD A,(DE)               ; A=character's animatory state
  INC E                   ; E=1
  RLCA
  JR C,BYSEAT_5           ; Jump if the character is facing right
  LD A,(DE)               ; A=character's x-coordinate
  XOR (HL)                ; The chairs in a classroom have either all odd or
  RRCA                    ; all even x-coordinates; is the character standing
                          ; beside one?
  JR NC,BYSEAT_7          ; Jump if not
  EX DE,HL
  XOR A                   ; Signal: the character is facing left and standing
                          ; beside a chair
  RET
; The contents of the classroom/chairs location tables are as follows:
;
; +-----------+-----------+
; | Room      | Chairs    |
; +-----+-----+-----+-----+---------------------------------------+
; | 0   | 23  | 11  | 22  | Blue Room                             |
; | 40  | 63  | 50  | 61  | Yellow Room                           |
; | 159 | 180 | 168 | 179 | Top-floor room in the girls' skool    |
; | 30  | 54  | 40  | 53  | Science Lab                           |
; | 159 | 179 | 167 | 178 | Middle-floor room in the girls' skool |
; +-----+-----+-----+-----+---------------------------------------+

; Unused
  DEFS 2

; Check whether a chair is occupied
;
; Used by the routine at CLEARSEAT. Returns with the zero flag set if the chair
; beside the character looking for a seat is already occupied by one of the
; potential occupants being checked.
;
; B Number of potential occupants to check
; C Animatory state mask (135 or 143)
; D Number of the first potential occupant to check
; H Number of the character looking for a seat (183-209)
; L 1
CHECKSEAT:
  CALL CHECKSEAT_0        ; Are any of the first B potential occupants sitting
                          ; here?
  RET Z                   ; Return if so
  LD BC,655               ; B=2 (BOY WANDER and ANGELFACE), C=143
  LD D,206                ; 206=BOY WANDER
; This entry point also is used by the routine at CLEARSEAT.
CHECKSEAT_0:
  LD E,1
  LD A,(DE)               ; A=x-coordinate of the potential occupant
  CP (HL)                 ; Compare with that of the character looking for a
                          ; seat
  JR NZ,CHECKSEAT_1       ; Jump ahead to consider the next potential occupant
                          ; if they don't match
  INC E                   ; E=2
  INC L                   ; L=2
  LD A,(DE)               ; A=y-coordinate of the potential occupant
  DEC E                   ; E=1
  CP (HL)                 ; Compare with the y-coordinate of the character
                          ; looking for a seat
  DEC HL                  ; L=1
  JR NZ,CHECKSEAT_1       ; Jump if the potential occupant is not in the same
                          ; location as the character looking for a seat
  DEC E                   ; E=0
  LD A,(DE)               ; A=potential occupant's animatory state
  AND C                   ; Discard the character-identifying bits and keep
                          ; only the 'direction' bit and 'action' bits
                          ; (C=10001111 for BOY WANDER, ANGELFACE or ERIC, and
                          ; 10000111 for anybody else)
  CP 4                    ; Is the potential occupant sitting in the chair
                          ; here?
  JR NZ,CHECKSEAT_1       ; Jump if not
  LD E,18                 ; Is there an uninterruptible subcommand routine
  LD A,(DE)               ; address in bytes 17 and 18 of the potential
  AND A                   ; occupant's buffer?
  RET Z                   ; Return if not
CHECKSEAT_1:
  INC D                   ; Next potential occupant to test
  DJNZ CHECKSEAT_0        ; Jump back until done
  RET

; Knock the current occupant (if any) out of a chair
;
; Used by the routines at FINDSEAT2 and SIT2. Knocks the current occupant (if
; any) out of the chair next to the character looking for a seat.
;
; H Number of the character looking for a seat (183-209)
CLEARSEAT:
  LD D,183                ; 183=little girl no. 1
  LD L,1
  LD BC,4487              ; B=17 (7 girls, 10 boys), C=135 (animatory state
                          ; mask)
  CALL CHECKSEAT_0        ; Are any of these characters sitting in the chair?
  JR Z,CLEARSEAT_0        ; Jump if so
  LD BC,647               ; B=2 (EINSTEIN and HAYLEY), C=135 (animatory state
                          ; mask)
  LD D,208                ; 208=EINSTEIN
  CALL CHECKSEAT          ; Is one of the main kids sitting here?
  JR Z,CLEARSEAT_0        ; Jump if so
  LD DE,53778             ; D=210 (ERIC), E=18
  INC B                   ; B=1 (just ERIC)
  XOR A                   ; Zero out byte 18 of ERIC's buffer (to indicate that
  LD (DE),A               ; he may be pushed out of the seat if he's in it)
  CALL CHECKSEAT_0        ; Is ERIC sitting here?
  RET NZ                  ; Return if not
  LD DE,STATUS            ; HL=STATUS (ERIC's primary status flags)
  EX DE,HL                ;
  SET 7,(HL)              ; Set bit 7 of ERIC's primary status flags,
                          ; indicating that he's been knocked to the floor;
                          ; however, bit 7 is ignored by the routine at
                          ; HANDLEERIC (because bit 2 is also set), so ERIC
                          ; actually stays in his seat (which is a bug)
  EX DE,HL                ; Restore the number of the character looking for a
                          ; seat to H
  RET
; A character (other than ERIC) must be knocked out of the chair.
CLEARSEAT_0:
  LD A,102                ; Place the address of the uninterruptible subcommand
  LD (DE),A               ; routine at UNSEAT1 into bytes 17 and 18 of the
  DEC E                   ; buffer of the character who's been knocked out of
  LD A,112                ; the chair
  LD (DE),A               ;
  RET

; Unused
  DEFB 0

; Deal with a character who's been dethroned (1)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a character's buffer by the routine at CLEARSEAT when he's been
; knocked out of a chair.
;
; H Dethroned character's number (183-209)
UNSEAT1:
  LD L,19                 ; Set the parameter controlling the delay before the
  LD (HL),4               ; character gets up to look for another seat
  LD L,17                 ; Replace the address of this routine in bytes 17 and
  LD (HL),127             ; 18 of the character's buffer with UNSEAT2
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  INC A                   ; A=animatory state of character sitting on the floor
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Deal with a character who's been dethroned (2)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a character's buffer by the routine at UNSEAT1.
;
; H Dethroned character's number (183-209)
UNSEAT2:
  LD L,19                 ; Byte 19 of the character's buffer holds the delay
                          ; counter
  DEC (HL)                ; Is it time for the character to get up off the
                          ; floor yet?
  RET NZ                  ; Return if not
  LD L,17                 ; Replace the address of this routine in bytes 17 and
  LD (HL),151             ; 18 of the character's buffer with FINDSEAT2
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  AND 248                 ; A=animatory state of the character standing up
  LD C,A                  ; Store this in C temporarily
  LD A,H                  ; A=number of the character knocked out of the chair
  CP 208                  ; Is this EINSTEIN?
  JR Z,UNSEAT2_0          ; Jump if so
  DEC E                   ; E=x-coordinate of the character upon standing up
                          ; (everyone but EINSTEIN will get up and try the seat
                          ; in front)
UNSEAT2_0:
  LD A,C                  ; A=animatory state of the character standing up
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB

; Deal with a character who is looking for a seat
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a character's buffer by the routine at UNSEAT2 (after he's been
; knocked out of his chair and just got up to look for another one - or the
; same one, in EINSTEIN's case), or by the routine at FINDSEAT1.
;
; H Character number (183-209)
FINDSEAT2:
  LD L,0                  ; Byte 0 holds the character's animatory state
  BIT 0,(HL)              ; Is the character midstride?
  JP NZ,WALK_0            ; Finish the stride if so
  CALL BYSEAT             ; Is the character standing beside a chair?
  AND A                   ;
  JR NZ,FINDSEAT2_0       ; Jump if not
  CALL CLEARSEAT          ; Knock anybody who's sitting here out of the way
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  AND 248
  ADD A,4                 ; A=animatory state of the character (as if sitting
                          ; in a chair)
  LD L,18                 ; Remove the address of this routine from bytes 17
  LD (HL),0               ; and 18 of the character's buffer
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB
FINDSEAT2_0:
  CP 4                    ; A=0 if the character must walk back to the
  SBC A,A                 ; rightmost chair in the room, or 255 if there is one
                          ; in front of him
  LD L,0                  ; Byte 0 holds the character's animatory state
  XOR (HL)                ; Reset the carry flag if the character must turn
  RLCA                    ; round
  JP WALK_6               ; Update the character's animatory state and update
                          ; the SRB

; Unused
  DEFB 0

; Make a character find a seat
;
; Used by command lists 0, 2, 4, 6, 8, 32, 34, 46, 48, 50, 52, 82, 84 and 86.
; Makes a character start looking for a seat.
;
; H Character number (183-209)
FINDSEAT1:
  LD L,4                  ; Remove the address of this primary command routine
  LD (HL),0               ; from bytes 3 and 4 of the character's buffer
  LD L,18                 ; Place the address of the uninterruptible subcommand
  LD (HL),102             ; routine at FINDSEAT2 (make character look for a
  DEC L                   ; seat) into bytes 17 and 18 of the character's
  LD (HL),151             ; buffer
  JP FINDSEAT2            ; Start looking for a seat

; Get the next character of a message being spoken or written
;
; Used by the routines at WRITECHR, SPEAK and TXT2BUF. Returns with the next
; character in A.
;
; H Number of the character speaking or writing (183-209)
NEXTCHR:
  LD L,16                 ; Bytes 15 and 16 hold the address of the next
  LD A,(HL)               ; character in the sub-submessage being
                          ; written/spoken (if any)
  AND A                   ; Are we working on a sub-submessage?
  JR NZ,NEXTCHR_0         ; Jump if so
  LD L,14                 ; Bytes 13 and 14 hold the address of the next
  LD A,(HL)               ; character in the submessage being written/spoken
                          ; (if any)
  AND A                   ; Are we working on a submessage?
  JR NZ,NEXTCHR_0         ; Jump if so
  LD L,12                 ; We're working on a top-level message
NEXTCHR_0:
  LD D,(HL)               ; DE=address of the next character in the
  DEC L                   ; (sub)(sub)message
  LD E,(HL)               ;
  LD A,(DE)               ; Pick up the character code in A
  INC DE                  ; Move the pointer to the next character and store
  LD (HL),E               ; the address for future reference
  INC L                   ;
  LD (HL),D               ;
  AND A                   ; Has the end of the (sub)(sub)message been reached?
  JR NZ,NEXTCHR_1         ; Jump if not
; We've reached the end of the (sub)(sub)message.
  LD (HL),0               ; Signal: end of (sub)(sub)message
  DEC L                   ; L=11 (top-level message), 13 (submessage) or 15
                          ; (sub-submessage)
  BIT 2,L                 ; Has the end of the top-level message been reached
                          ; (L=11)?
  RET Z                   ; Return if so
  DEC L                   ; Otherwise go up to the submessage or the top-level
  JR NEXTCHR_0            ; message and jump back to deal with it
; We haven't reached the end of the (sub)(sub)message yet. Determine whether
; the next code is a regular character code or a pointer to another message.
NEXTCHR_1:
  SUB 32
  CP 96
  JR NC,NEXTCHR_2         ; Jump with character codes 1-31 or 128 onwards
  ADD A,32
  RET                     ; Return with the standard ASCII code in A
NEXTCHR_2:
  ADD A,32
  CP 3
  RET C                   ; Return with character codes 1 and 2 (end of line)
; The next character in the message is actually a pointer to another message.
; Find the address of that message, and store it for future reference.
  LD E,A                  ; DE points to the LSB of the start address of the
  LD D,254                ; (sub)submessage
  INC L                   ; L=13 (submessage) or 15 (sub-submessage)
  LD A,(DE)               ; A=LSB of the start address of the (sub)submessage
  LD (HL),A               ; Store it in byte 13 or 15 of the character's buffer
  INC L                   ; L=14 (submessage) or 16 (sub-submessage)
  INC D                   ; A=MSB of the address of the (sub)submessage
  LD A,(DE)               ;
  LD (HL),A               ; Store it in byte 14 or 16 of the character's buffer
  JR NEXTCHR_0            ; Jump back to collect a character from this
                          ; (sub)submessage

; Unused
  DEFS 2

; Update the SRB for a blackboard
;
; Used by the routines at WRITECHR and WIPE. Updates the screen refresh buffer
; (SRB) so that the character squares at (E,D) and (E+1,D) are marked dirty.
; Two character squares in a row are marked dirty to ensure that blackboard
; contents are properly displayed (characters written on a blackboard may cross
; character square boundaries).
;
; D y-coordinate of the play area
; E x-coordinate of the play area
SRBXY:
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  SUB 2
  JR C,SRBXY_0            ; Jump if the far left wall of the boys' skool is
                          ; on-screen
  CP E                    ; Is the leftmost point requiring refresh off-screen
                          ; to the left by at least two character squares?
  RET NC                  ; Return if so (nothing to do)
SRBXY_0:
  ADD A,33                ; A=column of the play area at the far right of the
                          ; screen
  CP E                    ; Is the leftmost point requiring refresh off-screen
                          ; to the right?
  RET C                   ; Return if so (nothing to do)
  PUSH DE                 ; Save the coordinates for now
  SUB 32                  ; A=screen x-coordinate corresponding to the play
  CPL                     ; area x-coordinate in E (-1, 0-31)
  ADD A,E                 ;
  BIT 7,A                 ; Is the leftmost point requiring refresh on-screen?
  JR Z,SRBXY_1            ; Jump if so
  INC A                   ; A=0
SRBXY_1:
  RLCA                    ; A=8x (x=column of the screen containing the first
  RLCA                    ; character square to be refreshed)
  RLCA                    ;
  LD E,A                  ; Store this in E temporarily
  AND 56                  ; Set the instruction at BOARDSRB1 below to SET
  CPL                     ; n,(HL) where n is the appropriate bit in the SRB
  DEC A                   ; byte
  LD (26429),A            ;
  PUSH AF
  LD A,E                  ; A=8x again
  RLCA                    ; Point HL at the appropriate byte of the SRB
  RLCA                    ;
  AND 3                   ;
  ADD A,D                 ;
  ADD A,D                 ;
  ADD A,D                 ;
  ADD A,D                 ;
  LD E,A                  ;
  LD D,127                ;
  EX DE,HL                ;
BOARDSRB1:
  SET 0,(HL)              ; Set the appropriate bit in the SRB byte for the
                          ; leftmost character square that needs refreshing;
                          ; this instruction is modified earlier in this
                          ; routine to set the required bit
  POP AF
  ADD A,56
  JR C,SRBXY_2            ; Jump unless we've just set bit 0 of an SRB byte
  INC L                   ; Next SRB byte
  LD A,L                  ; Does this SRB byte correspond to the first byte of
  AND 3                   ; the next row of the screen?
  JR Z,SRBXY_3            ; If so, skip it
  LD A,254                ; Otherwise set bit 7 in this SRB byte
SRBXY_2:
  OR 192
  LD (26449),A            ; Set the instruction at BOARDSRB2 below to SET
                          ; m,(HL) where m is the appropriate bit in the SRB
                          ; byte
BOARDSRB2:
  SET 0,(HL)              ; Set the appropriate bit in the SRB byte for the
                          ; rightmost character square that needs refreshing;
                          ; this instruction is modified immediately before
                          ; execution to set the required bit
SRBXY_3:
  EX DE,HL                ; Restore the original contents of HL
  POP DE                  ; Restore the play area coordinates to DE
  RET

; Get the identifier and coordinates of the blackboard close to a character
;
; Used by the routines at WRITE, WRITECHR, WRITEBRD, BOARDID2 and DOCLASS. If
; the character is not standing at a blackboard, the routine returns with the
; carry flag set. Otherwise the routine returns with the carry flag reset, B
; holding the blackboard identifier (the LSB of BRBRDBUF, YRBRDBUF, TFBRDBUF,
; SLBRDBUF or MFBRDBUF), and DE holding the coordinates of the top left-hand
; square of the board.
;
; H Character number (200-204, 206)
BOARDID:
  LD B,3                  ; There are three blackboards on the top floors
  LD DE,BBSLEFTXT         ; The table at BBSLEFTXT contains the x-coordinates
                          ; of the left edges of the blackboards
  LD L,2                  ; Byte 2 of the character's buffer holds his
                          ; y-coordinate
  LD A,(HL)               ; Pick this up in A
  DEC L                   ; L=1
  CP B                    ; Is the character on the top floor?
  JR Z,BOARDID_0          ; Jump if so
  LD E,58                 ; DE=BBSLEFTXM
  DEC B                   ; B=2 (two blackboards on the middle floors)
  CP 10                   ; Is the character on the middle floor?
  JR NZ,BOARDID_1         ; Jump if not
BOARDID_0:
  LD A,(DE)               ; A=x-coordinate of the left edge of a blackboard
  CP (HL)                 ; Is the character to the left of this blackboard?
  JR NC,BOARDID_1         ; Jump if so
BOARDDIST:
  ADD A,4                 ; Is the character no more than 4 (or 12, if coming
  CP (HL)                 ; from BOARDID2) spaces to the right of the left edge
                          ; of this blackboard?
  JR NC,BOARDID_2         ; Jump if so
  INC E                   ; Next blackboard
  DJNZ BOARDID_0
BOARDID_1:
  SCF                     ; Signal: the character is not standing at a
                          ; blackboard
  RET
; The character is standing close enough to the blackboard (to either write on
; it or start wiping it).
BOARDID_2:
  EX DE,HL                ; Now HL points to the blackboard info table
  LD A,(HL)               ; A=x-coordinate of the left edge of the blackboard
                          ; (from the table at BBSLEFTXT)
  INC H
  LD B,(HL)               ; B=blackboard identifier (from the table at
                          ; BOARDIDS)
  INC H
  LD H,(HL)               ; H=y-coordinate of the top row of the blackboard
                          ; (from the table at BBSY)
  LD L,A                  ; Pass the x-coordinate to L
  AND A                   ; Clear the carry flag to indicate success
  EX DE,HL                ; DE=coordinates of the top-left corner of the
                          ; blackboard
  RET
; The contents of the blackboard info tables are as follows:
;
; +-----+-----+---+---------------------------------------+
; | x   | ID  | y | Room                                  |
; +-----+-----+---+---------------------------------------+
; | 3   | 84  | 3 | Blue Room                             |
; | 41  | 90  | 3 | Yellow Room                           |
; | 161 | 96  | 3 | Top-floor room in the girls' skool    |
; | 32  | 102 | 9 | Science Lab                           |
; | 161 | 108 | 9 | Middle-floor room in the girls' skool |
; +-----+-----+---+---------------------------------------+

; Move the bike if it's obstructing the boys' skool door or the gate
;
; Used by the routine at MOVEDOOR.
;
; A Bike's animatory state
; DE (96,17) or (136,17)
MVBIKE:
  AND 5                   ; Is the bike lying on the floor?
  JR NZ,MVBIKE_0          ; Jump if not
  LD H,211                ; 211=bike
  PUSH DE                 ; Save the bike's new location temporarily
  CALL UPDATESRB          ; Update the SRB for the bike's current animatory
                          ; state and location
  POP DE                  ; Restore the bike's new location to DE
  JP UPDATEAS             ; Update the bike's location and update the SRB
; The bike is not lying on the floor, so either ERIC is riding it, or it's
; travelling of its own accord. In this case, ALBERT must wait for it to pass
; before closing the skool door or gate.
MVBIKE_0:
  POP BC                  ; Drop the return address, which has the effect of
                          ; making ALBERT wait
  RET

; Unused
  DEFB 0

; Write a single character on a blackboard
;
; Used by the routine at WRITEBRD. Returns with the zero flag set if the end of
; the message has been reached.
;
; H Number of the character writing on the board (200-204, 206)
WRITECHR:
  CALL NEXTCHR            ; A=ASCII code of the next character from the message
                          ; being written
; This entry point is used by the routine at WRITING when ERIC's writing on a
; board.
WRITECHR_0:
  LD C,A                  ; C=ASCII code of the next character from the message
                          ; being written
  CALL BOARDID            ; Collect information about the blackboard
  LD A,C
  LD C,B                  ; Point BC at the blackboard's buffer (BRBRDBUF,
  LD B,127                ; YRBRDBUF, TFBRDBUF, SLBRDBUF or MFBRDBUF)
  AND A                   ; Have we reached the end of the message?
  JR NZ,WRITECHR_2        ; Jump if not
  DEC A                   ; A=255
WRITECHR_1:
  LD (BC),A               ; Set the first byte of the blackboard's buffer to
                          ; 255 if the message is finished, or the the pixel
                          ; column number for the next letter to be written
                          ; otherwise
  INC A                   ; Set the zero flag if the end of the message has
                          ; been reached
  RET
; We haven't reached the end of the message yet. Examine the next character to
; be written.
WRITECHR_2:
  CP 2                    ; Character code 2 is newline
  JR NZ,WRITECHR_3        ; Jump unless we're starting a new line
  LD A,64                 ; This is the pixel column number of the start of the
                          ; bottom line of the blackboard
  JR WRITECHR_1
; The next character to be written is printable (as opposed to newline).
WRITECHR_3:
  PUSH HL                 ; Save the character number (in H) for now
  LD L,A                  ; Place the ASCII code of the character to be written
                          ; in L
  LD H,215                ; Now (HL)=bit width of this character
  LD A,(BC)               ; A=current pixel column number on the blackboard
  AND 63                  ; Set the carry flag if there's not enough room on
  ADD A,192               ; the current line to print the next character (plus
  SCF                     ; one blank pixel column)
  ADC A,(HL)              ;
  LD A,(BC)               ; A=current pixel column number on the blackboard
  JR NC,WRITECHR_4        ; Jump if there's enough room on the current line for
                          ; the character
  CPL                     ; Otherwise move to the other line of the blackboard
  AND 64                  ; by setting the current pixel column number to 0 or
  LD (BC),A               ; 64 as appropriate
WRITECHR_4:
  RRCA                    ; Divide the pixel column number by 8 to get the
  RRCA                    ; corresponding character square number (0-7); also
  RRCA                    ; keep bit 3 for now to check which line of the board
  AND 15                  ; we're on
  CP 8                    ; Are we on the top line of the board?
  JR C,WRITECHR_5         ; Jump if so (A=0-7)
  INC D                   ; D=4 or 9 (y-coordinate of the bottom line of the
                          ; board)
  AND 7                   ; A=0-7
WRITECHR_5:
  ADD A,E                 ; E=x-coordinate of the blackboard tile on which the
  LD E,A                  ; next character will be written
  CALL SRBXY              ; Update the SRB for this blackboard
  LD A,(BC)               ; A=current pixel column number on the blackboard
  AND 7                   ; Modify the RES n,(HL) instruction at CHALK below to
  ADD A,A                 ; reset the appropriate bit for the current pixel
  ADD A,A                 ; column
  ADD A,A                 ;
  CPL                     ;
  SUB 65                  ;
  LD (26610),A            ;
  LD A,(BC)               ; A=current pixel column number on the blackboard
  ADD A,(HL)              ; Add the bit-width of character to be written
  INC A                   ; Add 1 to make a blank vertical pixel line after the
                          ; character to be written
  LD (BC),A               ; Set the pixel column number for the next character
                          ; to be written
  LD C,(HL)               ; C=bit-width of the character to be written
  EX DE,HL                ; Now DE points at the bitmap data for the character
                          ; to be written on the board, and HL holds the
                          ; coordinates of the blackboard tile on which the
                          ; character will be written
  LD A,H                  ; A=blackboard tile y-coordinate
  LD H,181                ; Pick up the Q value (0<=Q<=143) for the blackboard
  LD L,(HL)               ; tile from page 181 in L (see PRINTTILE)
  ADD A,160               ; Collect the LSB of the base address of the skool
  LD H,A                  ; UDG for the blackboard tile in L
  LD L,(HL)               ;
WRITECHR_6:
  LD H,128                ; The base page for all blackboard tile UDGs is 128
  INC D                   ; Point DE at the next pixel column of bitmap data
                          ; for the character to be written
  LD A,(DE)               ; Pick this up in A
  LD B,8                  ; 8 horizontal pixel lines in each character square
WRITECHR_7:
  RLCA                    ; Should a bit of chalk appear here?
  JR NC,WRITECHR_8        ; Jump if not
CHALK:
  RES 7,(HL)              ; Reset the appropriate bit in the skool UDG for the
                          ; blackboard tile, thus making a bit of chalk appear;
                          ; this instruction is modified by this routine to
                          ; reset the required bit
WRITECHR_8:
  INC H                   ; Next row of the blackboard tile UDG
  DJNZ WRITECHR_7         ; Jump back until all 8 rows are done
  LD A,(26610)            ; Change the RES n,(HL) instruction at CHALK above to
  OR 64                   ; RES n-1,(HL) if n>0, or RES 7,(HL) if n=0
  SUB 8                   ;
  AND 191                 ;
  LD (26610),A            ;
  CP 190                  ; Did we just draw the rightmost vertical pixel line
                          ; in the current blackboard tile?
  JR NZ,WRITECHR_9        ; Jump if so
  INC L                   ; Set L to the LSB of the base address of the skool
                          ; UDG for the next blackboard tile to the right
WRITECHR_9:
  DEC C                   ; Next pixel column of the character bitmap
  JR NZ,WRITECHR_6        ; Jump back to do the remaining pixel columns
  INC C                   ; Reset the zero flag (the message is not finished
                          ; yet)
  POP HL                  ; Restore the character number to H
  RET

; Unused
  DEFS 2

; Make a teacher wipe a blackboard
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of the teacher's buffer by the routine at DOCLASS.
;
; H Teacher's character number (201-204)
WIPE:
  CALL BOARDID2           ; Collect information about the board
  LD L,11                 ; 32 distinct actions are needed to wipe the board;
  LD (HL),32              ; store this counter in byte 11 of the teacher's
                          ; buffer
  INC L                   ; L=12
  LD A,E                  ; A=x-coordinate of the left edge of the board
  ADD A,7                 ; A=x-coordinate of the right edge of the board
  LD (HL),A               ; Store this in byte 12 of the teacher's buffer
  INC L                   ; Place the y-coordinate of the top line of the board
  LD (HL),D               ; into byte 13 of the teacher's buffer
  INC L                   ; And the board identifier (LSB of BRBRDBUF,
  LD (HL),B               ; YRBRDBUF, TFBRDBUF, SLBRDBUF or MFBRDBUF) into byte
                          ; 14
  LD L,9                  ; Change the routine address in bytes 9 and 10 of the
  LD (HL),35              ; teacher's buffer to WIPE0 (below)
WIPE0:
  LD L,11                 ; Byte 11 holds the board-wiping action counter
  DEC (HL)                ; Decrement the counter
  JP P,WIPE_0             ; Jump if the board has not yet been fully wiped
; The teacher has finished wiping the board. Mark the board as clean, and move
; the teacher on to his next task.
  LD L,14                 ; Collect the board identifier in L
  LD L,(HL)               ;
  LD H,127                ; HL now points to the board's buffer
  LD (HL),0               ; Signal that the board has been wiped by zeroing the
  INC L                   ; first two bytes of the blackboard's buffer
  LD (HL),0               ;
  JP RMSUBCMD             ; Now the job's been done, terminate this
                          ; interruptible subcommand
; The board's not clean yet. Work out what the teacher has to do next: wipe, or
; move down the board to the next spot.
WIPE_0:
  CALL UPDATESRB          ; Update the SRB for the teacher's current animatory
                          ; state and location
  AND 120                 ; A=teacher's base animatory state
  LD L,11                 ; Point HL at the board-wiping action counter
  BIT 0,(HL)              ; Is the teacher midstride or wiping (arm up)?
  JR Z,WIPE_3             ; Jump if so
  BIT 1,(HL)              ; Are we ready for a wipe action?
  JR Z,WIPE_1             ; Jump if so
  INC A                   ; A=animatory state of the teacher midstride
  JP UPDATEAS             ; Update the teacher's animatory state and update the
                          ; SRB
; The teacher's next action will be a wipe, so raise his arm.
WIPE_1:
  ADD A,7                 ; A=animatory state of the teacher with his arm up
  CALL UPDATEAS           ; Update the teacher's animatory state and update the
                          ; SRB
  LD L,12                 ; Collect the x-coordinate of the part of the board
  LD E,(HL)               ; to be wiped into E
  DEC (HL)                ; Decrease this x-coordinate ready for the next wipe
  INC L                   ; Collect the y-coordinate of the top line of the
  LD D,(HL)               ; blackboard into D
  CALL SRBXY              ; Update the SRB for the top line of the blackboard
  INC D                   ; D=y-coordinate of the bottom line of the blackboard
  CALL SRBXY              ; Update the SRB for the bottom line of the
                          ; blackboard
  LD A,D                  ; Prepare H for picking up skool UDG base address
  ADD A,160               ; LSBs later
  LD H,A                  ;
  LD D,181                ; Pick up the Q value (0<=Q<=143) for the lower
  LD A,(DE)               ; blackboard tile being wiped from page 181 in L
  LD L,A                  ;
  LD A,(HL)               ; Collect the LSB of the base address of the skool
  LD L,A                  ; UDG for this blackboard tile in L
  SUB 8                   ; Set E to the LSB of the base address of the skool
  LD E,A                  ; UDG for the upper blackboard tile
  LD H,128                ; The base page for all blackboard tile UDGs is 128
  LD D,H                  ;
  LD B,8                  ; 8 lines per character square
  LD A,255                ; 255=blank blackboard byte
WIPE_2:
  LD (HL),A               ; Clear a pixel line in the skool UDG for the lower
                          ; blackboard tile
  LD (DE),A               ; Clear a pixel line in the skool UDG for the upper
                          ; blackboard tile
  INC H                   ; Next pixel row of the lower blackboard tile UDG
  INC D                   ; Next pixel row of the upper blackboard tile UDG
  DJNZ WIPE_2             ; Jump back until the two blackboard tile UDGs have
                          ; been cleared
  RET
; The teacher is either wiping or midstride while on his way to the next spot
; along the board. At this point, A holds the teacher's base animatory state.
WIPE_3:
  BIT 1,(HL)              ; Is the teacher wiping?
  JR Z,WIPE_4             ; Jump if so
  DEC E                   ; The teacher is midstride, so now move him one space
                          ; to the left
WIPE_4:
  JP UPDATEAS             ; Update the teacher's animatory state and location
                          ; and update the SRB

; Unused
  DEFS 2

; Make a character write on a blackboard
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of the character's buffer by the routines at BWWRITE and DOCLASS.
;
; H Character number (201-204, 206)
WRITEBRD:
  CALL BOARDID            ; Collect information about the board
  INC B                   ; Point BC at the second byte of the board's buffer
  LD C,B                  ;
  LD B,127                ;
  LD A,(BC)               ; Pick this up in A
  AND A                   ; Has the board been written on already?
  JP NZ,RMSUBCMD          ; Terminate this interruptible subcommand if so
  LD A,H                  ; A=character's number
  LD (BC),A               ; Signal: this character wrote on the board
  DEC C                   ; Point BC at the first byte of the board's buffer
  LD A,1                  ; (BRBRDBUF, YRBRDBUF, TFBRDBUF, SLBRDBUF or
  LD (BC),A               ; MFBRDBUF) and initialise the pixel column to 1
  LD L,16                 ; Make sure there are no (sub)submessage addresses
  LD (HL),0               ; lying around in bytes 13-16 of the character's
  LD L,14                 ; buffer
  LD (HL),0               ;
  LD A,H                  ; A=character's number (201-204, 206)
  CP 206                  ; Set E as follows: 32=MR WITHIT, 38=MR ROCKITT,
  ADC A,59                ; 44=MR CREAK, 50=MISS TAKE, 56=BOY WANDER
  LD E,A                  ;
  ADC A,A                 ;
  ADD A,E                 ;
  ADD A,A                 ;
  LD E,A                  ;
WRITEBRD_0:
  CALL GETRANDOM          ; Collect a random number between 0 and 5 in A
  AND 7                   ;
  CP 6                    ;
  JR NC,WRITEBRD_0        ;
  ADD A,E                 ; Now E=blackboard message number
  LD E,A                  ;
  LD D,254                ; Pick up the address of the message and place it
  LD L,11                 ; into bytes 11 and 12 of the character's buffer
  LD A,(DE)               ;
  LD (HL),A               ;
  INC D                   ;
  INC L                   ;
  LD A,(DE)               ;
  LD (HL),A               ;
  LD L,9                  ; Replace the address of this routine in bytes 9 and
  LD (HL),189             ; 10 of the character's buffer with WRITEBRD0 (below)
WRITEBRD0:
  CALL WRITECHR           ; Write one letter on the board
  JR NZ,WRITEBRD_2        ; Jump unless the character has finished writing
; This entry point is used by the routine at INASSEMBLY.
WRITEBRD_1:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  AND 248                 ; A=base animatory state of the character (arm down)
  CALL UPDATEAS           ; Update the character's animatory state and update
                          ; the SRB
  JP RMSUBCMD             ; Terminate this interruptible subcommand
; The character hasn't finished writing yet. Figure out his next arm movement
; (up or down).
WRITEBRD_2:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  RRCA                    ; Set the carry flag if the character's arm is raised
  RLCA                    ;
  JR C,WRITEBRD_4         ; Jump if the character's arm is raised
  OR 7                    ; A=animatory state of the character with his arm up
WRITEBRD_3:
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB
WRITEBRD_4:
  AND 248                 ; A=base animatory state of character (arm down)
  JR WRITEBRD_3           ; Jump back to update the SRB

; Unused
  DEFS 4

; Update the SRB so that the speech bubble is not corrupted
;
; Used by the routines at UPDATESCR, RMBUBBLE and SPEAK. Resets the bits in the
; screen refresh buffer (SRB) that correspond to the speech bubble and lip, so
; that they are not overwritten by sprite tiles when the display is updated.
; Returns with the carry flag set if the speech bubble is on-screen, and with
; HL pointing at the first byte of the SRB.
BUBBLESRB:
  LD HL,LIPSRBLSB         ; LIPSRBLSB holds the LSB of the SRB address
  LD A,(HL)               ; corresponding to the lip of the speech bubble (or 0
                          ; if there is no bubble on-screen)
  LD L,A                  ; L=0 (and HL=SRB) if no one is speaking
  AND A                   ; Is anyone speaking at the moment?
  RET Z                   ; Return if not
  LD L,255                ; HL=LEFTCOL
  LD A,(HL)               ; A=leftmost column of the play area on screen
  LD L,250                ; HL=SBLEFTCOL, which holds the column of the play
                          ; area that was at the far left of the screen the
                          ; last time this routine was called
  CP (HL)                 ; Set the zero flag if the screen hasn't scrolled,
                          ; set the carry flag if the screen has scrolled
                          ; right, or reset the carry flag if it has scrolled
                          ; left
  LD (HL),A               ; Update SBLEFTCOL to the current leftmost column
                          ; on-screen
  LD L,248                ; HL=LIPSRBLSB
  JR Z,BUBBLESRB_3        ; Jump if the screen hasn't scrolled since the last
                          ; time this routine was called
; The screen has scrolled since the last time this routine was called. Check
; which way it scrolled and update LIPSRBLSB accordingly.
  LD A,(HL)               ; A=LSB of the speech bubble lip SRB address
  JR NC,BUBBLESRB_2       ; Jump if the screen has scrolled left
; The screen has scrolled right since the last time this routine was called.
  AND 3                   ; A=0-3 (quarter of the screen occupied by the bubble
                          ; before the scroll)
  CP 3                    ; Was the bubble in the rightmost quarter?
  JR NZ,BUBBLESRB_1       ; Jump if not
; The speech bubble has been scrolled off the screen.
BUBBLESRB_0:
  LD (HL),0               ; Set LIPSRBLSB to 0 (the bubble is no longer
                          ; on-screen)
  LD L,(HL)               ; L=0
  RET                     ; Return with the carry flag reset
BUBBLESRB_1:
  INC (HL)                ; The speech bubble has been scrolled to the right
                          ; (and is still on-screen)
  JR BUBBLESRB_3
; The screen has scrolled left since the last time this routine was called.
BUBBLESRB_2:
  AND 3                   ; A=0-3 (quarter of the screen occupied by the bubble
                          ; before the scroll)
  JR Z,BUBBLESRB_0        ; Jump if the bubble was in the leftmost quarter (and
                          ; is therefore no longer on-screen)
  DEC (HL)                ; The speech bubble has been scrolled to the left
                          ; (and is still on-screen)
; Now that LIPSRBLSB has been adjusted to compensate for any recent scrolling
; of the screen, adjust the appropriate SRB bytes for the speech bubble and
; lip.
BUBBLESRB_3:
  LD L,(HL)               ; HL=address of the SRB byte corresponding to the lip
                          ; of the speech bubble
  LD A,(LIPSRBBIT)        ; The bit set in A corresponds to the bit of the SRB
                          ; byte that corresponds to the lip of the speech
                          ; bubble
  CPL
  AND (HL)                ; Make sure this bit is reset
  LD (HL),A               ; Restore the SRB byte with the relevant bit reset
  LD A,L
  ADD A,252               ; Reset the bits of the SRB corresponding to the
  LD L,A                  ; bottom line of the speech bubble
  LD (HL),0               ;
  ADD A,252               ; Reset the bits of the SRB corresponding to the top
  LD L,A                  ; line of the speech bubble
  LD (HL),0               ;
  LD L,(HL)               ; L=0
  SCF                     ; Signal: the speech bubble is still on-screen
  RET

; Unused
  DEFB 0

; Remove the speech bubble
;
; Used by the routine at SPEAK. Sets the bits in the screen refresh buffer
; (SRB) that correspond to the the speech bubble and lip, so that they are
; removed (replaced by play area tiles) next time the display is updated.
RMBUBBLE:
  PUSH HL
  CALL BUBBLESRB          ; Reset the bits in the SRB corresponding to speech
                          ; bubble
  LD L,248                ; HL=LIPSRBLSB, which holds the LSB of the SRB
  LD A,(HL)               ; address corresponding to the lip of the speech
                          ; bubble (if any); pick this up in A
  AND A                   ; Is the speech bubble on-screen?
  JR Z,RMBUBBLE_0         ; Jump if not
  LD (HL),0               ; Signal: no one's talking now
  LD L,A                  ; HL points to the SRB byte corresponding to the lip
                          ; of the speech bubble
  LD A,(LIPSRBBIT)        ; The bit set in A is the bit of that SRB byte that
                          ; corresponds to the lip's location
  OR (HL)                 ; Update the speech bubble lip SRB byte with this bit
  LD (HL),A               ; set (forcing a refresh of the screen where the lip
                          ; is)
  LD A,L                  ; Update the SRB for where the bottom line of the
  ADD A,252               ; speech bubble is
  LD L,A                  ;
  LD (HL),255             ;
  ADD A,252               ; Update the SRB for where the top line of the speech
  LD L,A                  ; bubble is
  LD (HL),255             ;
RMBUBBLE_0:
  POP HL
  RET

; Print a speech bubble UDG
;
; Used by the routine at SHOWBUBBLE.
;
; DE UDG address
; HL Attribute file address
BUBBLEUDG:
  PUSH HL                 ; Save the attribute file address temporarily
  LD (HL),56              ; INK 0: PAPER 7
  LD A,H                  ; Set HL to the corresponding display file address
  AND 11                  ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  LD H,A                  ;
  EX DE,HL
  CALL PRINTTILE_13       ; Copy the speech bubble UDG onto the screen
  EX DE,HL
  POP HL                  ; Restore the attribute file address to HL
  RET

; Print the speech bubble
;
; Used by the routine at SPEAK. Returns with the carry flag set if the
; character about to speak is off-screen.
;
; H Number of the character about to speak (200-204, 208)
SHOWBUBBLE:
  LD A,(LEFTCOL)          ; B=leftmost column of the play area on screen
  LD B,A                  ;
  LD L,1                  ; Byte 1 holds the character's x-coordinate
  LD A,(HL)               ; A=x-coordinate of the character's head (and
  INC A                   ; therefore of the speech bubble lip when it's
                          ; printed)
  SUB B                   ; Return with the carry flag set if the character's
  RET C                   ; head is off-screen to the left
  CP 32                   ; Return with the carry flag set if the character's
  CCF                     ; head is off-screen to the right
  RET C                   ;
  LD C,A                  ; 0<=C<=31 (screen x-coordinate of the character's
                          ; head)
  AND 7                   ; Point DE at an entry in the 8-byte table at
  ADD A,120               ; SRBBITS, whose contents are (128, 64, 32, 16, 8, 4,
  LD E,A                  ; 2, 1)
  LD D,225                ;
  LD A,(DE)               ; The bit set in A corresponds to the bit that needs
  LD (LIPSRBBIT),A        ; to be set in the SRB byte for the lip of the speech
                          ; bubble; store it in LIPSRBBIT
  LD A,C                  ; A=screen column for lip of speech bubble (0-31)
  RRCA                    ; Set E to 0, 1, 2 or 3 (quarter of the screen in
  RRCA                    ; which the bubble will be printed)
  RRCA                    ;
  AND 3                   ;
  LD E,A                  ;
  LD A,H                  ; A=number of the character about to speak
  INC L                   ; L=2, which byte holds the character's y-coordinate
  CP 208                  ; A=0 for EINSTEIN, -1 for a teacher
  SBC A,A                 ;
  ADD A,(HL)              ; A=y-coordinate of the lip of the speech bubble
  PUSH HL
  ADD A,A                 ; Compute the LSB of the address of the SRB byte
  ADD A,A                 ; corresponding to the location of the speech bubble
  LD L,A                  ; lip and store it in LIPSRBLSB
  ADD A,E                 ;
  LD (LIPSRBLSB),A        ;
  LD A,B                  ; Put the current column of the play area at the far
  LD (SBLEFTCOL),A        ; left of the screen into SBLEFTCOL for later use by
                          ; the routine at BUBBLESRB
  LD H,11                 ; HL=attribute file address for the lip of the speech
  ADD HL,HL               ; bubble
  ADD HL,HL               ;
  ADD HL,HL               ;
  LD A,L                  ;
  ADD A,C                 ;
  LD L,A                  ;
  LD DE,LIPUDG            ; LIPUDG: Lip UDG
  CALL BUBBLEUDG          ; Print the lip of the speech bubble
  LD BC,65504             ; BC=-32
  ADD HL,BC               ; Up a line
  PUSH HL                 ; Store this attribute file address
  LD A,L                  ; HL=attribute file address for the top left corner
  AND 248                 ; of the speech bubble
  LD L,A                  ;
  ADD HL,BC               ;
  LD DE,SBTLUDG           ; SBTLUDG: Top left corner UDG
  CALL BUBBLEUDG          ; Print the top left corner of the speech bubble
  LD C,6                  ; There are 6 UDGs in the top middle section of the
                          ; speech bubble
SHOWBUBBLE_0:
  LD DE,SBTMUDG           ; SBTMUDG: Top-middle section UDG
  INC L
  CALL BUBBLEUDG          ; Print a top-middle section UDG
  DEC C
  JR NZ,SHOWBUBBLE_0      ; Jump back until all of the top middle section has
                          ; been printed
  LD DE,SBTRUDG           ; SBTRUDG: Top right corner UDG
  INC L
  CALL BUBBLEUDG          ; Print the top right corner of the speech bubble
  LD C,25                 ; HL=attribute file address for bottom left corner of
  ADD HL,BC               ; speech bubble
  LD DE,SBBLUDG           ; SBBLUDG: Bottom left corner UDG
  CALL BUBBLEUDG          ; Print the bottom left corner of the speech bubble
  LD C,6                  ; There are 6 UDGs in the bottom middle section of
                          ; the speech bubble
SHOWBUBBLE_1:
  LD DE,SBBMUDG           ; SBBMUDG: Bottom-middle section UDG
  INC L
  CALL BUBBLEUDG          ; Print a bottom-middle section UDG
  DEC C
  JR NZ,SHOWBUBBLE_1      ; Jump back until all of the bottom middle section
                          ; has been printed
  INC L
  LD DE,SBBRUDG           ; SBBRUDG: Bottom right corner UDG
  CALL BUBBLEUDG          ; Print the bottom right corner of the speech bubble
  POP HL                  ; Retrieve the attribute file address for the spot
                          ; above the lip of the speech bubble
  LD A,H                  ; Set HL to the address of the bottom pixel line of
  AND 11                  ; the character square above the lip
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,7                 ;
  LD H,A                  ;
  LD (HL),66              ; 'Open' the part above the lip (previously closed by
                          ; a middle section UDG)
  LD HL,23360             ; Clear the first 64 bytes at MSGBUFFER (which will
SHOWBUBBLE_2:
  DEC L                   ; be used as a buffer for the speech text graphic
  LD (HL),0               ; data by the routine at SPEAK)
  JR NZ,SHOWBUBBLE_2      ;
  POP HL                  ; Restore the character number to H
  AND A                   ; Return with the carry flag reset to signal that the
  RET                     ; speech bubble has been printed

; Unused
  DEFB 0

; Place a font character bitmap into a message graphic buffer
;
; Used by the routines at TXT2BUF and SPEAK. Appends a font character bitmap
; (corresponding to the ASCII code in A) to the existing contents of a message
; graphic buffer.
;
; A ASCII code
; C Pixel columns remaining in the buffer
; HL' Buffer address
CHR2BUF:
  LD E,A                  ; E=ASCII code (32-127)
  LD D,215                ; Point DE at the appropriate entry in the table of
                          ; font character bitmap widths at FCBWIDTHS
  LD A,(DE)               ; A=width of the font character in pixels (1-5)
  LD B,A                  ; Store this in B
CHR2BUF_0:
  INC D
  LD A,(DE)               ; A=bit pattern for one pixel column of the font
                          ; character
  CALL CHR2BUF_2          ; Slide this into the message buffer
  DEC C                   ; One fewer pixel column remaining in the message
                          ; buffer
  DJNZ CHR2BUF_0          ; Jump back until all pixel columns for the font
                          ; character have been copied into the message buffer
  DEC C                   ; One fewer pixel column remaining in the message
                          ; buffer
; This entry point is used by the routine at TXT2BUF.
CHR2BUF_1:
  XOR A                   ; A=0 (blank pixel column)
CHR2BUF_2:
  EXX
  PUSH HL
  LD C,8                  ; 8 pixels per column
CHR2BUF_3:
  LD B,32                 ; 32 (during startup) or 8 bytes per pixel row (this
                          ; instruction is set to LD B,8 before the game
                          ; starts; see GAMEPOKES)
  RRCA                    ; Push a pixel into the carry flag
CHR2BUF_4:
  RL (HL)                 ; Drag the pixel into the message buffer and shove
  DEC HL                  ; the existing pixels one place to the left
  DJNZ CHR2BUF_4          ;
  DEC C
  JR NZ,CHR2BUF_3         ; Jump back to collect the next pixel up
  POP HL                  ; Restore the address of the last byte in the message
                          ; buffer to HL'
  EXX
  RET

; Unused
  DEFB 0

; Make a character speak
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of the character's buffer by the routine at TELLCLASS, RSTTELLSIT,
; DOCLASS, DETENTION or FINDEXPEL.
;
; E Message number
; H Character number (200-204, 208)
SPEAK:
  LD L,11                 ; Store the message number in byte 11 of the
  LD (HL),E               ; character's buffer
; The address of this entry point is placed into bytes 9 and 10 of EINSTEIN's
; buffer by the routine at GRASSETC.
SPEAK_0:
  LD L,16                 ; Clear bytes 12-16 of the character's buffer, thus
  LD B,5                  ; removing the addresses of any old
SPEAK_1:
  LD (HL),0               ; (sub)(sub)messages
  DEC L                   ;
  DJNZ SPEAK_1            ;
  LD E,(HL)               ; E=message number
  LD D,254                ; Pick up the address of the message and place it
  LD A,(DE)               ; into bytes 11 and 12 of the character's buffer
  LD (HL),A               ;
  INC D                   ;
  LD A,(DE)               ;
  INC L                   ;
  LD (HL),A               ;
; If somebody else is already speaking, this entry point is used while this
; character waits his turn to speak.
SPEAK0:
  LD A,(LIPSRBLSB)        ; LIPSRBLSB holds the LSB of the SRB address
                          ; corresponding to the lip of the speech bubble (or 0
                          ; if there is no bubble on-screen)
  LD L,9                  ; Place the address of the entry point at SPEAK0 into
  LD (HL),29              ; bytes 9 and 10 of the character's buffer
  AND A                   ; Is anyone else speaking at the moment?
  RET NZ                  ; Return if so
  LD (HL),46              ; Place the address of the entry point at SPEAK1 into
                          ; bytes 9 and 10 of the character's buffer
  CALL SHOWBUBBLE         ; Print the speech bubble if the character's head is
                          ; on-screen
  RET NC                  ; Return if the character's head is on-screen
  JR SPEAK_4              ; Otherwise make this routine relinquish control of
                          ; the character
; This entry point is used while the character is speaking.
SPEAK1:
  PUSH HL
  CALL BUBBLESRB          ; Update the SRB so that the speech bubble is not
                          ; corrupted
  POP HL
  JR NC,SPEAK_3           ; Jump if the speech bubble is no longer on-screen
  EXX                     ; Point HL' at the end of the buffer (at MSGBUFFER)
  LD HL,23359             ; which will contain the speech text graphic data
  EXX                     ;
  LD B,2                  ; We scroll the message two letters at a time
SPEAK_2:
  CALL NEXTCHR            ; Collect in A the next character code from the
                          ; message
  AND A                   ; Has the message finished?
  JR NZ,SPEAK_5           ; Jump if not
; The message has finished, or the speech bubble has been scrolled off the
; screen. Either way, the character has finished speaking.
SPEAK_3:
  CALL RMBUBBLE           ; Update the SRB to get rid of the speech bubble
SPEAK_4:
  JP RMSUBCMD             ; Terminate this interruptible subcommand
; The character has not finished speaking.
SPEAK_5:
  PUSH BC
  CALL CHR2BUF            ; Roll the font character bitmap into the message
                          ; buffer
  POP BC
  DJNZ SPEAK_2            ; Jump back until two more letters have been rolled
                          ; into the buffer
  LD L,29                 ; Reset the walk/run bit in byte 29 of the
  RES 7,(HL)              ; character's buffer to make sure he speaks slowly
  LD A,(LIPSRBLSB)        ; Now A=LSB of the address of the SRB byte
  SUB 8                   ; corresponding to the top line of the speech bubble
  LD L,A                  ; Set HL to the display file address of the start of
  RRCA                    ; the top pixel line of text
  RRCA                    ;
  AND 24                  ;
  ADD A,68                ;
  LD H,A                  ;
  LD A,L                  ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  LD L,A                  ;
  NOP                     ;
  INC L                   ;
  LD DE,23298             ; The speech text graphic data is stored in the
                          ; 64-byte buffer at MSGBUFFER
  EX DE,HL
  CALL SPEAK_6            ; Print the top 4 pixel rows of the text inside the
                          ; speech bubble
  ADD A,32                ; Set DE to the display file address of the start of
  LD E,A                  ; the 5th pixel row of text
  JR C,SPEAK_7            ;
  LD A,D                  ;
  SUB 8                   ;
  LD D,A                  ;
SPEAK_6:
  LD A,E                  ; Save the LSB of the base display file address
SPEAK_7:
  LD B,4                  ; 4 pixel lines at a time
SPEAK_8:
  PUSH BC
  LD BC,6                 ; There are 6 character squares inside the speech
                          ; bubble
  LDIR                    ; Copy a row of 6 bytes to the screen
  INC L                   ; Move HL to the start of the next row of pixel data
  INC L                   ; in the message buffer
  LD E,A                  ; Restore the LSB of the base display file address
  INC D                   ; Next pixel row down
  POP BC
  DJNZ SPEAK_8            ; Jump back until all 4 pixel rows in this half have
                          ; been copied to the screen
  RET

; Unused
  DEFS 4

; Save the area of the screen that will be overwritten by a message box
;
; Used by the routines at GIVELINES, OBJFALL and WATCHERIC. Returns with the
; carry flag set if the message box would be off-screen. Otherwise copies the
; area of the screen that will be overwritten by the message box into the
; buffer at OVERBUF, and returns with the attribute file address for the
; top-left corner of the message box in DE.
;
; DE Coordinates of the point above the character's head
SCR2BUF:
  LD A,(LEFTCOL)          ; B=leftmost column of the play area on screen
  LD B,A                  ;
  LD A,E                  ; A=x-coordinate of the character's head
  AND 248                 ; A=x-coordinate of the left edge of the message box
  SUB B                   ; Return with the carry flag set if the message box
  CP 32                   ; would be off-screen to the right
  CCF                     ;
  RET C                   ;
  LD E,A                  ; E=0, 8, 16 or 24: screen x-coordinate of the left
                          ; edge of the message box
  LD A,D                  ; A=y-coordinate of the point above the character's
                          ; head
  SUB 3                   ; A=y-coordinate of the top line of the message box
  RET C                   ; Return with the carry flag set if this would be off
                          ; the top of the screen
; The message box is going to be printed. Prepare for that by saving the area
; of the screen that will be overwritten.
  LD D,0                  ; DE=0, 8, 16 or 24
  PUSH HL                 ; Save HL
  ADD A,A                 ; Set HL the to the attribute file address for the
  ADD A,A                 ; top left corner of the message box
  ADD A,A                 ;
  LD L,A                  ;
  LD H,22                 ;
  ADD HL,HL               ;
  ADD HL,HL               ;
  ADD HL,DE               ;
  PUSH HL                 ; Save this base attribute file address
  LD DE,OVERBUF           ; Point DE at the buffer used to save the screen area
                          ; overwritten by the message box (at OVERBUF)
  LD A,3                  ; Copy three rows of eight attribute bytes each from
SCR2BUF_0:
  LD BC,8                 ; the screen into the first 24 bytes of the buffer
  LDIR                    ;
  LD C,24                 ;
  ADD HL,BC               ;
  DEC A                   ;
  JR NZ,SCR2BUF_0         ;
  POP HL                  ; Restore the base attribute file address to HL
  PUSH HL                 ; And save it again
  LD A,H                  ; Set HL to the display file address for the top left
  SUB 80                  ; corner of the message box
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  LD H,A                  ;
  LD C,3                  ; There are 3 rows of UDGs to copy
SCR2BUF_1:
  LD A,8                  ; 8 pixel rows per UDG
  PUSH BC
SCR2BUF_2:
  PUSH HL
  LD C,8                  ; 8 UDGs per row
  LDIR                    ; Copy one row of pixels into the buffer
  POP HL
  INC H                   ; Move HL to the next row of pixels on screen
  DEC A
  JR NZ,SCR2BUF_2         ; Jump back until all 8 pixel rows for this row of
                          ; UDGs have been copied into the buffer
  LD A,L                  ; Move HL to the display file address for the start
  ADD A,32                ; of the top row of pixels for the next row of UDGs
  LD L,A                  ;
  JR C,SCR2BUF_3          ;
  LD A,H                  ;
  SUB 8                   ;
  LD H,A                  ;
SCR2BUF_3:
  POP BC
  DEC C                   ; One more row of UDGs copied
  JR NZ,SCR2BUF_1         ; Jump back until all 3 rows have been done
  POP DE                  ; Drop the base attribute file address into DE
  POP HL                  ; Restore the original contents of HL
  RET                     ; Return with the carry flag reset

; Unused
  DEFB 0

; Copy a graphic buffer to the screen
;
; Used by the routines at PRTMSGBOX (to print a message box above a character's
; head, or to print the lesson), GIVELINES (to restore the area of the screen
; overwritten by a message box) and PREPGAME (to print the Back to Skool logo
; and the Score/Lines/Hi-Sc box).
;
; DE Attribute file address
; HL Graphic buffer address
BUF2SCR:
  PUSH DE                 ; Save the attribute file address temporarily
  LD A,3                  ; There are 3 rows of character squares
BUF2SCR_0:
  LD BC,8                 ; There are 8 character squares in each row
  LDIR                    ; Copy one row of attribute bytes to the screen
  LD C,24                 ; Point DE at the start of the next row down
  EX DE,HL                ;
  ADD HL,BC               ;
  EX DE,HL                ;
  DEC A
  JR NZ,BUF2SCR_0         ; Jump back to copy the remaining attributes to the
                          ; screen
; The attribute bytes have been copied to the screen. Now for the graphic
; bytes.
  POP DE                  ; Restore the attribute file address to DE
  LD A,D                  ; Set DE to the appropriate display file address
  SUB 80                  ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  LD D,A                  ;
  LD C,3                  ; There are 3 rows of character squares
BUF2SCR_1:
  LD A,8                  ; There are 8 rows of pixels per character square
  PUSH BC                 ; Save the character square row counter
BUF2SCR_2:
  PUSH DE
  LD C,8                  ; There are 8 bytes in each row of pixels
  LDIR                    ; Copy one row of pixels to the screen
  POP DE
  INC D                   ; Point to the display file address of the next pixel
                          ; row
  DEC A
  JR NZ,BUF2SCR_2         ; Jump back to fill in the remaining pixel rows for
                          ; this row of character squares
  LD A,E                  ; Set DE to the start address of the first pixel row
  ADD A,32                ; in the next row of character squares
  LD E,A                  ;
  JR C,BUF2SCR_3          ;
  LD A,D                  ;
  SUB 8                   ;
  LD D,A                  ;
BUF2SCR_3:
  POP BC                  ; Restore the character square row counter to C
  DEC C
  JR NZ,BUF2SCR_1         ; Jump back to fill in the remaining rows of
                          ; character squares
  RET

; Unused
  DEFB 0

; Write a line of text into a graphic buffer
;
; Used by the routines at PRINTMSG and PRTMSGBOX. Writes a line of text into a
; graphic buffer (at MSGBUFFER or MSGBOX) and centres it.
;
; DE Message address
; HL Buffer address
TXT2BUF:
  LD (54795),DE           ; Save the message address in bytes 11 and 12 of the
                          ; character buffer in page 214
; This entry point is used by the routine at PRTMSGBOX to write the second line
; of text into the message box graphic buffer at MSGBOX.
TXT2BUF_0:
  PUSH HL                 ; Copy the buffer address to HL'
  EXX                     ;
  POP HL                  ;
BUFSIZE:
  LD BC,0                 ; B=0 (during startup) or 64, C=0 (this instruction
                          ; is set to LD BC,16384 before the game starts; see
                          ; GAMEPOKES)
TXT2BUF_1:
  LD (HL),C               ; Create 64 (or 256) empty pixel columns in the
  INC HL                  ; buffer
  DJNZ TXT2BUF_1          ;
  DEC HL
  EXX
  LD H,214                ; We are using the character buffer in page 214 to
                          ; process the message
TXTWIDTH:
  LD C,255                ; The message box is 255 (during startup) or 64
                          ; pixels wide (this instruction is set to LD C,64
                          ; before the game starts; see GAMEPOKES)
TXT2BUF_2:
  CALL NEXTCHR            ; A=ASCII code of the next character in the message
  CP 3                    ; Is the ASCII code 0, 1 or 2 (end of line)?
  JR C,TXT2BUF_3          ; Jump if so to centre the text
  CALL CHR2BUF            ; Slide the bitmap for CHR$(A) into the buffer
  JR TXT2BUF_2            ; Jump back to do the next character
TXT2BUF_3:
  SRL C                   ; Now C=half the number of unused pixel columns left
                          ; in the buffer
  RET Z                   ; Return if this is zero (the buffer is full)
TXT2BUF_4:
  CALL CHR2BUF_1          ; Create C empty pixel columns in the buffer, thus
  DEC C                   ; centring the message
  JR NZ,TXT2BUF_4         ;
  RET

; Unused
  DEFB 0

; Print a message box
;
; Used by the routines at GIVELINES and NEWLESSON2. Prepares the text of a
; message in the graphic buffer at MSGBOX and then copies the buffer to the
; screen.
;
; A Message number
; C Message box attribute byte
; DE Attribute file address at which to print the message box
PRTMSGBOX:
  LD HL,MSGBOX            ; The graphic data for the message box will be stored
                          ; in the buffer at MSGBOX before being copied to the
                          ; screen
  LD B,24                 ; 24=3 rows of 8 character squares
PRTMSGBOX_0:
  LD (HL),C               ; Prepare the attribute bytes in the buffer
  INC L                   ;
  DJNZ PRTMSGBOX_0        ;
  PUSH DE                 ; Save the attribute file address temporarily
  LD L,A                  ; Set DE to the start address of the message
  LD H,254                ;
  LD E,(HL)               ;
  INC H                   ;
  LD D,(HL)               ;
  LD HL,58456             ; Point HL at the address in the buffer corresponding
                          ; to the start of the fourth row of pixels from the
                          ; top of the message box (the first three rows are
                          ; always empty)
  CALL TXT2BUF            ; Prepare the first line of text in the buffer
  LD HL,58520             ; Point HL at the address in the buffer corresponding
                          ; to the start of the 12th row of pixels from the top
  CALL TXT2BUF_0          ; Prepare the second line of text in the buffer
  POP DE                  ; Retrieve the attribute file address for the message
                          ; box
  LD HL,MSGBOX            ; Point to the start of the buffer
  JP BUF2SCR              ; Transfer the buffer to the screen

; Print the score, lines total or hi-score (2)
;
; Continues from the routine at PRINTNUM. Prepares the buffer at NBUFG with the
; graphic data for the digits whose character codes are stored at MSG012, then
; copies the graphic data to the screen at the location specified by DE.
;
; DE Display file address
PRINTNUM2:
  PUSH DE                 ; Save the display file address
  LD HL,58143             ; Clear the 21-byte buffer at NBUFG, which will hold
  LD B,21                 ; the graphic data for the number to be printed (21
PRINTNUM2_0:
  LD (HL),0               ; bytes = 7 pixel rows, 3 bytes per row)
  DEC L                   ;
  DJNZ PRINTNUM2_0        ;
  LD L,B                  ; HL=MSG012 (where the ASCII codes of the digits are
                          ; stored)
PRINTNUM2_1:
  LD A,(HL)               ; Pick up the ASCII code of the next digit
  AND A                   ; Have we done all the digits?
  JR Z,PRINTNUM2_6        ; Jump if so
  LD E,A                  ; E=ASCII code for the digit (48-57)
  LD D,215                ; Point DE at the appropriate entry in the table of
                          ; font character bitmap widths at FCBWIDTHS
  INC HL                  ; Point HL at the next digit and save this address
  PUSH HL                 ; for later
  LD A,(DE)               ; A=width of the font character bitmap (in pixels)
  LD C,A                  ; Copy this to C
PRINTNUM2_2:
  INC D                   ; Point DE at the next bitmap pixel column
  LD A,(DE)               ; A=next pixel column of bitmap data for this digit
PRINTNUM2_3:
  LD L,31                 ; Point HL at the last byte of the graphic data
                          ; buffer
  LD B,7                  ; There are 7 rows of pixels in the buffer
  RRCA                    ; Skip the bottom row of pixels in the bitmap data
PRINTNUM2_4:
  RRCA                    ; Roll a pixel column into the buffer
  RL (HL)                 ;
  DEC HL                  ;
  RL (HL)                 ;
  DEC HL                  ;
  RL (HL)                 ;
  DEC HL                  ;
  DJNZ PRINTNUM2_4        ;
  DEC C                   ; Next pixel column
  JP M,PRINTNUM2_5        ; Jump if we've done them all (plus a blank column)
  JR NZ,PRINTNUM2_2       ; Jump back until all the pixel columns of the bitmap
                          ; for the digit have been done
  XOR A                   ; Then jump back to add a blank pixel column
  JR PRINTNUM2_3          ;
PRINTNUM2_5:
  POP HL                  ; HL=address of the next digit
  NOP
  JR PRINTNUM2_1          ; Jump back to deal with this digit
; The graphic data for the digits has been rolled into the buffer at NBUFG. Now
; we copy the buffer to the screen.
PRINTNUM2_6:
  LD L,11                 ; HL=NBUFG (start address of the buffer)
  LD BC,2047              ; B=7 (number of pixel rows), C=255
  POP DE                  ; DE=appropriate display file address
  LD A,E
PRINTNUM2_7:
  LDI                     ; Copy three bytes (one pixel row) from the buffer to
  LDI                     ; the screen
  LDI                     ;
  LD E,A                  ; Point DE at the start of the next row down
  INC D                   ;
  DJNZ PRINTNUM2_7        ; Jump back until all 7 pixel rows are done
  RET

; Unused
  DEFS 2

; Compute the decimal digits of a number
;
; Used by the routines at PRINTNUM and GIVELINES. Computes the ASCII codes for
; the digits of the number held in DE, and stores them in the buffer at MSG012.
;
; DE Number
NUM2ASCII:
  LD HL,NBUFG
  LD B,L                  ; B=11
NUM2ASCII_0:
  DEC L                   ; Clear the buffer at MSG012, which will be used to
  LD (HL),0               ; store the ASCII codes of the digits
  DJNZ NUM2ASCII_0        ;
  LD A,48                 ; 48 is the code for '0'
; Before inserting any ASCII codes into the buffer, we need to figure out
; whether the first digit is the 10000s, 1000s, 100s, 10s or units digit.
  LD BC,55536             ; BC=-10000
  EX DE,HL                ; DE=MSG012
  ADD HL,BC               ; Subtract 10000
  JR C,NUM2ASCII_1        ; Jump if it 'went'
  SBC HL,BC               ; Otherwise add the 10000 back
  LD BC,64536             ; BC=-1000
  ADD HL,BC               ; Subtract 1000
  JR C,NUM2ASCII_2        ; Jump if it 'went'
  SBC HL,BC               ; Otherwise add the 1000 back
  LD BC,65436             ; BC=-100
  ADD HL,BC               ; Subtract 100
  JR C,NUM2ASCII_3        ; Jump if it 'went'
  SBC HL,BC               ; Otherwise add the 100 back
  LD C,246                ; BC=-10
  ADD HL,BC               ; Subtract 10
  JR C,NUM2ASCII_4        ; Jump if it 'went'
  SBC HL,BC               ; Otherwise add the 10 back
  LD A,L                  ; A=units left
  AND A                   ; Are there any?
  RET Z                   ; Return if not (nothing to do)
  JR NUM2ASCII_5          ; Jump forward to place the units digit into the
                          ; buffer
; Compute and insert the 10000s digit.
NUM2ASCII_1:
  INC A                   ; Get the ASCII code for the 10000s digit in A
  ADD HL,BC               ;
  JR C,NUM2ASCII_1        ;
  SBC HL,BC
  LD BC,64536             ; BC=-1000
  LD (DE),A               ; Place the 10000s digit into the buffer
  LD A,47
  INC E                   ; Move DE along to the next slot in the buffer
; Compute and insert the 1000s digit.
NUM2ASCII_2:
  INC A                   ; Get the ASCII code for the 1000s digit in A
  ADD HL,BC               ;
  JR C,NUM2ASCII_2        ;
  SBC HL,BC
  LD BC,65436             ; BC=-100
  LD (DE),A               ; Place the 1000s digit into the buffer
  LD A,47
  INC E                   ; Move DE along to the next slot in the buffer
; Compute and insert the 100s digit.
NUM2ASCII_3:
  INC A                   ; Get the ASCII code for the 100s digit in A
  ADD HL,BC               ;
  JR C,NUM2ASCII_3        ;
  SBC HL,BC
  LD (DE),A               ; Place the 100s digit into the buffer
  LD C,246                ; BC=-10
  LD A,47
  INC E                   ; Move DE along to the next slot in the buffer
; Compute and insert the 10s digit.
NUM2ASCII_4:
  INC A                   ; Get the ASCII code for the 10s digit in A
  ADD HL,BC               ;
  JR C,NUM2ASCII_4        ;
  SBC HL,BC
  LD (DE),A               ; Place the 10s digit into the buffer
  INC E                   ; Move DE along to the next slot in the buffer
; Compute and insert the units digit.
NUM2ASCII_5:
  LD A,L                  ; A=number of units left
  ADD A,48                ; A=ASCII code for the units digit
  LD (DE),A               ; Place this into the buffer
  RET

; Unused
  DEFB 0

; Alter UDG references in the play area for a door, a window, a cup or the bike
;
; Used by the routines at CHKCOMBOS, INITDOORS, MVDOORWIN and CHKWATER. The UDG
; reference tables used by this routine are organised into entries of four
; bytes each. Each entry corresponds to a single UDG within the matrix of UDGs
; for the door, window, cup or bike:
;
; +----------------+--------------------------+
; | Byte           | Contents                 |
; +----------------+--------------------------+
; | 1              | y-coordinate             |
; | 2              | x-coordinate             |
; | 3              | UDG reference            |
; | 4 (bits 0-3)   | BRIGHT/PAPER attributes  |
; | 4 (bits 6 & 7) | UDG base page identifier |
; +----------------+--------------------------+
;
; The data tables used are located as follows:
;
; +------------+------------------------------------------------------+
; | Address    | Object                                               |
; +------------+------------------------------------------------------+
; | URTLSDSHUT | Left study door (closed)                             |
; | URTLSDOPEN | Left study door (open)                               |
; | URTRSDSHUT | Right study door (closed)                            |
; | URTRSDOPEN | Right study door (open)                              |
; | URTSLSSHUT | Science Lab storeroom door (closed)                  |
; | URTSLSOPEN | Science Lab storeroom door (open)                    |
; | URTBSDSHUT | Boys' skool door (closed)                            |
; | URTBSDOPEN | Boys' skool door (open)                              |
; | URTSGSHUT  | Skool gate (closed)                                  |
; | URTSGOPEN  | Skool gate (open)                                    |
; | URTDCDSHUT | Drinks cabinet door (closed)                         |
; | URTDCDOPEN | Drinks cabinet door (open)                           |
; | URTTFWSHUT | Top-floor window (closed)                            |
; | URTTFWOPEN | Top-floor window (open)                              |
; | URTMFWSHUT | Middle-floor window (closed)                         |
; | URTMFWOPEN | Middle-floor window (open)                           |
; | URTBSCUPSE | Cups in the boys' skool (empty)                      |
; | URTCUP1W   | Leftmost cup in the boys' skool (containing water)   |
; | URTCUP1S   | Leftmost cup in the boys' skool (containing sherry)  |
; | URTCUP2W   | Middle cup in the boys' skool (containing water)     |
; | URTCUP2S   | Middle cup in the boys' skool (containing sherry)    |
; | URTCUP3W   | Rightmost cup in the boys' skool (containing water)  |
; | URTCUP3S   | Rightmost cup in the boys' skool (containing sherry) |
; | URTCUP4E   | Cup in the girls' skool (empty)                      |
; | URTCUP4W   | Cup in the girls' skool (containing water)           |
; | URTCUP4S   | Cup in the girls' skool (containing sherry)          |
; | URTTREE    | Tree (with no bike attached)                         |
; | URTBIKE    | Tree (with bike attached)                            |
; +------------+------------------------------------------------------+
;
; HL UDG reference table address
ALTERUDGS:
  LD A,(HL)               ; Pick up the y-coordinate (Y) in D
  LD D,A                  ;
  INC A                   ; Return if we found the end-of-table marker (255)
  RET Z                   ;
  INC HL                  ; Point HL at byte 2 of this table entry
  LD E,(HL)               ; Pick up the x-coordinate (X) in E
  INC HL                  ; Point HL at byte 3 of this table entry
  PUSH HL
  LD A,(LEFTCOL)          ; C=leftmost column of the play area on screen
  LD C,A                  ;
  LD A,E                  ; Jump if (X,Y) is off-screen (no need to update the
  SUB C                   ; screen refresh buffer)
  JR C,ALTERUDGS_0        ;
  CP 32                   ;
  JR NC,ALTERUDGS_0       ;
; (X,Y) corresponds to a location that is currently on-screen, so we have to
; update the screen refresh buffer (SRB).
  LD C,A                  ; 0<=C<=31 (screen x-coordinate)
  AND 7                   ; Point HL at an entry in the 8-byte table at
  ADD A,120               ; SRBBITS, whose contents are (128, 64, 32, 16, 8, 4,
  LD L,A                  ; 2, 1)
  LD H,225                ;
  LD B,(HL)               ; Pick up this entry in B
  LD A,C                  ; 0<=A<=31 (screen x-coordinate)
  RRCA                    ; Point HL at the byte of the SRB corresponding to
  RRCA                    ; (X,Y)
  RRCA                    ;
  AND 3                   ;
  ADD A,D                 ;
  ADD A,D                 ;
  ADD A,D                 ;
  ADD A,D                 ;
  LD L,A                  ;
  LD H,127                ;
  LD A,(HL)               ; Set the appropriate bit in the SRB byte
  OR B                    ;
  LD (HL),A               ;
; The UDG reference (i.e. the LSB of the base address of the skool UDG) for the
; skool location (X,Y) must be modified to reflect the change in status of the
; door, window, cup or bike.
ALTERUDGS_0:
  LD L,E                  ; Point HL at the Q value (0<=Q<=143) in page 181 for
  LD H,181                ; the x-coordinate X (see PRINTTILE)
  LD E,(HL)               ; Pick up the Q value in E
  LD A,D                  ; Set DE to the address where the UDG reference for
  ADD A,160               ; (X,Y) is stored
  LD D,A                  ;
  POP HL                  ; Point HL at byte 3 of the table entry
  LD A,(HL)               ; Pick up the replacement UDG reference in A
  INC HL                  ; Point HL at byte 4 of the table entry
  LD (DE),A               ; Replace the UDG reference for the skool location
                          ; (X,Y) with the one collected from the table entry
; The attribute byte for the skool location (X,Y) must be modified too.
  LD A,(HL)               ; Pick up byte 4 of the table entry in A
  AND 15                  ; Keep only bits 0-3 (the BRIGHT and PAPER
                          ; attributes)
  LD B,A                  ; Copy them to B
  LD A,E                  ; A=Q (0<=Q<=143)
  LD C,A                  ; Save the Q value in C for later
  ADD A,104               ; Point DE at byte 180+INT(Q/2) of page Y+160, where
  SCF                     ; the BRIGHT and PAPER attributes for the skool
  RRA                     ; location (X,Y) are stored
  LD E,A                  ;
  JR C,ALTERUDGS_1        ; Jump if Q is odd (BRIGHT and PAPER in bits 0-3)
  LD A,B                  ; Shift the BRIGHT and PAPER attributes from bits 0-3
  RRCA                    ; to bits 4-7 of B
  RRCA                    ;
  RRCA                    ;
  RRCA                    ;
  LD B,A                  ;
  LD A,(DE)               ; Pick up the current attribute byte for the skool
  AND 15                  ; location (X,Y), keeping bits 0-3 as they are
  JR ALTERUDGS_2          ; Jump forward to adjust bits 4-7 appropriately
ALTERUDGS_1:
  LD A,(DE)               ; Pick up the current attribute byte for the skool
  AND 240                 ; location (X,Y), keeping bits 4-7 as they are
ALTERUDGS_2:
  OR B                    ; Adjust bits 0-3 (if Q is odd) or bits 4-7 (Q is
  LD (DE),A               ; even) of the attribute byte for the skool location
                          ; (X,Y)
; Finally, the base page (128, 136, 144 or 152) for the new skool UDG reference
; must be set.
  LD A,(HL)               ; Pick up byte 4 of the table entry in A again
  AND 192                 ; Keep only bits 6 and 7
  BIT 6,A                 ; Set B=10001000 if these bits are 11, 10000000 (10),
  JR Z,ALTERUDGS_3        ; 00001000 (01) or 00000000 (00)
  SUB 56                  ;
ALTERUDGS_3:
  LD B,A                  ;
  LD A,C                  ; A=Q
  LD C,136                ; C=10001000
  SRL A                   ; Set A=INT(Q/4), and shift B and C right (Q AND 3)
  JR NC,ALTERUDGS_4       ; times
  RRC B                   ;
  RRC C                   ;
ALTERUDGS_4:
  SRL A                   ;
  JR NC,ALTERUDGS_5       ;
  RRC B                   ;
  RRC C                   ;
  RRC B                   ;
  RRC C                   ;
ALTERUDGS_5:
  ADD A,144               ; Point DE at byte 144+INT(Q/4) of page Y+160, where
  LD E,A                  ; the UDG MSB identifier bit-pair for skool location
                          ; (X,Y) is held (see PRINTTILE)
  LD A,(DE)               ; Pick up the current MSB identifier byte in A
  OR C                    ; Replace bits (0,4), (1,5), (2,6) or (3,7) in this
  XOR C                   ; byte with the corresponding bits in B
  OR B                    ;
  LD (DE),A               ; Store the new MSB identifier byte
; Move to the next entry in the table.
  INC HL                  ; Point HL at the first byte of the next entry in the
                          ; table
  JP ALTERUDGS            ; Jump back to process it

; Unused
  DEFB 0

; Check whether any characters were punched
;
; Used by the routine at VICTIM. Returns with the carry flag reset if someone
; was hit in the face, and D holding the character number of the stricken one.
;
; B Number of potential victims to check
; C 0 if the puncher is facing right, 128 if left
; D Character number of the first potential victim to check
; H Puncher's character number: 207 (ANGELFACE) or 210 (ERIC)
CHECKHIT:
  CALL CHKTARGET          ; Are any characters in front of the puncher's fist?
  RET C                   ; Return if not
  LD E,0
  LD A,(DE)               ; A=animatory state of the character in front of the
                          ; puncher's fist
  AND 128                 ; Keep only the 'direction' bit (bit 7)
  XOR C                   ; Compare with the direction bit of the punching
                          ; character
  RET Z                   ; Return with the carry flag reset if the punched
                          ; character was facing the puncher (i.e. was hit in
                          ; the face)
  INC D                   ; Otherwise move on to the next potential victim
  DJNZ CHECKHIT           ; Jump back to consider any remaining potential
                          ; victim
  SCF                     ; Signal: no characters were hit
  RET

; Check whether any characters have been hit by a fist, pellet or conker
;
; Used by the routines at CHECKHIT, OBJFALL and PELLET. Returns with the carry
; flag reset if someone is in the same location as the fist, pellet or conker,
; and with D holding that person's character number.
;
; B Number of potential victims to check
; D Character number of the first potential victim to check
; H Puncher's, pellet's or conker's character number
CHKTARGET:
  EX DE,HL
CHKTARGET_0:
  LD L,1                  ; Point HL at byte 1 of the next potential target's
                          ; character buffer
  LD E,L                  ; Point DE at byte 1 of the puncher's, pellet's or
                          ; conker's character buffer
  LD A,(DE)               ; A=x-coordinate of the fist/pellet/conker
  CP (HL)                 ; Does this match the target character's
                          ; x-coordinate?
  JR Z,CHKTARGET_2        ; Jump if so
CHKTARGET_1:
  INC H                   ; Move to the next potential target
  DJNZ CHKTARGET_0        ; Jump back until all potential targets have been
                          ; checked
  EX DE,HL
  SCF                     ; Signal: no character was hit
  RET
; A potential victim of the fist, pellet or conker was found at the right
; x-coordinate. What about the y-coordinate?
CHKTARGET_2:
  INC L                   ; Point HL at byte 2 of the next potential target's
                          ; character buffer
  INC E                   ; Point DE at byte 2 of the puncher's, pellet's, or
                          ; conker's character buffer
  LD A,(DE)               ; A=y-coordinate of the fist/pellet/conker
  CP (HL)                 ; Does this match the target character's
                          ; y-coordinate?
  JR NZ,CHKTARGET_1       ; Jump back to consider the next character if not
; The potential victim's coordinates are a perfect match.
  LD A,H                  ; A=number of the stricken character
  CP 210                  ; Was ERIC hit?
  JR Z,CHKTARGET_4        ; Jump if so
  LD L,18                 ; Bytes 17 and 18 of the character's buffer may hold
  LD A,(HL)               ; the address of an uninterruptible subcommand
                          ; routine (which would prevent him from being knocked
                          ; over); pick up the MSB in A
CHKTARGET_3:
  AND A                   ; Can this character be knocked over at the moment?
  JR NZ,CHKTARGET_1       ; Jump back to consider the next character if not
  EX DE,HL                ; Transfer the stricken character's number to D
  RET
; ERIC was the potential victim.
CHKTARGET_4:
  LD A,(STATUS)           ; Collect ERIC's status flags from STATUS; now A=0
                          ; unless ERIC is engaged in some activity other than
                          ; walking
  JR CHKTARGET_3

; Unused
  DEFS 2

; Prepare the doors, windows, cups and bike for new game
;
; Used by the routine at PREPGAME. Opens the boys' skool door and the skool
; gate, closes all the other doors, closes the windows, empties the cups, and
; chains the bike to the tree.
INITDOORS:
  LD HL,DWURTINIT         ; Point HL at the first entry in the initialisation
                          ; table at DWURTINIT
INITDOORS_0:
  LD E,(HL)               ; Pick up an entry from the initialisation table in
  INC L                   ; DE, which will then point at a data table
  LD D,(HL)               ; concerning a door, window, cup or bike
  INC L                   ; Move HL along to the next entry in the
  PUSH HL                 ; initialisation table and save the pointer for now
  EX DE,HL                ; Point HL at the door/window/cup/bike data table
  CALL ALTERUDGS          ; Alter UDG references and attributes in the play
                          ; area
  POP HL                  ; Restore the pointer to the initialisation table
  LD A,L                  ; Have we reached the end of the initialisation
  CP 112                  ; table?
  JR NZ,INITDOORS_0       ; Jump back if not
  LD HL,DOORFLAGS         ; DOORFLAGS holds the door/window flags
  LD (HL),24              ; Set bits 3 and 4: skool gate and boys' skool door
                          ; open
  RET

; Unused
  DEFS 2

; Get the x-coordinate range within which a character can see or be seen
;
; Used by the routines at WITNESS and NEARESTKID. Returns with C and B holding
; the lower and upper x-coordinates of the range within which the target
; character can see or be seen. This routine is called with H holding either
; (a) the character number of a teacher (to determine where kids must be in
; order to get lines), or (b) the character number of a kid (to determine where
; a teacher must be in order to give lines).
;
; H Target character's number
VISRANGE:
  LD L,2                  ; Point HL at byte 2 of the character's buffer
  LD D,(HL)               ; D=character's y-coordinate
  DEC L                   ; L=1
  LD E,(HL)               ; E=character's x-coordinate
  LD C,0                  ; Prepare a lower limit of 0 in case E<10
  LD A,E                  ; A=X (character's x-coordinate)
  SUB 10
  JR C,VISRANGE_0
  LD C,A                  ; C=max(0, X-10) (lower limit of the range)
VISRANGE_0:
  ADD A,20
  LD B,A                  ; B=X+10 (upper limit of the range)
  RET

; Unused
  DEFB 0

; Control a boy during assembly
;
; Used by command lists 20, 40 and 88. Makes a boy find a spot to sit down in
; the assembly hall, and stay seated until assembly is finished.
;
; H Boy's character number (190-199, 206-208)
INASSEMBLY:
  LD A,H                  ; Determine the x-coordinate at which this boy will
  AND 3                   ; sit during assembly based on his character number
  CPL                     ; (H) and his current x-coordinate (in byte 1 of the
  LD L,1                  ; character buffer)
  ADD A,(HL)              ;
  LD L,11                 ; Store this x-coordinate in byte 11 of the boy's
  LD (HL),A               ; character buffer
; Having determined where the boy will sit, send him there.
  LD BC,WALK              ; Redirect control to the routine at WALK (walk to
  CALL CALLSUBCMD         ; location) and then return to INASSEMBLY_0
; This entry point is used after the boy has reached the point where he will
; sit.
INASSEMBLY_0:
  LD L,0                  ; Byte 0 of the character buffer holds the boy's
                          ; animatory state
  BIT 7,(HL)              ; Is he facing left?
  JP Z,WALK_7             ; Make him turn round to face the stage if so
  LD L,4                  ; Remove the address of this routine's entry point
  LD (HL),0               ; (INASSEMBLY_0) from bytes 3 and 4 of the character
                          ; buffer
  CALL UPDATESRB          ; Update the SRB for the boy's current animatory
                          ; state and location
  AND 240                 ; A=animatory state of the boy sitting on the floor
  ADD A,5                 ;
  CALL SUBCMD             ; Update the boy's animatory state, update the SRB,
                          ; and place the address of the entry point at
                          ; SITASSEM (below) into bytes 17 and 18 of the
                          ; character buffer
; This entry point is used after the boy has sat down facing the stage.
SITASSEM:
  LD A,(LFLAGS)           ; Check bit 7 of LFLAGS (set by ASSEMDUTY, reset by
  RLCA                    ; DETENTION): has assembly finished yet?
  RET C                   ; Return if not
  LD L,18                 ; Remove the address of this routine's entry point
  LD (HL),0               ; (SITASSEM) from bytes 17 and 18 of the character's
                          ; buffer, triggering a move to the next command in
                          ; the command list
  JP WRITEBRD_1           ; Make the boy stand up before continuing, though

; Unused
  DEFB 0

; Check whether a target character can be seen by another character
;
; Used by the routines at WITNESS and NEARESTKID. This routine checks through
; B' 'spotter' characters starting with character H'. If any spotter is close
; enough to the target to be able to see it, the routine returns with A holding
; a non-zero value, H holding the spotter's character number, and the carry
; flag set if the spotter is facing the right way to see it (reset otherwise).
; If no spotters are close enough to the target to be able to see it, the
; routine returns with A holding 0 and the carry flag reset.
;
; B Upper x-coordinate of the target range
; C Lower x-coordinate of the target range
; D Target character's y-coordinate
; E Target character's x-coordinate
; B' Number of spotters to check
; H' Character number of the first spotter to check
VISIBLE:
  EXX
VISIBLE_0:
  LD L,2                  ; Pick up the spotter's coordinates in DE'; we are
  LD D,(HL)               ; going to check whether this character can see the
  DEC L                   ; target
  LD E,(HL)               ;
  LD A,D                  ; Transfer the spotter's y-coordinate to A
  EXX
  SUB D                   ; Subtract the target's y-coordinate
  JR NC,VISIBLE_1         ; Jump if the spotter is below or level with the
                          ; target
  NEG
VISIBLE_1:
  CP 4                    ; Is the spotter within 3 y-coordinates of the
                          ; target?
  JR NC,VISIBLE_2         ; Jump if not
  EXX
  LD A,E                  ; A=spotter's x-coordinate
  EXX
  CP C                    ; Compare this with the lower and upper limits of the
  JR C,VISIBLE_2          ; range within which the target can be seen, and jump
  CP B                    ; to VISIBLE_4 if the spotter is within seeing range
  JR C,VISIBLE_4          ;
; This entry point is used by the routine at WITNESS.
VISIBLE_2:
  EXX
; This entry point is used by the routine at NEARESTKID.
VISIBLE_3:
  INC H                   ; The last spotter was not within seeing range of the
  DJNZ VISIBLE_0          ; target; try the next spotter
  EXX
  XOR A                   ; Signal: none of the spotters was within range
  RET
; We've found a spotter within seeing range of the target. But is he facing the
; right way?
VISIBLE_4:
  CP E                    ; Does the spotter's x-coordinate match the target's?
  JR Z,VISIBLE_5          ; Jump if so
  LD A,0                  ; The carry flag is now set if the spotter is to the
                          ; left of the target
  RRA                     ; A=128 if the spotter is to the left of the target,
                          ; 0 if to the right
  EXX
  LD L,0                  ; Point HL' at the spotter's animatory state
  XOR (HL)                ; Compare this orientation with the direction in
                          ; which the spotter is facing
  EXX
  RLCA
VISIBLE_5:
  CCF                     ; Set the carry flag if the spotter's facing the
                          ; right way to see the target
  LD A,B                  ; Set A to something non-zero to indicate that the
                          ; spotter was within range
  RET

; Check whether ERIC can be seen by an adult
;
; Used by the routine at CHKERIC2. Returns with the carry flag set if there is
; an adult nearby who can see ERIC, and A holding the adult's character number.
; Otherwise returns with the carry flag reset and A holding 0.
WITNESS:
  LD H,210                ; 210=ERIC
; This entry point is used by the routines at CATTY (with H=206: BOY WANDER)
; and VIOLENT (with H=207: ANGELFACE).
WITNESS_0:
  CALL VISRANGE           ; Collect in C and B the lower and upper
                          ; x-coordinates of the 'visibility region' (20
                          ; character spaces wide) around the boy
  EXX
  LD H,200                ; 200=MR WACKER
  LD B,6                  ; There are 6 adult characters
  EXX
  CALL VISIBLE            ; Find the first adult character inside the
                          ; visibility region (if any)
  JR C,WITNESS_2          ; Jump if we found one who can see the boy
WITNESS_1:
  AND A                   ; Are there any adult characters left to check?
  RET Z                   ; Return with the carry flag reset if not
  CALL VISIBLE_2          ; We must have found an adult character who is facing
                          ; the wrong way to see the boy; skip this character
                          ; and find the next one inside the visibility region
  JR NC,WITNESS_1         ; Jump if there are no others inside the visibility
                          ; region, or we found another one facing the wrong
                          ; way
; We found an adult character inside the visibility region and facing the right
; way to see the boy.
WITNESS_2:
  EXX
  LD A,H                  ; A=character number of the adult who saw the boy
  EXX
  RET                     ; Return with the carry flag set

; Check for main kids near a teacher
;
; Used by the routine at KNOCKED. Returns with the character number of the
; nearest main kid (or 0 if none of them was close enough) in A.
;
; H Teacher's character number (200-204)
NEARESTKID:
  CALL VISRANGE           ; Get the lower and upper x-coordinates of the
                          ; teacher's range of vision in C and B
  LD L,0                  ; Default assumption: no kids nearby
  EXX
  LD H,206                ; 206=BOY WANDER
  LD BC,1535              ; B'=5 (main kids including ERIC), C'=255 (initial
                          ; value for the distance of the nearest main kid)
  EXX
  CALL VISIBLE            ; Find the first main kid within range (if any)
NEARESTKID_0:
  AND A                   ; Set the zero flag if we've checked all the main
                          ; kids
  LD A,L                  ; A=character number of the nearest kid so far, or 0
                          ; if there are none
  RET Z                   ; Return if we've finished checking all the main kids
  LD A,E                  ; A=teacher's x-coordinate
  EXX
  SUB E                   ; Subtract the x-coordinate of the kid
  JR NC,NEARESTKID_1      ; Jump if the kid's to the left of teacher
  NEG
NEARESTKID_1:
  CP C                    ; Is this kid closer to the teacher than the last?
  JR NC,NEARESTKID_2      ; Jump if not
  LD C,A                  ; C'=distance from the teacher of the nearest kid
                          ; found so far
  LD A,H                  ; A=character number of this kid
  EXX
  LD L,A                  ; Store the character number of the nearest kid in L
  EXX
NEARESTKID_2:
  CALL VISIBLE_3          ; Find the next main kid within range (if any)
  JR NEARESTKID_0         ; Jump back to compare distances

; Check whether a location is on the assembly hall stage
;
; Used by the routine at GETREGION. Returns to the caller of GETREGION with the
; carry flag reset if the location is on the stage. Returns to GETREGION
; otherwise.
;
; D y-coordinate
; E x-coordinate
ONSTAGE:
  LD BC,46651             ; B=182, C=59
  LD A,D                  ; A=y-coordinate of the location
  CP 14                   ; The assembly hall stage's y-coordinate is 14
  RET NZ                  ; Return to GETREGION if the location is not there
  LD A,E                  ; A=x-coordinate of the location
  CP 90                   ; Return to GETREGION unless 74<=A<90 (the rough
  RET NC                  ; vicinity of the stage)
  CP 74                   ;
  RET C                   ;
  POP BC                  ; Drop the return address from the stack
  LD A,192                ; A=192: region identifier for the assembly hall
                          ; stage (see GETREGION)
  RET                     ; Return with the carry flag reset (indicating that
                          ; the location is not on a staircase) to the caller
                          ; of GETREGION

; Determine which floor ERIC's on when he's not standing directly on one
;
; Continues from the routine at GETERICY, having determined that ERIC's feet
; are not on the floor. On entry, the zero flag is set if (a) ERIC is jumping
; and the zero flag was set (by chance) upon entering the routine at GETERICY,
; or (b) bits 1 and 2 of ERIC's status flags at STATUS2 are both reset. Returns
; with D=17 if the zero flag is set upon entry (regardless of which floor ERIC
; is on, which is a bug); otherwise returns with D=3, 10, 14 or 17, indicating
; which floor ERIC is directly above.
;
; D ERIC's y-coordinate
; E ERIC's x-coordinate
GETERICY2:
  LD A,D                  ; A=ERIC's y-coordinate
  LD D,17                 ; 17=bottom floor
  RET Z                   ; Return with D=17 if the zero flag was set upon
                          ; entry (regardless of which floor ERIC is on, which
                          ; is a bug)
  LD D,3                  ; 3=top floor
  CP 4                    ; Is ERIC directly above the top floor?
  RET C                   ; Return with D=3 if so
  LD D,10                 ; 10=middle floor
  CP 11                   ; Is ERIC directly above the middle floor?
  RET C                   ; Return with D=10 if so
  LD D,17                 ; 17=bottom floor
  CP 15                   ; Is ERIC below the height of the stage?
  RET NC                  ; Return with D=17 if so
  LD A,E                  ; A=ERIC's x-coordinate
  CP 76                   ; Return with D=17 if ERIC is not directly above the
  RET C                   ; assembly hall stage
  CP 82                   ;
  RET NC                  ;
  LD D,14                 ; 14=assembly hall stage
  RET

; Put ERIC midstride
;
; Used by the routines at CHKWALLS and UP. Puts ERIC midstride after 'Q' (up),
; 'A' (down), 'O' (left) or 'P' (right) was pressed.
;
; BC ERIC's post-midstride coordinates
; DE ERIC's midstride coordinates
MVERIC1:
  PUSH DE                 ; Save ERIC's midstride coordinates temporarily
  LD HL,53765             ; Point HL at byte 5 of ERIC's buffer
  LD (HL),B               ; Place ERIC's post-midstride coordinates into bytes
  DEC L                   ; 4 and 5 of his buffer
  LD (HL),C               ;
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  POP DE                  ; Restore ERIC's midstride coordinates to DE
  LD L,3
  INC A                   ; A=ERIC's next animatory state (midstride)
  PUSH AF                 ; Save this briefly
  INC A                   ; A=ERIC's post-midstride animatory state
  AND 250                 ;
  LD (HL),A               ; Store this in byte 3 of ERIC's buffer
  POP AF                  ; A=ERIC's next animatory state (midstride)
  CALL UPDATEAS           ; Update ERIC's animatory state (to midstride) and
                          ; location and update the SRB
  LD L,4                  ; 4=small value for ERIC's main action timer (fast)
  LD A,(KEYCODE)          ; KEYCODE holds the keypress table offset of the last
                          ; key pressed
  BIT 3,A                 ; Was the last key pressed upper case (fast)?
  JR Z,MVERIC1_0          ; Jump if so
  LD L,9                  ; 9=large value for ERIC's main action timer (slow)
MVERIC1_0:
  LD H,L                  ; Initialise ERIC's main action timer at ERICTIMER
  LD (ERICTIMER2),HL      ; and the midstride/mid-action timer at ERICTIMER2 to
                          ; the same value
  JP WALKSOUND            ; Make a walking sound effect

; Unused
  DEFS 5

; Move ERIC from the midstride or mid-action position and scroll the screen if
; necessary
;
; Called from the main loop at MAINLOOP when ERIC is midstride or just about to
; finish an action (such as firing the water pistol or bending over to catch a
; mouse).
MVERIC2:
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; (midstride) and location
  LD L,3                  ; A=ERIC's post-midstride/post-action animatory state
  LD A,(HL)               ;
  INC L                   ; Collect ERIC's post-midstride/post-action
  LD E,(HL)               ; coordinates in DE
  INC L                   ;
  LD D,(HL)               ;
; This entry point is used by the routines at FALLING2 and STEPPEDOFF with
; H=210 (ERIC).
MVERIC2_0:
  CALL UPDATEAS           ; Update ERIC's animatory state and location and
                          ; update the SRB
  CALL WALKSOUND          ; Make a sound effect
; This entry point is used by the routine at SRBSCROLL with H=210 (ERIC).
MVERIC2_1:
  LD L,0                  ; Point HL at byte 0 of ERIC's buffer
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  BIT 7,(HL)              ; Check the 'direction' bit of ERIC's animatory state
  LD L,1                  ; Byte 1 of ERIC's buffer holds his x-coordinate
  JR Z,MVERIC2_2          ; Jump if ERIC's facing left
; ERIC is facing right. Check whether the screen should be scrolled left.
  CP 160                  ; Return if the whole of the girls' skool is
  RET Z                   ; on-screen (no need to scroll)
  SUB (HL)                ; Return if ERIC is 1-21 spaces from the left edge of
  CP 235                  ; the screen (no need to scroll); note that this
  RET NC                  ; doesn't handle the case where ERIC's x-coordinate
                          ; is 0, which is a bug
  CALL UPDATESCR          ; Update the display
  JP LSCROLL8             ; Scroll the screen to the left
; ERIC is facing left. Check whether the screen should be scrolled right.
MVERIC2_2:
  AND A                   ; Is the far left wall of the boys' skool on-screen?
  RET Z                   ; Return if so (no need to scroll the screen)
  SUB (HL)                ; Is ERIC more than 9 character spaces from the left
  CP 247                  ; edge of the screen?
  RET C                   ; Return if so (no need to scroll the screen)
  CALL UPDATESCR          ; Update the display
  JP RSCROLL8             ; Scroll the screen to the right

; Unused
  DEFS 2

; Turn ERIC round
;
; Used by the routines at RIGHT, LEFT, UP, DOWN and ONPLANT.
TURNERIC:
  CALL WALKSOUND          ; Make a sound effect
  LD A,(KEYCODE)          ; Collect the keypress offset of the last keypress
                          ; from KEYCODE
  BIT 3,A                 ; Bit 3 of A is reset if an upper case key was
                          ; pressed
  LD HL,1024              ; H=4 (fast), L=0 (not midstride)
  JR Z,TURNERIC_0         ; Jump if an upper case (fast) key was pressed
  LD H,9                  ; 9=slow
TURNERIC_0:
  LD (ERICTIMER2),HL      ; Initialise ERIC's main action timer at ERICTIMER to
                          ; 4 (fast) or 9 (slow), and reset the
                          ; midstride/mid-action timer at ERICTIMER2 to 0
  LD H,210                ; 210=ERIC
  JP WALK_7               ; Turn ERIC round

; Unused
  DEFB 0

; Check whether a character is on a staircase
;
; Used by the routines at JUMP, HIT, RIGHT, LEFT, UP, DOWN, DROPSBOMB and SIT
; to check whether ERIC is on a staircase, and by the routines at NEXTMV and
; FINDERIC to check whether ERIC or a teacher is on a staircase. Returns with
; the carry flag reset if the character is not on a staircase. Otherwise
; returns with the carry flag set and A holding 0 (if the staircase goes up and
; to the left) or 128 (if the staircase goes up and to the right).
;
; H Character number (200-204, 210)
ONSTAIRS:
  LD H,210                ; 210=ERIC
; This entry point is used by the routines at NEXTMV and FINDERIC.
ONSTAIRS_0:
  LD L,1                  ; Collect the character's coordinates in DE
  LD E,(HL)               ;
  INC L                   ;
  LD D,(HL)               ;
  LD A,D                  ; A=character's y-coordinate
  CP 3                    ; Is the character on the top floor?
  RET Z                   ; Return with the carry flag reset if so
  CP 10                   ; Is the character on the middle floor?
  RET Z                   ; Return with the carry flag reset if so
  CP 17                   ; Is the character on the bottom floor?
  RET Z                   ; Return with the carry flag reset if so
  CP 14                   ; The assembly hall stage has y-coordinate 14
  JR NZ,ONSTAIRS_1        ; Jump if the character's y-coordinate doesn't match
                          ; that
  LD A,E                  ; A=character's x-coordinate
  CP 82                   ; This is the x-coordinate of the right-hand edge of
                          ; the assembly hall stage
  JR NC,ONSTAIRS_1        ; Jump if the character's to the right of this
  CP 76                   ; This is the x-coordinate of the left-hand edge of
                          ; the assembly hall stage
  RET NC                  ; Return with the carry flag reset if the character's
                          ; on the stage
; The character is not standing on the top floor, middle floor, bottom floor or
; assembly hall stage, so he must be on a staircase. Determine the orientation
; of the staircase.
ONSTAIRS_1:
  LD A,D                  ; A=character's y-coordinate
  CP 10                   ; Is the character somewhere between the middle and
                          ; bottom floors?
  JR NC,ONSTAIRS_4        ; Jump if so
  LD A,E                  ; A=character's x-coordinate
  CP 64                   ; Is the character on the stairs leading up to the
                          ; Revision Library?
  JR C,ONSTAIRS_3         ; Jump if so
ONSTAIRS_2:
  LD A,0                  ; Signal: character is on an up-and-to-the-left
                          ; staircase
  SCF                     ; Signal: character's on a staircase
  RET                     ; Return with the carry flag set and A=0
ONSTAIRS_3:
  LD A,128                ; Signal: character is on an up-and-to-the-right
                          ; staircase
  SCF                     ; Signal: character's on a staircase
  RET                     ; Return with the carry flag set and A=128
ONSTAIRS_4:
  CP 15
  LD A,E                  ; A=character's x-coordinate
  JR C,ONSTAIRS_5         ; Jump if the character is on or above the level of
                          ; the assembly hall stage
  CP 128                  ; Is the character on either the stairs leading down
                          ; from the stage or the stairs leading down from the
                          ; toilets in the boys' skool?
  JR C,ONSTAIRS_2         ; Jump if so
  JR ONSTAIRS_3           ; The character is on the stairs leading down to the
                          ; bottom floor in the girls' skool
ONSTAIRS_5:
  CP 64                   ; Is the character on either the stairs leading down
                          ; to the stage or the stairs leading down to the
                          ; bottom floor in the girls' skool?
  JR NC,ONSTAIRS_3        ; Jump if so
  LD A,0                  ; Signal: character is on an up-and-to-the-left
                          ; staircase (the one leading down from the toilets in
                          ; the boys' skool, in fact)
  RET                     ; Return with the carry flag set and A=0

; Deal with ERIC when he has stepped off the stage
;
; Used by the routine at CHKWALLS.
OFFSTAGE:
  LD HL,STATUS            ; Set bit 1 at STATUS, indicating to the routine at
  LD (HL),2               ; HANDLEERIC that it should inspect STATUS2 to see
                          ; how to deal with ERIC
  JP STEPOFF              ; Handle ERIC's descent to the floor

; Unused
;
; Code remnants.
  DEFB 8
  RET

; Check for walls and closed doors in front of ERIC
;
; Used by the routines at RIGHT and LEFT. Impedes ERIC's progress if he's
; confronted by an immovable object, or opens the Science Lab storeroom door
; (if ERIC has the key and is trying to go right at the relevant location).
; Also sends ERIC down the first step of the staircase leading down to the
; assembly hall stage, or begins his descent off the assembly hall stage (if
; ERIC is trying to go left at the relevant location).
;
; D ERIC's y-coordinate
; E x-coordinate of the spot in front of ERIC
CHKWALLS:
  LD A,E                  ; A=x-coordinate of the spot in front of ERIC
  CP 191                  ; Is ERIC facing the far right wall of the girls'
                          ; skool?
  RET NC                  ; Return if so (ERIC can't walk through walls)
  LD HL,DOORFLAGS         ; DOORFLAGS holds the doors flags
  LD A,D                  ; A=ERIC's y-coordinate
  CP 3                    ; Is ERIC on the top floor?
  JR NZ,CHKWALLS_3        ; Jump if not
; ERIC is on the top floor.
  LD A,E                  ; A=x-coordinate of the spot in front of ERIC
  CP 159                  ; Is ERIC facing the far wall of the top-floor room
                          ; in the girls' skool?
  RET Z                   ; Return if so
  CP 92                   ; Is ERIC facing the window on the top floor of the
                          ; boys' skool?
  RET Z                   ; Return if so
  CP 72                   ; Is ERIC facing the head's left study door?
  JR NZ,CHKWALLS_0        ; Jump if not
  BIT 0,(HL)              ; Is the head's left study door closed?
  RET Z                   ; Return if so
CHKWALLS_0:
  CP 83                   ; Is ERIC facing the head's right study door?
  JR NZ,CHKWALLS_1        ; Jump if not
  BIT 1,(HL)              ; Is the head's right study door closed?
  RET Z                   ; Return if so
; ERIC's path is not blocked, so let him move forward.
CHKWALLS_1:
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  LD C,E                  ; Copy the coordinates of the spot in front of ERIC
  LD B,D                  ; (his post-midstride coordinates) from DE to BC
  BIT 7,A                 ; Is ERIC facing right?
  JR NZ,CHKWALLS_2        ; Jump if so
  INC E
  INC E
CHKWALLS_2:
  DEC E                   ; Set E to ERIC's current x-coordinate (which will
                          ; also be his midstride x-coordinate)
  JP MVERIC1              ; Put ERIC midstride
; ERIC's not on the top floor. Is he on the middle floor?
CHKWALLS_3:
  CP 10                   ; Is ERIC on the middle floor?
  JR NZ,CHKWALLS_6        ; Jump if not
; ERIC is on the middle floor.
  LD A,E                  ; A=x-coordinate of the spot in front of ERIC
  CP 159                  ; Is ERIC facing the far wall of the middle-floor
                          ; classroom in the girls' skool?
  RET Z                   ; Return if so
  CP 94                   ; Is ERIC facing the window on the middle floor of
                          ; the boys' skool?
  RET Z                   ; Return if so
  CP 63                   ; Is ERIC facing the far wall of the Science Lab
                          ; storeroom?
  RET Z                   ; Return if so
  CP 84                   ; Is ERIC about to descend the stairs from the middle
                          ; floor to the stage?
  JR NZ,CHKWALLS_4        ; Jump if not
  INC E                   ; E=85 (ERIC's current x-coordinate)
  JP DOWN_2               ; Start moving ERIC down a stair
CHKWALLS_4:
  CP 54                   ; Is ERIC facing the Science Lab storeroom door?
  JR NZ,CHKWALLS_1        ; Jump if not
  BIT 2,(HL)              ; Is the Science Lab storeroom door closed?
  JP Z,OPENLABRM          ; Jump if so (and open it if ERIC has the key)
CHKWALLS_5:
  RET Z                   ; Return if the door/gate is (still) closed
  JR CHKWALLS_1           ; Otherwise move ERIC forward
; ERIC's not on the top floor or the middle floor. Is he on the bottom floor?
CHKWALLS_6:
  CP 17                   ; Is ERIC on the bottom floor?
  LD A,E                  ; A=x-coordinate of the spot in front of ERIC
  JR NZ,CHKWALLS_8        ; Jump if not
; ERIC is on the bottom floor.
  CALL STOPERIC           ; Let ERIC go no further if ALBERT's in his way
  LD A,E                  ; A=x-coordinate of the spot in front of ERIC
  CP 94                   ; Is ERIC facing the boys' skool door?
  JR NZ,CHKWALLS_7        ; Jump if not
  BIT 3,(HL)              ; Is the boys' skool door closed?
  RET Z                   ; Return if so
CHKWALLS_7:
  CP 133                  ; Is ERIC facing the skool gate?
  JR NZ,CHKWALLS_1        ; Jump if not
  BIT 4,(HL)              ; Is the skool gate closed?
  JR CHKWALLS_5           ; Jump back and return if so, or move ERIC forward if
                          ; not
; ERIC is on the assembly hall stage.
CHKWALLS_8:
  CP 82                   ; Is ERIC about to ascend the stairs from the stage
                          ; to the middle floor?
  JR NZ,CHKWALLS_9        ; Jump if not
  DEC E                   ; E=81 (ERIC's current x-coordinate)
  JP UP_2                 ; Start moving ERIC up a stair
CHKWALLS_9:
  CP 76                   ; Is ERIC about to jump off the stage?
  JP Z,OFFSTAGE           ; Jump if so to deal with ERIC's descent to the floor
  JR CHKWALLS_1           ; Otherwise move ERIC forward

; 'P' pressed - right
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'P' or
; '8' is pressed.
RIGHT:
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  BIT 7,A                 ; Is ERIC facing left?
  JP Z,TURNERIC           ; Turn ERIC round if so
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  JR C,RIGHT_1            ; Jump if so
; This entry point is used by the routines at UP and DOWN.
RIGHT_0:
  INC E                   ; E=x-coordinate of the spot in front of ERIC
  JP CHKWALLS             ; Check for walls and closed doors in ERIC's path
; ERIC is on a staircase.
RIGHT_1:
  RLA                     ; Does the staircase ERIC's on go up and to the
                          ; right?
  JP C,UP_2               ; Go up a stair if so
  JP DOWN_2               ; Otherwise go down a stair

; 'O' pressed - left
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'O' or
; '5' is pressed.
LEFT:
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  BIT 7,A                 ; Is ERIC facing right?
  JP NZ,TURNERIC          ; Turn ERIC round if so
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  JR C,LEFT_1             ; Jump if so
; This entry point is used by the routines at UP and DOWN.
LEFT_0:
  DEC E                   ; E=x-coordinate of the spot in front of ERIC
  JP CHKWALLS             ; Check for walls and closed doors in ERIC's path
; ERIC is on a staircase.
LEFT_1:
  RLA                     ; Does the staircase ERIC's on go down and to the
                          ; left?
  JP C,DOWN_2             ; Go down a stair if so
  JP UP_2                 ; Otherwise go up a stair

; 'Q' pressed - up
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'Q' or
; '7' is pressed.
UP:
  CALL ONSTAIRS           ; Check whether ERIC is on a staircase
  LD L,0                  ; Point HL at byte 0 of ERIC's buffer
  JR C,UP_10              ; Jump if ERIC's on a staircase
  BIT 7,(HL)              ; Check the 'direction' bit of ERIC's animatory state
  LD A,D                  ; A=ERIC's y-coordinate
  JR Z,UP_7               ; Jump if ERIC's facing left
  CP 10                   ; Is ERIC on the middle floor?
  JR Z,UP_6               ; Jump if so
  CP 14                   ; Is ERIC on the assembly hall stage?
  JR Z,UP_5               ; Jump if so
  CP 17                   ; Is ERIC on the bottom floor?
UP_0:
  JP NZ,RIGHT_0           ; Move ERIC one space to the right if not
  LD A,E                  ; A=ERIC's x-coordinate
  CP 176                  ; This is the x-coordinate of the bottom of the
                          ; stairs leading up to the middle floor of the girls'
                          ; skool
UP_1:
  JR NZ,UP_0              ; Jump if ERIC is not at that point
; This entry point is used by the routines at CHKWALLS, RIGHT and LEFT.
UP_2:
  DEC D                   ; Up a stair
  LD B,D                  ; B=ERIC's post-midstride y-coordinate
; This entry point is used by the routine at DOWN.
UP_3:
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  LD C,E                  ; C=ERIC's current x-coordinate
  BIT 7,A                 ; Is ERIC facing left?
  JR Z,UP_4               ; Jump if so
  INC C
  INC C
UP_4:
  DEC C                   ; C=ERIC's post-midstride x-coordinate
  JP MVERIC1              ; Put ERIC midstride
; ERIC is on the assembly hall stage, facing right.
UP_5:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 81                   ; This is the x-coordinate of the bottom of the
                          ; stairs leading up from the stage to the middle
                          ; floor
  JR UP_1                 ; Move ERIC up a stair if he's here
; ERIC is on the middle floor, facing right.
UP_6:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 19                   ; This is the x-coordinate of the bottom of the
                          ; stairs leading up to the Revision Library
  JR UP_1                 ; Move ERIC up a stair if he's here
; ERIC is facing left.
UP_7:
  CP 10                   ; Is ERIC on the middle floor?
  JR NZ,UP_9              ; Jump if not
  LD A,E                  ; A=ERIC's x-coordinate
  CP 91                   ; This is the x-coordinate of the bottom of the
                          ; stairs leading up to the head's study
  JR Z,UP_2               ; Move ERIC up a stair if he's here
  CP 186                  ; This is the x-coordinate of the bottom of the
                          ; stairs leading up to the top floor of the girls'
                          ; skool
UP_8:
  JP NZ,LEFT_0            ; Move ERIC one space left if he's not here
  JR UP_2                 ; Otherwise move him up a stair
UP_9:
  CP 17                   ; Is ERIC on the bottom floor?
  JR NZ,UP_8              ; Jump if not
  LD A,E                  ; A=ERIC's x-coordinate
  CP 22                   ; This is the x-coordinate of the bottom of the
                          ; stairs leading up to the middle floor at the far
                          ; left of the boys' skool
  JR Z,UP_2               ; Move ERIC up a stair if he's here
  CP 83                   ; This is the x-coordinate of the bottom of the
                          ; stairs leading up to the assembly hall stage
  JR UP_8                 ; Move ERIC up a stair if he's here
; ERIC is on a staircase.
UP_10:
  XOR (HL)                ; Set the carry flag if ERIC is facing the wrong way
  RLA                     ; to ascend the stairs
  JP C,TURNERIC           ; Turn ERIC round if he's facing the wrong way
  JR UP_2                 ; Make ERIC go up a stair otherwise

; 'A' pressed - down
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'A' or
; '6' is pressed.
DOWN:
  CALL ONSTAIRS           ; Check whether ERIC is on a staircase
  LD L,0                  ; Point HL at byte 0 of ERIC's buffer
  JR C,DOWN_9             ; Jump if ERIC's on a staircase
  BIT 7,(HL)              ; Check the 'direction' bit of ERIC's animatory state
  LD A,D                  ; A=ERIC's y-coordinate
  JR Z,DOWN_5             ; Jump if ERIC is facing left
  CP 3                    ; Is ERIC on the top floor?
  JR Z,DOWN_4             ; Jump if so
  CP 10                   ; Is ERIC on the middle floor?
  JR Z,DOWN_3             ; Jump if so
  CP 14                   ; Is ERIC on the assembly hall stage?
DOWN_0:
  JP NZ,RIGHT_0           ; Move ERIC one space to the right if not
  LD A,E                  ; A=ERIC's x-coordinate
  CP 80                   ; This is the x-coordinate of the top of the stairs
                          ; leading from the stage to the bottom floor
DOWN_1:
  JR NZ,DOWN_0            ; Jump if ERIC is not here
; This entry point is used by the routines at CHKWALLS, RIGHT and LEFT.
DOWN_2:
  LD B,D                  ; B=ERIC's post-midstride y-coordinate (down a stair)
  INC B                   ;
  JP UP_3                 ; Move ERIC one space right or left too
; ERIC is on the middle floor, facing right.
DOWN_3:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 15                   ; This is the x-coordinate of the top of the stairs
                          ; leading down to the bottom floor at the far left of
                          ; the boy's skool
  JR DOWN_1               ; Move ERIC down a stair if he's here
; ERIC is on the top floor, facing right.
DOWN_4:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 84                   ; This is the x-coordinate of the top of the stairs
                          ; just outside the head's study
  JR Z,DOWN_2             ; Move ERIC down a stair if he's here
  CP 179                  ; This is the x-coordinate of the top of the stairs
                          ; leading down to the middle floor in the girls'
                          ; skool
  JR DOWN_1               ; Move ERIC down a stair if he's here
; ERIC is facing left.
DOWN_5:
  CP 10                   ; Is ERIC on the middle floor?
  JR Z,DOWN_8             ; Jump if so
  CP 3                    ; Is ERIC on the top floor?
DOWN_6:
  JP NZ,LEFT_0            ; Move ERIC one space left if not
  LD A,E                  ; A=ERIC's x-coordinate
  CP 26                   ; This is the x-coordinate of the top of the stairs
                          ; leading down from the Revision Library
DOWN_7:
  JR NZ,DOWN_6            ; Move ERIC one space left if he's not here
  JR DOWN_2               ; Otherwise move him down a stair
; ERIC is on the middle floor, facing left.
DOWN_8:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 85                   ; This is the x-coordinate of the top of the stairs
                          ; leading down to the stage
  JR Z,DOWN_2             ; Move ERIC down a stair if he's here
  CP 183                  ; This is the x-coordinate of the top of the stairs
                          ; leading down to the bottom floor in the girls'
                          ; skool
  JR DOWN_7               ; Move ERIC down a stair if he's here
; ERIC is on a staircase.
DOWN_9:
  XOR (HL)                ; Reset the carry flag if ERIC is facing the wrong
  RLA                     ; way to descend the stairs
  JP NC,TURNERIC          ; Turn ERIC round if he's facing the wrong way
  JR DOWN_2               ; Otherwise move him down a stair

; Unused
  DEFB 0

; Collect the identifier and coordinates of a blackboard that will be wiped
;
; Used by the routine at WIPE to collect information about the blackboard that
; is going to be wiped by a teacher. Returns with B holding the blackboard
; identifier (the LSB of BRBRDBUF, YRBRDBUF, TFBRDBUF, SLBRDBUF or MFBRDBUF),
; and DE holding the coordinates of the top left-hand square of the board.
;
; H Teacher's character number (201-204)
BOARDID2:
  LD A,12                 ; Change the instruction at BOARDDIST to ADD A,12
  LD (26477),A            ; (for the purpose of this routine, a teacher is
                          ; considered to be standing next to a blackboard if
                          ; he's within 12 spaces to the right of its left
                          ; edge)
  CALL BOARDID            ; Collect information about the blackboard the
                          ; teacher is standing next to
  LD A,4                  ; Change the instruction at BOARDDIST back to ADD A,4
  LD (26477),A            ;
  RET

; Check for walls, closed doors and ALBERT in front of the bike
;
; Used by the routines at WHEELBIKE (when the bike is travelling of its own
; accord, or ERIC is standing on the saddle) and RIDINGBIKE (when ERIC is
; sitting on the bike). Returns with the zero flag set if a wall or closed door
; lies in the bike's path, or the carry flag set if ALBERT is impeding the
; bike's progress.
;
; A ERIC's or the bike's animatory state
; E ERIC's or the bike's x-coordinate
BIKEPATH:
  RLCA                    ; Set the carry flag if the bike is facing right
  LD HL,DOORFLAGS         ; DOORFLAGS holds the doors flags
  LD A,E                  ; A=ERIC's/bike's x-coordinate
  JR NC,BIKEPATH_6        ; Jump if the bike is facing left
; The bike is travelling to the right.
  CP 189                  ; Has the bike hit the far right wall of the girls'
                          ; skool?
  RET Z                   ; Return with the zero flag set if so
  CP 93                   ; Is the boys' skool door in front of the bike?
  JR NZ,BIKEPATH_2        ; Jump if not
BIKEPATH_0:
  BIT 3,(HL)              ; Is the boys' skool door open?
  JR NZ,BIKEPATH_5        ; Jump if so
BIKEPATH_1:
  CP A                    ; Return with the zero flag set if the bike has hit
  RET                     ; the boys' skool door or the skool gate
BIKEPATH_2:
  CP 132                  ; Is the skool gate in front of the bike?
  JR NZ,BIKEPATH_4        ; Jump if not
BIKEPATH_3:
  BIT 4,(HL)              ; Is the skool gate closed?
  JR Z,BIKEPATH_1         ; Jump if so
  JR BIKEPATH_5
; Neither the skool door nor the skool gate is in the way. What about ALBERT?
BIKEPATH_4:
  LD A,(ALBERTCBUF)       ; A=ALBERT's animatory state
  CP 127                  ; 127: Is ALBERT facing left with his arm up?
  JR NZ,BIKEPATH_5        ; Jump if not
  LD A,(52481)            ; A=ALBERT's x-coordinate
  SUB 2                   ; A=x-coordinate of the spot 2 spaces in front of
                          ; ALBERT
  CP E                    ; Is the bike here?
  JR NZ,BIKEPATH_5        ; Jump if not
  SCF                     ; Signal: ALBERT is stopping the bike
  RET
; No obstruction lies in the bike's path.
BIKEPATH_5:
  LD A,L                  ; Set A to a non-zero value
  AND A                   ; Return with the zero and carry flags reset
  RET                     ;
; The bike is travelling to the left.
BIKEPATH_6:
  CP 1                    ; Has the bike hit the far left wall of the boys'
                          ; skool?
  RET Z                   ; Return with the zero flag set if so
  CP 96                   ; Is the boys' skool door in front of the bike?
  JR Z,BIKEPATH_0         ; Jump back if so to check whether it's open
  CP 135                  ; Is the skool gate in front of the bike?
  JR Z,BIKEPATH_3         ; Jump back if so to check whether it's open
  AND A                   ; Return with the zero and carry flags reset
  RET                     ;

; Unused
  DEFB 0

; Open or close a door or window
;
; Used by the routines at CHRMVDOOR, SHUTDOORS and OPENLABRM. On entry, A holds
; the door/window identifier:
;
; +-----+----------------------------+
; | ID  | Door/window                |
; +-----+----------------------------+
; | 1   | Left study door            |
; | 2   | Right study door           |
; | 4   | Science Lab storeroom door |
; | 8   | Boys' skool door           |
; | 16  | Skool gate                 |
; | 32  | Drinks cabinet door        |
; | 64  | Top floor window           |
; | 128 | Middle floor window        |
; +-----+----------------------------+
;
; A Door/window identifier (see table above)
; B 0 (close it) or 1 (open it)
MVDOORWIN:
  PUSH HL
  LD HL,DOORFLAGS         ; DOORFLAGS holds the doors flags
  LD C,A                  ; Copy the door identifier to C
  LD A,(HL)               ; Pick up the doors flags in A
  OR C
  DEC B                   ; Are we opening the door/window?
  JR Z,MVDOORWIN_0        ; Jump if so
  XOR C
MVDOORWIN_0:
  LD (HL),A               ; Restore the doors flags with the appropriate bit
                          ; set or reset
  LD A,C                  ; A=door identifier
  LD HL,57685             ; Point HL at the appropriate entry in the doors and
MVDOORWIN_1:
  INC L                   ; windows table at DWURTINIT
  INC L                   ;
  RRCA                    ;
  JR NC,MVDOORWIN_1       ;
  LD A,(HL)               ; Pick up the MSB of the entry in A
  ADC A,B                 ; Add 1 if opening the door/window
  DEC L                   ; Point HL at the LSB of the entry
  LD L,(HL)               ; Pick this up in L
  LD H,A                  ; Now HL=address from the doors and windows table
                          ; (+256 if opening)
  CALL ALTERUDGS          ; Alter UDG references in the play area and update
                          ; the SRB for the newly opened or closed door/window
  POP HL
  RET

; Check for a closed door in front of a character
;
; Used by the routines at NEXTMV and DOOROPEN. Returns with the carry flag set
; if the character is facing a closed door, and C holding the door identifier:
;
; +----+----------------------------+
; | ID | Door                       |
; +----+----------------------------+
; | 1  | Left study door            |
; | 2  | Right study door           |
; | 4  | Science Lab storeroom door |
; | 8  | Boys' skool door           |
; | 16 | Skool gate                 |
; +----+----------------------------+
;
; H Character number (183-210)
CHKDOOR:
  LD L,0                  ; Pick up the character's animatory state in A
  LD A,(HL)               ;
  RLCA                    ; Set A=-1 if the character is facing left, 1 if
  CCF                     ; facing right
  SBC A,A                 ;
  ADD A,A                 ;
  INC A                   ;
  INC L                   ; Byte 1 of the character's buffer holds his
                          ; x-coordinate
  ADD A,(HL)              ; Add this to A
  LD E,A                  ; E=x-coordinate of the location directly in front of
                          ; the character
  INC L                   ; L=2
  LD D,(HL)               ; Pick up the character's y-coordinate in D
  PUSH HL
  LD HL,DOORLOCS          ; DOORLOCS is the address of the door location table
  LD BC,1281              ; B=5 (there are 5 doors), C=1 (the left study door
                          ; is the first one)
CHKDOOR_0:
  LD A,E                  ; A=x-coordinate of the location directly in front of
                          ; the character
  CP (HL)                 ; Compare this with the x-coordinate of the door
  INC HL
  JR NZ,CHKDOOR_1         ; Jump if the x-coordinates don't match
  LD A,D                  ; A=character's y-coordinate
  CP (HL)                 ; Compare this with the y-coordinate of the door
  JR Z,CHKDOOR_2          ; Jump if they match (the character is standing at
                          ; this door)
CHKDOOR_1:
  INC HL
  RLC C                   ; Move the set bit in C one place to the left
  DJNZ CHKDOOR_0          ; Jump back to consider the remaining doors
  POP HL
  XOR A                   ; Reset the carry flag: there is no door in front of
                          ; the character
  RET
; The character is standing in front of a door.
CHKDOOR_2:
  LD A,(DOORFLAGS)        ; Pick up the doors flags from DOORFLAGS
  AND C                   ; Check the bit corresponding to the door
  POP HL
  RET NZ                  ; Return with the carry flag reset if the door is
                          ; open
  SCF                     ; Set the carry flag: the door is closed
  RET

; Unused
  DEFS 2

; Check for a closed door in a character's path and open it if allowed
;
; Used by the routine at WALK. Checks for a closed door in the character's path
; and makes him open it if he's an adult.
;
; H Character number (183-209)
DOOROPEN:
  CALL CHKDOOR            ; Check for closed doors in the character's path
  JP NC,UPDATESRB         ; Update the SRB for the character's current
                          ; animatory state and location if no doors need
                          ; opening
  LD A,H                  ; A=character number
  SUB 200                 ; Are we dealing with a teacher or ALBERT (i.e.
  CP 6                    ; someone who can open doors)?
  JR C,DOOROPEN_0         ; Jump if so
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  XOR 128                 ; Characters who can't open doors have bit 7 of their
                          ; animatory state flipped (i.e. they are turned
                          ; around)
  RET                     ; Return to place the character midstride
; The character we're dealing with is confronted by a closed door and is able
; to open it.
DOOROPEN_0:
  POP DE                  ; Drop the return address; we will handle updating
                          ; the SRB and setting the character's new animatory
                          ; state here instead
; This entry point is used by the routine at FINDERIC.
DOOROPEN_1:
  LD L,19                 ; Place the identifier of the closed door (see
  LD (HL),C               ; CHKDOOR) into byte 19 of the character's buffer
  INC L                   ; L=20
  LD (HL),1               ; Signal: open the door
; This entry point is used by the routines at MOVEDOOR and RSTDROPEN.
DOOROPEN_2:
  LD L,18                 ; Place the address of the uninterruptible subcommand
  LD (HL),112             ; routine at CHRMVDOOR into bytes 17 and 18 of the
  DEC L                   ; character's buffer
  LD (HL),183             ;
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  OR 7                    ; A=animatory state of the character with his arm up
                          ; (to open the door)
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Make a character open or close a door
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of the character's buffer by the routine at DOOROPEN.
;
; H Character number (200-205)
CHRMVDOOR:
  LD L,18                 ; Remove the address of this routine from bytes 17
  LD (HL),0               ; and 18 of the character's buffer
  INC L                   ; L=19
  LD A,(HL)               ; A=door identifier (see MVDOORWIN)
  INC L                   ; L=20
  LD B,(HL)               ; B=0 if the character is going to close the door, or
                          ; 1 if he's going to open it
; This entry point is used by the routine at TOWINDOW.
CHRMVDOOR_0:
  CALL MVDOORWIN          ; Open or close the door
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  AND 248                 ; A=animatory state of the character with his arm
                          ; down
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; Unused
  DEFS 3

; Close any temporarily open doors if necessary
;
; Used by the routine at MAINLOOP2. Checks the left study door, right study
; door, and Science Lab storeroom door, and closes any that are open (provided
; nobody is standing in the doorway).
SHUTDOORS:
  LD A,(LINESDELAY1)      ; Return unless LINESDELAY1 (which is decremented on
  CP 4                    ; every pass through the main loop, and cycles
  RET NZ                  ; through the values 1-15) holds 4 at the moment
  LD A,(DOORFLAGS)        ; DOORFLAGS holds the doors flags
  LD HL,318               ; H=1 (left study door), L=62
  BIT 0,A                 ; Is the left study door open?
  CALL NZ,SHUTDOORS_0     ; If so, close it if possible
  LD HL,576               ; H=2 (right study door), L=64
  BIT 1,A                 ; Is the right study door open?
  CALL NZ,SHUTDOORS_0     ; If so, close it if possible
  BIT 2,A                 ; Is the Science Lab storeroom door open?
  RET Z                   ; Return if not
  LD HL,1090              ; H=4 (Science Lab storeroom door), L=66
; At this point H holds the identifier of a door that is open, and L holds the
; LSB of the address of the entry in the table at DOORLOCS containing the
; location of the door.
SHUTDOORS_0:
  PUSH HL                 ; Save the door identifier briefly
  LD H,185                ; Point HL at the door location table entry
  LD A,(HL)               ; A=x-coordinate of the door
  INC L
  LD D,(HL)               ; D=y-coordinate of the door
  DEC A                   ; Set C=X-1 and E=X+2, where X is the x-coordinate of
  LD C,A                  ; the door; any character whose x-coordinate is in
  ADD A,3                 ; this range will prevent the door from closing
  LD E,A                  ;
  LD HL,46849             ; L=1, H=183 (little girl no. 1)
  LD B,32                 ; 32 game characters (183-214)
SHUTDOORS_1:
  LD A,(HL)               ; A=character's x-coordinate
  CP C                    ; Is the character to the left of the door?
  JR C,SHUTDOORS_2        ; Jump if so
  CP E                    ; Is the character to the right of the door?
  JR NC,SHUTDOORS_2       ; Jump if so
  INC L                   ; L=2
  LD A,D                  ; A=y-coordinate of the door
  SUB (HL)                ; Subtract the character's y-coordinate
  DEC L                   ; L=1
  CP 2                    ; Is the character standing in the doorway?
  JR C,SHUTDOORS_3        ; Jump if so
SHUTDOORS_2:
  INC H                   ; Next character
  DJNZ SHUTDOORS_1        ; Jump back until all 32 characters have been checked
  AND A                   ; Reset the carry flag: no one is standing in the
                          ; doorway
SHUTDOORS_3:
  POP HL                  ; Restore the door identifier to H
  LD A,H                  ; Copy it to A
  CALL NC,MVDOORWIN       ; Close the door if no one's in the way
  LD A,(DOORFLAGS)        ; Pick up the doors flags in A before returning
                          ; (ignored by the caller)
  RET

; Score 100 points and make the sound of a mouse being caught
;
; Used by the routine at CATCHMORF.
;
; A 10
CATCHMOUSE:
  CALL ADDPTS             ; Add 100 to the score and print it
  LD L,3                  ; Three squeaks
CATCHMOUSE_0:
  LD A,4                  ; Set the squeak sound effect parameters: A=4 (green
  LD BC,6674              ; border); B=26 (pitch); C=18 (alternating yellow
  LD D,6                  ; border); D=6 (duration)
  CALL SNDEFFECT          ; Squeak once
  LD D,60                 ; Pause briefly
CATCHMOUSE_1:
  DEC DE                  ;
  LD A,D                  ;
  OR E                    ;
  JR NZ,CATCHMOUSE_1      ;
  DEC L                   ; Next squeak
  JR NZ,CATCHMOUSE_0      ; Jump back until all 3 squeaks have been done
  RET

; Make a female character stop jumping
;
; Used by the routine at GIRLJPING1.
;
; H Character number (183-189, 204 or 209)
STOPJPING:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  LD A,252                ; Determine which floor the character is currently on
STOPJPING_0:
  ADD A,7                 ; or above (she may be on a chair or still in the
  CP D                    ; air)
  JR C,STOPJPING_0        ;
  LD D,A                  ; D=3, 10 or 17 (top, middle or bottom floor)
  JP KNOCKED_1            ; Restore the character's original animatory state
                          ; and put her feet back on the floor

; 'D' pressed - drop stinkbomb
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'D' or
; 'U' is pressed.
DROPSBOMB:
  LD A,(INVENTORY)        ; INVENTORY holds the inventory flags
  RLCA                    ; Does ERIC have any stinkbombs?
  RET NC                  ; Return if not
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  RET C                   ; Return if so
  CALL PREPSBBUF          ; If buffer 213 (normally used by BOY WANDER's
                          ; pellet) is available, prepare it and return here
  LD A,7                  ; 7: animatory state of ERIC dropping a stinkbomb
                          ; (arm up)
  LD HL,CLOUD             ; Routine at CLOUD: deal with a dropped stinkbomb
  JP CATCH_0              ; Update ERIC's animatory state and drop the
                          ; stinkbomb

; Make a walking sound effect (blue border)
;
; Used by the routines at COUNTDOWN, MVERIC1, MVERIC2, TURNERIC, MOUNTBIKE,
; ERICSITLIE, SITDOWN, STEPOFF and STEPPEDOFF.
WALKSOUND:
  PUSH BC
  LD A,1                  ; The border will be blue
; This entry point is used by the routine at WALKSOUNDY with A=6 (yellow
; border).
WALKSOUND_0:
  LD C,7                  ; Initialise the sound effect duration counter
WALKSOUND_1:
  XOR 16                  ; Produce the sound effect
  OUT (254),A             ;
  LD B,192                ;
WALKSOUND_2:
  DJNZ WALKSOUND_2        ;
  DEC C                   ;
  JR NZ,WALKSOUND_1       ;
  POP BC
  RET

; Prevent ERIC from getting past ALBERT if necessary
;
; Used by the routine at CHKWALLS. Impedes ERIC's progress if ALBERT is
; standing right in front of him with his arm up.
;
; E x-coordinate of the spot in front of ERIC
STOPERIC:
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  RLCA                    ; Is ERIC facing left?
  RET NC                  ; Return if so
  LD A,(ALBERTCBUF)       ; A=ALBERT's animatory state
  CP 127                  ; 127: Is ALBERT facing left with his arm up?
  RET NZ                  ; Return if not
  LD A,(52481)            ; A=ALBERT's x-coordinate
  SUB 1
  CP E                    ; Is ERIC standing in front of ALBERT (facing him)?
  RET NZ                  ; Return if not
  POP BC                  ; Drop the return address, preventing any forward
                          ; movement by ERIC
  RET                     ; Return to the main loop

; Unused
  DEFS 2

; Make a character open or close a door
;
; Used by command lists 22, 24, 26, 28, 76 and 78. This routine works on two
; parameters. First the door identifier:
;
; +----+---------------------+----------------+
; | ID | Door                | Command lists  |
; +----+---------------------+----------------+
; | 8  | Boys' skool door    | 76, 78         |
; | 16 | Skool gate          | 76, 78         |
; | 32 | Drinks cabinet door | 22, 24, 26, 28 |
; +----+---------------------+----------------+
;
; And then the action identifier:
;
; +---+----------+
; | 0 | Close it |
; | 1 | Open it  |
; +---+----------+
;
; H 204 (MISS TAKE) or 205 (ALBERT)
MOVEDOOR:
  LD L,3                  ; Replace the address of this routine in bytes 3 and
  LD (HL),133             ; 4 of the character's buffer with MOVEDOOR0 (below)
  LD L,19                 ; Copy the door identifier and open/close parameter
  CALL GETPARAMS          ; from the command list into bytes 19 and 20 of the
                          ; character's buffer
MOVEDOOR0:
  LD L,19
  LD A,(HL)               ; A=door identifier
  CP 8                    ; Is it the boys' skool door?
  JR NZ,MVBIKEGATE        ; Jump if not
  LD A,(54017)            ; A=x-coordinate of the bike
  CP 94                   ; Is the bike in the boys' skool?
  JR C,MOVEDOOR_1         ; Jump if so
  LD DE,4448              ; (E,D)=(96,17)
; This entry point is used by the routine at MVBIKEGATE with (E,D)=(136,17):
MOVEDOOR_0:
  CP E                    ; Set the carry flag if the bike is blocking the way
  LD A,(BIKECBUF)         ; A=bike's animatory state
  CALL C,MVBIKE           ; Move the bike to the location in DE if it's
                          ; blocking the boys' skool door or the skool gate
  LD H,205                ; 205=ALBERT
; This entry point is used by the routine at MVBIKEGATE.
MOVEDOOR_1:
  LD L,4                  ; Remove the address of this routine from bytes 3 and
  LD (HL),0               ; 4 of the character's buffer
  JP DOOROPEN_2           ; Open or close the door

; Unused
  DEFS 7

; Move the bike if it's obstructing the skool gate
;
; This is a branch of the routine at MOVEDOOR.
;
; A 8 (boys' skool door) or 16 (skool gate)
MVBIKEGATE:
  CP 16                   ; Is the skool gate being opened or closed?
  JR NZ,MOVEDOOR_1        ; Jump if not
  LD A,(54017)            ; A=bike's x-coordinate
  CP 133                  ; Is the bike to the left of the skool gate?
  JR C,MOVEDOOR_1         ; Jump if so
  LD DE,4488              ; E=136, D=17
  JR MOVEDOOR_0           ; Move the bike if it's obstructing the skool gate

; Unused
  DEFB 0

; Get the ASCII code of the last key pressed
;
; Used by the routines at WRITING, KEYOFFSET, ERICSITLIE, GETINPUT and
; EXITDEMO. Returns with the zero flag set if no key of interest was pressed.
; Otherwise returns with A holding the ASCII code of the last key pressed.
READKEY:
  LD A,(KEMPSTON)         ; A=0 if we're using the keyboard, 1 if using
                          ; Kempston
  AND A                   ; Are we using the keyboard?
  JR Z,READKEY_1          ; Jump if so
  IN A,(31)
  AND 31                  ; Any input from the joystick?
  JR Z,READKEY_1          ; Jump if not
  LD L,80                 ; 80='P' (RIGHT)
  RRCA
  JR C,READKEY_0          ; Jump if the joystick was moved right
  DEC L                   ; L=79='O' (LEFT)
  RRCA
  JR C,READKEY_0          ; Jump if the joystick was moved left
  LD L,65                 ; 65='A' (DOWN)
  RRCA
  JR C,READKEY_0          ; Jump if the joystick was moved down
  LD L,81                 ; 81='Q' (UP)
  RRCA
  JR C,READKEY_0          ; Jump if the joystick was moved up
  LD L,102                ; 102='f' (fire)
READKEY_0:
  LD A,L                  ; Pass the appropriate character code to A
  AND A                   ; Reset the zero flag (we have input)
  RET
; This entry point is used by the startup routines at CHANGENAME, GETNAMES and
; INPUTDEV2.
READKEY_1:
  LD HL,23611             ; Point HL at the system variable FLAGS
  BIT 5,(HL)              ; Check the keypress flag
  RES 5,(HL)              ; Reset the flag ready for the next keypress
  RET Z                   ; Return if no key was pressed
  LD A,(23560)            ; Collect the ASCII code of the key last pressed
  CP 13                   ; Was it ENTER?
  JR Z,READKEY_2          ; Jump if so
  CP 32                   ; Was it a control character?
  JR C,READKEY_3          ; Jump if so
READKEY_2:
  CP 128                  ; Was it an extended character?
  RET C                   ; Return if not
; This entry point is used by the routine at READKEY.
READKEY_3:
  XOR A                   ; Set the zero flag to indicate that no (relevant)
                          ; key was pressed
  RET

; Get the keypress offset of the last key pressed
;
; Used by the routines at RIDINGBIKE, ONSADDLE, GETINPUT and ONPLANT. Returns
; with the zero flag set if no game keys were pressed. Otherwise returns with A
; holding the value from the keypress offset table corresponding to the last
; key pressed. This offset (an even number from 80 to 120) points at an entry
; in the keypress address table at K_LEFTF. Each entry in that table is the
; address of the routine for handling a keypress.
KEYOFFSET:
  CALL READKEY            ; Collect the ASCII code of the last key pressed in A
  RET Z                   ; Return if no key was pressed
  SUB 48                  ; We're only interested in keys with codes 48 onwards
  JR C,READKEY_3          ; Jump if none of these was pressed
  LD L,A                  ; Point HL at the relevant entry in the keypress
  LD H,229                ; offset table at KEYTABLE
  LD A,(HL)               ; Pick up the offset from that table
  AND A                   ; Set the zero flag if the offset is 0 (i.e. not a
                          ; game key)
  RET

; Unused
  DEFS 2

; Control the bike when ERIC's not sitting on the saddle
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of the bike's buffer by the routine at RIDINGBIKE. It controls the
; bike when it is wheeling along on its own (after ERIC has dismounted), or
; when ERIC is standing on the saddle.
WHEELBIKE:
  LD HL,BIKEP             ; BIKEP holds the bike's momentum
  LD A,(HL)               ; Pick this up in A
  SUB 7                   ; Is it time for the bike to come to rest?
  JR NC,WHEELBIKE_1       ; Jump if not
; It's time for the bike to come to rest.
  LD L,237                ; HL=STATUS2 (ERIC's status flags)
  BIT 7,(HL)              ; Is ERIC standing on the saddle of the bike?
  JR Z,WHEELBIKE_0        ; Jump if not
  LD (HL),16              ; Set bit 4 at STATUS2: ERIC is falling and will not
                          ; land on his feet
WHEELBIKE_0:
  LD H,211                ; 211=bike
  CALL UPDATESRB          ; Update the SRB for the bike's current animatory
                          ; state and location
  DEC A                   ; A=animatory state of the bike resting on the floor
  LD L,18                 ; Remove the address of this routine from bytes 17
  LD (HL),0               ; and 18 of the bike's buffer
  JP UPDATEAS             ; Update the bike's animatory state and update the
                          ; SRB
; The bike still has some momentum left. Check whether it has hit anything.
WHEELBIKE_1:
  LD (HL),A               ; Set the bike's remaining momentum at BIKEP
  LD HL,(BIKECBUF)        ; E=bike's x-coordinate, A=bike's animatory state
  LD A,L                  ;
  LD E,H                  ;
  CALL BIKEPATH           ; Check for walls, closed doors or ALBERT in the
                          ; bike's path
  LD L,237                ; HL=STATUS2 (ERIC's status flags)
  JR NC,WHEELBIKE_2       ; Jump if ALBERT's not impeding the bike's progress
  BIT 7,(HL)              ; Is ERIC standing on the saddle of the bike?
  JR Z,WHEELBIKE_3        ; Jump if not
  LD (HL),16              ; Set bit 4: ERIC is falling and will not land on his
                          ; feet
WHEELBIKE_2:
  JR NZ,WHEELBIKE_3       ; Jump if there are no walls or closed doors in the
                          ; bike's path
  BIT 7,(HL)              ; Is ERIC standing on the saddle of the bike?
  JR Z,WHEELBIKE_0        ; Jump if not
  LD (HL),64              ; Set bit 6: ERIC has hit a wall or closed door while
                          ; standing on the saddle of the bike
  JR WHEELBIKE_0
WHEELBIKE_3:
  LD H,211                ; 211=bike
  CALL WHEELBIKE_5        ; Move the bike and one space forward and update the
                          ; SRB
  LD A,(STATUS2)          ; STATUS2 holds ERIC's status flags
  RLCA                    ; Is ERIC standing on the saddle of the bike?
  RET NC                  ; Return if not
; This entry point is used by the routine at RIDINGBIKE.
WHEELBIKE_4:
  LD H,210                ; 210=ERIC
; This entry point is used by the routine at PELLET.
WHEELBIKE_5:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  RLCA
  RRCA
  JR C,WHEELBIKE_6        ; Jump if the character is facing right
  DEC E
  DEC E
WHEELBIKE_6:
  INC E                   ; Now E=x-coordinate of the spot in front of the
                          ; character
  JP SRBSCROLL            ; Update the character's location, update the SRB,
                          ; and scroll the screen if necessary

; Unused
  DEFB 0

; Deal with ERIC when he's riding the bike
;
; Called by the routine at HANDLEERIC when bit 0 at STATUS2 is set (by the
; routine at ONSADDLE). Listens for and responds appropriately to keypresses
; while ERIC is sitting on the saddle of the bike.
RIDINGBIKE:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  JR NZ,RIDINGBIKE_5      ; Jump if not
  INC (HL)                ; Set ERIC's action timer to 1, ensuring that we pass
                          ; through the following section of code on the next
                          ; call to this routine if no pedalling is detected
                          ; this time (if 'left' or 'right' is pressed, the
                          ; action timer will be reset to 5)
  CALL KEYOFFSET          ; A=value from the keypress offset table
                          ; corresponding to the key just pressed (if any)
  LD HL,LBIKEKEY          ; LBIKEKEY holds the offset of the last key pressed
                          ; while ERIC was riding the bike
  JR Z,RIDINGBIKE_5       ; Jump if no keys were pressed since the last check
  SET 3,A                 ; We don't distinguish between fast (upper case) and
                          ; slow (lower case) keypresses while ERIC is riding
                          ; the bike
  CP 88                   ; Was 'left' pressed?
  JR Z,RIDINGBIKE_0       ; Jump if so
  CP 90                   ; Was 'right' pressed?
  JR NZ,RIDINGBIKE_2      ; Jump if not
RIDINGBIKE_0:
  CP (HL)                 ; Was the key just pressed the same as the last one
                          ; pressed?
  JR Z,RIDINGBIKE_5       ; Jump if so
; The 'left' key was pressed after a non-left key, or the 'right' key was
; pressed after a non-right key. In other words, ERIC pedalled.
  LD (HL),A               ; Store the offset of the key just pressed in
                          ; LBIKEKEY
  LD L,243                ; HL=ERICTIMER (ERIC's main action timer)
  LD (HL),5               ; Reset this to 5
  LD L,240                ; HL=BIKEP (bike momentum)
  LD A,(HL)               ; Increase the bike's momentum, but not beyond 255
  ADD A,12                ;
  JR NC,RIDINGBIKE_1      ;
  LD A,255                ;
RIDINGBIKE_1:
  LD (HL),A               ;
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  XOR 1                   ; Now A=ERIC's new animatory state (12, 13, 140 or
                          ; 141)
  CALL UPDATEAS           ; Update ERIC's animatory state and update the SRB
  JR RIDINGBIKE_5
; Neither 'left' nor 'right' was pressed.
RIDINGBIKE_2:
  CP 92                   ; Was 'up' pressed?
  JR NZ,RIDINGBIKE_4      ; Jump if not
  LD L,237                ; HL=STATUS2 (ERIC's status flags)
  LD (HL),128             ; Set bit 7: ERIC is standing on the saddle of the
                          ; bike
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  PUSH DE
  DEC D                   ; D=ERIC's new y-coordinate (up a level)
  NOP
  INC E
  BIT 7,A                 ; Is ERIC facing left?
  JR Z,RIDINGBIKE_3       ; Jump if so
  DEC E
  DEC E
RIDINGBIKE_3:
  AND 128                 ; Now A=ERIC's new animatory state, E=ERIC's new
                          ; x-coordinate
  CALL UPDATEAS           ; Update ERIC's animatory state and location and
                          ; update the SRB
  LD A,(HL)               ; A=ERIC's animatory state
  POP DE                  ; DE=ERIC's location before he jumped on the saddle
                          ; or got off the bike
  ADD A,25                ; A=animatory state of the bike standing upright (25
                          ; or 153)
  INC H                   ; H=211: bike
  CALL UPDATEAS           ; Update the bike's animatory state and update the
                          ; SRB
  LD HL,WHEELBIKE         ; Place the address of the uninterruptible subcommand
  LD (54033),HL           ; routine at WHEELBIKE into bytes 17 and 18 of the
                          ; bike's buffer
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  LD (HL),5               ; Reset this to 5
  JR RIDINGBIKE_7         ; Also reset the bike's speed counter (at BIKESPEED)
                          ; to 5
; Not one of 'left', 'right' and 'up' was pressed.
RIDINGBIKE_4:
  CP 94                   ; Was 'down' pressed?
  JR NZ,RIDINGBIKE_5      ; Jump if not
  LD L,237                ; HL=STATUS2 (ERIC's status flags)
  LD (HL),0               ; Reset all bits here, signalling that ERIC has his
                          ; feet back on the ground
  LD L,251                ; HL=STATUS (ERIC's other status flags)
  RES 1,(HL)              ; Reset bit 1 here, signalling that STATUS2 is empty
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  PUSH DE                 ; Save ERIC's current location
  JR RIDINGBIKE_3         ; Jump back to update the animatory state and
                          ; location of ERIC and the bike
; The following section of code is executed on every pass through this routine
; while ERIC is sitting on the saddle (in other words, unless 'up' or 'down'
; was pressed).
RIDINGBIKE_5:
  LD HL,BIKEP             ; BIKEP holds the bike's momentum
  DEC (HL)                ; Has the bike run out of momentum?
  JR NZ,RIDINGBIKE_8      ; Jump if not
RIDINGBIKE_6:
  LD H,210                ; 210=ERIC, who will now fall off the bike
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  PUSH DE                 ; Save ERIC's coordinates briefly
  AND 128                 ; Keep only the 'direction' bit of ERIC's animatory
                          ; state
  PUSH AF
  ADD A,4                 ; A=4 or 132 (ERIC sitting on a chair)
  CALL UPDATEAS           ; Update ERIC's animatory state and update the ARB
  POP AF                  ; A=0 if ERIC's facing left, 128 if facing right
  POP DE                  ; Restore ERIC's coordinates to DE
  ADD A,24                ; A=24 or 152 (bike lying on the floor)
  LD H,211                ; 211=bike
  CALL UPDATEAS           ; Update the bike's animatory state and update the
                          ; SRB
  LD HL,STATUS2           ; STATUS2 holds ERIC's status flags
  LD (HL),16              ; Set bit 4: ERIC will not land on his feet
RIDINGBIKE_7:
  LD L,239                ; HL=BIKESPEED (which holds the bike's speed counter)
  LD (HL),5               ; Reset this to 5
  RET
; The bike still has momentum.
RIDINGBIKE_8:
  LD L,239                ; HL=BIKESPEED (which holds the bike's speed counter)
  DEC (HL)                ; Is it time to move the bike forward?
  RET NZ                  ; Return if not
  LD (HL),7               ; Reset the bike's speed counter to 7
  LD HL,(ERICCBUF)        ; A=ERIC's animatory state, E=ERIC's x-coordinate
  LD A,L                  ;
  LD E,H                  ;
  CALL BIKEPATH           ; Are there any walls, closed doors or ALBERTs in
                          ; ERIC's path?
  JR C,RIDINGBIKE_6       ; Knock ERIC off the bike if so
  JR Z,RIDINGBIKE_6       ;
  JP WHEELBIKE_4          ; Otherwise move ERIC forward one space, update the
                          ; SRB, and scroll the screen if necessary

; Deal with ERIC when he's standing on the saddle of the bike
;
; Called by the routine at HANDLEERIC when bit 7 at STATUS2 is set (by the
; routine at RIDINGBIKE). Listens for and responds appropriately to keypresses
; while ERIC is standing on the saddle of the bike.
ONSADDLE:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  INC (HL)                ; Set ERIC's action timer to 1, ensuring that we pass
                          ; through the following section of code on the next
                          ; call to this routine if no keypress is detected
                          ; this time
  CALL KEYOFFSET          ; A=value from the keypress offset table
                          ; corresponding to the key just pressed (if any)
  RET Z                   ; Return if no game key was pressed
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  CP 114                  ; Was 'jump' pressed?
  JR Z,ONSADDLE_1         ; Jump if so
  SET 3,A                 ; Ignore the difference between fast (upper case) and
                          ; slow (lower case) keys when ERIC's on the bike
  CP 92                   ; Was 'up' pressed?
  JR Z,ONSADDLE_1         ; Jump if so
  CP 94                   ; Was 'down' pressed?
  RET NZ                  ; Return if not
; This entry point is used by the routine at MOUNTBIKE when ERIC has just got
; on the bike.
ONSADDLE_0:
  LD (HL),5               ; Reset ERIC's main action timer at ERICTIMER to 5
  LD L,237                ; HL=STATUS2 (ERIC's status flags)
  LD (HL),1               ; Set bit 0: ERIC is riding the bike
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  INC H                   ; H=211: bike
  CALL UPDATESRB          ; Update the SRB for the bike's current animatory
                          ; state and location
  LD L,1                  ; Point HL at byte 1 of the bike's buffer
  LD (HL),224             ; Set the bike's x-coordinate to 224 (out of sight)
  LD L,18                 ; Remove the address of the routine at WHEELBIKE from
  LD (HL),0               ; bytes 17 and 18 of the bike's buffer
  DEC H                   ; H=210: ERIC
  SUB 12                  ; A=animatory state of ERIC riding the bike
  JP UPDATEAS             ; Update ERIC's animatory state and update the SRB
; ERIC has tried to jump off the saddle of the bike. Check whether he has the
; frog and is underneath a cup.
ONSADDLE_1:
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  ADD A,2                 ; A=2 or 130
  DEC D                   ; Set D to the y-coordinate of the spot above ERIC's
  DEC D                   ; hand as he jumps from the saddle
  CALL FALLING_0          ; Prepare ERIC for his descent from the bike saddle
  LD H,210                ; 210=ERIC
  XOR A
  LD L,A                  ; Point HL at byte 0 of ERIC's buffer
  BIT 7,(HL)              ; Is ERIC facing left?
  JR Z,ONSADDLE_2         ; Jump if so
  LD A,2
ONSADDLE_2:
  INC L                   ; Point HL at byte 1 of ERIC's buffer
  ADD A,(HL)              ; A=x-coordinate of ERIC's hand
  LD E,A                  ; Now DE=coordinates of the spot above ERIC's hand
  LD HL,INVENTORY         ; INVENTORY holds the inventory flags
  BIT 2,(HL)              ; Has ERIC got the frog?
  RET Z                   ; Return if not
  CALL CHKHITCUP_0        ; Is ERIC's arm directly underneath a cup?
  RET NZ                  ; Return if not
  RES 2,(HL)              ; Signal: ERIC no longer has the frog
  LD H,212                ; 212=frog
  LD A,28                 ; 28: frog sitting (as on the floor or in a cup)
  DEC E                   ; Set DE to the frog's new in-cup coordinates
  LD D,11                 ;
  CALL UPDATEAS           ; Update the frog's animatory state and location and
                          ; update the SRB
  JP ERICSITLIE_16        ; Print the inventory and make a celebratory sound
                          ; effect

; Unused
  DEFB 0

; Display the frog in ERIC's inventory
;
; Continues from CATCHFROG. Removes the frog from active participation in the
; game and displays the frog UDG in the inventory.
;
; H 212 (frog)
FROG2INV:
  CALL OBJFALL_0          ; Remove the uninterruptible subcommand routine
                          ; address from bytes 17 and 18 of the frog's buffer
                          ; and place the frog out of sight
  JP ERICSITLIE_16        ; Print the inventory and make a frog-catching sound
                          ; effect

; 'M' pressed - mount bike
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'M' or
; 'B' is pressed.
;
; A ERIC's animatory state
; D ERIC's y-coordinate
; E ERIC's x-coordinate
MOUNTBIKE:
  AND 128                 ; A=0 if ERIC's facing left, 128 if facing right
  ADD A,25                ; A=25/153: bike standing upright
  LD B,A                  ; Copy the bike's animatory state to B
  LD HL,54018             ; H=211 (bike), L=2
  LD A,D                  ; A=ERIC's y-coordinate
  CP (HL)                 ; Is ERIC on the same floor as the bike?
  RET NZ                  ; Return if not
  DEC L                   ; L=1
  LD A,E                  ; A=ERIC's x-coordinate
  CP (HL)                 ; Is ERIC at the same x-coordinate as the bike?
  RET NZ                  ; Return if not
; ERIC is standing beside the bike. Prepare to place him on the saddle.
  DEC L                   ; L=0
  LD (HL),B               ; Fill in the animatory state of the bike
  CALL WALKSOUND          ; Make a sound effect
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  LD (HL),2               ; Set bit 1: examine the secondary status flags at
                          ; STATUS2
  LD A,18
  LD L,238                ; HL=LBIKEKEY (value from the keypress offset table
                          ; corresponding to the last key pressed while riding
                          ; the bike)
  LD (HL),A               ; Initialise this to 18 (which doesn't correspond to
                          ; any key)
  INC L                   ; Initialise the bike's speed counter (at BIKESPEED)
  LD (HL),A               ; to 18 (matching its initial momentum, set below, so
                          ; that it doesn't move forward before ERIC has
                          ; started pedalling)
  INC L                   ; Initialise the bike's momentum (at BIKEP) to 18
  LD (HL),A               ; (giving ERIC a decent chance to start pedalling
                          ; before the bike falls over)
  LD L,243                ; HL=ERICTIMER (ERIC's main action timer)
  JP ONSADDLE_0           ; Place ERIC on the bike

; Print the score, lines total or hi-score (1)
;
; Used by the routines at ADDPTS and PREPGAME.
;
; DE Number/10
; HL Display file address
PRINTNUM:
  PUSH HL                 ; Save the display file address
  CALL NUM2ASCII          ; Generate ASCII codes for the digits in the decimal
                          ; rendering of the number in DE
  POP DE                  ; Get the display file address in DE
  JP PRINTNUM2            ; Print the number on the screen

; Add to the score and print it
;
; Used by the routines at JUMPING, CHKCOMBOS, CATCHMOUSE, GIVELINES, OBJFALL,
; HITTING, KOKID, FROGFALL and PREPGAME.
;
; A Points to add/10
ADDPTS:
  PUSH DE
  LD E,A                  ; DE=number of points to add (divided by 10)
  LD D,0                  ;
  PUSH HL
  LD HL,(SCORE)           ; Add points to the score (stored at SCORE)
  ADD HL,DE               ;
  LD (SCORE),HL           ;
  EX DE,HL
  LD HL,20900             ; HL=display file address for printing the score
; This entry point is used by the routine at ADDLINES with HL=20932 (display
; file address for printing the lines total).
ADDPTS_0:
  CALL PRINTNUM           ; Print the new score or lines total
  POP HL
  POP DE
  RET

; Add to the lines total and print it
;
; Used by the routines at KISS, GIVELINES and PREPGAME. Adds to the lines total
; and prints the new total. Also sets MR WACKER on his way to expel ERIC if the
; new total is 10000 lines or more.
;
; A Number of lines to add/10
ADDLINES:
  PUSH DE
  PUSH HL
  LD E,A                  ; DE=number of lines to add
  LD D,0                  ;
  LD HL,(LINES)           ; HL=current lines total (divided by 10)
  LD BC,64536             ; BC=-1000
  ADD HL,BC               ; Does ERIC have less than 10000 lines?
  JR NC,ADDLINES_0        ; Jump if so
  ADD HL,DE               ; Add lines to ERIC's total
  JR ADDLINES_1
ADDLINES_0:
  ADD HL,DE               ; Add lines to ERIC's total
  JR NC,ADDLINES_1        ; Jump if ERIC still has less than 10000 lines
  PUSH HL
  CALL EXPEL              ; Set MR WACKER on his way to expel ERIC
  AND A                   ; Clear the carry flag ready for subtraction
  POP HL
ADDLINES_1:
  SBC HL,BC               ; Now HL=new lines total
  LD (LINES),HL           ; Store the new lines total at LINES
  EX DE,HL                ; Transfer the new lines total to DE
  LD HL,20932             ; HL=display file address for printing the lines
                          ; total
  JP ADDPTS_0             ; Print the new lines total

; Open the Science Lab storeroom door if ERIC has the key
;
; Used by the routine at CHKWALLS. Opens the Science Lab storeroom door if ERIC
; is facing it, has the key, and is trying to move forwards.
;
; H 127
OPENLABRM:
  LD L,235                ; HL=INVENTORY (inventory flags)
  BIT 1,(HL)              ; Has ERIC got the storeroom key?
  RET Z                   ; Return if not
  LD B,1                  ; 1=open (as opposed to 0=close)
  LD A,4                  ; This is the Science Lab storeroom door identifier
  JP MVDOORWIN            ; Open the storeroom door

; Unused
  DEFS 3

; Calculate appropriate coordinates for a message box
;
; Used by the routines at GIVELINES, OBJFALL and WATCHERIC. Returns with the
; coordinates of the messenger's head in DE.
;
; H Messenger's character number (200-205)
BOXCOORDS:
  LD L,1                  ; Point HL at byte 1 of the character's buffer
  LD E,(HL)               ; E=character's x-coordinate
  INC E                   ; E=x-coordinate of the character's head
  DEC L                   ; L=0
  LD A,(HL)               ; A=character's animatory state
  AND 7                   ; Keep only bits 0-2
  CP 6                    ; Set the zero flag if the character is sitting on
                          ; the floor
  LD A,0                  ; The y-coordinate offset should be 0 if the
                          ; character is standing
  JR NZ,BOXCOORDS_0       ; Jump if character is standing
  INC A                   ; A=1 (one level lower if the character is sitting on
                          ; the floor)
BOXCOORDS_0:
  LD L,2                  ; Point HL at byte 2 of the character's buffer
  ADD A,(HL)              ; Add the character's y-coordinate and the offset
  LD D,A                  ; D=y-coordinate of the character's head
  RET

; Make a teacher give lines
;
; Used by the routines at KNOCKED, CHKERIC2 and SWOTLINES. Prints the lines
; message (e.g. '100 LINES ERIC') in a message box above the teacher's head,
; and shortly afterwards prints the reprimand message (e.g. 'NO CATAPULTS') in
; the same box.
;
; A Lines recipient's character number (206-210)
; B Reprimand message number
; H Teacher's character number (200-204)
GIVELINES:
  PUSH AF                 ; Save the lines recipient's character number briefly
  CALL BOXCOORDS          ; Collect in DE the coordinates of the teacher's head
  POP AF                  ; Restore the lines recipient's character number to A
  ADD A,77                ; A=27, 28, 29, 30 or 31 (message number for the
                          ; lines recipient's name)
  LD (MSG005),A           ; Place it into MSG005 (the first byte of message 5,
                          ; which is used as a submessage of message 13: '[12]0
                          ; LINES^[5]')
  SUB 22                  ; Compute the attribute byte to be used for the lines
  RRA                     ; message: 15 (blue background) for BOY WANDER; 23
  ADD A,A                 ; (red background) for ANGELFACE or EINSTEIN; 31
  ADD A,A                 ; (magenta background) for HAYLEY or ERIC
  ADD A,A                 ;
  DEC A                   ;
  LD C,A                  ; Copy the lines message attribute byte to C
  NOP
  NOP
  NOP
  PUSH BC
  CALL SCR2BUF            ; Save the area of screen that will be overwritten by
                          ; the lines message
  POP BC
  RET C                   ; Return if the teacher giving lines is off-screen
  CALL GETRANDOM          ; A=random number
  AND 14                  ; Set L=10, 20, 30, 40, 50, 60, 70 or 80 (number of
  ADD A,2                 ; lines divided by 10)
  LD L,A                  ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,L                 ;
  LD L,A                  ;
  LD A,C                  ; A=attribute byte for the lines message
  PUSH BC
  CP 15                   ; Is the teacher giving lines to BOY WANDER?
  JR Z,GIVELINES_1        ; Jump if so
  CP 31
  LD A,L                  ; A=number of lines divided by 10
  JR Z,GIVELINES_0        ; Jump if the teacher is giving lines to ERIC or
                          ; HAYLEY
  CALL ADDPTS             ; Add EINSTEIN's or ANGELFACE's lines to the score
  JR GIVELINES_1          ; and print the score
GIVELINES_0:
  CALL ADDLINES           ; Add lines and print the new lines total
GIVELINES_1:
  PUSH DE
  EX DE,HL
  LD D,0                  ; DE=number of lines divided by 10
  CALL NUM2ASCII          ; Generate the ASCII codes for the decimal digits of
                          ; the number in DE
  POP DE                  ; Restore the attribute file address for the top left
                          ; corner of the lines message box to DE
  POP BC                  ; Restore the lines reprimand number to B. and the
                          ; attribute byte for the lines message to C
  PUSH BC                 ; Save them again
  PUSH DE
  LD A,13                 ; Message 13: '[12]0 LINES^[5]'
  CALL PRTMSGBOX          ; Print the lines message
; The lines message (e.g. '100 LINES ERIC') has been printed. Now for the
; accompanying sound effect.
  LD A,(MSGBOX)           ; A=attribute byte for the lines message
  LD DE,10240             ; Set the duration parameter for the beep
  RRCA                    ; The border colour for the beep will be the same as
  RRCA                    ; the PAPER colour of the lines message box
  RRCA                    ;
  AND 7                   ;
  PUSH AF
  LD BC,5136              ; B=20 (pitch), C=16 (no alternating colours)
  CALL SNDEFFECT          ; Beep
  POP AF                  ; A=PAPER colour of the lines message
  POP DE                  ; Restore the attribute file address for the top left
                          ; corner of the lines message box to DE
  POP BC                  ; Restore the lines reprimand number to B. and the
                          ; attribute byte for the lines message to C
; This entry point is used by the routines at OBJFALL (with A=6, B=14, C=70)
; and WATCHERIC (with A=2, B=98, C=50).
GIVELINES_2:
  PUSH DE
  PUSH AF
  LD A,B                  ; A=14, 98, or a reprimand message number
  CALL PRTMSGBOX          ; Print the message
  POP AF                  ; A=border colour to use during the beep
  LD DE,5120              ; Set the duration parameter for the beep
  LD BC,12816             ; B=50 (pitch), C=16 (no alternating colours)
  CALL SNDEFFECT          ; Beep
  POP DE                  ; Restore the attribute file address for the top left
                          ; corner of the message box to DE
  LD HL,OVERBUF           ; The part of the screen overwritten by the message
                          ; box was stored in the buffer at OVERBUF
  JP BUF2SCR              ; Restore the area of the screen overwritten by the
                          ; message box

; Unused
  DEFS 2

; Make a sound effect
;
; Used by the routines at CATCHMOUSE, GIVELINES, OBJFALL, FROGFALL and
; NEWLESSON2.
;
; A Initial border colour
; B Pitch
; C 16 + next border colour
; DE Duration
SNDEFFECT:
  PUSH HL
  LD L,B                  ; Save the pitch parameter in L
SNDEFFECT_0:
  OUT (254),A             ; Flip the border colour and the state of the speaker
  XOR C                   ;
  LD B,L                  ; This is the pitch timing delay
SNDEFFECT_1:
  DJNZ SNDEFFECT_1        ;
  DEC E                   ; Jump back unless the sound effect is finished
  JR NZ,SNDEFFECT_0       ;
  DEC D                   ;
  JR NZ,SNDEFFECT_0       ;
  LD A,1                  ; BORDER 1 (blue) before returning
  OUT (254),A             ;
  POP HL
  RET

; Check whether any adults were hit by the pellet, water, sherry or conker
;
; Used by the routine at OBJFALL. Returns with the zero flag set if a character
; was hit, and the stricken character's number in B.
;
; DE Pellet/water/sherry/conker's coordinates
; H Pellet/water/sherry/conker's character number (213 or 214)
ADULTHIT:
  LD B,6                  ; There are six adult characters
; This entry point is used by the routine at PELLET with B=5 (only five of the
; adults are pelletable) and H=213/214 (BOY WANDER's or ERIC's pellet).
ADULTHIT_0:
  LD C,H                  ; Save the projectile's character number in C
  LD H,200                ; 200=MR WACKER
ADULTHIT_1:
  LD L,2                  ; Point HL at byte 2 of the potential target's
                          ; character buffer
  LD A,D                  ; A=projectile's y-coordinate
  CP (HL)                 ; Does it match the y-coordinate of the character?
  JR NZ,ADULTHIT_4        ; Jump if not
  LD L,0
  LD A,(HL)               ; A=character's animatory state
  AND 135                 ; Is the character sitting on the floor facing left
  CP 6                    ; (already struck)?
  JR Z,ADULTHIT_2         ; Jump if so
  DEC L                   ; L=-1
  CP 134                  ; Is the character sitting on the floor facing right
                          ; (already struck)?
  JR Z,ADULTHIT_3         ; Jump if so
ADULTHIT_2:
  INC L
ADULTHIT_3:
  LD A,L                  ; A=-1 if the character is sitting on the floor
                          ; facing right, 0 if he's standing, or 1 if he's
                          ; sitting on the floor facing left
  LD L,1                  ; Byte 1 of the character's buffer holds his
                          ; x-coordinate
  ADD A,(HL)              ; Now A=x-coordinate of the character's head
  CP E                    ; Does it match the x-coordinate of the projectile?
  JR Z,ADULTHIT_5         ; Jump if so
ADULTHIT_4:
  INC H                   ; Next potential target
  DJNZ ADULTHIT_1         ; Jump back to consider any remaining characters
ADULTHIT_5:
  LD B,H                  ; B=number of the character who was hit by the
                          ; projectile (if any)
  LD H,C                  ; Restore the projectile's character number to H
  RET                     ; Return with the zero flag set if a character was
                          ; hit

; Unused
  DEFB 0

; Control the descent of the water, sherry or conker
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a catapult pellet's buffer by the routine at PELLET when it has
; hit either the tree or a water/sherry-filled cup, and thus released a conker
; or a drop of water or sherry.
;
; H Falling object's character number (213 or 214)
OBJFALL:
  LD L,2
  LD A,(HL)               ; A=y-coordinate of the falling object
  CP 17                   ; Has the object hit the bottom floor yet?
  JR NZ,OBJFALL_3         ; Jump if not
; This entry point is used by the routines at FROG2INV, PELLET, PREPSBBUF,
; ERICSITLIE, CLOUD and CHKWATER to 'kill' a non-human object (i.e. pellet,
; frog, desk lid, stinkbomb cloud, water/sherry fired from pistol).
OBJFALL_0:
  LD L,18                 ; Remove the address of this routine from bytes 17
  LD (HL),0               ; and 18 of the object's buffer
; This entry point is used by the routine at MVMOUSE to make a mouse disappear
; temporarily.
OBJFALL_1:
  CALL UPDATESRB          ; Update the SRB for the object's current animatory
                          ; state and location
; This entry point is used by the routine at WATER2 to hide water from the
; water pistol.
OBJFALL_2:
  LD L,1                  ; Set the object's x-coordinate to 224 (out of sight)
  LD (HL),224             ;
  RET
; The falling object has yet to hit the floor. Has it hit anything else on the
; way there?
OBJFALL_3:
  CP 13                   ; Is the object at the right height to hit someone?
  JR Z,OBJFALL_5          ; Jump if so
OBJFALL_4:
  CALL UPDATESRB          ; Update the SRB for the object's current animatory
                          ; state and location
  INC D                   ; The object will fall one more level
  JP UPDATEAS             ; Update the object's location and update the SRB
; The object is at the right height to hit someone. Check if it has.
OBJFALL_5:
  LD L,1                  ; Point HL at byte 1 of the object's buffer
  LD E,(HL)               ; E=falling object's x-coordinate
  LD D,17                 ; This is the y-coordinate of the bottom floor (the
                          ; only floor on which characters can be hit by a
                          ; falling object)
  CALL ADULTHIT           ; Check if any of the six adult characters were hit
  LD L,0                  ; Point HL at byte 0 of the object's buffer
  LD A,(HL)               ; A=animatory state of the falling object
  JR Z,OBJFALL_10         ; Jump if an adult character was hit by the falling
                          ; object
  CP 55                   ; 55: Is the falling object a conker?
  JR NZ,OBJFALL_4         ; Jump if not
; The falling object is a conker.
  LD L,2                  ; Set the conker's y-coordinate to 17 (bottom floor)
  LD (HL),17              ; for comparison with that of the potential targets
; Before calling the routine at CHKTARGET to check whether any of the four main
; kids (besides ERIC) have been hit, D should be set to 206 (BOY WANDER, the
; first main kid), and B should be set to 4 (the number of kids to check).
; However, at this point D=17 and B=206, which means that instead of checking
; the character buffers in pages 206-209, we end up checking pages 17-222. In
; that range, only pages 183-214 contain character buffers; treating the other
; pages in RAM as if they contained character buffers could lead to data/code
; corruption. Needless to say, this is a bug.
  CALL CHKTARGET          ; Check whether anyone was hit by the conker
  LD L,2                  ; Set the conker's y-coordinate back to 13
  LD (HL),13              ;
  JR C,OBJFALL_4          ; Jump if nobody was hit by the conker
  LD A,D                  ; A=number of the character hit by the conker
  CP 207                  ; Was it BOY WANDER (206)?
  JR C,OBJFALL_6          ; Jump if so
  CP 209
  LD A,10                 ; ERIC gets 100 points for conkering ANGELFACE or
                          ; EINSTEIN
  JR NC,OBJFALL_6         ; Jump unless ANGELFACE (207) or EINSTEIN (208) was
                          ; hit
  CALL ADDPTS             ; Add 100 to the score and print it
  PUSH DE
  LD D,40                 ; Make a sound effect with A=7 and C=18 (border
  LD BC,10258             ; alternating white and cyan)
  LD A,7                  ;
  CALL SNDEFFECT          ;
  POP DE
; This entry point is used by the routine at KOKID.
OBJFALL_6:
  PUSH DE
; This entry point is used by the routine at PELLET.
OBJFALL_7:
  CALL OBJFALL_0          ; Terminate the object
  POP DE
; This entry point is used by the routine at HITTING to knock a character over.
OBJFALL_8:
  EX DE,HL
  LD A,H                  ; A=number of the character hit by the object or fist
  CP 210                  ; Was it ERIC?
  JR NZ,OBJFALL_12        ; Jump if not
; This entry point is used by the routine at HITERIC.
OBJFALL_9:
  LD A,128                ; Set bit 7 at STATUS: ERIC has been knocked down
  LD (STATUS),A           ;
  RET
; An adult character was hit by the falling object.
OBJFALL_10:
  CP 55                   ; Is the falling object a conker?
  JR NZ,OBJFALL_13        ; Jump if not
; An adult character has been hit by either a conker or a drop of water or
; sherry.
OBJFALL_11:
  LD C,0                  ; Point BC at byte 0 of the character's buffer
  LD A,(BC)               ; A=animatory state of the adult character hit by the
                          ; object
  AND 7                   ; Is the character already sitting on the floor?
  CP 6                    ;
  JP Z,OBJFALL_4          ; Jump if so (the object will continue to fall)
  LD C,18                 ; Is there an uninterruptible subcommand routine
  LD A,(BC)               ; address in bytes 17 and 18 of the character's
  AND A                   ; buffer (which would prevent the character from
                          ; being knocked over)?
  JP NZ,OBJFALL_4         ; Jump if so
  PUSH BC
  JR OBJFALL_7            ; Terminate the object and knock the character over
; This section of code sets up the uninterruptible subcommand that will control
; the character who has been knocked over until it's time for him to get up.
OBJFALL_12:
  LD L,17                 ; Place the address of the uninterruptible subcommand
  LD (HL),150             ; routine at KNOCKED into bytes 17 and 18 of the
  INC L                   ; stricken character's buffer
  LD (HL),117             ;
  INC L                   ; Initialise the knockout delay counter in byte 19 of
  LD (HL),19              ; the character's buffer
  RET
; An adult character has been hit by a drop of water or sherry.
OBJFALL_13:
  LD DE,SRB
  CP 31                   ; 31: Is the falling object a drop of water?
  JR Z,OBJFALL_14         ; Jump if so
  LD E,4
OBJFALL_14:
  LD A,B                  ; A=number of the character who was hit by the drop
                          ; of water/sherry
  CP 204                  ; Is the stricken character one of the male teachers?
  JR NC,OBJFALL_11        ; Jump if not
  SUB 36                  ; Point DE at the relevant combination letter or
  ADD A,E                 ; number in one of the tables at COMBOS2 and
  LD E,A                  ; COMBOS2SLS
  EX DE,HL                ; (HL)=combination letter/number held by the teacher
                          ; just knocked down
  BIT 7,(HL)              ; Has ERIC already discovered this one?
  JR NZ,OBJFALL_15        ; Jump if so
  SET 7,(HL)              ; Signal: ERIC has discovered this combination
                          ; letter/number
  PUSH BC
  LD A,20                 ; Add 200 to the score and print it
  CALL ADDPTS             ;
  POP BC
OBJFALL_15:
  LD A,(HL)               ; A=character code of the combination number/letter
  AND 127                 ; just revealed
  LD (MSG005),A           ; Store this in the first byte of message 5, which is
                          ; used as a submessage of message 14: '[5]^ '
  PUSH DE
  PUSH BC
  LD H,B                  ; H=character number of the teacher just struck
                          ; (200-203)
  CALL BOXCOORDS          ; Get the coordinates of this teacher's head
  CALL SCR2BUF            ; Save the area of the screen that will be
                          ; overwritten by the letter/number message box
  JR C,OBJFALL_16         ; Jump if the teacher is off-screen
  LD BC,3654              ; Print the letter/number message box (message number
  LD A,6                  ; B=14) and make a sound effect
  CALL GIVELINES_2        ;
OBJFALL_16:
  POP BC                  ; Restore the stricken character's number to B
  POP HL                  ; Restore the falling object's character number to H
  JP OBJFALL_11           ; Terminate the falling object and knock the
                          ; character over

; Unused
  DEFS 6

; Deal with a character who has been knocked over
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of the stricken character's buffer by the routine at OBJFALL. It
; knocks the character to the floor, makes him give lines to any nearby kids
; (if he's a teacher), and then makes him get up.
;
; H Character number (183-209)
KNOCKED:
  LD L,19                 ; Byte 19 of the character's buffer holds the
                          ; knockout delay counter
  DEC (HL)                ; Has the character already got up?
  JP Z,RMUSUBCMD          ; Terminate this uninterruptible subcommand if so
  LD A,(HL)               ; A=knockout delay counter
  DEC A                   ; Is it time for the character to get up yet?
  JR NZ,KNOCKED_2         ; Jump if not
; This entry point is used by the routine at TOWINDOW.
KNOCKED_0:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
; This entry point is used by the routine at STOPJPING.
KNOCKED_1:
  LD L,20                 ; Byte 20 of the character's buffer holds the
                          ; pre-knockout animatory state saved earlier
  LD A,(HL)               ; Pick this up in A
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB
; It's not time for the character to get up yet.
KNOCKED_2:
  DEC A                   ; Jump forward unless the knockout delay counter has
  JR NZ,KNOCKED_3         ; reached 2
  LD A,H                  ; A=number of the stricken character
  CP 205                  ; Is it ALBERT?
  RET NZ                  ; Return if not
  LD A,(32740)            ; Pick up the MSB of the lesson clock
  AND A                   ; Is the lesson nearly over?
  RET Z                   ; Return if so
  LD L,19                 ; Set the knockout delay counter to 3 so that ALBERT
  INC (HL)                ; will not get up until the lesson's almost over
  RET
; The knockout delay counter has not reached 2 yet. Is it time to knock the
; character down?
KNOCKED_3:
  CP 15                   ; Is it time for the character to fall over?
  JR NZ,KNOCKED_6         ; Jump if not
  LD A,H                  ; A=number of the stricken character
  LD DE,32740             ; Point DE at the MSB of the lesson clock
  CP 205                  ; Was ALBERT struck?
  JR NZ,KNOCKED_4         ; Jump if not
  LD A,(DE)               ; Add 12 to the MSB of the lesson clock if ALBERT was
  ADD A,12                ; knocked down, thus giving ERIC some extra time to
  LD (DE),A               ; work with
  JR KNOCKED_5            ; Knock ALBERT down
KNOCKED_4:
  CP 209                  ; Was HAYLEY struck?
  JR NZ,KNOCKED_5         ; Jump if not
  LD E,226                ; DE=KISSCOUNT (kiss counter)
  LD A,(DE)               ; Pick this up in A
  AND A                   ; Are there any kisses left?
  JR Z,KNOCKED_5          ; Jump if not
  DEC A                   ; Otherwise decrease the kiss counter by 1
  LD (DE),A               ;
; Knock the character down.
KNOCKED_5:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  LD L,20                 ; Store the character's current animatory state in
  LD (HL),A               ; byte 20 of the buffer for retrieval later (when the
                          ; character gets up)
  AND 248                 ; A=animatory state of the character sitting or lying
  ADD A,6                 ; on the floor
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB
; It's not time to knock the character down.
KNOCKED_6:
  CP 8                    ; Is it time for a felled teacher to give lines?
  RET NZ                  ; Return if not
  LD A,H                  ; A=number of the stricken character
  CP 200                  ; Return if it's not a teacher
  RET C                   ;
  CP 205                  ;
  RET NC                  ;
  PUSH HL
  CALL NEARESTKID         ; Find the main kid who is closest to the teacher and
                          ; within lines-giving range (if any)
  POP HL
  AND A                   ; Is there a main kid close enough to the felled
                          ; teacher?
  RET Z                   ; Return if not
  LD B,15                 ; Message 15: NOW DON'T DO IT AGAIN
  JP GIVELINES            ; Give lines to the nearest main kid

; Unused
  DEFS 3

; Check whether a pellet has hit a cup
;
; Used by routine at CHKCUPFULL. Returns with the zero flag set if the pellet
; is about to hit a cup.
;
; H Pellet's character number (213 or 214)
CHKHITCUP:
  LD L,1                  ; Point HL at byte 1 of the pellet's buffer
  LD E,(HL)               ; E=pellet's x-coordinate
  INC L                   ; L=2
  LD D,(HL)               ; D=pellet's y-coordinate
  INC E                   ; Adjust the coordinates for comparison with those of
  INC D                   ; the cup
  LD A,D                  ; A=pellet's (adjusted) y-coordinate
  CP 14                   ; All the cups have a y-coordinate of 14
  RET NZ                  ; Return with the zero flag reset if the pellet's not
                          ; at the right height to hit a cup
; This entry point is used by the routine at ONSADDLE with E holding the
; x-coordinate of ERIC's hand as he jumps off the saddle of the bike.
CHKHITCUP_0:
  LD A,E                  ; A=x-coordinate of the pellet or ERIC's hand
; This entry point is used by the routine at CHKWATER with A holding the
; x-coordinate of the stream of water or sherry fired from the waterpistol.
CHKHITCUP_1:
  CP 25                   ; Has the leftmost cup on the shelf in the boys'
                          ; skool been hit?
  RET Z                   ; Return with the zero flag set if so
  CP 27                   ; Has the middle cup on the shelf in the boys' skool
                          ; been hit?
  RET Z                   ; Return with the zero flag set if so
  CP 30                   ; Has the rightmost cup on the shelf in the boys'
                          ; skool been hit?
  RET Z                   ; Return with the zero flag set if so
  CP 186                  ; Has the cup on the shelf in the girls' skool been
                          ; hit?
  RET                     ; Return with the zero flag set if so

; Check whether a pellet has hit a cup containing water, sherry or the frog
;
; Used by the routine at PELLET. Returns with the zero flag set if the pellet
; hit a water- or sherry-filled cup. Otherwise it returns with the zero flag
; reset, but releases the frog if the pellet hit the cup containing it.
;
; H Pellet's character number (213 or 214)
CHKCUPFULL:
  CALL CHKHITCUP          ; Has the pellet hit a cup?
  RET NZ                  ; Return with the zero flag reset if not
  LD A,(INVENTORY)        ; Pick up the inventory flags from INVENTORY
  BIT 1,A                 ; Has ERIC got the Science Lab storeroom key?
  JR Z,CHKCUPFULL_0       ; Jump if not
  LD A,(54273)            ; A=frog's x-coordinate
  INC A                   ; Adjust it for comparison with that of the cup that
                          ; was hit
  CP E                    ; Does the frog's x-coordinate match that of the cup?
  JR NZ,CHKCUPFULL_0      ; Jump if not
  LD A,(54274)            ; A=frog's y-coordinate
  CP 11                   ; Is the frog in the cup?
  JR NZ,CHKCUPFULL_0      ; Jump if not
  PUSH HL
  LD HL,FROGFALL          ; Place the address of the uninterruptible subcommand
  LD (54289),HL           ; routine at FROGFALL into bytes 17 and 18 of the
                          ; frog's buffer, thus releasing the frog
  POP HL
CHKCUPFULL_0:
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  LD B,A                  ; Return if the cup is off-screen to the left
  LD A,E                  ;
  SUB B                   ;
  RET C                   ;
  CP 32                   ; Return if the cup is off-screen to the right
  RET NC                  ;
  ADD A,192               ; Point DE at the attribute file byte for the cup
  LD E,A                  ;
  LD D,89                 ;
  LD A,(DE)               ; Pick up the attribute byte of the cup
  CP 40                   ; Does the cup contain water (PAPER 5)?
  JR NZ,CHKCUPFULL_1      ; Jump if not
  LD A,31                 ; 31: Drop of water
  RET                     ; Return with the zero flag set
CHKCUPFULL_1:
  CP 88                   ; Does the cup contain sherry (BRIGHT 1: PAPER 3)?
  RET NZ                  ; Return if not
  LD A,159                ; 159: Drop of sherry
  RET                     ; Return with the zero flag set

; Unused
  DEFB 0

; Check for walls, doors and windows in the path of a mouse, frog or pellet
;
; Used by the routines at PELLET, FROGFALL and MVMOUSE. Returns with the carry
; flag set if there is a wall, closed door or closed window blocking the path
; of the mouse, frog or pellet
;
; DE Coordinates of the spot in front of the mouse/frog/pellet
CHKOBJPATH:
  PUSH HL
  LD HL,DOORFLAGS         ; DOORFLAGS contains the doors flags
  LD A,E                  ; A=x-coordinate of the point in front of the
                          ; mouse/frog/pellet
  CP 190                  ; This is the x-coordinate of the far wall of the
                          ; girls' skool
  JR NC,CHKOBJPATH_2      ; Jump if the mouse/frog/pellet is facing this wall
  LD A,D                  ; A=y-coordinate of the mouse/frog/pellet
  CP 5                    ; Is the mouse/frog/pellet on the top floor (or just
                          ; below it)?
  JR NC,CHKOBJPATH_3      ; Jump if not
; The mouse/frog/pellet is on the top floor.
  LD A,E                  ; A=x-coordinate of the point in front of the
                          ; mouse/frog/pellet
  CP 159                  ; This is the x-coordinate of the left wall of the
                          ; girls' skool
  JR Z,CHKOBJPATH_2       ; Jump if the mouse/frog/pellet is facing this wall
  LD D,1                  ; Set bit 0: left study door
  CP 72                   ; This is the x-coordinate of the left study door
  JR Z,CHKOBJPATH_1       ; Jump if the mouse/frog/pellet is facing this door
  INC D                   ; Set bit 1: right study door
  CP 84                   ; This is the x-coordinate of the right study door
  JR Z,CHKOBJPATH_1       ; Jump if the mouse/frog/pellet is facing this door
  LD D,64                 ; Set bit 6: top-floor window
  CP 92                   ; This is the x-coordinate of the top-floor window
  JR Z,CHKOBJPATH_1       ; Jump if the mouse/frog/pellet is facing this window
  AND A                   ; Clear the carry flag: no obstacles ahead
CHKOBJPATH_0:
  POP HL                  ; Restore the character number of the
                          ; mouse/frog/pellet to H
  RET                     ; Return with the carry flag reset
CHKOBJPATH_1:
  LD A,D                  ; A holds the identifier of the door or window in
                          ; front of the mouse/frog/pellet
  AND (HL)                ; Is this door/window open?
  JR NZ,CHKOBJPATH_0      ; Jump if so
CHKOBJPATH_2:
  SCF                     ; Set the carry flag: no admittance
  POP HL                  ; Restore the character number of the
                          ; mouse/frog/pellet to H
  RET                     ; Return with the carry flag set
CHKOBJPATH_3:
  CP 12
  LD A,E
  JR NC,CHKOBJPATH_5      ; Jump if the mouse/frog/pellet is on the bottom
                          ; floor (or just above it)
; The mouse/frog/pellet is on the middle floor.
  CP 159                  ; This is the x-coordinate of the left wall of the
                          ; girls' skool
  JR Z,CHKOBJPATH_2       ; Jump if the mouse/frog/pellet is facing this wall
  CP 62                   ; This is the x-coordinate of the far wall of the
                          ; Science Lab storeroom
  JR Z,CHKOBJPATH_2       ; Jump if the mouse/frog/pellet is facing this wall
  CP 53                   ; This is the x-coordinate of the Science Lab
                          ; storeroom door
  LD D,4                  ; Set bit 2: Science Lab storeroom door
  JR Z,CHKOBJPATH_1       ; Jump if the mouse/frog/pellet is facing this door
  LD D,128                ; Set bit 7: middle-floor window
  CP 94                   ; This is the x-coordinate of the middle-floor window
  JR Z,CHKOBJPATH_1       ; Jump if the mouse/frog/pellet is facing this window
CHKOBJPATH_4:
  AND A                   ; Clear the carry flag: no obstacles ahead
  POP HL                  ; Restore the character number of the
                          ; mouse/frog/pellet to H
  RET                     ; Return with the carry flag reset
; The mouse/frog/pellet is on the bottom floor.
CHKOBJPATH_5:
  LD D,8                  ; Set bit 3: boys' skool door
  CP 94                   ; This is the x-coordinate of the boys' skool door
  JR Z,CHKOBJPATH_1       ; Jump if mouse/frog/pellet is facing this door
  LD D,16                 ; Set bit 4: skool gate
  CP 133                  ; This is the x-coordinate of the skool gate
  JR Z,CHKOBJPATH_1       ; Jump if the mouse/frog/pellet is facing this
  JR CHKOBJPATH_4         ; Otherwise it's all clear

; Control the flight of a catapult pellet
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a catapult pellet's buffer by the routine at CATTY. It controls
; the pellet from the beginning of its flight to the end, including any
; collisions with cups, trees, other characters, and teachers' heads.
;
; H 213 (BOY WANDER's pellet) or 214 (ERIC's pellet)
PELLET:
  LD L,19                 ; Byte 19 of the pellet's buffer holds the distance
                          ; left to travel
  DEC (HL)                ; Has the pellet finished travelling?
  JP Z,OBJFALL_0          ; Terminate the pellet if so
  LD A,(HL)               ; A=distance left to travel
  CP 5                    ; Does the pellet have fewer than 5 spaces left to
                          ; travel?
  JR C,PELLET_1           ; Jump if so (to check for hittable obstacles)
; The pellet is merrily wending its way through the air. It's not ready to hit
; anything vulnerable yet, or has simply failed to do so. The only thing that
; can stop it at the moment is a wall, door or window.
PELLET_0:
  LD L,0                  ; Point HL at byte 0 of the pellet's buffer
  LD A,(HL)               ; A=pellet's animatory state
  RLCA                    ; Set the carry flag if the pellet is travelling to
  CCF                     ; the left
  SBC A,A                 ; A=-1 if the pellet is travelling leftwards, 1 if
  ADD A,A                 ; rightwards
  INC A                   ;
  INC L                   ; Add the pellet's x-coordinate to obtain that of the
  ADD A,(HL)              ; location directly in front of the pellet
  LD E,A                  ; Store this in E
  INC L                   ; L=2
  LD D,(HL)               ; D=pellet's y-coordinate
  CALL CHKOBJPATH         ; Check for closed doors, windows and walls in the
                          ; pellet's path
  JP C,OBJFALL_0          ; Terminate the pellet if there is one
  JP WHEELBIKE_5          ; Otherwise just advance the pellet one space
; The pellet is ready to hit a vulnerable character, cup or tree.
PELLET_1:
  CALL CHKCUPFULL         ; Has the pellet hit a water- or sherry-filled cup?
  JR NZ,PELLET_3          ; Jump if not
; The pellet has hit a water- or sherry-filled cup, or a conker in the tree.
; The A register holds the animatory state of the object (drop of water/sherry,
; or conker) that the pellet will become.
PELLET_2:
  LD L,18                 ; Place the address of the uninterruptible subcommand
  LD (HL),116             ; routine at OBJFALL into bytes 17 and 18 of the
  DEC L                   ; catapult pellet's buffer
  LD (HL),200             ;
  PUSH AF                 ; Save the new animatory state of the transmuted
                          ; pellet
  CALL UPDATESRB          ; Update the SRB for the pellet's current location
  POP AF                  ; Restore the transmuted pellet's animatory state to
                          ; A
  DEC D                   ; The projectile will ascend two levels
  DEC D                   ;
  JP UPDATEAS             ; Update the projectile's location and update the SRB
; The pellet hasn't hit a water- or sherry-filled cup. Has it hit a teacher?
PELLET_3:
  LD L,1                  ; Point HL at byte 1 of the pellet's buffer
  LD E,(HL)               ; E=pellet's x-coordinate
  INC L                   ; L=2
  LD D,(HL)               ; D=pellet's y-coordinate
  LD B,5                  ; There are 5 teachers who can be hit by a pellet
  CALL ADULTHIT_0         ; Has the pellet hit any of these teachers?
  JR NZ,PELLET_8          ; Jump if not
  LD C,0                  ; Point BC at byte 0 of the stricken teacher's buffer
  LD A,(BC)               ; A=animatory state of this teacher
  AND 7                   ; Is the teacher already sitting on the floor?
  CP 6                    ;
  JR NZ,PELLET_7          ; Jump if not
  LD L,19                 ; Place 6 into byte 19 of the pellet's buffer (the
  LD (HL),A               ; distance the pellet will travel after bouncing off
                          ; the teacher's head)
; This section of code controls the pellet's motion on its ascent from the
; teacher's head.
PELLET_4:
  LD L,19                 ; Byte 19 of the pellet's buffer holds the distance
                          ; left to travel
  DEC (HL)                ; Decrement this
  JP Z,OBJFALL_0          ; Terminate the pellet if its journey is over
  CALL CHKCUPFULL         ; Has the pellet hit a water- or sherry-filled cup?
  JR Z,PELLET_2           ; Jump if so
  CALL UPDATESRB          ; Update the SRB for the pellet's current location
  DEC D                   ; Has the pellet hit the ceiling on the top floor?
  JR NZ,PELLET_5          ; Jump if not (having raised the pellet one level)
  INC D                   ; D=1; this makes the pellet hover just below the
                          ; ceiling on the top floor before disappearing, which
                          ; is a bug
PELLET_5:
  CALL SUBCMD             ; Replace the address of this routine in bytes 17 and
                          ; 18 of the pellet's buffer with PELLET_6 (the
                          ; address of the next instruction)
PELLET_6:
  JR PELLET_4
; The pellet has hit a teacher who's standing up.
PELLET_7:
  LD C,18                 ; Point BC at byte 18 of the teacher's buffer
  LD A,(BC)               ; A=MSB of a routine address, or 0
  PUSH BC
  AND A                   ; Is there an uninterruptible subcommand routine
                          ; address in bytes 17 and 18 of the teacher's buffer?
  JP Z,OBJFALL_7          ; Jump if not (this teacher can be knocked over)
  POP BC
; The pellet hasn't hit any liquid-containing cups or vulnerable teachers. Has
; it hit any kids?
PELLET_8:
  LD D,206                ; 206=BOY WANDER
  LD B,5                  ; There are 5 kids who can be knocked over
  CALL CHKTARGET          ; Has the pellet hit any of them?
  JP NC,KOKID             ; Knock them down if so
; The pellet hasn't hit any liquid-containing cups, teachers or kids. Has it
; hit a conker in the tree?
  LD L,19                 ; Byte 19 of the pellet's buffer holds the distance
                          ; left to travel
  LD A,(HL)               ; Pick this up in A
  DEC A                   ; Is there only one space left to travel?
  JP Z,PELLET_0           ; Jump if so
  LD L,2                  ; Point HL at byte 2 of the pellet's buffer
  LD A,(HL)               ; A=pellet's y-coordinate
  CP 4                    ; Was the pellet fired on the top floor?
  JR NC,PELLET_9          ; Jump if not
  DEC L                   ; L=1
  LD A,(HL)               ; A=pellet's x-coordinate
  CP 128                  ; Jump if the pellet is to the right of the tree (it
  JR NC,PELLET_9          ; was fired in the girls' skool)
  CP 96                   ; Set the carry flag if A>=96 (i.e. the pellet is in
  CCF                     ; the tree)
PELLET_9:
  JP NC,PELLET_0          ; Jump unless the pellet will knock a conker out of
                          ; the tree
  LD A,55                 ; 55: animatory state of a conker
  JP PELLET_2             ; Transmute the pellet into a conker

; Unused
  DEFS 2

; Initialise an uninterruptible subcommand
;
; Used by the routines at HITERIC, INASSEMBLY, PELLET, CATTY, BWFIRING,
; HITTING, FROGFALL, ERICSITLIE, CLOUD, TOWINDOW, WATER and CHKWATER to place
; the address of the instruction following CALL SUBCMD into bytes 17 and 18 of
; the character's buffer. Thus that address becomes the address of the
; character's current uninterruptible subcommand routine.
;
; A Character's new animatory state
; DE Character's new coordinates
; H Character number (183-214)
SUBCMD:
  POP BC                  ; Get the return address in BC
  LD L,17                 ; Place it into bytes 17 and 18 of the character's
  LD (HL),C               ; buffer
  INC L                   ;
  LD (HL),B               ;
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB

; Check whether a character is on the top, middle or bottom floor
;
; Used by the routines at CATTY, VIOLENT, RELEASE, CHKWATER and LANDING.
; Returns with the zero flag set if and only if the character is exactly on the
; top, middle or bottom floor.
;
; H Character number (206, 207, 210, 214)
ONTMBFLOOR:
  LD L,2                  ; Byte 2 of the character's buffer holds his
                          ; y-coordinate
  LD A,(HL)               ; Pick this up in A
  CP 3                    ; Is the character on the top floor?
  RET Z                   ; Return with the zero flag set if so
  CP 10                   ; Is the character on the middle floor?
  RET Z                   ; Return with the zero flag set if so
  CP 17                   ; Is the character on the bottom floor?
  RET                     ; Return with the zero flag set if so

; Make BOY WANDER fire his catapult now and then
;
; The address of this continual subcommand routine is placed into bytes 23 and
; 24 of BOY WANDER's buffer by command lists 32, 46 and 50, and is also used by
; the routine at HITFIRE. It decides whether BOY WANDER should let rip with a
; pellet, and gets the catapult-firing action under way if so.
;
; H 206 (BOY WANDER)
CATTY:
  LD A,(54546)            ; Pick up byte 18 of buffer 213 (BOY WANDER's pellet)
  AND A                   ; Is there a uninterruptible subcommand routine
                          ; address in bytes 17 and 18 of the buffer?
  RET NZ                  ; Return if so (this buffer is in use)
  LD L,A                  ; L=0
  BIT 0,(HL)              ; Is BOY WANDER midstride?
  RET NZ                  ; Return if so
  INC L                   ; L=1
  LD A,(HL)               ; A=BOY WANDER's x-coordinate
  AND 3                   ; Return unless BOY WANDER's x-coordinate is
  RET NZ                  ; divisible by 4
  CALL ONTMBFLOOR         ; Is BOY WANDER exactly on the top, middle, or bottom
                          ; floor?
  RET NZ                  ; Return if not
  CALL GETRANDOM          ; A=random number
  RRCA                    ; Return half the time
  RET NC                  ;
  CALL WITNESS_0          ; Can any adults see BOY WANDER?
  RET C                   ; Return if so
  POP BC                  ; Drop the return address (MVCHARS_11)
  POP BC                  ; Pop the value of HL that was pushed on to the stack
                          ; by the routine at MVCHARS before coming here; RET
                          ; will now take us back either to MVCHARS to move the
                          ; next character, or to the main loop
  CALL UPDATESRB          ; Update the SRB for BOY WANDER's current animatory
                          ; state and location
  LD L,20                 ; Store BOY WANDER's current animatory state in byte
  LD (HL),A               ; 20 of his buffer for the time being
  AND 240
  ADD A,10                ; A=26 or 154: BOY WANDER raising his arm to fire
  CALL SUBCMD             ; Update BOY WANDER's animatory state and hand over
                          ; to CATTY_0 (below)
CATTY_0:
  CALL UPDATESRB          ; Update the SRB for BOY WANDER's current animatory
                          ; state
  INC A                   ; A=27 or 155: BOY WANDER firing the catapult
  CALL SUBCMD             ; Update BOY WANDER's animatory state and hand over
                          ; to CATTY_1 (below)
CATTY_1:
  LD L,17                 ; Place the address of the uninterruptible subcommand
  LD (HL),179             ; routine at BWFIRING into bytes 17 and 18 of BOY
                          ; WANDER's buffer
  LD A,(54546)            ; Pick up byte 18 of buffer 213 (BOY WANDER's pellet)
  AND A                   ; Is this buffer already being used?
  RET NZ                  ; Return if so
  LD B,213                ; Use buffer 213 for BOY WANDER's catapult pellet
; This entry point is used by the routine at PREPCATTY with H=210 (ERIC) and
; B=214 (ERIC's pellet).
CATTY_2:
  LD L,0                  ; Point HL at byte 0 of the shooter's character
                          ; buffer
  LD A,(HL)               ; A=shooter's animatory state
  AND 128                 ; A=128 if the pellet will travel right, 0 if left
  ADD A,79                ; A=79/207: catapult pellet travelling left/right
  INC L                   ; Collect the initial coordinates of the pellet from
  LD E,(HL)               ; the shooter's buffer into DE
  INC L                   ;
  LD D,(HL)               ;
  LD H,B                  ; H=buffer to be used for the pellet (213 if it's BOY
                          ; WANDER's, 214 if it's ERIC's)
  LD (HL),D               ; Fill in the initial coordinates and animatory state
  DEC L                   ; of the pellet
  LD (HL),E               ;
  DEC L                   ;
  LD (HL),A               ;
  LD L,17                 ; Place the address of the pellet-controlling
  LD (HL),172             ; uninterruptible subcommand routine at PELLET into
  INC L                   ; bytes 17 and 18 of the pellet's buffer
  LD (HL),118             ;
  INC L                   ; L=19
  LD (HL),13              ; The pellet will travel 13 spaces
  RET

; Deal with BOY WANDER when he is firing
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of BOY WANDER's buffer by the routine at CATTY just after BOY
; WANDER has raised his catapult and fired. It controls BOY WANDER while he is
; lowering the catapult.
;
; H 206 (BOY WANDER)
BWFIRING:
  CALL UPDATESRB          ; Update the SRB for BOY WANDER's current animatory
                          ; state
  DEC A                   ; A=26 or 154: BOY WANDER lowering his arm
; This entry point is used by the routine at HITTING with H=207 (ANGELFACE) or
; H=210 (ERIC).
BWFIRING_0:
  CALL SUBCMD             ; Update the character's animatory state and return
                          ; to BWFIRING_1 (below) next time if we're dealing
                          ; with BOY WANDER or ANGELFACE
; This entry point is used by the routine at HITERIC with H=209 (HAYLEY).
BWFIRING_1:
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
; This entry point is used by the routine at KISS with H=209 (HAYLEY).
BWFIRING_2:
  LD L,20                 ; Byte 20 of the character's buffer holds the
                          ; animatory state of the character that was saved
                          ; before this action (firing/hitting/kissing) started
  LD A,(HL)               ; Pick this up in A so it can be restored
  CALL SUBCMD             ; Update the character's animatory state and return
                          ; to BWFIRING_3 (below) next time
BWFIRING_3:
  JP RMUSUBCMD            ; Terminate this uninterruptible subcommand

; Unused
  DEFS 2

; Check whether any characters were or can be punched by ERIC or ANGELFACE
;
; Used by the routines at VIOLENT and HITTING. On entry, B holds 3 when
; checking whether it's worth ANGELFACE raising his fist (VIOLENT), or 2 when
; ERIC or ANGELFACE has already raised his fist (HITTING). Returns with the
; carry flag reset if anyone was hit or is within hitting range, and D holding
; that unfortunate character's number.
;
; B 2 or 3
; H 207 (ANGELFACE) or 210 (ERIC)
VICTIM:
  LD L,0                  ; Point HL at byte 0 of the punching character's
                          ; buffer
  BIT 7,(HL)              ; Set the zero flag if the character is facing left
  LD L,1
  LD C,(HL)               ; C=punching character's x-coordinate
  PUSH BC                 ; Save this x-coordinate for now
  LD A,B                  ; A=2 (fist already raised) or 3
  JR NZ,VICTIM_0          ; Jump if the punching character is facing right
  NEG
VICTIM_0:
  ADD A,(HL)              ; A=x-coordinate of the spot 2 or 3 spaces in front
                          ; of the punching character
  LD (HL),A               ; Set this as the character's x-coordinate
                          ; temporarily
  DEC L                   ; L=0
  LD A,(HL)               ; A=punching character's animatory state
  CPL                     ; C=0 if the character's facing right, 128 if left
  AND 128                 ;
  LD C,A                  ;
  LD D,206                ; 206=BOY WANDER (first main kid)
  LD B,5                  ; There are 5 main kids
  CALL CHECKHIT           ; Were any of them hit?
  JR NC,VICTIM_1          ; Jump if so
  LD D,183                ; 183=little girl no. 1
  LD B,17                 ; There are 17 little kids (10 boys, 7 girls)
  CALL CHECKHIT           ; Reset the carry flag if any of them were hit
VICTIM_1:
  POP BC                  ; Restore the punching character's x-coordinate
  LD L,1                  ;
  LD (HL),C               ;
  RET                     ; Return with the carry flag reset if someone was hit
                          ; or found within hitting range, and D holding that
                          ; character's number

; Make ANGELFACE hit now and then
;
; Used by the routines at HITHAYLEY and HITFIRE. Decides whether ANGELFACE
; should throw a punch, and gets the punching action under way if so.
;
; H 207 (ANGELFACE)
VIOLENT:
  LD L,0                  ; Byte 0 of ANGELFACE's buffer holds his animatory
                          ; state
  BIT 0,(HL)              ; Is ANGELFACE midstride?
  RET NZ                  ; Return if so
  CALL ONTMBFLOOR         ; Is ANGELFACE exactly on the top, middle or bottom
                          ; floor?
  RET NZ                  ; Return if not
  CALL GETRANDOM          ; A=random number
  RLCA                    ; Return 50% of the time
  RET NC                  ;
  CALL WITNESS_0          ; Can any adults see ANGELFACE?
  RET C                   ; Return if so
  LD B,3                  ; Is there anyone 3 spaces in front of ANGELFACE and
  CALL VICTIM             ; facing him?
  RET C                   ; Return if not
  POP BC                  ; Drop the return address (MVCHARS_11)
  POP BC                  ; Pop the value of HL that was pushed on to the stack
                          ; by the routine at MVCHARS before coming here; RET
                          ; will now take us back either to MVCHARS to move the
                          ; next character, or to the main loop
  LD HL,HITTING           ; Place the address of the uninterruptible subcommand
  LD (53009),HL           ; routine at HITTING into bytes 17 and 18 of
                          ; ANGELFACE's buffer
  RET

; Deal with ANGELFACE when he is hitting
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of ANGELFACE's buffer by the routine at VIOLENT. It controls
; ANGELFACE from the beginning to the end of the swing, and knocks over any
; unfortunate kid whose face comes into contact with the fist.
;
; H 207 (ANGELFACE)
HITTING:
  CALL UPDATESRB          ; Update the SRB for ANGELFACE's current animatory
                          ; state and location
  LD L,20                 ; Store ANGELFACE's current (pre-punch) animatory
  LD (HL),A               ; state in byte 20 of his buffer temporarily
  AND 240
  ADD A,8                 ; A=40 or 168: ANGELFACE raising his arm
  CALL SUBCMD             ; Update ANGELFACE's animatory state and return to
                          ; HITTING_0 (below) next time
; This entry point is used by the routine at HIT to check whether ERIC has hit
; anyone.
HITTING_0:
  LD B,2                  ; Is there anyone 2 spaces in front of the puncher
  CALL VICTIM             ; and facing him?
  JR C,HITTING_1          ; Jump if not
  CALL OBJFALL_8          ; Knock the punched character over
  EX DE,HL                ; H=puncher's character number, D=victim's character
                          ; number
  LD A,H                  ; A=puncher's character number
  CP 210                  ; Was ERIC the assailant?
  JR NZ,HITTING_1         ; Jump if not
  LD A,D                  ; D=victim's character number
  CP 207                  ; Did ERIC knock ANGELFACE over?
  JR NZ,HITTING_1         ; Jump if not
  LD A,3                  ; ERIC has punched ANGELFACE; add 30 to the score and
  CALL ADDPTS             ; print it
HITTING_1:
  CALL UPDATESRB          ; Update the SRB for the punching character's current
                          ; animatory state
  INC A                   ; A=animatory state of the punching character with
                          ; his fist fully raised
  JP BWFIRING_0           ; Update the punching character's animatory state,
                          ; then start the process of lowering the puncher's
                          ; arm (if it was ANGELFACE)

; Unused
  DEFB 0

; Knock down a main kid hit by a pellet
;
; Used by the routine at PELLET. Knocks down a main kid that was hit by a
; catapult pellet. Also adds 30 to the score if the pellet was ERIC's and the
; victim was ANGELFACE.
;
; D Kid's character number (206-210)
; H Pellet's character number (213 or 214)
KOKID:
  LD A,H                  ; A=pellet's character number
  CP 214                  ; Is this ERIC's catapult pellet?
  JR NZ,KOKID_0           ; Jump if not (it's BOY WANDER's)
  LD A,D                  ; A=character number of the kid hit by the pellet
  CP 208                  ; Did the pellet hit ANGELFACE?
  JR NZ,KOKID_0           ; Jump if not
  LD A,3                  ; ERIC's pellet hit ANGELFACE; add 30 to the score
  CALL ADDPTS             ; and print it
KOKID_0:
  JP OBJFALL_6            ; Terminate the pellet and knock the kid over

; Deal with the frog when it has been knocked out of a cup
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 the frog's buffer by the routine at CHKCUPFULL when a catapult
; pellet has knocked the frog out of a cup. It controls the frog up to the
; point where it hits the floor, and deals with any collision with MISS TAKE's
; head on the way.
FROGFALL:
  LD A,(INVENTORY)        ; INVENTORY holds the inventory flags
  RRCA                    ; Has ERIC got the safe key already?
  JR C,FROGFALL_5         ; Jump if so
  LD HL,TAKECBUF          ; Point HL at byte 0 of MISS TAKE's buffer
  LD A,(HL)               ; A=MISS TAKE's animatory state
  INC L                   ; L=1
  LD E,(HL)               ; E=MISS TAKE's x-coordinate
  INC L                   ; L=2
  CP 118                  ; 118: Is MISS TAKE sitting on the floor facing left?
  JR Z,FROGFALL_0         ; Jump if so
  CP 246                  ; 246: Is she sitting on the floor facing right?
  LD A,252                ; A=-4
  JR NZ,FROGFALL_1        ; Jump if not
  DEC E
  DEC E
FROGFALL_0:
  INC E                   ; Now E=x-coordinate the frog would have to be at to
                          ; be in line with MISS TAKE's head as she sits on the
                          ; floor
  LD A,253                ; A=-3
FROGFALL_1:
  ADD A,(HL)              ; A=y-coordinate the frog would have to be at to be
                          ; level with MISS TAKE's head
  LD H,212                ; 212=frog
  CP (HL)                 ; Compare with the frog's current y-coordinate
  JR NZ,FROGFALL_5        ; Jump if the frog has not reached the level of MISS
                          ; TAKE's head
  DEC L                   ; L=1
  LD A,(HL)               ; A=frog's x-coordinate
  CP E                    ; Compare with the x-coordinate of MISS TAKE's head
  JR NZ,FROGFALL_5        ; Jump if they don't match
; ERIC's pellet has knocked the frog out of the cup and onto MISS TAKE's head.
; Celebrate!
  LD HL,INVENTORY         ; INVENTORY holds the inventory flags
  SET 0,(HL)              ; Give ERIC the safe key
  LD A,200                ; Add 2000 to the score and print it
  CALL ADDPTS             ;
  LD HL,768               ; Make the safe-key-getting celebratory sound effect
FROGFALL_2:
  LD D,1                  ;
  LD A,L                  ;
  AND 63                  ;
  INC A                   ;
  LD B,A                  ;
  LD C,(HL)               ;
  SET 4,C                 ;
  CALL SNDEFFECT          ;
  DEC L                   ;
  JR NZ,FROGFALL_2        ;
  NOP                     ;
  NOP                     ;
  NOP                     ;
  CALL PRINTINV           ; Print the inventory (which now includes the safe
                          ; key)
  LD H,212                ; 212=frog
  CALL UPDATESRB          ; Update the SRB for the frog's current animatory
                          ; state and location
  DEC E                   ; Move the frog one space to the left
  INC A                   ; A=29: frog facing left, airborne (phase 1)
FROGFALL_3:
  INC D                   ; Make the frog fall one level
FROGFALL_4:
  JP UPDATEAS             ; Update the frog's animatory state and location and
                          ; update the SRB
; The frog is still above the floor, but not in contact with MISS TAKE's head.
FROGFALL_5:
  CALL UPDATESRB          ; Update the SRB for the frog's current animatory
                          ; state and location
  BIT 4,D                 ; Is the frog about to hit the floor (D=16)?
  JR Z,FROGFALL_3         ; Jump if not (to make it fall another level)
  CP 28                   ; The frog's animatory state will be 29 if it bounced
                          ; off MISS TAKE's head
  LD A,28                 ; 28=frog sitting still, facing left
  JR NZ,FROGFALL_4        ; Jump if the frog bounced off MISS TAKE's head
  INC D                   ; D=17 (bottom floor)
; This entry point is used by the routine at MVFROG.
FROGFALL_6:
  CALL SUBCMD             ; Update the frog's animatory state and location and
                          ; then resume control at MVFROG

; Control the frog
;
; This uninterruptible subcommand routine controls the frog's movements. The
; main entry point is used when the frog is sitting on the floor, deciding
; whether to stay put or take a hop.
;
; H 212 (frog)
MVFROG:
  LD DE,(53761)           ; Pick up ERIC's coordinates in DE
  LD L,2                  ; Point HL at byte 2 of the frog's buffer
  LD A,(HL)               ; A=frog's y-coordinate
  CP D                    ; Does it match ERIC's y-coordinate?
  JR NZ,MVFROG_1          ; Jump if not
  DEC L                   ; L=1
  LD A,(HL)               ; A=frog's x-coordinate
  SUB E                   ; Is ERIC to the left of the frog?
  JR NC,MVFROG_0          ; Jump if so
  NEG
MVFROG_0:
  CP 4                    ; Now A=horizontal distance between the frog and ERIC
  JR NC,MVFROG_1          ; Jump if this is greater than 3
  CALL GETRANDOM          ; A=random number >= 192
  OR 192                  ;
  JR MVFROG_2
MVFROG_1:
  CALL GETRANDOM          ; A=random number
  CP 192                  ; Return 3 times out of 4 (without moving the frog)
  RET C                   ; when ERIC is not nearby
; The frog has decided to move. It has three options: turn round, short hop
; (one space forward), or long hop (three spaces forward).
MVFROG_2:
  CP 240                  ; A>=192 at this point
  JR C,MVFROG_5           ; Jump if 192<=A<240 (75% of the time) to make a
                          ; short or long hop
MVFROG_3:
  CALL UPDATESRB          ; Update the SRB for the frog's current animatory
                          ; state and location
  INC A                   ; A=29/157: frog airborne (phase one)
  CALL SUBCMD             ; Update the frog's animatory state and then resume
                          ; control at entry point MVFROG_4 (below)
; This entry point is used when the frog has jumped into the air in order to
; turn round (either by choice, or because a wall or closed door is in the
; way).
MVFROG_4:
  CALL UPDATESRB          ; Update the SRB for the frog's current animatory
                          ; state
  DEC A                   ; A=28/156: frog sitting
  XOR 128                 ; Turn the frog round
  JP FROGFALL_6           ; Update the frog's animatory state and then resume
                          ; control at the main entry point (MVFROG)
; The frog is planning a hop. Decide between a short hop (one space forward)
; and a long hop (three spaces forward).
MVFROG_5:
  LD L,0                  ; Point HL at byte 0 of the frog's buffer
  CP 216                  ; Set the carry flag if 192<=A<216
  BIT 7,(HL)              ; Set the zero flag if the frog is facing left
  LD L,1                  ; Pick up the frog's x-coordinate in E and
  LD E,(HL)               ; y-coordinate in D
  LD L,2                  ;
  LD D,(HL)               ;
  JR C,MVFROG_11          ; Jump if A<216 (50% of the time) to consider a long
                          ; hop
  JR NZ,MVFROG_6          ; Jump if the frog is facing right
  DEC E
  DEC E
MVFROG_6:
  INC E                   ; E=x-coordinate of the spot directly in front of the
                          ; frog
  LD HL,DOORFLAGS         ; DOORFLAGS holds the door/window status flags
  LD A,(HL)               ; Pick these up in A
  LD B,A                  ; Save a copy of them in B briefly
  AND 63                  ; Reset bits 6 and 7 of the door/window status flags
  LD (HL),A               ; (i.e. pretend that the windows are closed)
  CALL CHKOBJPATH         ; Check for closed doors and walls in the frog's path
  LD (HL),B               ; Restore the original door/window status flags
  LD H,212                ; 212=frog
  JR C,MVFROG_3           ; Turn the frog round if there's a wall or closed
                          ; door in its path
MVFROG_7:
  CALL UPDATESRB          ; Update the SRB for the frog's current animatory
                          ; state and location
  INC A                   ; A=29/157: frog airborne (phase one)
MVFROG_8:
  RLCA
  RRCA
  JR C,MVFROG_9           ; Jump if the frog is facing right
  DEC E
  DEC E
MVFROG_9:
  INC E                   ; Now E=x-coordinate of the spot directly in front of
                          ; the frog
  CALL SUBCMD             ; Update the frog's animatory state and then resume
                          ; control at entry point MVFROG_10 (below)
; This entry point is used when the frog has just jumped into the air for a
; short hop, or is just about to finish a long hop. Either way, the frog is
; going to land now.
MVFROG_10:
  CALL UPDATESRB          ; Update the SRB for the frog's current animatory
                          ; state and location
  DEC A                   ; A=28/156: frog sitting
  JP FROGFALL_6           ; Update the frog's animatory state and then resume
                          ; control at the main entry point (MVFROG)
; The frog is planning a long hop. Check whether the coast is clear up to three
; spaces ahead.
MVFROG_11:
  LD C,0
  JR NZ,MVFROG_12         ; Jump if the frog is facing right
  DEC C
  DEC C
MVFROG_12:
  INC C                   ; C=1 if the frog's facing right, 255 if facing left
  LD HL,DOORFLAGS         ; DOORFLAGS holds the door/window status flags
  LD A,(HL)               ; Pick these up in A
  LD B,A                  ; Save a copy of them in B briefly
  AND 63                  ; Reset bits 6 and 7 of the door/window status flags
  LD (HL),A               ; (i.e. pretend that the windows are closed)
  PUSH BC
  LD B,3                  ; We will test up to 3 spaces in front of the frog
MVFROG_13:
  LD A,E                  ; Set E to the x-coordinate of the spot 1, 2 or 3
  ADD A,C                 ; spaces in front of the frog
  LD E,A                  ;
  PUSH DE
  CALL CHKOBJPATH         ; Check for walls and closed doors in the frog's path
  POP DE
  JR C,MVFROG_14          ; Jump if there is one
  DJNZ MVFROG_13          ; Jump back until we've checked up to 3 spaces in
                          ; front of the frog
MVFROG_14:
  POP BC
  LD (HL),B               ; Restore the original door/window status flags to
                          ; DOORFLAGS
  LD H,212                ; 212=frog
  JP C,MVFROG_3           ; Turn the frog round if there's a wall or closed
                          ; door less than four spaces ahead
  CALL MVFROG_7           ; Otherwise move the frog one space forward and
                          ; adjust its animatory state to 29/157 (airborne,
                          ; phase one)
  LD L,17                 ; Change the address in bytes 17 and 18 of the frog's
  LD (HL),85              ; buffer to MVFROG_15 (below) so that we resume there
  RET
; This entry point is used when the frog has just jumped into the air in order
; to make a long hop (three spaces forward). It is also used by the routine at
; MVMOUSE to introduce the frog into the game with an initial animatory state
; of 29 (facing left, airborne, phase 1) and location (59,10).
MVFROG_15:
  CALL MVFROG_7           ; Move the frog one space forward and adjust its
                          ; animatory state to 30/158 (airborne, phase 2)
  LD L,17                 ; Change the address in bytes 17 and 18 of the frog's
  LD (HL),93              ; buffer to FROGHIGH (below) so that we resume there
  RET
; This entry point is used when the frog is at the highest point of a long hop.
FROGHIGH:
  CALL UPDATESRB          ; Update the SRB for the frog's current animatory
                          ; state and location
  DEC A                   ; A=29/157: frog airborne (phase one)
  JP MVFROG_8             ; Move the frog one space forward

; Unused
  DEFS 2

; Control a female character while she's standing on a chair or jumping (1)
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a female character's buffer by the routine at STARTJPING. It
; deals with a female character while she is either standing on a chair or
; jumping up and down (to evade a mouse).
;
; H Character number (183-189, 204 or 209)
GIRLJPING1:
  LD L,19                 ; Byte 19 of the character's buffer holds a counter
                          ; determining how much longer she will continue
                          ; jumping
  DEC (HL)                ; Is it time to stop jumping?
  JR NZ,GIRLJPING2        ; Jump if not
  CALL STOPJPING          ; Return this character to the floor
  LD L,18                 ; Remove the address of this routine from bytes 17
  LD (HL),0               ; and 18 of the character's buffer
  RET

; Unused
  DEFB 0

; Make a female character stand on a chair or start jumping
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a female character's buffer by the routine at CHKGIRLS when
; there's a mouse in the vicinity. It makes the female character either stand
; on a chair (if there's one beside her) or start jumping up and down.
;
; H Character number (183-189, 204 or 209)
STARTJPING:
  LD L,17                 ; Replace the address of this routine in bytes 17 and
  LD (HL),102             ; 18 of the character's buffer with that of
                          ; GIRLJPING1
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  LD L,20                 ; Store the character's current animatory state in
  LD (HL),A               ; byte 20 of her buffer, so it can be restored when
                          ; she's finished jumping
  INC L                   ; Initialise byte 21 to 0 on the assumption that
  LD (HL),0               ; there is no chair to stand on
  DEC D                   ; Jump up
  PUSH DE
  LD L,0                  ; Pretend the character is facing left by resetting
  RES 7,(HL)              ; bit 7 of her animatory state (to make the chair
                          ; check work)
  CALL BYSEAT             ; Check whether this character is beside a chair
  POP DE
  LD L,21
  AND A                   ; Is this character standing beside a chair?
  JR NZ,STARTJPING_0      ; Jump if not
  INC (HL)                ; Set byte 21 to 1 to indicate that the character is
                          ; standing on a chair
STARTJPING_0:
  DEC L                   ; Collect the character's original animatory state
  LD A,(HL)               ; from byte 20
  AND 248                 ; A=character's base animatory state
  JP UPDATEAS             ; Update the character's animatory state and location
                          ; and update the SRB

; Control a female character while she's standing on a chair or jumping (2)
;
; Used by the routine at GIRLJPING1. Does nothing if the female character is
; standing on a chair; otherwise makes her jump up or return to the floor.
;
; H Character number (183-189, 204 or 209)
GIRLJPING2:
  LD L,21                 ; Bit 0 of byte 21 of the character's buffer is set
  BIT 0,(HL)              ; if she's standing on a chair
  RET NZ                  ; Return if she is standing on a chair
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state and location
  LD L,19                 ; The counter at byte 19 is even if the character is
                          ; in the air, odd if she's got her feet on the floor
  INC D                   ; Down a level
  BIT 0,(HL)              ; Is this character in the air at the moment?
  JR Z,GIRLJPING2_0       ; Jump if so
  DEC D                   ; Up a level
  DEC D                   ;
GIRLJPING2_0:
  JP UPDATEAS             ; Update the character's y-coordinate  and update the
                          ; SRB

; Unused
  DEFS 2

; Make any female characters near a mouse start or continue jumping
;
; Used by the routine at MVMOUSE. Does nothing if the mouse is off-screen.
; Otherwise, for each female character that is facing the mouse and within five
; spaces to the left or right of it, either (a) makes her stand on a chair or
; start jumping if she isn't already, or (b) makes her stay on the chair or
; keep jumping for a little longer.
;
; H Mouse's character number (198, 199, 206-208, 212)
CHKGIRLS:
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  LD B,A                  ; Copy this to B
  LD L,1                  ; Point HL at byte 1 of the mouse's buffer
  LD A,(HL)               ; A=mouse's x-coordinate
  LD E,A                  ; Copy this to E
  SUB B
  INC A
  CP 32                   ; Is this mouse off-screen?
  RET NC                  ; Return if so
  INC L                   ; L=2
  LD D,(HL)               ; D=mouse's y-coordinate
  LD H,183                ; 183=little girl no. 1
  LD B,7                  ; There are 7 little girls to check
  CALL CHKGIRLS_0         ; Make them start jumping if necessary
  LD H,204                ; 204=MISS TAKE
  INC B                   ; B=1
  CALL CHKGIRLS_0         ; Make MISS TAKE start jumping if necessary
  LD H,209                ; 209=HAYLEY
  INC B                   ; B=1
; At this point H holds the number of the first female character to check, and
; B the number of characters to check (for proximity to a mouse).
CHKGIRLS_0:
  LD L,2                  ; Point HL at byte 2 of the female character's buffer
  LD A,D                  ; A=mouse's y-coordinate
  SUB (HL)                ; Subtract that of the female character under
                          ; consideration
  JR Z,CHKGIRLS_1         ; Jump if the female character is standing on the
                          ; same floor as the mouse
  DEC A                   ; Is the female character one level above the mouse?
  JR NZ,CHKGIRLS_5        ; Jump ahead to consider the next character if not
  LD L,18                 ; Pick up the MSB of any uninterruptible subcommand
  LD A,(HL)               ; routine address in bytes 17 and 18 of the
                          ; character's buffer
  CP 121                  ; Is this character jumping (121=MSB of GIRLJPING1 or
                          ; STARTJPING)?
  JR NZ,CHKGIRLS_5        ; Jump ahead to consider the next character if not
CHKGIRLS_1:
  LD L,1                  ; Point HL at byte 1 of the character's buffer
  LD A,(HL)               ; A=female character's x-coordinate
  SUB E                   ; Subtract that of the mouse
  LD L,0                  ; Point HL at byte 0 of the character's buffer
  JR Z,CHKGIRLS_3         ; Jump if the girl is standing right where the mouse
                          ; is
  JR NC,CHKGIRLS_2        ; Jump if the girl is standing to the right of the
                          ; mouse
  CP 251                  ; Is the girl within 5 spaces to the left of the
                          ; mouse?
  JR C,CHKGIRLS_5         ; Jump ahead to consider the next character if not
  BIT 7,(HL)              ; Is the female character facing left?
  JR Z,CHKGIRLS_5         ; Jump ahead to consider the next character if so
  JR CHKGIRLS_3
CHKGIRLS_2:
  CP 6                    ; Is the girl within 5 spaces to the right of the
                          ; mouse?
  JR NC,CHKGIRLS_5        ; Jump ahead to consider the next character if not
  BIT 7,(HL)              ; Is the female character facing right?
  JR NZ,CHKGIRLS_5        ; Jump ahead to consider the next character if so
; The female character is located within 5 spaces of the mouse, and is facing
; it. Figure out whether she's already jumping, or can start jumping.
CHKGIRLS_3:
  LD L,18                 ; Pick up the MSB of any uninterruptible subcommand
  LD A,(HL)               ; routine address in bytes 17 and 18 of the female
                          ; character's buffer
  CP 121                  ; Set the zero flag if the routine address is
                          ; GIRLJPING1 or STARTJPING (i.e. the character is
                          ; already jumping)
  INC HL                  ; Point HL at byte 19 of the character's buffer
  JR NZ,CHKGIRLS_4        ; Jump if the character is not jumping
  SET 4,(HL)              ; Otherwise ensure this character continues jumping
                          ; for a little longer by setting the counter in byte
                          ; 19 to at least 16
  JR CHKGIRLS_5           ; Jump ahead to consider the next character
CHKGIRLS_4:
  AND A                   ; Is there an uninterruptible subcommand routine
                          ; address other than GIRLJPING1 or STARTJPING in
                          ; bytes 17 and 18 of the character's buffer (meaning
                          ; she is otherwise occupied)?
  JR NZ,CHKGIRLS_5        ; Jump ahead to consider the next character if so
; The female character is not jumping at the moment. Make her start.
  LD (HL),21              ; Set the delay parameter at byte 19 determining how
  DEC L                   ; long the female character will jump up and down
  LD (HL),121             ; Place the address of the uninterruptible subcommand
  DEC L                   ; routine at STARTJPING into bytes 17 and 18 of the
  LD (HL),116             ; female character's buffer
CHKGIRLS_5:
  INC H                   ; Next female character
  DJNZ CHKGIRLS_0         ; Jump back until all the characters have been
                          ; checked
  RET

; Unused
  DEFS 2

; Control a mouse
;
; The address of this uninterruptible subcommand routine is placed into bytes
; 17 and 18 of a mouse's buffer by the routine at PREPMICE (if the mouse was
; released by ERIC) or by the routine at PREPGAME (when preparing for a new
; game). Bytes 16 and 19-22 of the mouse's buffer are used to store the
; following values that control its movement:
;
; +------+--------------------------------------------------------------------+
; | Byte | Contents                                                           |
; +------+--------------------------------------------------------------------+
; | 16   | Counter decremented each time the mouse hides (if the mouse was    |
; |      | released by ERIC); when it reaches 0, the mouse dies (returns      |
; |      | control to the character whose buffer it borrowed)                 |
; | 19   | Direction of travel, and the distance remaining to travel before   |
; |      | hiding or considering a change of direction: it is positive (bit 7 |
; |      | reset) if the mouse is travelling right, and decremented on each   |
; |      | pass; it is negative (bit 7 set) if the mouse is travelling left,  |
; |      | and incremented on each pass; while the mouse is hiding, it is 0   |
; | 20   | Counter decremented on each change of direction or distance to     |
; |      | travel; when it reaches 0, the mouse hides                         |
; | 21   | Counter determining how much longer the mouse has left to hide     |
; | 22   | x-coordinate of the mouse when it last hid                         |
; +------+--------------------------------------------------------------------+
;
; H Mouse's character number (198, 199, 206-208, 212)
MVMOUSE:
  LD L,19                 ; A=remaining distance to travel
  LD A,(HL)               ;
  AND A                   ; Is this mouse (a) hiding, (b) about to hide, or (c)
                          ; about to consider a change of direction and
                          ; distance to travel?
  JR Z,MVMOUSE_9          ; Jump if so
MVMOUSE_0:
  LD A,(DOORFLAGS)        ; DOORFLAGS holds the door/window status flags
  PUSH AF                 ; Save these flags
  AND 63                  ; Reset bits 6 and 7 of the door/window status flags
  LD (DOORFLAGS),A        ; (i.e. pretend that the windows are closed)
MVMOUSE_1:
  BIT 7,(HL)              ; Set the zero flag if the mouse is travelling right
  LD L,1                  ; Collect the mouse's x-coordinate in E
  LD E,(HL)               ;
  JR Z,MVMOUSE_2          ; Jump if the mouse is travelling right
  DEC E
  DEC E
MVMOUSE_2:
  INC E                   ; E=x-coordinate of the spot in front of the mouse
  INC L                   ; L=2
  LD A,(HL)               ; A=mouse's y-coordinate
  LD D,A                  ; Copy this to D
  CP 10                   ; Is the mouse on the middle floor?
  JR NZ,MVMOUSE_3         ; Jump if not
  LD A,E                  ; A=x-coordinate of the spot in front of the mouse
  CP 84                   ; Is the mouse at the top of the stairs leading down
                          ; to the stage?
  JR Z,MVMOUSE_4          ; Turn the mouse round if so
MVMOUSE_3:
  CALL CHKOBJPATH         ; Check for walls and closed doors in the mouse's
                          ; path
  JR NC,MVMOUSE_5         ; Jump if there are none
MVMOUSE_4:
  LD L,19                 ; Turn the mouse round without changing the distance
  LD A,(HL)               ; remaining to travel
  NEG                     ;
  LD (HL),A               ;
  JR MVMOUSE_1            ; See what's in front of the mouse in this direction
; There are no walls or closed doors in the mouse's path.
MVMOUSE_5:
  POP AF                  ; Restore the door/window status flags
  LD (DOORFLAGS),A        ;
  CALL UPDATESRB          ; Update the SRB for the mouse's current animatory
                          ; state and location
  LD L,19                 ; Is the mouse travelling right?
  BIT 7,(HL)              ;
  JR Z,MVMOUSE_6          ; Jump if so
  INC (HL)                ; Decrease the distance left to travel
  DEC E                   ; Move the mouse one space left
  LD A,47                 ; 47: animatory state of a mouse travelling left
  JR MVMOUSE_8
MVMOUSE_6:
  DEC (HL)                ; Decrease the distance left to travel
  INC E                   ; Move the mouse one space right
MVMOUSE_7:
  LD A,175                ; 175: animatory state of a mouse travelling right
MVMOUSE_8:
  JP UPDATEAS             ; Update the mouse's animatory state and location and
                          ; update the SRB
; Byte 19 holds 0, which means (a) the mouse is hiding at the moment, or (b)
; it's time for the mouse to consider a change of direction and distance to
; travel, or (c) it's time to hide for a bit.
MVMOUSE_9:
  INC L                   ; Pick up byte 20 in A
  LD A,(HL)               ;
  AND A                   ; Is the mouse already hiding?
  JR Z,MVMOUSE_12         ; Jump if so
  DEC (HL)                ; Is it time for the mouse to hide temporarily?
  JR NZ,MVMOUSE_10        ; Jump if not
  LD L,1                  ; Point HL at byte 1 of the mouse's buffer
  LD A,(HL)               ; A=mouse's x-coordinate
  LD L,22                 ; Store this in byte 22 of the mouse's buffer for
  LD (HL),A               ; later retrieval
  JP OBJFALL_1            ; Make the mouse hide
; It's time for the mouse to consider a change of direction and distance to
; travel.
MVMOUSE_10:
  PUSH HL
  CALL CHKGIRLS           ; Make any girls near this mouse start jumping
  POP HL
  CALL GETRANDOM          ; A=random number
  AND 131                 ; A=2-5 (bit 7 reset) or 130-133 (bit 7 set)
  ADD A,2                 ;
  RLCA                    ; A=2-5 (bit 7 reset), with the carry flag holding
  AND A                   ; the previous contents of bit 7
  RRA                     ;
  JR C,MVMOUSE_11         ; Jump if bit 7 was set (the new direction is right)
  NEG                     ; The new direction is left
MVMOUSE_11:
  LD L,19                 ; Set the new direction and distance to travel
  LD (HL),A               ;
  JP MVMOUSE_0            ; Start moving
; The mouse is currently hiding. Check whether it should transform into a frog
; before reappearing (in case ERIC obtained the Science Lab storeroom key in
; the meantime).
MVMOUSE_12:
  INC L                   ; L=21
  DEC (HL)                ; Is it time for the mouse to come out of hiding?
  RET NZ                  ; Return if not
  LD A,H                  ; A=mouse's character number
  CP 212                  ; Was this mouse released by ERIC?
  JR NZ,MVMOUSE_14        ; Jump if so
  LD A,(INVENTORY)        ; INVENTORY holds the inventory flags
  BIT 1,A                 ; Has ERIC got the Science Lab storeroom key?
  JR Z,MVMOUSE_13         ; Jump if not
  LD A,29                 ; 29: animatory state of the frog
  LD DE,2619              ; (E,D)=(59,10) (location where the frog enters the
                          ; game)
  CALL UPDATEAS           ; Update the frog's animatory state and location and
                          ; update the SRB
  JP MVFROG_15            ; Bring the frog into the game
; It's time for this mouse to come out of hiding.
MVMOUSE_13:
  CALL GETRANDOM          ; A=random number between 5 and 20; this determines
  AND 15                  ; how long the mouse will hide next time
  ADD A,5                 ;
  LD (HL),A               ; Store this in byte 21 of the mouse's buffer
  CALL GETRANDOM          ; A=random number between 2 and 5; this determines
  AND 3                   ; how many changes of direction and distance to
  ADD A,2                 ; travel the mouse will make before hiding again
  DEC L                   ; Store this in byte 20 of the mouse's buffer
  LD (HL),A               ;
  LD L,22                 ; E=x-coordinate of the mouse before it hid
  LD E,(HL)               ;
  LD L,2                  ; D=y-coordinate of the mouse
  LD D,(HL)               ;
  JP MVMOUSE_7            ; Unhide the mouse
; This mouse was released by ERIC and is currently hiding. It's due to come out
; of hiding, or die.
MVMOUSE_14:
  LD L,16                 ; Byte 16 holds the mouse's lifespan counter
  DEC (HL)                ; Is it time for this mouse to disappear for ever?
  JR NZ,MVMOUSE_16        ; Jump if not
; This mouse must die, and return control of the buffer it hijacked (198, 199,
; 206-208) to its original owner (little boy no. 9 or 10, BOY WANDER, ANGELFACE
; or EINSTEIN).
MVMOUSE_15:
  LD L,29                 ; Set bits 0 and 7 at byte 29: restart the command
  LD (HL),129             ; list and make the character run
  LD L,10                 ; Remove any interruptible subcommand routine address
  LD (HL),0               ; that may have been in the character's buffer before
                          ; it was hijacked
  LD L,15                 ; Pick up from byte 15 the animatory state of the
  LD A,(HL)               ; character whose buffer the mouse borrowed
  AND 240                 ; A=character's base animatory state
  LD L,0                  ; Restore this character's animatory state to its
  LD (HL),A               ; base value
  INC L                   ; Make the character reappear at x-coordinate 32
  LD (HL),32              ; (which should be off-screen because ERIC cannot
                          ; release mice in the boys' skool)
  JP RMUSUBCMD            ; Terminate this uninterruptible subcommand, thus
                          ; returning control to the character who normally
                          ; owns the buffer
; This mouse (released by ERIC) is about to come out of hiding.
MVMOUSE_16:
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  CP 144                  ; Is at least half of the girls' skool on-screen?
  JR C,MVMOUSE_15         ; Terminate the mouse if not
  LD L,21
  JR MVMOUSE_13           ; Otherwise make the mouse reappear

; Release some mice (if possible)
;
; The address of this routine is placed into ERICDATA1 by the routine at
; RELEASE. It releases up to five mice, depending on (a) how many ERIC has
; caught, and (b) the availability of character buffers 198 and 199 (little
; boys 9 and 10) and 206-208 (BOY WANDER, ANGELFACE and EINSTEIN).
;
; H 210 (ERIC)
DROPMICE:
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  CP 144                  ; Is at least half of the girls' skool on-screen?
  RET C                   ; Return if not
  LD A,(MOUSETALLY)       ; MOUSETALLY holds the number of mice ERIC has caught
  AND A                   ; Has ERIC caught any mice?
  RET Z                   ; Return if not
  PUSH HL
  PUSH AF                 ; Save the number of captive mice
  LD L,2                  ; Collect ERIC's coordinates in DE
  LD D,(HL)               ;
  DEC L                   ;
  LD E,(HL)               ;
  INC E                   ; E=x-coordinate of the spot in front of ERIC (if
                          ; he's facing right)
  DEC L                   ; L=0
  BIT 7,(HL)              ; Is ERIC facing right?
  JR NZ,DROPMICE_0        ; Jump if so
  DEC E                   ; E=x-coordinate of the spot in front of ERIC (if
  DEC E                   ; he's facing left)
DROPMICE_0:
  CALL PREPMICE           ; Release up to five mice at the spot in front of
                          ; ERIC
  POP AF                  ; A=number of mice ERIC had before trying to release
                          ; some
  LD HL,MOUSETALLY        ; MOUSETALLY holds the number of mice ERIC has left
  CP (HL)                 ; Set the zero flag if ERIC couldn't release any mice
  LD A,(HL)               ; A=number of mice left in ERIC's pocket
  CALL NZ,CATCHMORF_6     ; Print these in the inventory
  POP HL
  RET

; Prepare character buffers for released mice
;
; Used by the routine at DROPMICE. Releases up to five mice from ERIC's pocket.
; Each mouse will use a character buffer at page 198, 199 or 206-208, provided
; that the character who usually occupies it is off-screen.
;
; DE Coordinates at which to release the mice
PREPMICE:
  LD H,198                ; Use buffers 198 and 199 (little boys nos. 9 and 10)
  LD B,2                  ; for released mice if possible
  CALL PREPMICE_0         ; Prepare these buffers
  RET Z                   ; Return if ERIC has no mice left
  LD H,206                ; Also use buffers 206-208 (BOY WANDER, ANGELFACE and
  LD B,3                  ; EINSTEIN) for released mice if possible
PREPMICE_0:
  LD L,18                 ; Pick up the MSB of the uninterruptible subcommand
  LD A,(HL)               ; routine address in bytes 17 and 18 of the
                          ; character's buffer
  CP 122                  ; Does the MSB match that of MVMOUSE?
  JR Z,PREPMICE_1         ; Jump if so
  LD L,1                  ; A=x-coordinate of the character using this buffer
  LD A,(HL)               ;
  CP 142                  ; Is this character inside or close to the girls'
                          ; skool?
  JR NC,PREPMICE_1        ; Jump if so (we can't use his buffer)
  DEC L                   ; L=0
  LD A,(HL)               ; A=character's animatory state
  LD L,15                 ; Store this in byte 15 of the buffer for retrieval
  LD (HL),A               ; when the mouse dies
  CALL GETRANDOM          ; A=random number between 10 and 41; this determines
  AND 31                  ; the lifespan of the mouse
  ADD A,10                ;
  INC L                   ; Store this lifespan parameter in byte 16 of the
  LD (HL),A               ; buffer
  INC L                   ; Place the address of the uninterruptible subcommand
  LD (HL),22              ; routine at MVMOUSE into bytes 17 and 18 of the
  INC L                   ; character's buffer
  LD (HL),122             ;
  XOR A                   ; Set bytes 19 and 20 to 0 so that the mouse is
  INC L                   ; initially hidden (see MVMOUSE for details on how
  LD (HL),A               ; these bytes are used)
  INC L                   ;
  LD (HL),A               ;
  INC L                   ; Set byte 21 (time remaining before coming out of
  LD (HL),B               ; hiding) to 3, 2 or 1
  INC L                   ; Store the x-coordinate of the mouse in byte 22, so
  LD (HL),E               ; it appears there when it comes out of hiding
  LD L,2                  ; Store the y-coordinate in byte 2
  LD (HL),D               ;
  LD A,(MOUSETALLY)       ; MOUSETALLY holds the number of mice ERIC has caught
  DEC A                   ; One fewer now
  LD (MOUSETALLY),A       ;
  RET Z                   ; Return if ERIC has no mice left
PREPMICE_1:
  INC H                   ; Point to the next potential buffer for a released
                          ; mouse
  DJNZ PREPMICE_0         ; Jump back until all have been considered
  RET

; Unused
  DEFB 0

; Make ERIC catch a mouse or frog (if present)
;
; The address of this routine is placed into ERICDATA1 by the routine at CATCH
; after 'C' has been pressed. Adds the mouse or frog to ERIC's inventory if he
; has managed to catch it. If the captive animal is a mouse, another one is set
; free for ERIC to catch.
;
; H 210 (ERIC)
CATCHMORF:
  LD L,0                  ; Point HL at byte 0 of ERIC's buffer
  LD A,(HL)               ; A=ERIC's animatory state
  RLCA                    ; Set A to -1 if ERIC is facing left, or 1 if he's
  SBC A,A                 ; facing right
  ADD A,A                 ;
  CPL                     ;
  INC L                   ; L=1
  ADD A,(HL)              ; Now A=x-coordinate of the spot in front of ERIC
  INC L                   ; L=2
  LD DE,(54273)           ; Pick up the coordinates of the animal using buffer
                          ; 212 (mouse or frog)
  CP E                    ; Is there a mouse or frog at this x-coordinate?
  RET NZ                  ; Return if not
  LD A,(HL)               ; A=ERIC's y-coordinate
  CP D                    ; Is the mouse/frog on this floor?
  RET NZ                  ; Return if not
  LD A,(ANIMALCBUF)       ; Pick up the mouse/frog's animatory state in A
  AND 127                 ; Discard all but the 'direction' bit (bit 7)
  CP 47                   ; 47: Is it a mouse?
  JP NZ,CATCHFROG         ; Jump if not (it's the frog)
; ERIC has managed to catch the mouse.
  LD H,212                ; H=212 (the mouse's buffer)
  CALL UPDATESRB          ; Update the SRB for the mouse's current animatory
                          ; state and location
; Determine where to release another mouse.
  LD HL,LEFTCOL           ; LEFTCOL holds the column of the play area at the
                          ; far left of the screen
CATCHMORF_0:
  CALL GETRANDOM          ; A=R, a random number from 0 to 7; we'll use this to
  AND 7                   ; determine where the new free mouse will be placed
  LD D,A                  ; Copy this random number to D
  CP 2                    ; Jump if R>=2
  JR NC,CATCHMORF_1       ;
; R=0 or 1. The next section of code is intended to set the x-coordinate for
; the the new free mouse to 170 provided that X<128, or try again with a new
; random number (R) if X>=128. However, at this point HL may have been set to
; 32522 (byte 10 of the screen refresh buffer) or 32600 (byte 4 of the buffer
; for the Blue Room blackboard) by the LD L,10 or LD L,88 instruction below,
; and so the contents of 32522 or 32600 (instead of X at LEFTCOL) may be
; compared with 128. Thus it is possible for the new free mouse to appear at
; x-coordinate 170 even when X>=128, which means it could appear on-screen.
; This is a bug.
  LD A,(HL)               ; A=X (column of the play area at the far left of the
                          ; screen), or the contents of 32522 or 32600
  RLCA
  JR C,CATCHMORF_0        ; Jump if A>=128 to get another random number
  LD L,170                ; This will be the x-coordinate for the new free
                          ; mouse
  JR CATCHMORF_3          ; Go and figure out the y-coordinate
CATCHMORF_1:
  CP 5
  LD A,(HL)               ; A=X (column of the play area at the far left of the
                          ; screen), or the contents of 32522 or 32600
  JR NC,CATCHMORF_2       ; Jump if R>=5
; R=2, 3 or 4. The next section of code is intended to set the x-coordinate for
; the new free mouse to 88 provided that X<56 or X>=104, or try again with a
; new random number (R) if 56<=X<104. However, as noted above, HL may now hold
; 32522 or 32600 instead of LEFTCOL, and so the contents of 32522 or 32600
; (instead of X) may be compared with 56 and 104. Thus it is possible for the
; new free mouse to appear at x-coordinate 88 even when 56<=X<104, which means
; it could appear on-screen. This is a bug.
  CP 56
  LD L,88                 ; This is a possible x-coordinate for the new free
                          ; mouse
  JR C,CATCHMORF_3        ; Jump if A<56 to figure out the y-coordinate
  CP 104                  ; Jump if 56<=A<104 to get another random number
  JR C,CATCHMORF_0        ;
  JR CATCHMORF_3          ; Go and figure out the y-coordinate for the mouse
; R=5, 6 or 7. The next section of code is intended to set the x-coordinate for
; the new free mouse to 10 provided that X>=56, or try again with a new random
; number (R) if X<56. However, as noted above, HL may now hold 32522 or 32600
; instead of LEFTCOL, and so the contents of 32522 or 32600 (instead of X) may
; be compared with 56. Thus it is possible for the new free mouse to appear at
; x-coordinate 10 even when X<56, which means it could appear on-screen. This
; is a bug.
CATCHMORF_2:
  LD L,10                 ; This is a possible x-coordinate for the new free
                          ; mouse
  CP 56                   ; Jump if A<56 to get another random number
  JR C,CATCHMORF_0        ;
; The x-coordinate for the new free mouse (10, 88 or 170) has been determined.
; Now to determine the mouse's y-coordinate.
CATCHMORF_3:
  LD A,D                  ; A=R (the random number from 0 to 7)
CATCHMORF_4:
  SUB 3                   ; Convert this to a random number from 0 to 2 by
  JR NC,CATCHMORF_4       ; taking the remainder when divided by 3
  ADD A,3                 ;
  LD H,17                 ; This is a possible y-coordinate for the new free
                          ; mouse (bottom floor)
  JR Z,CATCHMORF_5        ; Jump if the random number is 0
  DEC A
  LD H,10                 ; This is a possible y-coordinate for the new free
                          ; mouse (middle floor)
  JR Z,CATCHMORF_5        ; Jump if the random number was 1
  LD H,3                  ; This is the y-coordinate for the new free mouse
                          ; (top floor)
CATCHMORF_5:
  LD (54273),HL           ; Fill in the location of the new free mouse
  LD A,10                 ; Add 100 to the score, print it and make a sound
  CALL CATCHMOUSE         ; effect
  LD HL,MOUSETALLY        ; MOUSETALLY holds the number of mice ERIC has caught
  INC (HL)                ; Increase this by 1
  LD A,(HL)
; Now it's time to update the on-screen mouse inventory. This entry point is
; used by the routine at DROPMICE.
CATCHMORF_6:
  LD A,(MOUSETALLY)       ; A=number of mice in ERIC's pocket
  LD C,A                  ; C will count the number of mice remaining to be
                          ; printed in the inventory
  LD L,168                ; 168=LSB of the display file address for the spot in
                          ; the inventory for the first captive mouse
CATCHMORF_7:
  LD A,C                  ; A=number of mice left to print
  AND A                   ; Set the zero flag if there are none left
  LD B,8                  ; There are 8 bytes in the mouse UDG/inventory slot
  LD H,80                 ; Set HL to the appropriate display file address
  JR NZ,CATCHMORF_9       ; Jump if we need to draw a mouse here
CATCHMORF_8:
  LD (HL),A               ; Otherwise blank out this spot in the mouse
  INC H                   ; inventory
  DJNZ CATCHMORF_8        ;
  JR CATCHMORF_11         ; Move to the next spot in the mouse inventory
CATCHMORF_9:
  LD DE,MOUSEUDG          ; The captured mouse UDG is at MOUSEUDG
  DEC C                   ; Decrease the captured mouse counter
CATCHMORF_10:
  LD A,(DE)               ; Display a captured mouse in the inventory
  LD (HL),A               ;
  INC H                   ;
  INC E                   ;
  DJNZ CATCHMORF_10       ;
CATCHMORF_11:
  INC L                   ; Point to the next spot in the mouse inventory
  BIT 4,L                 ; Have we drawn all 8 spots yet (L=176)?
  JR Z,CATCHMORF_7        ; Jump back to draw the next one if not
  LD H,210                ; 210=ERIC
  RET

; Unused
  DEFS 3

; Update the SRB for ERIC or his pellet and scroll the screen if necessary
;
; Used by the routine at WHEELBIKE. Updates the screen refresh buffer (SRB) for
; ERIC's new animatory state and location when he's riding the bike, or ERIC's
; pellet's new location as it flies through the air.
;
; A ERIC's/pellet's new animatory state
; D ERIC's/pellet's new y-coordinate
; E ERIC's/pellet's new x-coordinate
; H Character number: 210 (ERIC) or 214 (ERIC's pellet)
SRBSCROLL:
  CALL UPDATEAS           ; Update the animatory state and location of ERIC or
                          ; his pellet and update the SRB
  LD A,H                  ; A=character number
  CP 210                  ; Is this ERIC?
  RET NZ                  ; Return if not
  JP MVERIC2_1            ; Scroll the screen if necessary

; Make ERIC catch the frog (if possible)
;
; Used by the routine at CATCHMORF. Adds the frog to ERIC's inventory and makes
; a sound effect if the frog is sitting (i.e. not airborne) at the spot in
; front of ERIC.
;
; A Frog's animatory state (with bit 7 reset)
CATCHFROG:
  CP 28                   ; 28: Is the frog sitting down?
  RET NZ                  ; Return if not (ERIC cannot catch an airborne frog)
  LD HL,INVENTORY         ; INVENTORY holds the inventory flags
  SET 2,(HL)              ; Signal: got the frog
  LD H,212                ; 212=frog
  JP FROG2INV             ; Print the inventory and make a sound effect

; Print the inventory
;
; Used by the routines at THROW, JUMPING, FROGFALL, ERICSITLIE and CLOUD.
; Prints the safe key, Science Lab storeroom key, frog, water pistol and
; stinkbombs, depending on which of these items ERIC has in his possession.
; (The mouse inventory is handled separately at CATCHMORF_6.)
PRINTINV:
  LD HL,32746             ; L=234 (LSB of the display file address for the
                          ; first slot in the inventory: 20714)
  LD C,6                  ; Clear the six slots in the current on-screen
PRINTINV_0:
  LD H,80                 ; inventory
  LD B,8                  ;
PRINTINV_1:
  LD (HL),0               ;
  INC H                   ;
  DJNZ PRINTINV_1         ;
  INC L                   ;
  DEC C                   ;
  JR NZ,PRINTINV_0        ;
  LD DE,20714             ; DE=display file address for the first slot in the
                          ; inventory
  LD BC,SAFEKEYUDG        ; The safe key UDG is at SAFEKEYUDG
  LD HL,INVENTORY         ; INVENTORY holds the inventory flags
  LD A,6                  ; INK 6
  BIT 0,(HL)              ; Has ERIC got the safe key?
  CALL NZ,PRINTITEM       ; Print it if so
  INC B                   ; BC=SLSKEYUDG: Science Lab storeroom key UDG
  LD A,7                  ; INK 7
  BIT 1,(HL)              ; Has ERIC got the Science Lab storeroom key?
  CALL NZ,PRINTITEM       ; Print it if so
  LD A,4                  ; INK 4
  INC B                   ; BC=FROGUDG: frog UDG
  BIT 2,(HL)              ; Has ERIC got the frog?
  CALL NZ,PRINTITEM       ; Print it if so
  INC B                   ; BC=WPLEFTUDG: water pistol UDG (left half)
  BIT 3,(HL)              ; Has ERIC got the water pistol?
  JR Z,PRINTINV_3         ; Jump if not
  LD A,5                  ; INK 5 for the water-filled pistol
  BIT 4,(HL)              ; Has ERIC got the water pistol with sherry in it?
  JR Z,PRINTINV_2         ; Jump if not
  LD A,67                 ; INK 3: BRIGHT 1 for the sherry-filled pistol
PRINTINV_2:
  PUSH AF
  CALL PRINTITEM          ; Print the left half of the water pistol
  POP AF
  INC B                   ; BC=WPRIGHTUDG: water pistol UDG (right half)
  CALL PRINTITEM          ; Print the right half of the water pistol
  DEC B
PRINTINV_3:
  INC B
  INC B                   ; BC=STINKB3UDG: 3 stinkbombs UDG
  LD A,7                  ; INK 7
  BIT 5,(HL)              ; Has ERIC got 3 stinkbombs?
  JR NZ,PRINTINV_4        ; Jump if so
  INC B                   ; BC=STINKB2UDG: 2 stinkbombs UDG
  BIT 6,(HL)              ; Has ERIC got 2 stinkbombs?
  JR NZ,PRINTINV_4        ; Jump if so
  BIT 7,(HL)              ; Has ERIC got 1 stinkbomb?
  JR Z,PRINTINV_5         ; Jump if not
  INC B                   ; BC=STINKB1UDG: 1 stinkbomb UDG
PRINTINV_4:
  CALL PRINTITEM          ; Print the stinkbombs
PRINTINV_5:
  LD H,210                ; 210=ERIC
  RET

; Unused
  DEFB 0

; Print an inventory item
;
; Used by the routine at PRINTINV. Prints the safe key, Science Lab storeroom
; key, frog, left or right half of the water pistol, or stinkbombs in one of
; the inventory slots.
;
; A Attribute byte
; BC UDG address
; DE Display file address
PRINTITEM:
  LD D,90                 ; Set the attribute byte
  LD (DE),A               ;
  LD D,80                 ; Point DE back at the display file
PRINTITEM_0:
  LD A,(BC)               ; Copy the UDG onto the screen
  INC C                   ;
  LD (DE),A               ;
  INC D                   ;
  BIT 3,D                 ;
  JR Z,PRINTITEM_0        ;
  LD C,232                ; Reset C
  INC E                   ; Move DE to the next slot in the on-screen inventory
  RET

; Collect the bit mask and game status buffer address for an event identifier
;
; Used by the routine at CHECKSIG, and also by the unused routines at XSIGRAISE
; and SIGLOWER. The event identifier in A (0, 7-12), taken modulo 8,
; corresponds to a bit in one of the bytes at LFLAGS (A<=7) or LSIGS (A>=8). On
; exit, HL points to the appropriate byte and A holds the bit mask (single bit
; set), as follows:
;
; +-------+------+------------------------------------------------------------+
; | Entry | Exit | Event                                                      |
; +-------+------+------------------------------------------------------------+
; | 0     | 1    | End of lesson                                              |
; | 7     | 128  | Time to sit down for assembly                              |
; | 8     | 1    | Teacher has arrived at the top-floor room in the girls'    |
; |       |      | skool                                                      |
; | 9     | 2    | Teacher has arrived at the middle-floor room in the girls' |
; |       |      | skool                                                      |
; | 10    | 4    | Teacher has arrived at the Blue Room                       |
; | 11    | 8    | Teacher has arrived at the Yellow Room                     |
; | 12    | 16   | Teacher has arrived at the Science Lab                     |
; +-------+------+------------------------------------------------------------+
;
; A Event ID from a command list
GETFLAGPTR:
  AND A                   ; Set the carry flag if the event identifier is odd
  RRA                     ;
  LD E,A                  ; Copy bits 1-3 of the event ID into bits 0-2 of E
  LD A,0                  ; A=1 if the event ID is an even number, 2 if it's an
  ADC A,A                 ; odd number
  INC A                   ;
  RR E                    ; Now bits 2 and 3 of the event ID are in bits 0 and
                          ; 1 of E
  JR NC,GETFLAGPTR_0      ; Jump if the event ID is 0, 8, 9 or 12 (bit 1 reset)
  ADD A,A                 ; A=4 (event ID = 10) or 8 (event ID = 7 or 11)
  ADD A,A                 ;
GETFLAGPTR_0:
  SCF
  RR E                    ; E=128 (event ID <= 7) or 129
  JR NC,GETFLAGPTR_1      ; Jump if the event ID is 0, 8, 9, 10 or 11 (bit 2
                          ; reset)
  ADD A,A                 ; A=16 (event ID = 12) or 128 (event ID = 7)
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
GETFLAGPTR_1:
  LD D,127                ; HL=LFLAGS or LSIGS
  EX DE,HL                ;
  RET

; Check whether a signal has been raised
;
; Used by the routines at WALKCHECK, BWWRITE and RSTTELLSIT. Returns with the
; zero flag reset if the signal has been raised.
;
; A Event identifier
; H Character number (183-209)
CHECKSIG:
  CALL GETFLAGPTR         ; Convert the event ID into an appropriate bit mask
  AND (HL)                ; Set the zero flag if the signal for this event
                          ; hasn't been raised (HL=LFLAGS or LSIGS)
  EX DE,HL                ; Restore the character number to H
  RET                     ; Return with the zero flag set if the signal hasn't
                          ; been raised

; Raise the signal for a certain event (unused)
;
; This routine is not used.
;
; H Character number (183-209)
XSIGRAISE:
  CALL GETPARAM           ; Collect the next byte (the event identifier) from
                          ; the command list and continue into the routine at
                          ; SIGRAISE

; Raise the signal for a certain event
;
; Used by the routine at RSTTELLSIT to raise the signal for the event
; identifier in A. In practice only event IDs 8-12 are used, which indicate to
; anyone listening that the teacher has arrived at the classroom door.
;
; A Event identifier (8-12) from the teacher's command list
; H Teacher's character number (201-204)
SIGRAISE:
  CALL GETFLAGPTR         ; Convert the event ID into an appropriate bit mask
                          ; (1, 2, 4, 8 or 16)
  OR (HL)                 ; Merge the existing event flags in LSIGS
; This entry point is used by the (unused) routine at SIGLOWER.
SIGRAISE_0:
  LD (HL),A               ; Set or reset the appropriate bit at LSIGS
  EX DE,HL                ; Restore the character number to H
  RET

; Unused
  DEFS 2

; Lower the signal for a certain event (unused)
;
; This routine is not used.
;
; H Character number (183-209)
SIGLOWER:
  CALL GETPARAM           ; Get the next byte (the event ID) from the command
                          ; list
  CALL GETFLAGPTR         ; Convert the event ID into an appropriate bit mask
  CPL                     ; Set all but the event bit
  AND (HL)                ; Merge the existing event flags, keeping the event
                          ; bit reset
  JR SIGRAISE_0           ; Reset the appropriate bit at LFLAGS or LSIGS

; Check whether a character should continue walking up and down
;
; Used by the routines at MVTILL and WALKABOUT after a character has reached a
; walkabout destination. It checks the signal for the event specified by byte 6
; of the character's buffer, and returns with the carry flag reset if the
; signal has been raised and the character has returned to the walkabout
; origin.
;
; H Character number (183-209)
WALKCHECK:
  LD L,6                  ; Pick up the event identifier in A
  LD A,(HL)               ;
  DEC L                   ; L=5
  CALL CHECKSIG           ; Has the signal been raised for this event?
  JR Z,WALKCHECK_2        ; Jump if not
; This entry point is used by the routine at WALKABOUT.
WALKCHECK_0:
  LD A,(HL)               ; A=x-coordinate of the location relative to which
                          ; the character is performing his walkabout (the
                          ; 'walkabout origin'), stored in byte 5
  LD L,1                  ; Byte 1 of the character's buffer holds his current
                          ; x-coordinate
  CP (HL)                 ; Is the character at the walkabout origin (and
                          ; therefore ready to respond to the signal)?
  JR NZ,WALKCHECK_1       ; Return him to the walkabout origin if not
  DEC L                   ; Point HL at byte 0 of the character's buffer
  BIT 0,(HL)              ; Is the character midstride?
  RET Z                   ; Return with the carry flag reset if not
WALKCHECK_1:
  SCF                     ; Signal: not ready to advance in the command list
                          ; yet
  LD L,11                 ; Fill in the new walkabout destination (either the
  LD (HL),A               ; origin or some location within 7 spaces to the left
                          ; of the origin)
  LD BC,WALK              ; Return with BC holding the address of the routine
  RET                     ; at WALK
; The time hasn't come or the event hasn't happened yet, so set the character
; off on another mini-jaunt.
WALKCHECK_2:
  CALL GETRANDOM          ; A=random number
  AND 7                   ; 0<=A<=7
  SUB 7                   ; -7<=A<=0
  ADD A,(HL)              ; Add the x-coordinate of the walkabout origin to
                          ; give the new walkabout destination
  JR WALKCHECK_1

; Make a character walk up and down until a certain time
;
; Used by command lists 0, 2, 4, 6, 8, 10, 12, 14, 16, 20, 26, 32, 34, 36, 38,
; 40, 46, 48, 50, 52, 72, 74, 76, 80, 82, 84, 86 and 88 to make a character
; walk up and down within 7 spaces to the left of a fixed location (the
; 'walkabout origin') until a specified time, e.g. the end of the lesson or
; when a teacher arrives at the doorway of the classroom.
;
; H Character number (183-209)
MVTILL:
  CALL GETPARAM           ; Get the next byte (the event identifier) from the
                          ; command list
  LD L,6                  ; Place it in byte 6 of the character's buffer
  LD (HL),A               ;
MVTILL_0:
  CALL WALKCHECK          ; Has the specified time arrived, and is the
                          ; character at the walkabout origin?
  JP NC,NEXTCMD           ; Move to the next command in the command list if so
  CALL CALLSUBCMD         ; Otherwise send the character off to another
                          ; walkabout destination via the routine at WALK, and
                          ; return to the instruction at MVTILL_1 (below) when
                          ; done
MVTILL_1:
  JR MVTILL_0

; Make a character walk up and down a few times or until a certain time
;
; Used by command lists 18, 42 and 44 to make a character walk about a fixed
; location until a specified time, or until a certain number of walkabouts have
; been performed.
;
; H Character number (183-209)
WALKABOUT:
  LD L,6                  ; Collect the event ID (always 0, denoting the end of
  CALL GETPARAMS          ; the lesson) and the maximum walkabout count from
                          ; the command list and copy them into bytes 6 and 7
                          ; of the character's buffer
WALKABOUT_0:
  LD L,7                  ; Decrease the walkabout counter at byte 7 of the
  DEC (HL)                ; character's buffer
  JR NZ,WALKABOUT_3       ; If the character has not yet performed the maximum
                          ; number of walkabouts, check whether the specified
                          ; time has arrived
  INC (HL)                ; Set the walkabout counter back to 1 to ensure that
                          ; we return to this execution path next time (to
                          ; check whether the character has returned to the
                          ; walkabout origin)
  LD L,5                  ; Byte 5 holds the x-coordinate of the walkabout
                          ; origin
  CALL WALKCHECK_0        ; Reset the carry flag if the character is at the
                          ; walkabout origin
WALKABOUT_1:
  JP NC,NEXTCMD           ; Move to the next command in the command list if the
                          ; specified time has arrived or the character has
                          ; completed all his walkabouts
  CALL CALLSUBCMD         ; Otherwise send the character off to another
                          ; walkabout destination via the routine at WALK, and
                          ; return to the instruction at WALKABOUT_2 (below)
                          ; when done
WALKABOUT_2:
  JR WALKABOUT_0
WALKABOUT_3:
  CALL WALKCHECK          ; Reset the carry flag if the specified time has
                          ; arrived and the character is at the walkabout
                          ; origin
  JR WALKABOUT_1

; Unused
  DEFS 2

; Get the identifier for ERIC's location
;
; Used by the routines at PRESENT, CHKERIC1 and ERICHIT. Returns with the carry
; flag set if ERIC's on the stage or a forbidden staircase (though this flag is
; ignored by the callers), and the location identifier (0-8) in A. See the
; routine at GETREGION for a description of the location identifiers.
ERICLOCID:
  CALL GETERICY           ; D=ERIC's y-coordinate (or the y-coordinate of the
                          ; floor he's closest to if his feet are not on the
                          ; floor)
  LD BC,47419             ; B=185; C=59
  CALL GETREGION_0        ; Collect the identifier for ERIC's location (0-8) in
                          ; A
  RET NC                  ; Return if ERIC's on the top, middle or bottom floor
; ERIC's not on the top, middle or bottom floor. He must be on a staircase or
; the assembly hall stage.
  LD A,E                  ; A=ERIC's x-coordinate
  SUB 48                  ; Return with A=0 (forbidden zone) and the carry flag
  CP 192                  ; set if E>=48 (ERIC must be on the stage, or a
  SBC A,A                 ; staircase he's not allowed on)
  INC A                   ;
  SCF                     ;
  RET Z                   ;
  ADD A,A                 ; Otherwise return with A=2 and the carry flag reset
  RET                     ; (ERIC's on one of the staircases at the far left of
                          ; the boys' skool)

; Check whether ERIC is where he should be
;
; Used by the routines at INCLASS, SEEKERIC, DINDUTY, ASSEMDUTY and DOCLASS.
; Returns with the zero flag set if and only if ERIC is where he should be
; during dinner, assembly, or class.
PRESENT:
  CALL ERICLOCID          ; A=location identifier for ERIC
  LD C,A                  ; Copy this to C
  LD A,(LESSONDESC)       ; LESSONDESC holds the current lesson descriptor
  AND 15                  ; Bits 0-3 tell us which room or area ERIC should be
                          ; in
  CP C                    ; Is ERIC in that room or area?
  RET                     ; Return with the zero flag set if so

; Make any nearby teacher give ERIC lines if necessary (1)
;
; Used by the routine at MAINLOOP2. Makes any nearby teacher give ERIC lines
; for rule breakages other than hitting, firing a catapult, firing a
; waterpistol, writing on a blackboard, or dropping a stinkbomb.
CHKERIC1:
  LD HL,LINESDELAY1       ; Decrement the LSB of the lines-giving delay counter
  DEC (HL)                ; at LINESDELAY1
  RET NZ                  ; Return unless it's time to inspect the MSB
  LD (HL),15              ; Reset the LSB to 15
  INC L                   ; HL=LINESDELAY2 (MSB of the lines-giving delay
                          ; counter)
  INC (HL)                ; Has the lines-giving delay counter reached 0?
  DEC (HL)                ;
  JR Z,CHKERIC1_0         ; Jump if so
  DEC (HL)                ; Otherwise decrement the MSB of the counter
  LD A,(HL)               ; Pick up the MSB in A
  CP 6                    ; Has enough time passed since the last time ERIC was
                          ; given lines?
  RET NC                  ; Return if not
; The MSB of the lines-giving delay counter (which is reset to 10 by the
; routine at CHKERIC2 when ERIC is given lines) is now 5 or less. This means
; the minimum interval between two lines-givings (75 passes through the main
; loop) has elapsed. However, the teacher who gave ERIC lines last time must
; wait until the MSB reaches zero before he can give ERIC lines again.
CHKERIC1_0:
  CALL ERICLOCID          ; A=location identifier for ERIC
  JR CHKERIC2             ; Skip over the routine at STALKERIC

; Make MR WACKER find the truant ERIC
;
; Used by the command list at CLGETERIC, the address of which is placed into
; bytes 27 and 28 of MR WACKER's buffer by the routine at WATCHERIC.
;
; H 200 (MR WACKER)
STALKERIC:
  LD BC,FINDTRUANT        ; Place the address of the interruptible subcommand
  JP DOSUBCMD             ; routine at FINDTRUANT into bytes 9 and 10 of MR
                          ; WACKER's buffer and jump to it

; Unused
  DEFS 2

; Make any nearby teacher give ERIC lines if necessary (2)
;
; Continues from CHKERIC1. On entry, A contains a location identifier for ERIC:
;
; +----+-------------------------------------------------------+
; | ID | Region                                                |
; +----+-------------------------------------------------------+
; | 0  | None of the places below (ERIC is never allowed here) |
; | 1  | Playground                                            |
; | 2  | Various places in the boys' skool outside classrooms  |
; | 3  | Assembly hall                                         |
; | 4  | Dining hall                                           |
; | 5  | Revision Library, just outside the Yellow Room door   |
; | 6  | Science Lab                                           |
; | 7  | Blue Room                                             |
; | 8  | Yellow Room                                           |
; +----+-------------------------------------------------------+
;
; A Location identifier for ERIC (0-8)
CHKERIC2:
  AND A                   ; Is ERIC in a forbidden zone?
  JP Z,CHKERIC2_10        ; Jump with A=0 (0+62=62: YOU ARE NOT ALLOWED HERE)
                          ; if so
; ERIC's not in a forbidden zone. Check whether he's standing on a plant, or
; riding the bike in the boys' skool.
  LD B,A                  ; B=ERIC's location identifier (1-8)
  LD L,251                ; HL=STATUS (ERIC's status flags)
  BIT 1,(HL)              ; Is ERIC on the bike, on a plant, or falling?
  JR Z,CHKERIC2_1         ; Jump if not
; ERIC is on the bike, on a plant, or falling.
  LD L,237                ; HL=STATUS2 (ERIC's other status flags)
  LD A,(HL)               ; Pick these up in A
  AND 131                 ; Check bits 0, 1 and 7
  JR Z,CHKERIC2_1         ; Jump if ERIC's falling
  AND 2                   ; A=2 (2+62=64: GET OFF THE PLANTS) if ERIC's
                          ; standing on a plant or plant pot
CHKERIC2_0:
  JP NZ,CHKERIC2_10       ; Jump if ERIC is (a) standing on a plant or plant
                          ; pot, or (b) riding or standing on the saddle of the
                          ; bike inside the boys' skool
  DEC B                   ; B=0 now if ERIC's in the playground
  LD A,3                  ; 3+62=65: DON'T RIDE BIKES IN HERE
  JR NZ,CHKERIC2_0        ; Jump if ERIC's riding the bike in the boys' skool
  INC B                   ; B=ERIC's location identifier (1-8)
; Now to check whether ERIC's in the room or area he's supposed to be in this
; period.
CHKERIC2_1:
  LD L,224                ; HL=LESSONDESC (lesson descriptor)
  LD A,(HL)               ; Pick this up in A
  AND 15                  ; Keep only the 'room' bits (bits 0-3)
  LD C,A                  ; Copy the room bits to C
; Now the value in C indicates where this lesson takes place:
;
; +---+------------------+
; | C | Lesson/location  |
; +---+------------------+
; | 2 | Playtime         |
; | 3 | Assembly         |
; | 4 | Dinner           |
; | 5 | Revision Library |
; | 6 | Science Lab      |
; | 7 | Blue Room        |
; | 8 | Yellow Room      |
; +---+------------------+
  LD L,128                ; HL=LFLAGS (various game status flags)
  BIT 6,(HL)              ; Should ERIC be in class, assembly or the dinner
                          ; hall by now?
  JR NZ,CHKERIC2_5        ; Jump if so
  LD L,228                ; Pick up the MSB of the lesson clock
  LD A,(HL)               ;
  CP 12                   ; Should ERIC by now have left the classroom he was
                          ; in last period?
  JR C,CHKERIC2_3         ; Jump if so
; It's too soon to be telling ERIC to GET ALONG NOW. Check whether he's sitting
; on the floor anywhere outside the assembly hall.
  LD L,251                ; HL=STATUS (ERIC's status flags)
  BIT 2,(HL)              ; Is ERIC sitting or lying down?
  RET Z                   ; Return if not
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  CP 4                    ; 4: Is ERIC sitting in a chair?
  RET Z                   ; Return if so
  CP 133                  ; 133: ERIC sitting on the floor facing right
CHKERIC2_2:
  LD A,4                  ; 4+62=66: GET OFF THE FLOOR
  JP NZ,CHKERIC2_10       ; Give lines if ERIC's sitting on the floor facing
                          ; left, or lying down
  LD A,B                  ; A=ERIC's location identifier (1-8)
  CP 3                    ; ERIC is sitting on the floor facing right; is he in
                          ; the assembly hall?
  JR NZ,CHKERIC2_2        ; Give lines if not
  RET
; By now, ERIC should have left the classroom he was in last period.
CHKERIC2_3:
  LD A,5                  ; Location IDs 6-8 correspond to the boys' skool
                          ; classrooms
  CP B                    ; Is ERIC in the Blue Room, Yellow Room or Science
                          ; Lab?
  JR C,CHKERIC2_5         ; Jump if so
  LD A,2                  ; Is it PLAYTIME?
  CP C                    ;
  JR Z,CHKERIC2_4         ; Jump if so
  DEC A                   ; A=1
  CP B                    ; B=1 if ERIC's in the playground
  LD A,5                  ; 5+62=67: GET BACK TO SCHOOL
  JR Z,CHKERIC2_10        ; Give lines if ERIC is in the playground
  LD A,C                  ; Is it ASSEMBLY?
  CP 3                    ;
  JR NZ,CHKERIC2_4        ; Jump if not
  CP B                    ; Is ERIC in the assembly hall?
  JR Z,CHKERIC2_6         ; Jump if so
CHKERIC2_4:
  LD L,251                ; HL=STATUS (ERIC's status flags)
  BIT 2,(HL)              ; Is ERIC sitting or lying down?
  RET Z                   ; Return if not
  LD A,4                  ; 4+62=66: GET OFF THE FLOOR
  JR CHKERIC2_10          ; Give lines
; ERIC should be in class, assembly or the dinner hall by now.
CHKERIC2_5:
  LD A,B                  ; A=ERIC's location identifier (1-8)
  CP C                    ; Compare that with where he should be
  LD A,6                  ; 6+62=68: GET ALONG NOW
  JR NZ,CHKERIC2_10       ; Give lines if ERIC's not where he should be
  DEC A                   ; A=5
  CP C                    ; C > 5 if this lesson is in a classroom
  JR C,CHKERIC2_8         ; Jump if this lesson is in a classroom
  LD A,C                  ; Is it ASSEMBLY?
  CP 3                    ;
  JR NZ,CHKERIC2_4        ; Jump if not
CHKERIC2_6:
  LD L,128                ; HL=LFLAGS (various game status flags)
  BIT 7,(HL)              ; Bit 7 is set if ERIC should be sitting down now
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  JR NZ,CHKERIC2_7        ; Jump if ERIC should be sitting facing the stage now
  CP 133                  ; 133: Is ERIC sitting on the floor facing right?
  JR NZ,CHKERIC2_4        ; Jump if not
  RET
; It's assembly, ERIC is in the assembly hall, and he should be sitting down
; facing the stage now. Check whether he is.
CHKERIC2_7:
  CP 133                  ; 133: Is ERIC sitting on the floor facing right?
  RET Z                   ; Return if so
  LD L,251                ; HL=STATUS (ERIC's status flags)
  LD A,7                  ; 7+62=69: SIT FACING THE STAGE
  BIT 2,(HL)              ; Is ERIC sitting (facing left) or lying down?
  JR Z,CHKERIC2_10        ; Tell ERIC to sit facing the stage if not (this is a
                          ; bug)
  JR CHKERIC2_9           ; Otherwise tell ERIC to sit down
; This lesson takes place in a classroom, and ERIC is present. Check whether
; ERIC is sitting in a chair.
CHKERIC2_8:
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  CP 4                    ; 4: Is ERIC sitting in a chair?
  RET Z                   ; Return if so
  LD L,251                ; HL=STATUS (ERIC's status flags)
  BIT 2,(HL)              ; Bit 2 is set if ERIC's sitting or lying on the
                          ; floor
  LD A,4                  ; 4+62=66: GET OFF THE FLOOR
  JR NZ,CHKERIC2_10       ; Give lines if ERIC's sitting or lying on the floor
  LD L,128                ; HL=LFLAGS (various game status flags)
  BIT 7,(HL)              ; Should ERIC be seated by now (bit 7 is set by
                          ; DOCLASS)?
  RET Z                   ; Return if not
CHKERIC2_9:
  LD A,8                  ; 8+62=70: NOW SIT DOWN
  NOP
  NOP
; At this point, A holds the lines reprimand ID.
CHKERIC2_10:
  AND A                   ; Is ERIC in a forbidden zone?
  JR NZ,CHKERIC2_13       ; Jump if not
; ERIC is somewhere he's never allowed to be.
  LD L,128                ; HL=LFLAGS (various game status flags)
  BIT 5,(HL)              ; Is MISS TAKE chasing ERIC?
  JR NZ,CHKERIC2_12       ; Jump if so
  LD A,(LESSONDESC)       ; A=lesson descriptor
  CP 2                    ; Is it PLAYTIME?
  JR Z,CHKERIC2_12        ; Jump if so
  LD A,E                  ; A=ERIC's x-coordinate
  CP 160                  ; Is ERIC in the girls' skool?
  JR C,CHKERIC2_12        ; Jump if not
; So it's not playtime, ERIC is in the girls' skool, and MISS TAKE is not (yet)
; chasing him out. Should she start?
  LD HL,52226             ; Point HL at byte 2 of MISS TAKE's buffer
  LD A,(HL)               ; A=MISS TAKE's y-coordinate
  CP D                    ; Are ERIC and MISS TAKE on the same floor?
  JR NZ,CHKERIC2_11       ; Jump if not
  LD L,29                 ; Set bit 0 of byte 29 of MISS TAKE's character
  SET 0,(HL)              ; buffer, triggering a restart of her command list
                          ; (set to CLHERDERIC below)
  DEC L                   ; Place the address of the command list at CLHERDERIC
  LD (HL),126             ; into bytes 27 and 28 of MISS TAKE's buffer; this
  DEC L                   ; command list contains a single routine address:
  LD (HL),115             ; HERDERIC1
  LD HL,LFLAGS            ; LFLAGS holds various game status flags
  SET 5,(HL)              ; Signal: MISS TAKE is chasing ERIC
CHKERIC2_11:
  LD H,127                ; Point HL at the game status buffer page
CHKERIC2_12:
  XOR A                   ; 0+62=62: YOU ARE NOT ALLOWED HERE
; Check how long it's been since ERIC last got lines.
CHKERIC2_13:
  LD L,247                ; HL=LINESDELAY2 (MSB of the lines-giving delay
                          ; counter)
  DEC (HL)                ; Reset the zero flag if it's too soon for the
  INC (HL)                ; teacher who gave ERIC lines last time to give him
                          ; lines again
; This entry point is used by the routine at CLOUD with the zero flag set
; (meaning any teacher may give ERIC lines) and A holding a lines reprimand ID.
CHKERIC2_14:
  PUSH AF                 ; Save the lines reprimand ID and the zero flag
  NOP
  NOP
  JR Z,CHKERIC2_15        ; Jump if any teacher (including the one who gave him
                          ; lines last time) can give ERIC lines now
; It's too soon for the teacher who gave ERIC lines last time to give him lines
; again. Remove that teacher from lines-giving range temporarily.
  LD L,245                ; Collect from LASTLINES into H the character number
  LD H,(HL)               ; of the teacher who last gave ERIC lines
  LD L,1                  ; A=this teacher's x-coordinate
  LD A,(HL)               ;
  LD (HL),224             ; Set this teacher's x-coordinate to 224 temporarily
                          ; to take him out of lines-giving range
  LD L,A                  ; Save the teacher's real x-coordinate in L
; Now to find out which teacher (if any) should give ERIC lines.
CHKERIC2_15:
  PUSH HL
  CALL WITNESS            ; Find the first adult within lines-giving range of
                          ; ERIC
  POP HL
  LD D,A                  ; D=character number of this adult, or 0 if there are
                          ; none in range
  POP AF
  LD B,A                  ; B=lines reprimand ID
  JR Z,CHKERIC2_16        ; Jump unless we need to restore the x-coordinate of
                          ; the teacher who last gave ERIC lines
  LD A,L                  ; Restore the x-coordinate of the teacher who last
  LD L,1                  ; gave ERIC lines
  LD (HL),A               ;
CHKERIC2_16:
  LD A,D                  ; A=character number of the adult who can see ERIC,
                          ; or 0 if there are no adults in range
  CP 205                  ; Is it ALBERT (and therefore none of the teachers)?
  RET Z                   ; Return if so (ALBERT doesn't give lines)
  AND A                   ; Can any adult character see ERIC?
  RET Z                   ; Return if not
; A teacher is within lines-giving range.
  LD HL,LFLAGS            ; LFLAGS holds various game status flags
  BIT 1,(HL)              ; Is MR WACKER chasing ERIC to expel him?
  RET NZ                  ; Return if so
  LD L,245                ; Store the character number of the lines-giver at
  LD (HL),A               ; LASTLINES
  LD L,247                ; Reset the MSB of the lines-giving delay counter at
  LD (HL),10              ; LINESDELAY2 to 10
  LD A,B                  ; A=lines reprimand ID
  CP 6                    ; 6+62=68: GET ALONG NOW
  JR NZ,CHKERIC2_17       ; Jump unless ERIC is late for class, dinner or
                          ; assembly
; ERIC is late for class, dinner or assembly. The appropriate lines reprimand
; depends on who saw him: his teacher for this period, or some other teacher.
  LD A,(LESSONDESC)       ; Collect the lesson descriptor from LESSONDESC
  AND 240                 ; Keep only the bits relating to ERIC's teacher and
  RLCA                    ; shift them into bits 0-3
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  ADD A,199               ; Now A=character number of ERIC's teacher (200-203)
  CP D                    ; Was it that teacher who saw ERIC?
  JR NZ,CHKERIC2_17       ; Jump if not
; ERIC's teacher for this period is the lines-giver. His lines reprimand
; alternates between 'COME ALONG YOU MONSTER' and 'DON'T KEEP ME WAITING'.
  LD L,128                ; HL=LFLAGS (various game status flags)
  LD A,16                 ; Set bit 4 in A
  LD B,9                  ; 9+62=71: COME ALONG YOU MONSTER
  XOR (HL)                ; Flip bit 4 in LFLAGS, toggling between 71 and 72
  LD (HL),A               ;
  BIT 4,(HL)              ; Should ERIC's teacher say 'COME ALONG YOU MONSTER'
                          ; this time (bit 4 set)?
  JR NZ,CHKERIC2_17       ; Jump if so
  INC B                   ; 10+62=72: DON'T KEEP ME WAITING
; At this point D holds the character number of the teacher who is going to
; give ERIC lines, and B holds the lines reprimand ID.
CHKERIC2_17:
  EX DE,HL                ; Now H=character number of the lines-giver
  LD A,B                  ; B=lines reprimand ID
  ADD A,62                ;
  LD B,A                  ;
  LD A,210                ; 210=ERIC (the lines recipient)
  JP GIVELINES            ; Make the teacher give lines to ERIC

; Command list used to make MISS TAKE chase ERIC
;
; Used by the routine at CHKERIC2.
CLHERDERIC:
  DEFW HERDERIC1          ; Make MISS TAKE chase ERIC

; Make MISS TAKE chase ERIC (1)
;
; The address of this routine is found in the command list at CLHERDERIC, the
; address of which is placed into bytes 27 and 28 of MISS TAKE's buffer by the
; routine at CHKERIC2.
HERDERIC1:
  LD BC,HERDERIC2         ; Place the address of the interruptible subcommand
  JP DOSUBCMD             ; routine at HERDERIC2 (chase ERIC) into bytes 9 and
                          ; 10 of MISS TAKE's buffer and jump to it

; Make MISS TAKE chase ERIC (2)
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of MISS TAKE's buffer by the routine at HERDERIC1. It makes MISS TAKE
; stalk ERIC while he's in the girls' skool or the girls' playground, but stand
; on guard by the skool gate if ERIC is on the boys' side.
;
; H 204 (MISS TAKE)
HERDERIC2:
  LD L,29                 ; Point HL at byte 29 of MISS TAKE's buffer
  LD A,205                ; 205=ALBERT
  SET 7,(HL)              ; Set bit 7 at byte 29 of MISS TAKE's buffer, making
                          ; her run
  LD (LASTLINES),A        ; Set the character number of the adult who last gave
                          ; ERIC lines (stored at LASTLINES) to 205 (ALBERT);
                          ; this has the effect of making any teacher
                          ; (including whoever last gave ERIC lines) available
                          ; for lines-giving ASAP (see CHKERIC1 and CHKERIC2)
  LD L,0                  ; Point HL at byte 0 of MISS TAKE's character buffer
  BIT 0,(HL)              ; Is MISS TAKE midstride?
  JP NZ,FINDERIC_0        ; Finish the stride if so
; Now check whether a command list restart has been requested. This request
; will have been made if the lesson in which MISS TAKE started chasing ERIC has
; ended, and the next lesson has just begun (see NEWLESSON1).
  LD L,29                 ; Set the zero flag if MISS TAKE should continue
  BIT 0,(HL)              ; hounding ERIC
; This entry point is used by the routines at SEEKERIC and FINDTRUANT with the
; zero flag reset and H holding the character number of a teacher chasing ERIC.
HERDERIC2_0:
  LD L,1                  ; Collect the teacher's coordinates in DE
  LD E,(HL)               ;
  INC HL                  ;
  LD D,(HL)               ;
  JR Z,HERDERIC2_1        ; Jump if this teacher is MISS TAKE and she should
                          ; continue chasing ERIC
  CALL GETREGION          ; Is the teacher on a staircase?
  JP NC,RMSUBCMD          ; If not, terminate this interruptible subcommand
; The chase is over, and the teacher is on a staircase. It's not a good idea to
; remove the interruptible subcommand routine address from bytes 9 and 10 of
; the teacher's buffer at this point, though, because the next command in the
; teacher's command list will be GOTO, and that command will not work if the
; teacher's starting point is on a staircase. So continue chasing ERIC until
; the top or bottom of the staircase has been reached.
  JP FINDERIC             ; Continue chasing ERIC
  NOP                     ;
  NOP                     ;
  NOP                     ;
; MISS TAKE is chasing ERIC, but she only chases so far.
HERDERIC2_1:
  LD A,(53761)            ; A=ERIC's x-coordinate
  CP 138                  ; This x-coordinate is just to the right of the plant
                          ; in the girls' playground
  JR NC,HERDERIC2_2       ; Jump if ERIC's to the right of this
  LD A,E                  ; A=MISS TAKE's x-coordinate
  CP 137                  ; This is the x-coordinate where MISS TAKE stands on
                          ; guard for the truant ERIC
  RET Z                   ; Return if MISS TAKE is already here
HERDERIC2_2:
  JP FINDERIC             ; Otherwise MISS TAKE continues chasing ERIC

; Unused
  DEFB 0

; Print the lesson and ring the bell
;
; Used by the routine at NEWLESSON1. Prints the lesson or 'DEMO.MODE' in the
; lesson box, and makes the bell sound effect.
NEWLESSON2:
  LD HL,GAMEMODE          ; GAMEMODE holds the current game mode
  LD A,(HL)               ; A=255 if we're in demo mode, 0 otherwise
  INC A                   ; Set the zero flag if we're in demo mode
  LD A,63                 ; Message 63: DEMO.MODE
  JR Z,NEWLESSON2_1       ; Jump if we're in demo mode
; A real game is in progress. First, figure out what to print in the bottom row
; of the lesson box (the room name, 'LIBRARY', 'PLAYTIME', 'ASSEMBLY' or
; 'DINNER').
  LD L,224                ; HL=LESSONDESC (lesson descriptor)
  LD A,(HL)               ; Pick this up in A
  AND 15                  ; Keep only the room identifier bits
  LD E,32                 ; 32 is the ASCII code for SPACE
  ADD A,126               ; D=message number for the text to go on the bottom
  LD D,A                  ; row of the lesson box
  CP 131                  ; Is it PLAYTIME (128), ASSEMBLY (129) or DINNER
                          ; (130)?
  JR C,NEWLESSON2_0       ; Jump if so
  LD E,135                ; Message 135: REVISION
  JR Z,NEWLESSON2_0       ; Jump if it's REVISION LIBRARY (D=131: LIBRARY)
; Next, figure out what to print (if anything) in the top row of the lesson box
; (the teacher's name, or 'REVISION').
  LD A,(HL)               ; A=lesson descriptor
  AND 240                 ; Keep only the teacher-identifying bits and push
  RLCA                    ; them into bits 0-3
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  ADD A,20                ; E=22, 23 or 24 (teacher's name)
  LD E,A                  ;
; Now we can construct message 16 (teacher/room) ready for printing.
NEWLESSON2_0:
  LD L,136                ; Place the teacher message number (or ASCII code for
  LD (HL),E               ; SPACE) in MSG006
  LD L,138                ; Place the room message number in MSG005
  LD (HL),D               ;
  LD A,16                 ; Message 16: teacher/room
; This entry point is used by the routine at JUMPING.
NEWLESSON2_1:
  LD DE,23216             ; DE=base display file address for the lesson box
  LD C,87                 ; INK 7: PAPER 2: BRIGHT 1
  CALL PRTMSGBOX          ; Print the lesson
  LD BC,32784             ; Set the parameters for the bell sound effect
  LD D,C                  ;
  LD A,1                  ;
  JP SNDEFFECT            ; Ring the bell

; Unused
  DEFB 120,120

; Make BOY WANDER write on a blackboard
;
; Used by BOY WANDER's command lists 32, 46, 50, 54 and 56. Makes BOY WANDER
; write on the blackboard he's standing next to unless a certain event has
; already happened or the board is dirty.
;
; H 206 (BOY WANDER)
BWWRITE:
  CALL GETPARAM           ; Collect the event identifier parameter from the
                          ; command list
  CALL CHECKSIG           ; Check whether the event (the arrival of a teacher
                          ; or the end of the lesson) has happened
  LD BC,WRITEBRD          ; Redirect control to the routine at WRITEBRD (write
  CALL Z,CALLSUBCMD       ; on board) if the event has not happened, then
                          ; return to BWWRITE_0 (below)
BWWRITE_0:
  JP NEXTCMD              ; Move to the next command in the command list

  ORG 32512

; Screen refresh buffer (SRB)
;
; Used by the routines at UPDATEAS, UPDATESCR, SRBXY, BUBBLESRB and RMBUBBLE.
; Each byte of the SRB corresponds to a segment of 8 character squares on the
; screen, the leftmost of which will be at x=0, x=8, x=16 or x=24. Each bit set
; in an SRB byte corresponds to a character square in the segment that needs
; refreshing. Bit 7 corresponds to the leftmost character square in the
; segment, bit 0 to the rightmost character square.
SRB:
  DEFS 84

; Buffer for the Blue Room blackboard
;
; Used by the routines at WRITE, WRITING, WRITECHR, WIPE and WRITEBRD.
BRBRDBUF:
  DEFB 0                  ; Next clean pixel column (0-127)
  DEFB 0                  ; Number of the character who last wrote on the board
  DEFS 4                  ; Characters written by ERIC

; Buffer for the Yellow Room blackboard
;
; Used by the routines at WRITE, WRITING, WRITECHR, WIPE and WRITEBRD.
YRBRDBUF:
  DEFB 0                  ; Next clean pixel column (0-127)
  DEFB 0                  ; Number of the character who last wrote on the board
  DEFS 4                  ; Characters written by ERIC

; Buffer for the blackboard in the top-floor classroom of the girls' skool
;
; Used by the routines at WRITE, WRITING, WRITECHR, WIPE and WRITEBRD.
TFBRDBUF:
  DEFB 0                  ; Next clean pixel column (0-127)
  DEFB 0                  ; Number of the character who last wrote on the board
  DEFS 4                  ; Characters written by ERIC

; Buffer for the Science Lab blackboard
;
; Used by the routines at WRITE, WRITING, WRITECHR, WIPE and WRITEBRD.
SLBRDBUF:
  DEFB 0                  ; Next clean pixel column (0-127)
  DEFB 0                  ; Number of the character who last wrote on the board
  DEFS 4                  ; Characters written by ERIC

; Buffer for the blackboard in the middle-floor classroom of the girls' skool
;
; Used by the routines at WRITE, WRITING, WRITECHR, WIPE and WRITEBRD.
MFBRDBUF:
  DEFB 0                  ; Next clean pixel column (0-127)
  DEFB 0                  ; Number of the character who last wrote on the board
  DEFS 4                  ; Characters written by ERIC

; Unused
  DEFS 14

; Various flags
;
; Various status flags for the current lesson.
;
; +-----+---------------------------------------------------------------------+
; | Bit | Meaning if set                                                      |
; +-----+---------------------------------------------------------------------+
; | 0   | End of lesson/playtime (always reset)                               |
; | 1   | MR WACKER is looking for ERIC to expel him (see EXPEL)              |
; | 2   | MR WACKER is looking for the truant ERIC (see WATCHERIC)            |
; | 3   | EINSTEIN is talking (set and checked by SWOTSPK; reset and checked  |
; |     | by GRASSETC)                                                        |
; | 4   | Lines reprimand toggle for the teacher fetching ERIC (see CHKERIC2) |
; | 5   | MISS TAKE is chasing ERIC (see CHKERIC2)                            |
; | 6   | ERIC should be in class (set by DOCLASS), dinner (set by DINDUTY)   |
; |     | or assembly (set by ASSEMDUTY) (checked by CHKERIC2)                |
; | 7   | Kids should be sitting in the assembly hall (set by ASSEMDUTY;      |
; |     | reset by DETENTION; checked by INASSEMBLY and CHKERIC2, and by      |
; |     | command lists 20, 40 and 88)                                        |
; |     | ERIC's teacher's next absence reprimand should be STAY TILL I       |
; |     | DISMISS YOU instead of DON'T BE LATE AGAIN (set and checked by      |
; |     | DOCLASS)                                                            |
; |     | ERIC should be sitting down in class (set by DOCLASS; checked by    |
; |     | CHKERIC2)                                                           |
; +-----+---------------------------------------------------------------------+
LFLAGS:
  DEFB 0

; Lesson signal flags
;
; Used by the classroom-based command lists to indicate that or check whether a
; teacher has arrived at the classroom door to start the lesson.
;
; +--------+-----------------------------------------------------------------+
; | Bit(s) | Meaning if set                                                  |
; +--------+-----------------------------------------------------------------+
; | 0      | Teacher has arrived at the top-floor classroom in the girls'    |
; |        | skool                                                           |
; | 1      | Teacher has arrived at the middle-floor classroom in the girls' |
; |        | skool                                                           |
; | 2      | Teacher has arrived at the Blue Room                            |
; | 3      | Teacher has arrived at the Yellow Room                          |
; | 4      | Teacher has arrived at the Science Lab                          |
; | 5-7    | Unused                                                          |
; +--------+-----------------------------------------------------------------+
LSIGS:
  DEFB 0

; Unused
GSBUNUSED:
  DEFS 6

; Message 6: '{teacher}'
;
; Used by the routine at NEWLESSON2 when printing the top line of text in the
; lesson box; it is a submessage of message 16.
MSG006:
  DEFB 0                  ; The teacher message number (22, 23, 24) or 32 (' ':
                          ; space) goes here
  DEFB 0                  ; End marker

; Message 5: '{lines recipient}/combination number or letter/{room}'
;
; Used by the routines at GIVELINES (as a submessage of message 13), OBJFALL
; (as a submessage of message 14) and NEWLESSON2 (as a submessage of message
; 16).
MSG005:
  DEFB 0                  ; The message number of the lines recipient (27, 28,
                          ; 29, 30, 31), the combination letter/number, or the
                          ; room message number (128, 129, 130, 131, 132, 133,
                          ; 134) goes here
  DEFB 0                  ; End marker

; Message 10: '{mountain}/{king}/{animal}/{verb}'
;
; Used by the routines at DOCLASS and DETENTION; it is a submessage of messages
; 74, 75, 78, 79, 82, 83 and 96.
MSG010:
  DEFB 0                  ; The message number for the mountain, king, animal
                          ; or verb goes here
  DEFB 0                  ; End marker

; Message 11: '{country}/{year}/{habitat}/{noun}'
;
; Used by the routines at DOCLASS and DETENTION; it is a submessage of messages
; 73, 76, 77, 80, 81, 84 and 96.
MSG011:
  DEFB 0                  ; The message number for the country, year (last
                          ; three digits), habitat or noun goes here
  DEFB 0                  ; End marker

; Message 7: '{grassee}'
;
; Used by the routine at DOCLASS to hold the character number of whoever is
; being grassed up by EINSTEIN for writing on the board (206=BOY WANDER, or
; 210=ERIC); it is a submessage of message 88. Note that storing the character
; number (206 or 210) here is a mistake; it should be the message number of the
; character's name (27 or 31) instead. In any case, this message (along with
; parent message 88) is never used, because of a bug in the section of code at
; DOCLASS_12.
MSG007:
  DEFB 0
  DEFB 0                  ; End marker

; Unused
  DEFS 10

; Bike and Science Lab storeroom combinations
;
; Used by the routines at CHKCOMBOS and PREPGAME.
COMBOS:
  DEFS 4                  ; The bike combination digits are stored here
  DEFS 4                  ; The storeroom combination letters are stored here

; Copy of the bike and Science Lab storeroom combinations
;
; Used by the routine at OBJFALL_13. The digits or letters discovered by ERIC
; have bit 7 set (to indicate that he has already scored points for doing so).
COMBOS2:
  DEFS 4                  ; A copy of the bike combination digits is stored
                          ; here
COMBOS2SLS:
  DEFS 4                  ; A copy of the storeroom combination letters is
                          ; stored here

; Unused
  DEFS 43

; Various ERIC-related data (1)
;
; Used in conjunction with the contents of ERICDATA2. Holds the ASCII code of
; the last key pressed while writing on a blackboard (see WRITING), or the
; descent table identifier (252-255; see FALLING), or the LSB of the address of
; a routine (or an entry point to a routine) for dealing either with ERIC or
; with the result of an action by ERIC (see BENDING):
;
; +-----------+----------------------------------------------+----------------+
; | Address   | Purpose                                      | Placed here by |
; +-----------+----------------------------------------------+----------------+
; | THROW_0   | Remove the water pistol from the inventory   | THROW          |
; | HIT_0     | Deal with ERIC when he's raising his fist    | HIT            |
; | HIT_1     | Deal with ERIC when he's lowering his fist   |                |
; | FIRE_0    | Deal with ERIC when he's raising the         | FIRE           |
; |           | catapult                                     |                |
; | FIRE_1    | Launch a pellet from ERIC's catapult         |                |
; | FIRE_2    | Deal with ERIC after he's launched a         |                |
; |           | catapult pellet                              |                |
; | KISS_6    | Deal with ERIC when he's kissing HAYLEY      | KISS           |
; | DROPMICE  | Release some mice                            | RELEASE        |
; | CATCHMORF | Make ERIC catch a mouse or frog (if present) | CATCH          |
; | CLOUD     | Deal with a dropped stinkbomb                | DROPSBOMB      |
; | WATER     | Control water fired from the pistol          | FIREWP         |
; +-----------+----------------------------------------------+----------------+
ERICDATA1:
  DEFB 0

; Various ERIC-related data (2)
;
; Used in conjunction with the contents of ERICDATA1. Holds the identifier of
; the blackboard ERIC wrote on (see WRITE), or the descent table entry pointer
; (160-180; see FALLING), or the MSB of the address of a routine (or an entry
; point to a routine) for dealing either with ERIC or with the result of an
; action by ERIC (see BENDING, and ERICDATA1 for a list of the routine and
; entry point addresses).
ERICDATA2:
  DEFB 0

; Last value seen in the system variable FRAMES
;
; Stores the value of the LSB of the system variable FRAMES as it stood at the
; end of the last pass through the main loop (see MAINLOOP).
LFRAMES:
  DEFB 0

; ID of the desk containing the water pistol
;
; Used by the routine at ERICSITLIE.
WPDESKID:
  DEFB 0

; ID of the desk containing the stinkbombs
;
; Used by the routine at ERICSITLIE.
SBDESKID:
  DEFB 0

; ERIC's stand-up delay counter
;
; Used by the routine at ERICSITLIE while in demo mode.
UPDELAY:
  DEFB 0

; ERIC's knockout delay counter
;
; Used by the routine at ERICHIT.
KODELAY:
  DEFB 0

; Game mode indicator
;
; Used by the routines at JUMPING, NEWLESSON2, ERICSITLIE and START. Holds 255
; if in demo mode, 0 if a game is in progress, or 1 if ERIC has just opened the
; safe.
GAMEMODE:
  DEFB 0

; Current lesson number
;
; Used by the routine at NEWLESSON1. Holds the index (192-255) into the main
; timetable. Adjusted by the routine at PREPGAME before starting a new game.
LESSONNO:
  DEFB 0

; Lesson descriptor
;
; Used by the routines at PRESENT, CHKERIC2, NEWLESSON2, DOCLASS and
; NEWLESSON1. The room ID is stored in bits 0-3, and the teacher ID in bits
; 4-7. See the list of lesson descriptors at LDESCS.
LESSONDESC:
  DEFB 0

; Number of mice caught
;
; Used by the routines at DROPMICE, PREPMICE and CATCHMORF.
MOUSETALLY:
  DEFB 0

; Kiss counter
;
; Used by the routines at KISS (which decreases the counter by 7 whenever ERIC
; kisses HAYLEY) and KNOCKED (which decreases the counter by 1 whenever HAYLEY
; is knocked over). Initialised to 40 by the routine at PREPGAME.
KISSCOUNT:
  DEFB 0

; Lesson clock
;
; Decremented in the main loop at MAINLOOP. When it reaches 0, it is reset to
; 4096 by the routine at NEWLESSON1. It is also checked by the routines at
; KNOCKED, CHKERIC2, RSTTELLSIT, DINDUTY and ASSEMDUTY, and modified by the
; routines at KNOCKED, DETENTION, EXPEL, FINDEXPEL and PREPGAME.
CLOCK:
  DEFW 0

; Score
;
; Used by the routine at ADDPTS.
SCORE:
  DEFW 0

; Lines total
;
; Used by the routines at KISS and ADDLINES.
LINES:
  DEFW 0

; Hi-score
;
; Used by the routine at FINDEXPEL.
HISCORE:
  DEFW 0

; Inventory flags
;
; Used by the routine at PRINTINV. Bit set=got, bit reset=haven't.
;
; +-----+--------------------+------------+----------+---------------------+
; | Bit | Item               | Set        | Reset    | Checked             |
; +-----+--------------------+------------+----------+---------------------+
; | 0   | Safe key           | FROGFALL   |          | JUMPING, FROGFALL   |
; | 1   | Science Lab        | CHKCOMBOS  |          | CHKCOMBOS,          |
; |     | storeroom key      |            |          | OPENLABRM,          |
; |     |                    |            |          | CHKCUPFULL, MVMOUSE |
; | 2   | Frog               | CATCHFROG  | ONSADDLE | ONSADDLE            |
; | 3   | Water pistol (with | ERICSITLIE | THROW    | THROW, JUMPING,     |
; |     | water if bit 4     |            |          | FIREWP              |
; |     | reset)             |            |          |                     |
; | 4   | Water pistol with  | JUMPING    | THROW    | THROW, JUMPING,     |
; |     | sherry             |            |          | FIREWP, CHKWATER    |
; | 5   | 3 stinkbombs       | ERICSITLIE | CLOUD    |                     |
; | 6   | 2 stinkbombs       | ERICSITLIE | CLOUD    |                     |
; | 7   | 1 stinkbomb        | ERICSITLIE | CLOUD    | DROPSBOMB           |
; +-----+--------------------+------------+----------+---------------------+
INVENTORY:
  DEFB 0

; Input device indicator
;
; Set by the routine at INPUTDEV2, and checked by the routine at READKEY. Holds
; 0 if the keyboard is being used, or 1 if the Kempston joystick is being used.
KEMPSTON:
  DEFB 0

; ERIC's secondary status flags
;
; Used by the routine at HANDLEERIC to decide how to deal with ERIC when bit 1
; is set at STATUS.
;
; +-----+---------------------------------------------------+------------+
; | Bit | Meaning if set                                    | Routine    |
; +-----+---------------------------------------------------+------------+
; | 0   | ERIC is riding the bike                           | RIDINGBIKE |
; | 1   | ERIC is standing on a plant or plant pot          | ONPLANT    |
; | 2   | ERIC is stepping off a plant, a plant pot, or the | STEPPEDOFF |
; |     | stage                                             |            |
; | 3   | ERIC is falling and will land on his feet         | LANDING    |
; | 4   | ERIC is falling and will not land on his feet     | FALLING    |
; | 5   | ERIC has stepped out of the top-floor window      | BIGFALL    |
; | 6   | ERIC is falling from the saddle of the bike       | OFFSADDLE  |
; | 7   | ERIC is standing on the saddle of the bike        | ONSADDLE   |
; +-----+---------------------------------------------------+------------+
STATUS2:
  DEFB 0

; Last key pressed while riding the bike
;
; Used by the routine at RIDINGBIKE. Holds the value from the keypress offset
; table corresponding to the last key pressed while riding the bike.
LBIKEKEY:
  DEFB 0

; Counter that determines the bike's speed while ERIC's on it
;
; Used by the routine at RIDINGBIKE.
BIKESPEED:
  DEFB 0

; Bike's momentum
;
; Used by the routine at RIDINGBIKE.
BIKEP:
  DEFB 0

; Last key pressed
;
; Set by the routine at MAINLOOP; checked by the routines at MVERIC1 and
; TURNERIC. Holds the value from the keypress offset table corresponding to the
; last key pressed.
KEYCODE:
  DEFB 0

; ERIC's midstride/mid-action timer
;
; Zero unless ERIC is midstride (see MVERIC1) or mid-action (see BENDING); if
; non-zero, the value is copied to the main action timer at ERICTIMER after
; ERIC has been moved from the midstride or mid-action position (see MAINLOOP).
ERICTIMER2:
  DEFB 0

; ERIC's main action timer
;
; Decremented by the main loop at MAINLOOP; when it becomes zero, the keyboard
; is checked and ERIC is moved accordingly. Also used by many routines that
; deal with ERIC to decide when to change ERIC's animatory state or location,
; or when to check the keyboard.
ERICTIMER:
  DEFB 0

; Door/window status flags
;
; Used by the routine at MVDOORWIN. Bit set=open, bit reset=closed.
;
; +-----+----------------------------+
; | Bit | Door/window                |
; +-----+----------------------------+
; | 0   | Left study door            |
; | 1   | Right study door           |
; | 2   | Science Lab storeroom door |
; | 3   | Boys' skool door           |
; | 4   | Skool gate                 |
; | 5   | Drinks cabinet door        |
; | 6   | Top-floor window           |
; | 7   | Middle-floor window        |
; +-----+----------------------------+
DOORFLAGS:
  DEFB 0

; Character number of the teacher who last gave ERIC lines
;
; Used by the routine at CHKERIC2.
LASTLINES:
  DEFB 0

; LSB of the lines-giving delay counter
;
; Used by the routine at CHKERIC1. Also used by the routine at SHUTDOORS when
; timing the closing of the left study, right study and Science Lab storeroom
; doors.
LINESDELAY1:
  DEFB 0

; MSB of the lines-giving delay counter
;
; Used by the routines at CHKERIC1 and CHKERIC2.
LINESDELAY2:
  DEFB 0

; LSB of the address of the SRB byte corresponding to the lip of the speech
; bubble
;
; Used by the routine at SHOWBUBBLE. Holds 0 if no one's speaking.
LIPSRBLSB:
  DEFB 0

; Speech bubble lip SRB bit
;
; The bit set at this byte indicates the bit of the SRB byte (referred to by
; LIPSRBLSB) that corresponds to the lip of the speech bubble (see SHOWBUBBLE).
LIPSRBBIT:
  DEFB 0

; Leftmost column of the play area on screen the last time the SRB was updated
; for the speech bubble
;
; Holds the column of the play area that was at the far left of the screen the
; last time the routine at BUBBLESRB (update the SRB for the speech bubble) was
; called.
SBLEFTCOL:
  DEFB 0

; ERIC's primary status flags
;
; Used by the routine at HANDLEERIC to decide how to deal with ERIC.
;
; +-----+------------------------------------------------------+------------+
; | Bit | Meaning if set                                       | Routine    |
; +-----+------------------------------------------------------+------------+
; | 0   | ERIC is jumping                                      | JUMPING    |
; | 1   | Examine the secondary status flags at STATUS2        |            |
; | 2   | ERIC is sitting or lying down                        | ERICSITLIE |
; | 3   | ERIC is bending over                                 | BENDING    |
; |     | ERIC is dropping a stinkbomb                         |            |
; |     | ERIC is firing the water pistol                      |            |
; |     | ERIC is moving forward as if to kiss (but will miss) |            |
; | 4   | ERIC is writing on a blackboard                      | WRITING    |
; | 5   | ERIC is firing the catapult                          | MIDFHK     |
; |     | ERIC is hitting                                      |            |
; |     | ERIC is kissing HAYLEY                               |            |
; | 6   | MR WACKER is expelling ERIC                          |            |
; | 7   | ERIC has been knocked over                           | ERICHIT    |
; +-----+------------------------------------------------------+------------+
STATUS:
  DEFB 0

; Number of the character just moved (183-214)
;
; Used by the routine at MVCHARS.
LASTCHAR:
  DEFB 0

; Random number seed
;
; Used by the routine at GETRANDOM.
RANDSEED:
  DEFW 36035              ; This is the initial value of the seed

; x-coordinate of the leftmost column of the play area on screen
;
; Modified by the routines at LSCROLL8 and RSCROLL8.
LEFTCOL:
  DEFB 0

  ORG 32768

; Skool graphic data (tiles 0-255, base page 128)
;
; Used by the routine at PRINTTILE. Tiles 0-79 are used by the blackboards:
;
; +-------+---------------------------------------+
; | Tiles | Blackboard                            |
; +-------+---------------------------------------+
; | 0-15  | Blue Room                             |
; | 16-31 | Yellow Room                           |
; | 32-47 | Top-floor room in the girls' skool    |
; | 48-63 | Science Lab                           |
; | 64-79 | Middle-floor room in the girls' skool |
; +-------+---------------------------------------+
SGD128:
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,255,255,255,0,1,1,255,255,1,31,30,63,0,36
  DEFB 36,255,127,255,0,128,129,128,255,62,0,129,255,0,8,206
  DEFB 2,254,0,8,56,32,63,8,14,255,2,0,128,0,255,255
  DEFB 255,255,128,24,248,193,193,255,255,255,179,192,1,255,254,0
  DEFB 16,255,0,0,144,255,112,0,16,252,53,31,16,1,0,255
  DEFB 0,8,1,0,252,255,128,255,0,31,255,31,1,223,215,215
  DEFB 215,255,128,255,255,255,255,255,255,255,0,255,255,255,255,0
  DEFB 255,255,255,255,255,255,255,255,200,104,111,104,255,255,255,255
  DEFB 0,252,254,245,245,245,255,255,129,129,135,159,255,191,191,191
  DEFB 255,255,128,191,255,253,250,250,255,255,215,225,2,3,255,255
  DEFB 255,0,64,255,6,253,0,2,254,255,207,228,244,244,244,3
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,127,31,0,0,3,1,255,1,1,23,30,64,0,36
  DEFB 39,247,127,0,0,129,128,128,247,62,255,129,247,0,200,4
  DEFB 2,255,0,8,16,32,255,8,4,254,2,0,128,0,23,239
  DEFB 128,191,128,24,249,193,65,1,255,128,179,192,0,0,131,0
  DEFB 16,0,231,0,16,0,243,0,16,7,53,31,16,0,0,128
  DEFB 0,8,1,0,20,128,0,128,0,15,255,15,1,208,208,208
  DEFB 208,255,0,0,0,0,0,255,0,0,255,0,0,0,255,126
  DEFB 0,0,0,255,0,0,0,255,200,104,104,111,0,0,0,0
  DEFB 255,4,7,5,5,5,16,0,129,129,135,191,128,128,128,128
  DEFB 0,0,143,191,255,253,250,251,0,255,215,225,3,251,0,0
  DEFB 255,255,127,0,5,253,255,254,254,255,207,100,52,52,52,3
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,63,7,0,0,7,1,255,1,1,31,30,128,224,36
  DEFB 39,255,127,0,0,129,128,128,255,62,0,129,255,0,200,15
  DEFB 2,255,0,8,248,32,255,8,15,253,2,255,128,131,23,255
  DEFB 128,159,128,24,251,193,193,1,255,191,161,64,0,255,139,0
  DEFB 16,255,231,0,16,255,240,0,16,245,53,63,16,0,0,128
  DEFB 0,12,1,130,20,128,0,255,0,31,127,15,1,215,215,215
  DEFB 208,208,0,255,255,255,255,0,255,255,165,255,255,255,128,66
  DEFB 255,255,255,0,255,255,255,223,200,104,111,111,255,255,255,255
  DEFB 255,252,245,245,245,5,255,255,129,131,143,191,191,191,191,128
  DEFB 255,255,143,191,255,251,248,251,255,255,183,193,3,251,255,255
  DEFB 192,64,127,255,5,253,0,255,254,223,207,100,244,244,52,2
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,31,1,0,0,15,1,255,1,1,12,30,255,32,36
  DEFB 63,28,127,255,0,129,128,128,28,62,0,129,0,0,200,8
  DEFB 2,255,0,8,8,32,255,8,8,251,2,255,128,135,23,0
  DEFB 128,143,128,24,255,1,193,1,254,191,158,64,0,254,199,0
  DEFB 16,0,195,0,16,127,248,0,16,245,117,63,16,0,0,128
  DEFB 0,15,1,134,20,128,0,0,240,63,127,15,1,215,215,213
  DEFB 208,208,0,1,245,253,117,0,64,255,165,0,119,255,128,66
  DEFB 85,175,87,0,215,170,255,223,200,104,104,111,80,207,175,254
  DEFB 255,0,245,245,117,5,0,0,129,131,143,191,175,181,170,128
  DEFB 255,174,143,191,255,251,255,255,238,255,119,193,255,255,191,255
  DEFB 192,64,127,175,5,254,0,255,254,223,207,100,244,180,52,3
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,15,0,0,0,31,1,127,1,1,28,30,255,60,36
  DEFB 63,62,127,255,0,129,128,128,30,62,255,255,0,0,200,8
  DEFB 2,255,0,8,8,32,255,8,8,251,2,255,128,139,23,0
  DEFB 128,135,128,24,255,1,193,1,255,191,128,255,0,248,239,255
  DEFB 16,0,129,0,16,31,252,0,16,245,245,255,31,0,0,255
  DEFB 248,16,1,138,20,128,255,0,48,127,63,7,129,213,213,213
  DEFB 208,240,0,0,245,85,117,0,128,185,102,0,170,2,128,36
  DEFB 85,175,85,0,85,170,85,223,200,104,104,239,32,74,169,74
  DEFB 255,0,85,149,85,5,63,254,129,131,143,191,170,181,170,128
  DEFB 64,174,159,255,255,251,250,255,85,255,119,131,3,255,170,157
  DEFB 192,127,255,175,5,255,255,254,254,223,207,100,180,180,52,2
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,7,0,0,0,63,1,31,1,1,30,30,255,36,36
  DEFB 255,127,127,255,0,129,128,128,62,62,129,0,0,0,200,7
  DEFB 2,255,0,8,240,32,255,8,7,251,2,255,128,139,23,0
  DEFB 128,131,128,24,255,1,193,1,255,161,191,255,0,251,255,255
  DEFB 16,24,255,0,31,223,255,240,31,245,245,255,0,0,0,255
  DEFB 8,32,1,138,20,128,255,0,80,127,63,7,129,213,213,213
  DEFB 208,255,0,0,245,85,117,255,0,179,60,0,213,4,128,24
  DEFB 85,173,85,0,85,170,85,207,200,104,104,255,0,74,173,74
  DEFB 255,0,85,181,85,5,32,2,129,131,143,255,170,181,170,128
  DEFB 32,174,159,255,254,251,251,255,171,247,240,130,251,255,170,205
  DEFB 128,127,255,175,5,255,255,252,255,223,199,52,180,180,52,2
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,3,0,0,0,127,1,7,1,1,30,14,193,39,36
  DEFB 255,127,127,129,128,129,128,128,62,24,129,0,0,0,207,2
  DEFB 2,255,0,248,32,32,255,15,2,251,2,252,128,11,23,0
  DEFB 128,129,192,24,255,1,193,1,255,161,128,255,0,251,0,255
  DEFB 95,0,0,240,0,223,0,80,64,245,5,255,64,0,63,255
  DEFB 72,64,1,10,20,128,255,0,144,255,63,3,193,213,213,213
  DEFB 208,255,0,0,213,85,85,255,0,167,60,0,234,137,128,36
  DEFB 85,173,85,0,85,170,85,207,200,104,104,255,0,74,172,74
  DEFB 255,0,85,53,85,5,32,2,129,135,159,255,170,181,170,128
  DEFB 145,170,159,255,254,251,251,255,87,231,240,2,251,255,170,229
  DEFB 128,127,255,171,7,255,255,252,255,223,199,52,180,180,52,2
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
  DEFB 255,0,1,0,0,255,255,1,1,1,255,30,6,129,36,36
  DEFB 255,127,28,1,128,129,128,128,62,0,129,0,0,15,192,2
  DEFB 2,255,248,0,32,32,255,0,2,251,3,224,127,127,251,0
  DEFB 255,128,56,24,255,1,193,255,255,161,255,255,0,253,255,255
  DEFB 128,129,255,144,0,191,255,144,128,245,255,255,128,255,64,255
  DEFB 136,128,0,126,248,128,255,0,144,255,31,3,193,213,213,213
  DEFB 208,255,0,0,213,85,85,255,0,175,126,0,245,83,128,126
  DEFB 85,173,85,0,85,170,85,207,232,104,104,255,0,74,174,74
  DEFB 255,0,85,117,85,5,63,254,129,135,159,255,170,181,170,128
  DEFB 202,170,159,255,253,251,251,255,175,231,224,2,251,255,170,245
  DEFB 128,127,255,171,2,255,255,255,255,207,231,244,180,180,52,2

; Skool graphic data (tiles 0-255, base page 136)
;
; Used by the routine at PRINTTILE.
  DEFB 255,1,127,255,31,63,0,255,255,6,224,255,252,255,0,63
  DEFB 255,0,134,255,0,30,255,0,136,147,255,0,192,141,255,0
  DEFB 1,20,247,0,3,128,128,225,248,0,252,0,0,0,0,6
  DEFB 253,17,19,21,21,53,181,0,1,15,12,252,0,0,127,255
  DEFB 0,0,255,255,0,3,147,255,0,0,74,70,255,0,127,0
  DEFB 149,49,255,0,254,0,123,96,255,0,0,253,141,0,128,224
  DEFB 252,15,63,255,207,140,152,255,68,116,52,52,12,12,31,31
  DEFB 159,0,0,64,127,64,64,0,0,2,254,2,255,191,191,255
  DEFB 4,252,236,236,236,255,15,12,172,175,143,143,0,0,255,253
  DEFB 253,0,3,56,0,255,128,131,3,255,252,255,129,0,248,0
  DEFB 255,9,192,0,192,1,131,255,7,7,31,31,24,223,0,0
  DEFB 127,80,223,223,0,255,254,254,0,240,82,6,64,1,1,0
  DEFB 0,0,255,48,48,62,62,177,0,255,255,79,96,97,254,14
  DEFB 14,8,255,1,193,213,193,255,0,0,128,159,151,255,7,68
  DEFB 72,0,252,248,255,1,32,8,2,1,255,192,0,96,224,224
  DEFB 255,255,65,16,4,2,255,35,63,1,0,0,0,255,1,161
  DEFB 129,255,127,255,63,255,0,128,255,133,250,255,2,255,0,79
  DEFB 7,0,252,240,0,18,14,0,146,186,248,0,127,29,63,247
  DEFB 255,10,143,0,3,224,128,158,250,0,7,0,255,255,0,135
  DEFB 253,17,21,21,85,181,253,0,1,12,13,255,254,0,127,0
  DEFB 255,192,255,0,239,3,147,0,255,0,74,70,0,127,255,0
  DEFB 149,49,0,0,22,0,113,224,0,253,0,253,141,255,128,240
  DEFB 252,15,191,255,207,152,152,247,68,52,180,244,12,12,31,31
  DEFB 159,127,127,64,64,64,64,254,254,2,2,2,128,128,128,255
  DEFB 4,12,12,12,12,239,15,12,172,172,140,141,126,0,1,5
  DEFB 133,0,3,248,0,255,128,131,3,255,254,255,129,255,240,0
  DEFB 255,9,224,0,192,2,131,247,7,7,31,24,24,255,126,0
  DEFB 192,80,223,223,0,0,254,254,0,16,83,8,64,0,0,126
  DEFB 0,255,255,48,48,62,62,113,0,1,254,80,96,129,252,14
  DEFB 14,8,253,1,205,213,193,255,0,0,128,246,246,199,7,68
  DEFB 72,0,4,8,255,0,32,8,2,1,135,224,0,239,239,239
  DEFB 255,1,64,16,4,2,255,59,63,1,255,255,255,255,0,161
  DEFB 127,255,127,31,63,127,255,255,255,133,250,255,255,255,0,3
  DEFB 255,0,224,255,0,18,255,0,151,170,255,0,120,21,255,247
  DEFB 1,6,255,0,2,144,128,255,250,0,255,0,255,255,0,133
  DEFB 253,17,21,21,245,181,253,0,1,143,13,255,254,3,0,255
  DEFB 0,48,0,255,40,3,19,255,1,0,74,127,255,127,255,0
  DEFB 149,255,255,255,22,0,96,255,255,253,0,253,253,0,128,240
  DEFB 254,255,191,239,207,152,152,231,68,244,180,244,12,12,31,31
  DEFB 159,64,64,64,64,64,64,2,2,2,2,2,191,191,191,255
  DEFB 4,236,236,236,12,239,15,28,172,172,143,140,66,0,1,253
  DEFB 5,0,0,56,0,128,128,131,3,255,254,129,129,2,224,0
  DEFB 255,5,224,0,0,3,131,231,7,7,31,24,24,255,74,0
  DEFB 64,80,223,223,0,0,254,254,0,16,86,48,64,0,0,102
  DEFB 0,0,255,48,48,62,62,17,0,1,254,80,96,1,240,14
  DEFB 14,8,249,1,213,213,193,255,0,0,128,149,151,7,7,68
  DEFB 72,0,248,255,255,0,16,8,2,0,129,240,0,239,235,239
  DEFB 255,1,32,16,4,129,255,55,63,1,253,235,231,255,0,161
  DEFB 64,255,0,31,63,128,255,128,0,133,6,0,1,0,0,31
  DEFB 0,0,252,0,0,18,0,0,151,169,0,0,72,255,0,247
  DEFB 0,128,0,1,2,128,191,0,6,128,0,0,0,0,0,133
  DEFB 249,17,21,21,53,181,253,0,1,12,13,255,254,12,255,0
  DEFB 0,12,255,0,40,3,243,0,1,255,127,0,0,127,255,255
  DEFB 255,0,0,255,22,255,224,0,0,253,255,253,1,0,192,240
  DEFB 62,255,191,239,207,152,152,231,68,52,180,244,12,12,31,31
  DEFB 223,64,64,64,64,64,64,2,2,2,2,2,191,191,191,254
  DEFB 4,236,236,172,12,207,15,60,172,140,141,140,70,60,1,133
  DEFB 5,0,0,0,0,128,128,131,3,255,255,129,129,2,192,1
  DEFB 255,3,240,0,0,2,131,199,7,23,31,24,24,255,102,0
  DEFB 95,80,223,255,0,255,254,255,0,208,90,64,64,0,0,66
  DEFB 0,0,255,48,48,62,62,13,0,1,252,80,64,1,224,14
  DEFB 14,8,241,3,213,213,193,254,0,0,128,243,246,7,7,68
  DEFB 72,0,8,1,1,0,16,4,2,0,128,248,31,239,234,235
  DEFB 224,1,32,8,4,129,63,48,63,195,244,171,165,0,0,161
  DEFB 64,255,127,31,63,128,0,255,0,133,254,0,255,0,0,31
  DEFB 0,0,242,0,1,18,0,0,146,168,0,0,79,48,0,247
  DEFB 64,64,0,3,2,128,161,0,254,0,0,0,0,0,0,133
  DEFB 249,17,21,21,53,181,253,0,0,13,13,255,254,48,0,255
  DEFB 0,3,0,239,40,3,3,255,1,0,127,255,127,127,255,0
  DEFB 255,255,247,0,22,0,96,255,253,253,1,253,255,255,192,248
  DEFB 62,63,191,239,207,152,152,231,68,180,180,244,12,12,31,31
  DEFB 223,64,64,64,64,64,64,2,2,2,2,2,170,189,170,254
  DEFB 4,172,172,172,12,207,15,44,172,143,140,143,78,34,253,5
  DEFB 253,0,0,0,0,128,128,131,3,255,255,129,129,2,128,7
  DEFB 65,3,240,0,0,2,131,135,7,31,24,24,24,255,114,0
  DEFB 223,80,223,48,0,255,254,0,0,208,90,64,192,0,0,90
  DEFB 1,0,255,48,48,62,62,3,224,1,248,81,128,1,128,14
  DEFB 14,8,225,7,213,217,193,248,0,0,128,147,167,7,7,68
  DEFB 72,0,254,255,255,15,16,4,2,0,128,252,48,232,234,237
  DEFB 224,1,32,8,4,129,15,48,63,70,18,170,165,0,254,127
  DEFB 64,252,128,31,63,0,0,136,0,125,1,0,145,0,14,20
  DEFB 0,0,145,0,7,18,0,0,242,196,0,0,89,32,0,247
  DEFB 224,130,0,3,2,128,161,0,1,0,0,0,0,0,0,125
  DEFB 249,17,21,21,53,181,253,1,0,13,13,255,255,255,0,0
  DEFB 0,255,0,40,40,243,0,1,1,127,70,0,64,127,255,255
  DEFB 59,0,20,0,22,255,96,0,5,252,253,253,0,0,192,248
  DEFB 31,63,255,239,143,152,152,199,68,180,52,252,12,12,31,31
  DEFB 223,64,64,64,64,127,64,2,2,2,2,254,170,173,170,252
  DEFB 4,172,172,172,12,143,15,108,172,157,140,140,60,190,133,5
  DEFB 1,3,0,0,255,128,128,131,3,255,255,129,129,2,0,31
  DEFB 33,129,248,192,0,3,131,7,7,31,24,24,152,255,60,0
  DEFB 223,80,223,16,0,255,254,0,0,209,94,64,48,255,0,60
  DEFB 254,0,254,48,48,62,63,255,16,1,240,81,0,3,0,14
  DEFB 14,248,193,29,213,193,193,240,0,0,128,251,252,7,7,68
  DEFB 72,0,2,2,0,16,16,4,1,0,128,254,127,239,234,237
  DEFB 224,129,32,8,2,157,3,48,63,199,250,170,165,0,255,95
  DEFB 64,240,255,31,63,0,0,162,0,33,255,0,73,0,5,20
  DEFB 0,56,144,0,13,30,0,128,178,196,0,0,105,63,0,255
  DEFB 80,130,0,3,2,128,161,0,255,31,0,255,0,0,128,33
  DEFB 241,17,21,21,53,181,253,1,0,13,13,255,255,0,0,0
  DEFB 0,0,0,40,40,19,0,1,1,78,110,0,64,127,255,159
  DEFB 49,0,20,0,22,127,96,0,5,252,253,141,0,0,224,248
  DEFB 31,31,255,239,143,152,152,199,68,180,52,252,12,28,31,31
  DEFB 223,64,64,127,64,0,64,2,2,254,2,0,170,165,170,252
  DEFB 4,172,172,172,12,143,15,204,172,140,143,140,24,126,5,253
  DEFB 1,3,0,0,255,128,128,131,3,254,255,129,129,255,0,127
  DEFB 33,128,248,192,0,3,131,7,7,31,24,24,159,255,24,127
  DEFB 223,80,223,0,255,254,254,0,240,82,90,64,12,56,255,24
  DEFB 0,0,252,48,48,62,48,1,12,255,224,80,1,3,0,14
  DEFB 15,0,193,57,213,197,193,192,3,15,135,143,159,7,255,68
  DEFB 248,248,252,254,255,32,16,4,1,0,128,255,96,232,234,238
  DEFB 255,65,32,8,2,191,3,48,63,135,11,170,165,255,161,80
  DEFB 254,128,255,31,63,0,255,128,7,31,255,0,1,254,3,4
  DEFB 7,240,16,254,8,30,7,135,146,196,254,128,73,63,7,255
  DEFB 40,130,254,3,2,128,161,7,255,15,254,255,7,254,192,31
  DEFB 241,17,21,21,53,181,255,1,0,13,13,255,255,27,0,255
  DEFB 255,214,0,239,239,147,0,255,255,74,70,0,127,127,255,157
  DEFB 49,0,247,127,250,127,96,0,253,252,253,221,0,0,224,252
  DEFB 31,255,255,207,143,152,152,199,68,180,52,252,12,28,31,159
  DEFB 223,127,64,0,64,127,64,254,2,0,2,254,170,181,170,252
  DEFB 4,172,172,172,12,143,15,172,172,140,140,140,0,62,5,5
  DEFB 1,3,0,0,255,255,255,255,3,252,255,255,255,2,0,255
  DEFB 17,192,252,192,0,131,131,7,7,31,24,24,223,255,0,128
  DEFB 223,80,223,0,0,252,254,0,8,82,90,64,3,6,255,0
  DEFB 0,0,48,48,62,62,48,0,2,255,192,80,25,3,0,14
  DEFB 8,0,129,225,213,193,193,128,254,8,252,254,250,7,68,79
  DEFB 0,8,4,2,255,32,8,4,1,255,128,255,127,239,239,239
  DEFB 255,65,16,8,2,191,3,48,191,255,255,255,255,255,161,80

; Skool graphic data (tiles 0-255, base page 144)
;
; Used by the routine at PRINTTILE. Tiles 1, 11, 21 and 30 are unused.
  DEFB 80,240,128,151,128,128,129,0,0,0,1,0,247,0,0,0
  DEFB 0,0,255,255,65,80,255,8,9,0,0,0,255,255,63,252
  DEFB 0,253,241,248,8,0,4,0,255,0,2,234,2,131,255,1
  DEFB 115,5,5,255,16,19,4,80,247,16,240,0,1,64,15,24
  DEFB 0,191,255,0,255,193,193,252,0,3,0,0,224,0,255,255
  DEFB 1,249,5,5,253,1,129,3,251,63,0,0,252,0,0,2
  DEFB 250,28,0,1,124,8,0,60,2,7,7,32,0,6,3,255
  DEFB 128,1,224,0,254,0,253,63,16,97,48,254,191,7,63,95
  DEFB 5,127,0,3,1,26,255,252,0,232,0,0,242,244,160,248
  DEFB 95,255,15,255,171,234,94,54,109,223,255,0,0,211,4,1
  DEFB 127,3,1,248,191,255,255,255,247,191,245,255,127,0,233,240
  DEFB 0,152,252,246,192,24,255,252,0,240,128,192,240,248,255,0
  DEFB 254,101,0,31,0,15,3,232,192,0,248,0,240,62,255,3
  DEFB 0,30,251,249,96,0,0,0,142,240,244,127,224,23,128,0
  DEFB 0,252,192,255,32,240,240,4,0,0,0,48,52,52,52,48
  DEFB 0,4,0,3,3,0,128,128,4,0,1,3,7,8,15,0
  DEFB 80,255,128,159,128,128,255,255,255,255,0,255,255,0,248,255
  DEFB 255,255,255,1,64,240,255,8,9,254,254,254,255,1,51,248
  DEFB 1,241,193,128,8,0,4,0,255,0,2,250,2,131,255,1
  DEFB 243,5,5,255,16,19,4,80,255,16,240,1,1,0,57,96
  DEFB 0,195,255,0,255,193,193,254,0,3,0,128,255,0,225,255
  DEFB 1,255,5,5,1,1,241,3,255,30,248,7,248,0,0,3
  DEFB 3,14,0,0,248,4,0,27,7,9,8,112,130,6,1,250
  DEFB 96,1,216,3,218,0,255,0,18,106,16,255,255,15,94,95
  DEFB 2,255,0,3,1,19,131,0,0,240,0,0,242,248,160,252
  DEFB 191,255,31,255,199,170,94,38,91,255,255,0,56,96,2,1
  DEFB 9,7,34,252,255,255,255,255,247,255,247,255,247,64,249,120
  DEFB 0,152,247,252,128,12,255,248,5,240,128,192,240,248,255,0
  DEFB 255,125,0,127,1,15,7,208,128,0,248,0,240,16,243,0
  DEFB 0,30,115,249,0,0,0,0,159,16,21,248,248,192,128,0
  DEFB 32,252,65,252,48,255,224,8,0,0,0,52,52,52,52,32
  DEFB 0,4,0,3,2,128,128,128,4,0,1,3,4,8,14,0
  DEFB 80,255,128,128,128,128,128,94,239,255,0,255,0,0,8,78
  DEFB 255,255,255,1,127,255,255,8,9,106,174,254,255,129,243,240
  DEFB 7,193,193,128,127,0,4,0,255,0,2,2,2,131,253,1
  DEFB 243,5,5,255,16,19,4,80,255,16,248,7,3,8,72,0
  DEFB 192,0,255,0,255,193,193,255,0,3,0,128,127,2,0,241
  DEFB 1,197,5,5,1,1,255,3,255,3,248,11,0,31,0,1
  DEFB 192,7,0,0,240,14,128,15,7,2,255,241,135,6,0,7
  DEFB 56,1,244,7,252,0,255,0,156,244,32,255,255,31,158,63
  DEFB 1,199,1,3,1,54,7,0,128,240,1,128,254,240,160,254
  DEFB 95,255,63,255,171,230,90,44,171,255,252,0,254,200,2,6
  DEFB 248,252,36,254,191,251,255,255,247,255,245,255,15,192,187,124
  DEFB 0,240,15,0,192,12,255,240,14,224,192,192,240,248,255,0
  DEFB 156,15,0,3,7,63,14,160,0,0,255,0,224,24,255,0
  DEFB 57,61,251,248,0,0,0,0,204,16,30,240,31,207,0,224
  DEFB 96,248,66,252,120,158,0,16,0,0,0,52,52,52,52,32
  DEFB 0,8,0,3,4,128,128,128,4,0,1,3,4,8,14,0
  DEFB 80,255,128,128,128,128,255,174,167,245,0,255,0,0,248,102
  DEFB 82,242,0,1,127,255,255,8,8,106,170,254,63,225,255,192
  DEFB 15,65,193,128,65,0,8,0,255,0,2,2,2,131,249,1
  DEFB 255,5,5,255,16,19,2,80,255,16,254,3,3,5,4,0
  DEFB 48,0,255,0,241,193,193,255,0,3,64,0,15,13,0,129
  DEFB 1,5,253,5,1,249,255,3,255,0,255,115,0,32,1,0
  DEFB 32,3,192,3,224,31,64,7,1,4,32,251,152,134,0,255
  DEFB 200,0,250,142,166,0,0,0,248,160,64,7,255,63,158,155
  DEFB 0,0,3,3,3,93,7,0,160,248,1,192,246,239,160,254
  DEFB 255,255,63,255,155,166,90,117,111,255,252,0,31,24,1,25
  DEFB 255,240,56,253,127,251,255,255,239,253,251,255,0,192,253,124
  DEFB 0,240,1,0,224,6,255,224,8,224,192,192,240,254,127,0
  DEFB 128,5,0,15,15,125,31,192,0,0,249,0,40,31,255,0
  DEFB 79,126,251,255,0,0,224,0,237,24,20,239,15,207,0,224
  DEFB 160,138,244,248,120,193,0,32,31,255,0,52,52,52,255,64
  DEFB 255,8,0,255,4,128,128,255,4,0,3,3,7,8,14,0
  DEFB 80,255,128,128,128,255,0,215,147,85,255,255,0,0,16,51
  DEFB 82,73,7,1,80,255,255,8,8,106,170,78,255,33,255,128
  DEFB 63,65,193,128,65,0,8,0,255,0,2,2,2,255,241,1
  DEFB 255,29,5,255,16,19,2,80,255,16,255,1,25,3,0,0
  DEFB 204,0,252,0,193,255,193,255,0,3,128,0,63,16,0,1
  DEFB 31,5,5,5,1,255,255,3,251,0,255,7,0,252,3,0
  DEFB 32,1,255,7,192,0,32,1,7,0,192,253,192,254,128,255
  DEFB 127,0,253,124,39,0,0,0,249,32,64,3,255,63,15,79
  DEFB 224,0,3,3,6,126,7,0,192,248,1,224,247,248,160,255
  DEFB 255,55,63,255,155,182,91,117,111,191,252,7,131,138,1,224
  DEFB 252,28,8,252,255,251,191,239,223,243,251,255,0,192,253,56
  DEFB 64,224,0,0,216,159,190,192,40,192,192,192,240,255,3,0
  DEFB 248,2,0,7,7,113,255,192,0,0,255,0,168,14,169,0
  DEFB 135,254,255,255,0,0,16,0,238,216,20,208,60,254,192,0
  DEFB 192,131,72,212,120,64,0,32,64,0,0,52,52,52,54,64
  DEFB 1,16,0,3,4,128,128,128,8,0,3,7,4,15,14,128
  DEFB 80,255,128,128,255,255,255,235,201,85,255,255,0,255,248,57
  DEFB 82,100,255,193,80,255,255,8,8,42,170,166,255,33,255,0
  DEFB 255,65,193,128,34,0,8,255,255,0,2,2,250,255,193,1
  DEFB 245,101,5,255,16,16,2,176,255,16,255,1,24,3,0,0
  DEFB 70,0,224,1,193,193,193,255,0,195,128,0,249,108,0,1
  DEFB 249,5,5,5,1,255,63,3,255,0,255,15,0,255,4,32
  DEFB 16,0,255,15,192,0,208,0,31,0,0,255,120,254,192,255
  DEFB 63,0,95,24,0,0,0,0,253,32,64,1,135,63,143,47
  DEFB 31,0,3,1,4,253,7,0,128,255,0,240,246,232,160,255
  DEFB 252,31,63,243,155,174,90,126,215,255,255,7,39,251,1,64
  DEFB 224,252,28,245,255,255,255,247,255,251,251,127,128,240,249,48
  DEFB 224,120,0,128,239,255,62,128,72,128,192,192,240,250,0,56
  DEFB 236,1,0,15,13,7,253,192,0,255,3,0,126,249,185,0
  DEFB 15,63,255,250,0,248,248,0,207,84,28,160,253,255,0,192
  DEFB 128,254,126,222,248,96,0,64,128,0,32,52,52,60,52,64
  DEFB 2,16,2,3,8,128,0,128,8,0,3,4,4,15,14,128
  DEFB 80,0,128,128,192,255,0,117,229,85,0,0,0,0,8,60
  DEFB 82,114,255,65,80,0,252,9,8,170,170,82,255,33,0,0
  DEFB 255,67,193,128,34,0,16,240,0,0,2,2,6,255,129,1
  DEFB 197,133,253,255,16,16,1,24,16,16,255,7,124,1,51,0
  DEFB 59,0,0,63,193,193,193,0,0,227,128,0,224,159,0,1
  DEFB 249,5,5,5,1,1,15,195,255,0,7,30,0,255,0,16
  DEFB 8,0,255,31,160,0,232,0,3,0,0,255,12,126,255,0
  DEFB 7,0,44,24,0,1,0,0,254,37,32,0,3,62,207,19
  DEFB 224,0,3,0,13,119,15,0,128,255,0,248,254,164,175,95
  DEFB 252,15,63,147,187,174,94,126,91,254,255,7,38,30,1,32
  DEFB 31,254,176,247,255,255,191,255,223,247,251,127,192,208,255,48
  DEFB 224,135,248,128,190,255,126,0,88,0,192,224,240,251,0,120
  DEFB 237,0,3,139,27,31,250,192,0,255,0,192,190,19,25,0
  DEFB 31,84,249,238,0,255,0,12,255,84,28,64,127,248,0,248
  DEFB 128,255,2,133,255,224,0,255,0,0,48,52,52,60,52,255
  DEFB 2,255,3,3,255,128,0,128,248,1,3,7,8,15,14,128
  DEFB 208,0,159,128,191,255,255,255,255,255,0,0,0,255,255,255
  DEFB 255,255,255,65,80,0,248,9,255,254,254,254,255,63,0,0
  DEFB 255,205,207,128,28,7,15,255,0,254,250,2,254,255,1,31
  DEFB 5,5,129,248,19,252,254,239,16,16,255,3,127,7,15,0
  DEFB 118,0,0,255,193,193,255,0,2,251,128,192,0,127,0,1
  DEFB 249,5,5,5,1,1,3,227,255,1,3,254,0,240,0,12
  DEFB 248,0,255,62,17,0,247,0,7,0,0,255,6,7,255,0
  DEFB 1,0,63,248,0,2,0,0,255,42,16,31,3,62,255,10
  DEFB 255,0,3,0,21,239,31,0,220,255,0,244,242,163,223,191
  DEFB 127,7,63,159,170,174,182,111,167,255,255,3,197,4,1,16
  DEFB 243,255,224,255,255,255,255,255,191,245,255,255,224,248,240,0
  DEFB 8,223,251,128,255,255,254,0,208,128,192,240,240,254,0,248
  DEFB 255,0,15,2,55,255,244,192,0,0,0,248,127,36,9,0
  DEFB 30,255,249,228,0,0,0,140,255,116,60,128,63,128,0,192
  DEFB 192,255,3,7,112,252,0,0,0,0,48,52,52,52,48,0
  DEFB 2,0,3,3,0,128,128,0,0,1,3,4,15,15,14,128

; Skool graphic data (tiles 0-223, base page 152, byte 1/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 128,192,32,224,96,64,0,196,0,16,0,4,0,32,0,100
  DEFB 255,143,255,129,173,165,165,177,129,128,255,129,1,7,0,7
  DEFB 127,7,255,252,255,255,255,2,255,127,127,0,255,254,255,4
  DEFB 255,134,134,140,204,254,0,255,63,48,192,255,0,130,130,226
  DEFB 255,0,0,4,255,4,255,0,255,207,160,168,255,227,131,140
  DEFB 143,143,255,0,255,133,128,0,1,0,255,255,1,255,32,0
  DEFB 0,32,0,255,224,255,0,255,2,217,0,4,0,255,253,4
  DEFB 0,192,124,128,160,255,255,95,0,4,8,5,21,0,9,255
  DEFB 254,245,0,160,63,132,5,1,1,65,255,255,128,1,143,136
  DEFB 72,75,65,241,255,225,1,65,193,97,225,255,129,1,253,5
  DEFB 221,255,0,3,50,67,18,0,0,159,20,1,52,180,1,240
  DEFB 255,255,192,191,237,127,128,95,95,1,128,0,192,0,208,1
  DEFB 64,2,124,120,255,255,129,208,6,0,252,5,15,255,253,248
  DEFB 255,249,192,37,192,37,223,255,248,251,254,255,64,95,132,63

; Speech bubble lip UDG
;
; Used by the routine at SHOWBUBBLE.
LIPUDG:
  DEFB 66,66,34,36,36,20,20,8

; Safe key UDG
;
; Used by the routine at PRINTINV.
SAFEKEYUDG:
  DEFB 7,5,7,8,16,32,80,160

; Skool graphic data (tiles 240-249, base page 152, byte 1/8)
;
; Used by the routine at PRINTTILE.
  DEFB 255,255,224,146,73,45,146,73,45,160

; Unused
  DEFS 2

; Water animation table entry (phase 2)
;
; Used by the routine at WATER. The next entry is at WANIM3.
WANIM2:
  DEFB 92                 ; 92: water fired from the pistol (phase 2)
  DEFB 254                ; -2: x-coordinate increment
  DEFB 255                ; -1: y-coordinate increment
  DEFB 0                  ; 0=cannot hit anything at this phase

; Skool graphic data (tiles 0-223, base page 152, byte 2/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 128,64,32,96,96,64,0,252,0,16,0,4,0,16,0,124
  DEFB 255,143,255,129,165,165,165,129,131,128,253,1,1,15,0,15
  DEFB 127,15,255,253,255,254,0,133,255,127,127,0,255,254,255,4
  DEFB 255,134,134,140,204,254,255,255,63,48,192,255,0,130,130,242
  DEFB 255,0,0,4,192,4,255,0,192,192,175,175,255,227,131,140
  DEFB 140,140,255,0,128,8,0,255,255,0,0,0,0,0,160,0
  DEFB 63,63,0,0,0,0,255,1,2,1,252,252,0,0,3,4
  DEFB 0,127,0,191,160,0,128,223,0,252,248,245,21,0,9,0
  DEFB 2,247,255,160,0,127,5,1,1,65,0,0,255,255,136,136
  DEFB 239,75,65,249,255,225,1,65,65,225,225,253,193,1,5,253
  DEFB 229,129,0,15,122,67,23,255,255,191,84,3,180,244,1,240
  DEFB 128,0,64,128,13,64,160,80,81,1,128,0,223,255,208,1
  DEFB 64,0,68,72,255,254,49,208,6,0,240,5,57,192,37,96
  DEFB 255,255,192,37,192,37,255,255,248,193,6,15,32,130,8,82

; Top left corner of the speech bubble UDG
;
; Used by the routine at SHOWBUBBLE.
SBTLUDG:
  DEFB 62,65,64,128,128,128,128,128

; Science Lab storeroom key UDG
;
; Used by the routine at PRINTINV.
SLSKEYUDG:
  DEFB 8,20,8,8,8,8,8,12

; Skool graphic data (tiles 240-249, base page 152, byte 2/8)
;
; Used by the routine at PRINTTILE.
  DEFB 73,46,32,146,73,45,146,73,45,160

; Unused
  DEFS 2

; Water animation table entry (phase 3)
;
; Used by the routine at WATER. The previous entry is at WANIM2, and the next
; entry is at WANIM4.
WANIM3:
  DEFB 108                ; 108: water fired from the pistol (phase 3)
  DEFB 254                ; -2: x-coordinate increment
  DEFB 0                  ; 0: y-coordinate increment
  DEFB 1                  ; 1=may hit a cup at this phase

; Skool graphic data (tiles 0-223, base page 152, byte 3/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 128,64,32,96,96,64,12,32,0,96,0,2,1,12,0,16
  DEFB 255,135,191,129,165,165,165,129,135,128,249,1,1,31,0,15
  DEFB 255,15,255,253,255,252,255,133,255,255,127,0,255,254,254,134
  DEFB 255,134,134,140,239,254,128,255,63,48,192,255,255,130,130,242
  DEFB 255,255,0,4,128,4,255,255,128,191,168,160,251,227,131,140
  DEFB 143,140,255,255,255,16,255,1,0,124,255,255,255,255,112,255
  DEFB 32,0,0,224,0,255,165,255,2,255,4,0,0,255,5,5
  DEFB 0,0,255,160,191,255,255,223,0,16,255,21,245,0,9,255
  DEFB 254,214,192,160,127,255,1,255,1,81,255,1,255,0,136,200
  DEFB 192,69,65,249,253,193,1,65,65,97,161,249,225,129,5,65
  DEFB 243,129,0,51,122,67,20,0,1,131,244,1,180,255,255,224
  DEFB 191,255,64,191,237,64,128,95,80,1,128,0,208,1,208,1
  DEFB 64,0,120,127,208,6,15,208,6,192,255,253,232,192,37,32
  DEFB 255,197,192,37,192,37,255,255,248,159,135,247,31,1,240,146

; Top-middle section of the speech bubble UDG
;
; Used by the routine at SHOWBUBBLE.
SBTMUDG:
  DEFB 126,129,0,0,0,0,0,0

; Captured frog UDG
;
; Used by the routine at PRINTINV.
FROGUDG:
  DEFB 8,20,34,68,156,164,136,124

; Skool graphic data (tiles 240-249, base page 152, byte 3/8)
;
; Used by the routine at PRINTTILE.
  DEFB 73,45,32,146,73,45,146,73,45,160

; Unused
  DEFS 2

; Water animation table entry (phase 4)
;
; Used by the routine at WATER. The previous entry is at WANIM3, and the next
; entry is at WANIM5.
WANIM4:
  DEFB 116                ; 116: water fired from the pistol (phase 4)
  DEFB 255                ; -1: x-coordinate increment
  DEFB 0                  ; 0: y-coordinate increment
  DEFB 0                  ; 0=cannot hit anything at this phase

; Skool graphic data (tiles 0-223, base page 152, byte 4/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 192,192,32,96,96,64,255,63,255,128,255,2,255,3,192,248
  DEFB 255,135,159,129,165,165,165,129,143,128,249,1,1,31,1,31
  DEFB 255,31,255,253,224,249,255,133,127,255,127,0,255,254,254,134
  DEFB 247,134,134,140,239,254,128,191,63,48,192,255,255,130,194,242
  DEFB 255,255,0,4,128,4,255,255,128,160,168,224,251,195,131,140
  DEFB 142,140,255,255,128,16,0,1,0,134,181,175,255,0,104,0
  DEFB 32,0,0,160,0,255,165,20,2,0,4,0,0,238,11,7
  DEFB 0,0,145,175,128,255,64,223,0,16,84,213,4,1,9,191
  DEFB 4,150,191,160,64,0,1,0,255,65,159,2,255,128,136,200
  DEFB 64,68,65,253,253,193,193,65,65,225,33,241,241,65,5,61
  DEFB 251,137,255,195,74,67,20,0,1,255,52,1,180,255,255,224
  DEFB 175,237,64,191,237,64,128,81,80,3,128,0,208,1,208,1
  DEFB 64,0,72,65,208,6,0,208,6,224,64,37,36,192,37,32
  DEFB 240,5,192,37,192,37,255,255,254,33,203,11,0,0,0,146

; Top-right corner of the speech bubble UDG
;
; Used by the routine at SHOWBUBBLE.
SBTRUDG:
  DEFB 124,130,2,1,1,1,1,1

; Left half of the waterpistol UDG
;
; Used by the routine at PRINTINV.
WPLEFTUDG:
  DEFB 31,31,60,60,127,126,254,254

; Skool graphic data (tiles 240-249, base page 152, byte 4/8)
;
; Used by the routine at PRINTTILE.
  DEFB 73,45,32,146,73,45,146,73,45,32

; Unused
  DEFS 2

; Water animation table entry (phase 5)
;
; Used by the routine at WATER. The previous entry is at WANIM4, and the next
; entry is at WANIM6.
WANIM5:
  DEFB 124                ; 124: water fired from the pistol (phase 5+)
  DEFB 0                  ; 0: x-coordinate increment
  DEFB 1                  ; +1: y-coordinate increment
  DEFB 0                  ; 0=cannot hit anything at this phase

; Skool graphic data (tiles 0-223, base page 152, byte 5/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 192,32,224,96,96,64,172,32,0,0,16,1,1,0,96,4
  DEFB 191,131,143,129,165,165,165,129,159,128,241,1,1,63,1,31
  DEFB 255,31,254,253,192,251,255,133,127,127,127,0,255,0,254,138
  DEFB 246,134,134,140,255,254,128,191,63,48,192,1,255,130,195,254
  DEFB 0,255,0,252,128,4,0,251,128,175,168,255,251,195,131,140
  DEFB 142,140,159,255,128,8,255,5,255,255,181,170,255,0,156,63
  DEFB 40,255,131,160,0,255,102,28,2,252,4,255,0,85,22,6
  DEFB 0,0,128,175,255,157,95,81,0,16,5,213,255,3,9,170
  DEFB 244,150,160,160,125,255,1,255,0,65,149,3,130,192,136,232
  DEFB 96,66,193,253,249,129,65,65,65,97,33,225,249,33,5,25
  DEFB 1,149,0,195,74,67,20,0,1,3,52,1,180,255,255,192
  DEFB 175,173,64,170,173,95,128,80,95,6,0,0,208,1,208,1
  DEFB 64,0,126,127,208,6,0,208,6,248,64,37,32,192,37,32
  DEFB 255,253,255,253,192,37,191,255,255,66,76,133,0,0,0,146

; Bottom-left corner of the speech bubble UDG
;
; Used by the routine at SHOWBUBBLE.
SBBLUDG:
  DEFB 128,128,128,128,128,64,65,62

; Right half of the waterpistol UDG
;
; Used by the routine at PRINTINV.
WPRIGHTUDG:
  DEFB 252,252,128,128,128,0,0,0

; Skool graphic data (tiles 240-249, base page 152, byte 5/8)
;
; Used by the routine at PRINTTILE.
  DEFB 73,45,160,146,73,45,146,73,45,32

; Unused
  DEFS 2

; Water animation table entry (phase 6+)
;
; Used by the routine at WATER. The previous entry is at WANIM5, and the unused
; entry for phase 7+ is at WANIM7.
WANIM6:
  DEFB 124                ; 124: water fired from the pistol (phase 5+)
  DEFB 0                  ; 0: x-coordinate increment
  DEFB 1                  ; +1: y-coordinate increment
  DEFB 2                  ; 2=may hit a plant or the ground at this phase

; Skool graphic data (tiles 0-223, base page 152, byte 6/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 64,32,224,96,96,64,95,32,0,0,8,1,15,0,240,2
  DEFB 191,131,135,129,165,165,165,129,191,128,225,1,3,63,3,63
  DEFB 255,63,254,255,192,251,255,125,127,127,255,0,254,0,254,138
  DEFB 230,134,134,140,255,0,128,191,63,48,192,1,2,130,194,0
  DEFB 0,0,7,0,128,252,0,9,143,168,168,255,243,195,131,141
  DEFB 142,140,191,255,191,135,225,1,255,193,181,170,255,255,0,33
  DEFB 32,255,4,160,0,255,36,227,2,196,4,255,192,171,28,6
  DEFB 255,0,191,175,255,205,80,81,240,16,245,213,255,5,9,170
  DEFB 20,150,160,160,149,1,1,255,127,65,149,2,130,160,136,104
  DEFB 96,67,225,253,249,129,65,65,65,97,33,193,253,33,5,57
  DEFB 255,137,1,67,74,67,21,255,255,3,52,1,180,255,255,192
  DEFB 175,173,64,170,173,81,128,80,64,28,0,0,208,1,208,1
  DEFB 64,0,66,66,208,6,0,208,6,252,64,37,32,192,37,32
  DEFB 192,37,192,37,192,37,252,127,250,66,127,196,0,0,0,146

; Bottom-middle section of the speech bubble UDG
;
; Used by the routine at SHOWBUBBLE.
SBBMUDG:
  DEFB 0,0,0,0,0,0,129,126

; Three stinkbombs UDG
;
; Used by the routine at PRINTINV.
STINKB3UDG:
  DEFB 32,80,34,5,18,40,16,0

; Skool graphic data (tiles 240-249, base page 152, byte 6/8)
;
; Used by the routine at PRINTTILE.
  DEFB 73,45,160,146,73,45,146,73,45,32

; Unused
  DEFS 2
; If the following bytes were used, they would be the entry for phase 7+ in the
; water animation table (see the entry for phase 6+ at WANIM6, and the routine
; at WATER).
WANIM7:
  DEFB 124                ; 124: water fired from pistol (phase 5+)
  DEFB 0                  ; 0: x-coordinate increment
  DEFB 1                  ; +1: y-coordinate increment
  DEFB 2                  ; 2=may hit a plant or the ground at this phase

; Skool graphic data (tiles 0-223, base page 152, byte 7/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 192,32,96,96,96,64,228,63,192,255,8,255,57,255,216,254
  DEFB 159,129,131,129,165,165,165,129,255,128,225,1,3,127,3,63
  DEFB 255,63,252,255,128,251,252,33,127,127,255,0,254,0,252,10
  DEFB 230,134,142,140,255,0,128,63,63,112,192,1,130,130,226,0
  DEFB 0,0,4,0,128,0,0,8,142,168,168,255,243,131,131,141
  DEFB 142,140,255,255,166,128,1,1,255,129,181,170,255,33,0,32
  DEFB 32,255,4,160,0,255,60,34,218,4,4,255,160,87,4,6
  DEFB 128,0,191,160,255,229,95,81,8,16,245,21,255,5,9,170
  DEFB 244,150,160,160,253,1,1,255,65,127,149,2,130,144,136,88
  DEFB 80,65,225,255,241,1,65,65,65,97,33,129,255,17,5,89
  DEFB 1,129,254,67,122,71,21,0,131,3,52,1,180,255,255,192
  DEFB 171,173,64,191,255,80,128,95,64,56,0,0,208,1,223,255
  DEFB 120,0,124,126,208,6,0,255,254,254,64,37,51,192,253,32
  DEFB 192,37,192,37,192,37,243,112,251,68,95,228,0,0,0,146

; Bottom-right corner of the speech bubble UDG
;
; Used by the routine at SHOWBUBBLE.
SBBRUDG:
  DEFB 1,1,1,1,1,2,130,124

; Two stinkbombs UDG
;
; Used by the routine at PRINTINV.
STINKB2UDG:
  DEFB 4,10,4,0,32,80,32,0

; Skool graphic data (tiles 240-249, base page 152, byte 7/8)
;
; Used by the routine at PRINTTILE.
  DEFB 73,45,160,146,73,45,82,73,46,32

; Unused
  DEFS 2

; x-coordinates of the plant pots
;
; Used by the routine at CHECKPOT. The y-coordinates of the plant pots can be
; found at POTSY.
POTSX:
  DEFB 91                 ; x-coordinate of the plant pot near the top-floor
                          ; window
  DEFB 93                 ; x-coordinate of the plant pot near the middle-floor
                          ; window
  DEFB 132                ; x-coordinate of the plant pot to the left of the
                          ; skool gate
  DEFB 135                ; x-coordinate of the plant pot to the right of the
                          ; skool gate

; Skool graphic data (tiles 0-223, base page 152, byte 8/8)
;
; Used by the routine at PRINTTILE. Tiles 175-184 are unused; they would be
; used by the left study door when it's open (see URTLSDOPEN), but that door is
; always shut.
  DEFB 64,224,224,96,96,192,196,0,32,0,4,0,33,0,252,0
  DEFB 159,129,129,177,165,165,173,129,255,129,193,1,7,255,7,63
  DEFB 255,127,255,255,128,251,224,31,127,255,255,4,254,0,252,126
  DEFB 198,134,140,140,255,0,128,63,63,224,192,1,130,130,226,0
  DEFB 0,0,4,0,128,0,1,8,254,175,168,255,243,131,135,140
  DEFB 142,140,255,255,191,255,255,1,255,255,181,170,255,255,255,63
  DEFB 32,255,4,160,0,255,126,227,255,252,4,255,192,174,4,6
  DEFB 127,255,191,160,255,0,64,81,252,255,245,21,255,9,9,0
  DEFB 4,150,160,224,135,5,1,255,65,0,149,255,130,144,136,248
  DEFB 80,65,241,255,241,1,65,65,65,225,33,1,255,9,5,25
  DEFB 1,255,50,67,122,121,21,0,143,255,52,1,180,255,255,192
  DEFB 171,173,64,128,0,80,128,80,64,255,255,192,208,1,192,0
  DEFB 72,0,68,66,208,6,0,255,254,255,192,37,47,255,129,224
  DEFB 192,37,192,37,255,253,245,15,126,64,100,68,0,0,0,146

; Captured mouse UDG
;
; Used by the routine at CATCHMORF.
MOUSEUDG:
  DEFB 0,0,34,35,70,95,60,40

; Single stinkbomb UDG
;
; Used by the routine at PRINTINV.
STINKB1UDG:
  DEFB 0,0,16,40,16,0,0,0

; Skool graphic data (tiles 240-249, base page 152, byte 8/8)
;
; Used by the routine at PRINTTILE.
  DEFB 73,45,32,146,73,45,63,255,255,224

; Unused
  DEFS 2

; y-coordinates of the plant pots
;
; Used by the routine at CHECKPOT. The x-coordinates of the plant pots can be
; found at POTSX.
POTSY:
  DEFB 2                  ; y-coordinate of the plant pot near the top-floor
                          ; window
  DEFB 9                  ; y-coordinate of the plant pot near the middle-floor
                          ; window
  DEFB 16                 ; y-coordinate of the plant pot to the  left of the
                          ; skool gate
  DEFB 16                 ; y-coordinate of the plant pot to the right of the
                          ; skool gate

; Skool UDG references (LSBs) for row 0
;
; Used by the routine at PRINTTILE.
  DEFB 82,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,170,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,249,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,80,80,80,80,80,80,80,99,80,80
  DEFB 80,80,80,133,80,80,80,80,80,80,80,80,80,80,210,80
  DEFB 80,80,80,80,80,80,80,46,59,81,81,95,107,120,136,155
  DEFB 173,191,81,81,81,81,81,81,81,81,81,81,81,81,81,81
  DEFB 81,81,81,81,81,16,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,55,80,80,80,76,80,80,80,80,80,80,148

; Skool UDG MSB indicators for row 0
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4
  DEFB 1,0,0,2,0,16,144,240,192,0,0,0,0,68,0,0
  DEFB 0,136,136,17

; BRIGHT/PAPER attributes for skool UDGs in row 0
;
; Used by the routine at PRINTTILE.
  DEFB 86,102,102,102,102,102,85,85,85,119,119,119,119,119,119,119
  DEFB 119,119,119,102,119,117,85,85,85,102,102,102,102,102,111,119
  DEFB 119,119,255,244,79,116,68,70,119,102,126,238,84,68,85,85
  DEFB 85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85
  DEFB 85,85,85,102,238,238,238,236

; Entry 0 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E0:
  DEFB 69                 ; Entry 0 in descent table 252
DT253E0:
  DEFB 160                ; Entry 0 in descent table 253
DT254E0:
  DEFB 160                ; Entry 0 in descent table 254
DT255E0:
  DEFB 96                 ; Entry 0 in descent table 255

; Skool UDG references (LSBs) for row 1
;
; Used by the routine at PRINTTILE.
  DEFB 81,82,80,80,80,80,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,171,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,250,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,80,80,80,80,80,80,80,100,80,80
  DEFB 80,80,127,134,80,80,80,80,80,80,80,80,80,202,87,80
  DEFB 80,80,80,80,80,22,31,47,60,74,89,96,108,121,137,156
  DEFB 174,192,203,81,81,81,81,81,81,81,81,81,81,81,81,81
  DEFB 81,81,81,81,81,17,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,46,56,80,80,80,77,80,80,80,80,80,80,149

; Skool UDG MSB indicators for row 1
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4
  DEFB 3,0,0,4,0,112,240,240,224,0,0,0,0,68,0,0
  DEFB 17,136,136,17

; BRIGHT/PAPER attributes for skool UDGs in row 1
;
; Used by the routine at PRINTTILE.
  DEFB 85,103,119,119,119,102,86,102,85,85,103,119,116,68,68,68
  DEFB 68,68,119,102,101,85,86,102,117,102,119,119,119,118,110,119
  DEFB 119,119,114,36,68,68,68,70,102,102,102,238,84,68,68,85
  DEFB 85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85
  DEFB 85,85,85,118,231,119,118,108

; Entry 1 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E1:
  DEFB 69                 ; Entry 1 in descent table 252
DT253E1:
  DEFB 160                ; Entry 1 in descent table 253
DT254E1:
  DEFB 160                ; Entry 1 in descent table 254
DT255E1:
  DEFB 96                 ; Entry 1 in descent table 255

; Skool UDG references (LSBs) for row 2
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,81,81,81,81,81,81,81,81,81,81,128,135,81
  DEFB 128,135,81,128,135,162,172,81,81,81,81,81,81,81,81,128
  DEFB 135,81,128,135,81,128,251,81,128,135,81,128,135,81,128,135
  DEFB 81,81,81,81,81,81,81,81,81,81,81,81,81,101,81,81
  DEFB 81,81,128,135,140,145,148,148,163,174,81,161,191,118,211,103
  DEFB 81,81,81,85,81,23,32,48,61,75,81,97,109,122,138,157
  DEFB 175,193,204,215,81,81,81,81,81,81,81,81,81,81,81,81
  DEFB 81,81,81,81,81,216,81,81,81,81,81,81,81,81,81,81
  DEFB 81,81,81,128,57,81,81,81,78,81,81,81,81,125,136,150

; Skool UDG MSB indicators for row 2
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4
  DEFB 3,15,29,14,0,112,208,240,240,0,0,0,0,0,0,0
  DEFB 1,136,136,119

; BRIGHT/PAPER attributes for skool UDGs in row 2
;
; Used by the routine at PRINTTILE.
  DEFB 85,85,85,85,85,85,93,213,221,93,213,116,68,68,68,77
  DEFB 212,221,77,214,221,109,214,221,102,102,102,102,102,102,110,119
  DEFB 119,119,4,68,64,68,4,70,102,102,102,228,68,68,68,69
  DEFB 85,85,85,85,85,85,85,85,85,85,85,85,85,85,85,85
  DEFB 85,85,102,102,230,102,102,108

; Entry 2 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E2:
  DEFB 69                 ; Entry 2 in descent table 252
DT253E2:
  DEFB 160                ; Entry 2 in descent table 253
DT254E2:
  DEFB 2                  ; Entry 2 in descent table 254
DT255E2:
  DEFB 96                 ; Entry 2 in descent table 255

; Skool UDG references (LSBs) for row 3
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,0,1,2,3,4,5,6,7,81,81,81,81,81
  DEFB 81,81,81,81,81,81,173,180,181,188,189,192,196,205,210,81
  DEFB 81,220,224,232,238,243,252,16,17,18,19,20,21,22,23,81
  DEFB 81,81,81,81,81,81,81,81,81,81,81,81,81,102,81,81
  DEFB 124,243,129,136,141,146,149,155,164,81,81,81,81,118,212,103
  DEFB 81,236,249,6,14,24,33,49,62,76,90,98,110,123,139,158
  DEFB 176,194,205,216,223,81,81,81,81,234,81,242,81,81,81,81
  DEFB 81,81,81,81,81,216,32,33,34,35,36,37,38,39,81,81
  DEFB 81,81,81,128,58,165,165,103,79,89,98,108,81,126,137,151

; Skool UDG MSB indicators for row 3
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4
  DEFB 11,15,8,6,22,240,240,240,240,128,80,0,0,0,0,0
  DEFB 1,136,255,119

; BRIGHT/PAPER attributes for skool UDGs in row 3
;
; Used by the routine at PRINTTILE.
  DEFB 85,95,255,255,255,245,85,85,85,85,85,119,119,119,119,116
  DEFB 71,119,119,111,255,255,255,246,102,102,102,102,102,102,110,119
  DEFB 102,103,68,221,68,68,68,102,102,102,102,68,68,68,68,68
  DEFB 85,69,85,85,85,85,85,85,85,85,85,255,255,255,255,85
  DEFB 85,85,109,214,230,102,102,236

; Entry 3 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E3:
  DEFB 69                 ; Entry 3 in descent table 252
DT253E3:
  DEFB 96                 ; Entry 3 in descent table 253
DT254E3:
  DEFB 64                 ; Entry 3 in descent table 254
DT255E3:
  DEFB 96                 ; Entry 3 in descent table 255

; Skool UDG references (LSBs) for row 4
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,8,9,10,11,12,13,14,15,81,81,81,81,81
  DEFB 81,81,81,81,81,81,174,181,185,188,182,193,197,206,211,81
  DEFB 81,221,225,232,239,238,252,24,25,26,27,28,29,30,31,81
  DEFB 81,81,81,81,81,81,81,81,81,81,81,81,81,102,81,81
  DEFB 125,238,130,137,142,147,150,156,81,175,180,184,81,203,213,218
  DEFB 225,237,250,7,15,25,34,50,63,81,81,99,111,124,140,159
  DEFB 177,195,206,217,224,81,81,81,81,235,81,152,245,81,81,81
  DEFB 81,81,81,81,81,216,40,41,42,43,44,45,46,47,81,81
  DEFB 81,81,81,128,58,165,165,103,80,90,225,109,117,127,138,152

; Skool UDG MSB indicators for row 4
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4
  DEFB 11,15,7,7,30,240,144,240,240,128,65,128,0,0,0,0
  DEFB 1,136,221,255

; BRIGHT/PAPER attributes for skool UDGs in row 4
;
; Used by the routine at PRINTTILE.
  DEFB 85,95,255,255,255,245,85,85,85,85,85,119,119,119,119,116
  DEFB 71,119,119,111,255,255,255,246,102,102,102,102,102,102,110,119
  DEFB 102,103,244,221,68,68,76,102,102,102,102,68,68,68,68,68
  DEFB 85,84,85,85,85,85,85,85,85,85,85,255,255,255,255,85
  DEFB 85,85,109,214,230,102,102,236

; Entry 4 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E4:
  DEFB 69                 ; Entry 4 in descent table 252
DT253E4:
  DEFB 100                ; Entry 4 in descent table 253
DT254E4:
  DEFB 64                 ; Entry 4 in descent table 254
DT255E4:
  DEFB 64                 ; Entry 4 in descent table 255

; Skool UDG references (LSBs) for row 5
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,80,80,80,80,80,80,80,121,123,125,125,123,125
  DEFB 123,125,123,125,123,163,175,182,181,182,182,194,198,207,212,81
  DEFB 81,222,225,225,238,225,253,80,80,80,80,80,80,80,80,35
  DEFB 123,125,123,125,123,125,123,125,123,125,123,163,81,102,81,81
  DEFB 126,225,131,138,143,81,151,156,165,176,181,185,192,204,214,219
  DEFB 226,238,251,8,16,26,35,87,64,77,91,100,81,125,141,160
  DEFB 178,81,207,218,225,228,81,81,81,236,81,152,103,81,81,81
  DEFB 81,81,81,81,81,216,80,80,80,80,80,80,37,38,125,123
  DEFB 123,125,123,47,58,165,165,103,80,91,99,110,118,128,139,153

; Skool UDG MSB indicators for row 5
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,4
  DEFB 11,11,15,15,30,224,240,112,176,192,65,0,0,0,0,204
  DEFB 17,136,255,255

; BRIGHT/PAPER attributes for skool UDGs in row 5
;
; Used by the routine at PRINTTILE.
  DEFB 85,95,255,255,255,253,221,221,221,221,221,127,255,255,255,244
  DEFB 207,255,255,111,255,255,255,254,238,238,238,238,238,238,238,255
  DEFB 238,231,244,221,204,204,204,110,238,238,238,238,68,68,68,69
  DEFB 85,84,85,85,85,85,85,85,85,85,85,255,255,255,245,221
  DEFB 221,221,109,222,230,238,238,236

; Entry 5 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E5:
  DEFB 69                 ; Entry 5 in descent table 252
DT253E5:
  DEFB 37                 ; Entry 5 in descent table 253
DT254E5:
  DEFB 64                 ; Entry 5 in descent table 254
DT255E5:
  DEFB 64                 ; Entry 5 in descent table 255

; Skool UDG references (LSBs) for row 6
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,81,81,81,81,81,81,81,122,124,126,126,124,126
  DEFB 124,126,124,126,124,164,176,84,84,84,84,84,84,84,213,81
  DEFB 81,223,84,84,84,84,254,81,81,81,81,81,81,81,81,36
  DEFB 124,126,124,126,124,126,124,126,124,126,124,84,81,102,81,81
  DEFB 223,84,132,139,144,81,152,157,166,177,108,186,193,205,214,220
  DEFB 227,239,252,9,17,27,36,87,65,78,92,101,112,126,142,161
  DEFB 179,196,208,219,226,229,81,81,81,236,81,152,103,81,81,81
  DEFB 81,81,81,81,81,216,81,81,81,81,81,81,36,124,126,124
  DEFB 124,126,124,84,58,84,84,178,81,84,100,111,119,129,140,154

; Skool UDG MSB indicators for row 6
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,1,0,0,1,4
  DEFB 3,11,15,15,30,224,240,240,240,192,65,0,0,0,0,8
  DEFB 1,136,187,255

; BRIGHT/PAPER attributes for skool UDGs in row 6
;
; Used by the routine at PRINTTILE.
  DEFB 85,85,93,221,221,221,221,221,221,221,221,127,255,255,255,244
  DEFB 207,255,255,102,238,238,238,238,238,238,238,238,238,238,238,255
  DEFB 238,231,244,204,204,68,204,110,238,238,238,238,68,68,68,69
  DEFB 85,68,69,85,85,85,85,85,85,85,85,85,93,221,221,221
  DEFB 221,221,102,238,230,238,238,236

; Entry 6 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E6:
  DEFB 69                 ; Entry 6 in descent table 252
DT253E6:
  DEFB 69                 ; Entry 6 in descent table 253
DT254E6:
  DEFB 22                 ; Entry 6 in descent table 254
DT255E6:
  DEFB 64                 ; Entry 6 in descent table 255

; Skool UDG references (LSBs) for row 7
;
; Used by the routine at PRINTTILE.
  DEFB 83,88,80,80,80,80,80,80,80,80,80,80,80,129,80,80
  DEFB 80,80,80,80,80,80,177,183,183,183,190,195,199,80,80,80
  DEFB 80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,48,80,80,80,80,80,80,80,80,103,80,80
  DEFB 80,80,80,80,80,80,80,153,167,80,80,80,80,80,215,221
  DEFB 228,240,253,253,18,28,80,51,66,79,93,102,113,127,143,162
  DEFB 180,197,209,220,227,230,81,81,81,236,81,152,103,81,81,81
  DEFB 81,81,81,81,81,18,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,48,59,64,64,70,82,80,80,80,80,80,80,155

; Skool UDG MSB indicators for row 7
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,8,0,4
  DEFB 0,1,8,3,15,208,240,240,240,192,65,0,0,68,0,0
  DEFB 17,255,136,17

; BRIGHT/PAPER attributes for skool UDGs in row 7
;
; Used by the routine at PRINTTILE.
  DEFB 119,119,119,119,119,127,255,103,119,119,119,127,255,102,102,103
  DEFB 119,119,119,119,119,119,119,119,119,119,119,119,119,119,127,119
  DEFB 119,103,119,126,226,119,196,102,238,238,238,102,102,68,68,68
  DEFB 85,69,85,85,85,85,85,85,85,85,86,102,102,102,102,102
  DEFB 102,102,102,238,238,238,238,238

; Entry 7 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E7:
  DEFB 0                  ; Entry 7 in descent table 252
DT253E7:
  DEFB 69                 ; Entry 7 in descent table 253
DT254E7:
  DEFB 0                  ; Entry 7 in descent table 254
DT255E7:
  DEFB 64                 ; Entry 7 in descent table 255

; Skool UDG references (LSBs) for row 8
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,81,81,81,81,81,81,81,81,81,81,103,103,81
  DEFB 81,81,81,81,81,81,81,81,81,165,178,81,200,81,81,81
  DEFB 81,81,81,81,81,81,81,81,81,81,81,81,23,27,81,100
  DEFB 81,81,81,81,49,81,81,81,35,73,79,85,90,104,80,80
  DEFB 80,80,80,80,80,80,153,158,168,80,80,80,194,206,81,168
  DEFB 87,81,81,81,81,81,81,52,67,80,81,103,114,128,144,163
  DEFB 181,198,210,221,140,81,81,81,81,236,81,152,103,81,81,81
  DEFB 81,81,81,81,81,19,81,81,81,81,81,81,81,81,81,81
  DEFB 81,81,81,49,140,89,81,81,81,81,81,81,81,81,87,87

; Skool UDG MSB indicators for row 8
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,12,0,8,15,12
  DEFB 0,3,8,13,0,16,208,240,240,0,65,0,0,68,0,0
  DEFB 17,0,0,0

; BRIGHT/PAPER attributes for skool UDGs in row 8
;
; Used by the routine at PRINTTILE.
  DEFB 119,255,255,255,255,255,255,102,102,102,102,102,102,102,102,102
  DEFB 102,102,102,102,102,102,102,102,102,102,117,85,85,85,95,119
  DEFB 119,103,119,238,231,119,102,102,102,102,102,102,110,85,84,68
  DEFB 68,85,85,85,85,85,85,85,85,85,86,102,102,102,102,102
  DEFB 102,110,102,110,102,102,102,110

; Entry 8 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E8:
  DEFB 4                  ; Entry 8 in descent table 252
DT253E8:
  DEFB 0                  ; Entry 8 in descent table 253
DT254E8:
  DEFB 4                  ; Entry 8 in descent table 254
DT255E8:
  DEFB 64                 ; Entry 8 in descent table 255

; Skool UDG references (LSBs) for row 9
;
; Used by the routine at PRINTTILE.
  DEFB 84,84,89,94,100,81,109,114,109,114,109,114,109,130,103,137
  DEFB 141,145,149,153,81,81,81,81,165,178,81,81,201,81,48,49
  DEFB 50,51,52,53,54,55,81,81,81,14,17,20,24,28,32,37
  DEFB 81,81,81,81,50,55,61,65,69,74,80,86,91,104,113,119
  DEFB 113,119,113,119,113,119,128,159,168,178,182,187,195,81,81,168
  DEFB 229,241,81,81,81,81,37,53,68,81,81,104,115,129,145,164
  DEFB 80,199,211,222,81,81,81,81,81,236,81,152,103,81,81,81
  DEFB 81,81,81,81,81,20,64,65,66,67,68,69,70,71,81,81
  DEFB 81,81,81,50,81,140,89,81,81,81,81,81,165,165,216,87

; Skool UDG MSB indicators for row 9
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,7,15,0,15,15,15
  DEFB 15,15,15,9,12,48,208,240,112,0,65,0,0,68,0,0
  DEFB 17,0,0,0

; BRIGHT/PAPER attributes for skool UDGs in row 9
;
; Used by the routine at PRINTTILE.
  DEFB 255,255,255,255,255,255,255,111,255,255,102,102,238,238,102,255
  DEFB 255,255,255,102,102,102,102,102,102,102,117,85,85,85,95,255
  DEFB 255,255,255,254,231,126,102,238,102,102,102,102,69,85,69,84
  DEFB 68,85,85,85,85,85,85,85,85,85,85,255,255,255,255,102
  DEFB 102,110,238,230,102,102,221,110

; Entry 9 in descent tables 252-255
;
; Used by the routine at FALLING.
DT252E9:
  DEFB 0                  ; Entry 9 in descent table 252
DT253E9:
  DEFB 4                  ; Entry 9 in descent table 253
DT254E9:
  DEFB 0                  ; Entry 9 in descent table 254
DT255E9:
  DEFB 64                 ; Entry 9 in descent table 255

; Skool UDG references (LSBs) for row 10
;
; Used by the routine at PRINTTILE.
  DEFB 81,81,87,95,101,106,110,115,119,115,119,115,119,131,103,138
  DEFB 142,146,150,154,81,81,81,165,178,81,81,81,201,81,56,57
  DEFB 58,59,60,61,62,63,255,7,12,15,18,21,25,29,33,38
  DEFB 41,43,43,46,51,56,62,66,70,75,81,87,92,104,114,120
  DEFB 114,120,114,120,114,120,128,80,168,178,182,118,195,81,81,168
  DEFB 230,242,89,81,81,81,38,54,69,82,81,105,116,81,146,80
  DEFB 182,200,212,81,81,81,81,81,81,236,81,152,103,81,81,81
  DEFB 81,81,81,81,81,21,72,73,74,75,76,77,78,79,81,81
  DEFB 81,81,81,51,81,81,140,89,81,81,81,81,165,165,216,87

; Skool UDG MSB indicators for row 10
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,1,15,15,15,15,15,15
  DEFB 15,14,15,9,12,48,208,160,224,0,65,0,0,68,0,0
  DEFB 17,0,0,0

; BRIGHT/PAPER attributes for skool UDGs in row 10
;
; Used by the routine at PRINTTILE.
  DEFB 255,247,255,255,255,255,255,111,255,255,102,110,238,238,102,255
  DEFB 255,255,255,101,86,101,100,102,102,102,117,85,85,85,95,255
  DEFB 255,255,255,254,231,238,102,238,102,230,102,110,85,85,85,85
  DEFB 69,85,85,85,85,85,85,85,85,85,85,255,255,255,255,102
  DEFB 102,110,238,238,102,102,221,110

; Unused
  DEFB 80

; Entry 10 in descent table 253
;
; Used by the routine at FALLING.
DT253E10:
  DEFB 0                  ; Entry 10 in descent table 253

; Unused
  DEFB 77

; Entry 10 in descent table 255
;
; Used by the routine at FALLING.
DT255E10:
  DEFB 68                 ; Entry 10 in descent table 255

; Skool UDG references (LSBs) for row 11
;
; Used by the routine at PRINTTILE.
  DEFB 81,81,87,95,102,107,111,116,120,116,120,116,120,131,103,81
  DEFB 81,81,81,81,81,158,165,178,81,81,81,81,202,187,214,215
  DEFB 80,80,80,80,80,80,0,8,13,16,19,22,26,30,34,39
  DEFB 42,44,45,167,52,57,63,67,71,76,82,88,63,105,115,121
  DEFB 115,121,115,121,115,121,128,80,168,178,182,118,196,109,216,222
  DEFB 231,243,254,89,81,81,81,177,70,83,81,81,81,130,78,165
  DEFB 183,81,81,81,81,81,81,81,81,237,81,152,246,81,81,81
  DEFB 81,81,81,81,81,22,80,80,80,80,80,80,80,80,41,37
  DEFB 37,41,43,51,81,81,81,140,89,81,81,81,84,84,27,87

; Skool UDG MSB indicators for row 11
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,3,15,15,14,15,15,15
  DEFB 15,14,15,11,14,1,192,82,128,0,65,128,0,68,0,48
  DEFB 243,0,0,34

; BRIGHT/PAPER attributes for skool UDGs in row 11
;
; Used by the routine at PRINTTILE.
  DEFB 255,247,255,255,255,255,255,238,238,238,238,238,238,238,127,255
  DEFB 255,255,255,255,255,255,255,255,255,255,117,238,238,238,239,255
  DEFB 255,255,255,254,231,238,102,238,102,238,102,102,85,85,85,84
  DEFB 85,85,85,85,85,85,85,85,85,85,85,255,255,255,255,238
  DEFB 238,238,238,238,230,110,238,238

; Unused
  DEFB 22,100,56

; Entry 11 in descent table 255
;
; Used by the routine at FALLING.
DT255E11:
  DEFB 68                 ; Entry 11 in descent table 255

; Skool UDG references (LSBs) for row 12
;
; Used by the routine at PRINTTILE.
  DEFB 81,81,87,95,103,81,112,117,112,117,112,117,112,131,103,81
  DEFB 81,81,81,81,158,165,178,81,81,81,81,81,201,81,81,81
  DEFB 103,81,81,81,81,244,1,9,1,9,1,9,1,9,1,9
  DEFB 1,9,1,47,53,58,64,68,72,77,208,89,93,106,116,122
  DEFB 116,122,116,122,116,122,128,80,168,178,182,118,197,207,81,104
  DEFB 232,244,255,10,19,81,81,177,140,84,81,81,81,131,147,166
  DEFB 184,81,81,81,81,81,81,81,81,236,81,152,103,81,249,81
  DEFB 81,81,81,81,81,23,81,81,81,81,81,244,1,39,1,9
  DEFB 9,1,9,51,81,81,81,81,140,89,81,81,81,81,87,87

; Skool UDG MSB indicators for row 12
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,3,15,15,15,15,13,15
  DEFB 15,14,15,13,30,129,64,112,128,0,65,32,0,68,0,79
  DEFB 31,0,0,0

; BRIGHT/PAPER attributes for skool UDGs in row 12
;
; Used by the routine at PRINTTILE.
  DEFB 255,247,255,255,255,255,255,238,238,238,238,238,238,238,127,255
  DEFB 110,238,238,255,255,255,255,255,255,255,117,238,238,238,239,255
  DEFB 255,255,255,254,231,238,102,102,110,238,238,238,238,85,85,68
  DEFB 85,85,85,85,85,85,85,85,85,85,86,102,110,238,238,238
  DEFB 238,238,238,238,238,102,238,238

; Unused
  DEFB 83,100,56

; Entry 12 in descent table 255
;
; Used by the routine at FALLING.
DT255E12:
  DEFB 68                 ; Entry 12 in descent table 255

; Skool UDG references (LSBs) for row 13
;
; Used by the routine at PRINTTILE.
  DEFB 85,85,90,96,80,80,113,118,113,118,113,118,113,132,136,139
  DEFB 143,143,143,155,159,166,143,143,143,143,143,143,203,208,208,208
  DEFB 80,80,80,80,80,245,2,10,2,10,2,10,2,10,2,40
  DEFB 2,40,2,40,54,59,208,208,208,78,80,59,208,107,117,123
  DEFB 117,123,117,123,117,123,128,80,168,178,182,118,197,207,81,223
  DEFB 233,245,0,81,20,29,39,55,36,87,81,81,81,81,148,167
  DEFB 185,81,81,81,81,81,117,232,233,238,240,243,247,240,250,255
  DEFB 6,8,10,12,14,24,80,80,80,80,80,245,2,10,2,10
  DEFB 40,2,40,52,60,65,65,71,83,92,101,80,80,80,80,156

; Skool UDG MSB indicators for row 13
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,3,15,15,15,12,5,7
  DEFB 15,14,15,13,44,240,128,48,128,48,240,240,255,204,0,15
  DEFB 31,255,238,17

; BRIGHT/PAPER attributes for skool UDGs in row 13
;
; Used by the routine at PRINTTILE.
  DEFB 255,247,255,255,255,255,255,238,238,238,238,238,238,238,127,255
  DEFB 255,255,255,255,255,255,255,255,255,255,117,238,238,238,239,255
  DEFB 255,255,255,254,231,238,102,102,110,238,238,238,238,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,70,238,238,238,238,238
  DEFB 238,238,254,238,238,230,102,110

; Unused
  DEFB 93,240,248

; Entry 13 in descent table 255
;
; Used by the routine at FALLING.
DT255E13:
  DEFB 5                  ; Entry 13 in descent table 255

; Skool UDG references (LSBs) for row 14
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,81,81,81,81,81,81,81,81,81,81,87,81,87
  DEFB 144,147,81,81,81,81,81,81,186,81,191,81,186,81,81,81
  DEFB 216,81,226,208,81,81,81,81,81,81,81,81,81,81,81,81
  DEFB 81,81,81,81,81,81,81,81,81,81,81,81,103,108,118,112
  DEFB 118,112,118,112,118,112,128,80,168,178,182,118,197,207,217,224
  DEFB 234,246,40,40,40,40,40,56,71,85,81,81,81,81,149,80
  DEFB 103,81,81,81,81,81,231,180,180,239,241,244,180,248,251,0
  DEFB 7,9,11,13,15,25,81,128,81,81,81,81,81,81,81,81
  DEFB 81,81,81,81,61,81,66,209,103,103,102,81,81,130,141,157

; Skool UDG MSB indicators for row 14
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6
  DEFB 10,10,15,15,60,240,192,32,0,33,120,121,255,205,0,0
  DEFB 0,171,34,119

; BRIGHT/PAPER attributes for skool UDGs in row 14
;
; Used by the routine at PRINTTILE.
  DEFB 68,68,68,68,68,68,68,68,119,119,119,119,119,119,119,255
  DEFB 119,119,119,119,119,119,119,119,119,119,119,119,119,119,255,255
  DEFB 255,255,255,254,231,238,102,102,238,102,102,102,110,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,71,119,119,119,119,119
  DEFB 119,119,254,238,231,119,127,255

; Unused
  DEFB 95,112,248

; Entry 14 in descent table 255
;
; Used by the routine at FALLING.
DT255E14:
  DEFB 69                 ; Entry 14 in descent table 255

; Skool UDG references (LSBs) for row 15
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,81,81,81,81,81,81,81,81,81,81,81,87,81,140
  DEFB 89,148,151,81,81,167,179,184,187,187,187,187,204,209,81,81
  DEFB 216,81,227,233,81,81,81,81,81,81,81,81,81,81,81,81
  DEFB 81,81,81,81,81,81,81,81,81,81,81,81,103,109,118,112
  DEFB 118,112,118,112,118,112,128,80,169,178,182,118,197,208,115,165
  DEFB 103,152,85,85,85,85,41,57,72,87,81,81,81,81,150,168
  DEFB 186,81,81,81,81,81,81,81,81,81,81,81,81,81,252,1
  DEFB 81,81,81,81,81,26,81,128,81,81,81,81,81,81,81,81
  DEFB 81,81,81,81,61,66,69,165,84,93,103,112,120,131,142,158

; Skool UDG MSB indicators for row 15
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,6
  DEFB 10,10,15,12,4,48,128,48,128,0,0,49,0,69,0,0
  DEFB 0,238,255,255

; BRIGHT/PAPER attributes for skool UDGs in row 15
;
; Used by the routine at PRINTTILE.
  DEFB 68,68,68,68,68,68,68,68,71,119,119,119,119,119,119,255
  DEFB 119,119,119,119,119,119,119,119,119,119,119,119,119,119,255,255
  DEFB 255,255,255,254,231,238,102,102,238,102,102,102,110,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,64,119,119,119,119,119
  DEFB 119,119,254,238,119,119,127,255

; Unused
  DEFB 132,240,248

; Entry 15 in descent table 255
;
; Used by the routine at FALLING.
DT255E15:
  DEFB 69                 ; Entry 15 in descent table 255

; Skool UDG references (LSBs) for row 16
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,91,97,104,108,108,108,108,108,108,108,127,133,81,81
  DEFB 140,89,152,151,81,81,81,81,81,81,81,81,81,81,81,81
  DEFB 217,80,228,234,240,195,3,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,80,80,80,80,80,80,94,131,118,112
  DEFB 118,112,118,112,118,112,128,160,170,179,183,188,198,209,165,178
  DEFB 103,247,2,85,85,85,42,57,152,86,81,81,81,87,151,169
  DEFB 187,81,81,81,81,81,81,81,81,81,81,81,81,81,253,2
  DEFB 81,81,81,81,81,27,81,33,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,62,67,165,72,85,94,104,113,121,132,143,83

; Skool UDG MSB indicators for row 16
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,10
  DEFB 10,11,15,12,36,48,72,48,128,0,0,49,0,85,0,0
  DEFB 0,221,255,254

; BRIGHT/PAPER attributes for skool UDGs in row 16
;
; Used by the routine at PRINTTILE.
  DEFB 68,68,68,68,68,68,68,204,204,119,119,119,119,119,119,255
  DEFB 119,119,255,247,119,119,119,119,119,119,119,255,255,255,255,255
  DEFB 255,255,255,254,238,238,102,102,230,102,102,102,236,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,68,119,119,119,119,119
  DEFB 119,119,254,231,119,119,127,255

; Unused
  DEFB 135,112,248

; Entry 16 in descent table 255
;
; Used by the routine at FALLING.
DT255E16:
  DEFB 69                 ; Entry 16 in descent table 255

; Skool UDG references (LSBs) for row 17
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,92,98,105,81,81,81,81,81,81,81,81,87,81,81
  DEFB 81,140,89,156,160,81,81,81,81,81,81,81,81,81,81,81
  DEFB 218,80,229,235,178,81,4,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,80,80,80,80,80,80,95,110,80,80
  DEFB 80,80,80,80,80,80,154,161,131,81,81,189,199,84,178,81
  DEFB 103,195,3,12,12,12,43,57,152,87,81,81,81,132,152,170
  DEFB 188,81,81,81,81,81,81,81,81,81,81,81,81,81,254,3
  DEFB 81,81,81,81,81,87,30,78,80,80,80,80,80,80,80,80
  DEFB 80,80,80,44,63,165,68,73,86,95,105,114,122,133,144,159

; Skool UDG MSB indicators for row 17
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,12
  DEFB 0,3,1,8,52,240,72,112,128,0,0,49,0,35,0,0
  DEFB 17,187,255,255

; BRIGHT/PAPER attributes for skool UDGs in row 17
;
; Used by the routine at PRINTTILE.
  DEFB 68,68,76,204,204,204,204,204,204,199,255,255,255,255,255,255
  DEFB 119,119,255,247,119,119,119,119,119,119,119,255,255,255,255,119
  DEFB 119,119,119,126,238,238,102,102,230,238,238,230,236,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,68,119,119,119,119,119
  DEFB 119,255,238,127,255,255,255,255

; Unused
  DEFB 176,228,56

; Entry 17 in descent table 255
;
; Used by the routine at FALLING.
DT255E17:
  DEFB 70                 ; Entry 17 in descent table 255

; Skool UDG references (LSBs) for row 18
;
; Used by the routine at PRINTTILE.
  DEFB 81,87,93,99,99,99,99,99,99,99,99,99,99,134,81,81
  DEFB 81,81,140,89,161,168,81,81,81,81,81,81,81,81,81,81
  DEFB 219,80,230,236,241,246,5,11,11,11,11,11,11,11,11,11
  DEFB 11,11,11,11,11,11,11,11,11,11,11,11,96,111,80,80
  DEFB 80,80,80,80,80,80,80,162,171,84,84,89,200,81,81,81
  DEFB 103,195,4,13,13,13,44,57,152,88,81,81,117,133,153,171
  DEFB 189,201,213,81,81,81,81,81,81,81,81,81,81,81,254,3
  DEFB 81,81,81,81,81,87,31,80,36,11,11,11,11,11,11,11
  DEFB 40,80,44,53,165,68,178,74,87,96,106,115,123,134,145,160

; Skool UDG MSB indicators for row 18
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,3,15,15,15,15,15,12
  DEFB 0,1,8,8,52,240,72,240,224,0,0,49,0,34,143,15
  DEFB 187,85,255,255

; BRIGHT/PAPER attributes for skool UDGs in row 18
;
; Used by the routine at PRINTTILE.
  DEFB 68,102,102,102,102,102,100,204,204,204,127,255,255,255,255,255
  DEFB 119,119,119,255,255,255,255,255,255,255,255,255,255,255,255,119
  DEFB 119,119,119,126,110,238,102,238,230,238,238,230,236,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,68,119,255,255,255,255
  DEFB 255,254,231,127,255,255,255,255

; Unused
  DEFB 183,100,77

; Entry 18 in descent table 255
;
; Used by the routine at FALLING.
DT255E18:
  DEFB 0                  ; Entry 18 in descent table 255

; Skool UDG references (LSBs) for row 19
;
; Used by the routine at PRINTTILE.
  DEFB 81,86,80,80,80,80,80,80,80,80,80,80,80,80,85,85
  DEFB 85,85,85,157,135,169,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,231,237,242,247,6,6,6,6,6,6,6,6,6,6
  DEFB 6,6,6,6,6,6,6,6,6,6,83,6,97,112,80,80
  DEFB 80,80,80,80,80,80,80,154,172,43,43,190,201,80,80,80
  DEFB 235,248,5,5,5,5,45,58,73,80,94,106,118,134,154,172
  DEFB 190,202,214,81,81,81,81,81,81,81,81,81,81,81,207,4
  DEFB 81,81,81,81,81,28,32,34,6,6,6,6,6,6,6,6
  DEFB 41,80,45,54,80,80,80,75,88,97,107,116,124,135,146,87

; Skool UDG MSB indicators for row 19
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,3,15,15,15,15,15,12
  DEFB 0,1,15,8,60,240,176,240,224,0,0,19,0,119,15,15
  DEFB 187,17,255,238

; BRIGHT/PAPER attributes for skool UDGs in row 19
;
; Used by the routine at PRINTTILE.
  DEFB 68,102,102,102,102,102,100,204,204,204,199,119,119,119,127,247
  DEFB 119,119,119,119,119,119,119,119,119,119,119,119,119,119,127,119
  DEFB 119,119,119,102,110,238,230,102,230,102,102,102,236,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,68,119,119,119,119,119
  DEFB 119,238,255,255,255,255,255,255

; Unused
  DEFB 186,100,56

; Entry 19 in descent table 255
;
; Used by the routine at FALLING.
DT255E19:
  DEFB 64                 ; Entry 19 in descent table 255

; Skool UDG references (LSBs) for row 20
;
; Used by the routine at PRINTTILE.
  DEFB 86,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,248,208,208,208,208,208,208,208,31,208,208
  DEFB 208,208,208,208,208,60,208,208,208,208,208,208,98,80,80,80
  DEFB 80,80,80,80,80,80,80,80,173,80,80,80,80,80,80,80
  DEFB 80,80,80,80,80,80,80,80,80,80,81,81,119,135,81,81
  DEFB 81,81,81,81,81,81,81,81,81,81,81,81,81,36,122,5
  DEFB 36,81,81,81,81,29,80,35,208,208,208,208,208,60,208,208
  DEFB 42,80,159,166,80,80,80,80,80,80,80,80,80,80,147,90

; Skool UDG MSB indicators for row 20
;
; Used by the routine at PRINTTILE.
  DEFB 0,0,0,0,0,0,0,0,0,0,0,4,0,4,0,8
  DEFB 0,0,8,0,0,0,0,192,0,0,0,81,128,85,0,4
  DEFB 136,0,0,34

; BRIGHT/PAPER attributes for skool UDGs in row 20
;
; Used by the routine at PRINTTILE.
  DEFB 71,102,102,102,102,102,100,68,68,68,71,119,119,119,127,247
  DEFB 119,119,119,119,119,119,119,119,119,119,119,119,119,119,119,119
  DEFB 119,119,119,118,102,102,102,102,102,102,102,102,102,68,68,68
  DEFB 68,68,68,68,68,68,68,68,68,68,68,119,119,119,119,119
  DEFB 126,238,255,255,255,255,255,255

; Unused
  DEFB 179,228,77

; Entry 20 in descent table 255
;
; Used by the routine at FALLING.
DT255E20:
  DEFB 32                 ; Entry 20 in descent table 255

; Play area column pointers (Q values)
;
; Used by the routine at PRINTTILE. Each byte (Q value) here corresponds to one
; of the 192 columns of the play area (x-coordinates 0-191). The Q value for an
; x-coordinate uniquely determines the appearance of the corresponding column
; of the play area; in other words, play area columns that have the same Q
; value are identical in appearance.
;
; The first 32 Q values correspond to x-coordinates 0-31. In this range,
; x-coordinates 11 and 13 have the same Q value (11), and x-coordinates 28 and
; 29 have the same Q value (27).
QVALUES:
  DEFB 0,1,2,3,4,5,6,7         ; 0<=x<=31
  DEFB 8,9,10,11,12,11,13,14   ;
  DEFB 15,16,17,18,19,20,21,22 ;
  DEFB 23,24,25,26,27,27,28,29 ;
; The next 32 Q values correspond to x-coordinates 32-63. No two x-coordinates
; in this range have the same Q value.
  DEFB 30,31,32,33,34,35,36,37 ; 32<=x<=63
  DEFB 38,39,40,41,42,43,44,45 ;
  DEFB 46,47,48,49,50,51,52,53 ;
  DEFB 54,55,56,57,58,59,60,61 ;
; The next 32 Q values correspond to x-coordinates 64-95. In this range,
; x-coordinates 64, 66 and 68 have the same Q value (62), and x-coordinates 65,
; 67 and 69 have the same Q value (63).
  DEFB 62,63,62,63,62,63,64,65 ; 64<=x<=95
  DEFB 66,67,68,69,70,71,72,73 ;
  DEFB 74,75,76,77,78,79,80,81 ;
  DEFB 82,83,84,85,86,87,88,89 ;
; The next 32 Q values correspond to x-coordinates 96-127. In this range,
; x-coordinates 110, 112-120 and 122-127 have the same Q value (104).
  DEFB 90,91,92,93,94,95,96,97         ; 96<=x<=127
  DEFB 98,99,100,101,102,103,104,105   ;
  DEFB 104,104,104,104,104,104,104,104 ;
  DEFB 104,106,104,104,104,104,104,104 ;
; The next 32 Q values correspond to x-coordinates 128-159. In this range,
; x-coordinates 128-130, 138-146 and 148-157 have the same Q value (104).
  DEFB 104,104,104,107,108,109,110,111 ; 128<=x<=159
  DEFB 112,113,104,104,104,104,104,104 ;
  DEFB 104,104,104,114,104,104,104,104 ;
  DEFB 104,104,104,104,104,104,115,116 ;
; The next 32 Q values correspond to x-coordinates 160-191. In this range,
; x-coordinates 169, 171, 173 and 175 have the same Q value (126), and
; x-coordinates 170, 172 and 174 have the same Q value (127).
  DEFB 117,118,119,120,121,122,123,124 ; 160<=x<=191
  DEFB 125,126,127,126,127,126,127,126 ;
  DEFB 128,129,130,131,132,133,134,135 ;
  DEFB 136,137,138,139,140,141,142,143 ;

; Main timetable
;
; Used by the routine at NEWLESSON1.
TIMETABLE:
  DEFB 59                 ; 192: PLAYTIME (this is an initial playtime)
  DEFB 37                 ; 193: MR CREAK - BLUE ROOM
  DEFB 52                 ; 194: ASSEMBLY
  DEFB 41                 ; 195: MR WITHIT - BLUE ROOM
  DEFB 47                 ; 196: MR ROCKITT - SCIENCE LAB
  DEFB 58                 ; 197: PLAYTIME
  DEFB 45                 ; 198: MR WITHIT - SCIENCE LAB
  DEFB 53                 ; 199: REVISION LIBRARY
  DEFB 56                 ; 200: PLAYTIME (this is an initial playtime)
  DEFB 38                 ; 201: MR CREAK - BLUE ROOM
  DEFB 54                 ; 202: REVISION LIBRARY
  DEFB 50                 ; 203: DINNER (MR WACKER)
  DEFB 55                 ; 204: PLAYTIME
  DEFB 39                 ; 205: MR CREAK - YELLOW ROOM
  DEFB 52                 ; 206: ASSEMBLY
  DEFB 46                 ; 207: MR WITHIT - SCIENCE LAB
  DEFB 59                 ; 208: PLAYTIME (this is an initial playtime)
  DEFB 40                 ; 209: MR CREAK - YELLOW ROOM
  DEFB 52                 ; 210: ASSEMBLY
  DEFB 48                 ; 211: MR ROCKITT - SCIENCE LAB
  DEFB 54                 ; 212: REVISION LIBRARY
  DEFB 57                 ; 213: PLAYTIME
  DEFB 49                 ; 214: MR ROCKITT - SCIENCE LAB
  DEFB 37                 ; 215: MR CREAK - BLUE ROOM
  DEFB 58                 ; 216: PLAYTIME (this is an initial playtime)
  DEFB 48                 ; 217: MR ROCKITT - SCIENCE LAB
  DEFB 54                 ; 218: REVISION LIBRARY
  DEFB 51                 ; 219: DINNER (MR WITHIT)
  DEFB 58                 ; 220: PLAYTIME
  DEFB 47                 ; 221: MR ROCKITT - SCIENCE LAB
  DEFB 52                 ; 222: ASSEMBLY
  DEFB 39                 ; 223: MR CREAK - YELLOW ROOM
  DEFB 59                 ; 224: PLAYTIME (this is an initial playtime)
  DEFB 53                 ; 225: REVISION LIBRARY
  DEFB 49                 ; 226: MR ROCKITT - SCIENCE LAB
  DEFB 52                 ; 227: ASSEMBLY
  DEFB 58                 ; 228: PLAYTIME
  DEFB 41                 ; 229: MR WITHIT - BLUE ROOM
  DEFB 54                 ; 230: REVISION LIBRARY
  DEFB 47                 ; 231: MR ROCKITT - SCIENCE LAB
  DEFB 55                 ; 232: PLAYTIME (this is an initial playtime)
  DEFB 48                 ; 233: MR ROCKITT - SCIENCE LAB
  DEFB 40                 ; 234: MR CREAK - YELLOW ROOM
  DEFB 54                 ; 235: REVISION LIBRARY
  DEFB 56                 ; 236: PLAYTIME
  DEFB 45                 ; 237: MR WITHIT - SCIENCE LAB
  DEFB 52                 ; 238: ASSEMBLY
  DEFB 44                 ; 239: MR WITHIT - YELLOW ROOM
  DEFB 59                 ; 240: PLAYTIME (this is an initial playtime)
  DEFB 49                 ; 241: MR ROCKITT - SCIENCE LAB
  DEFB 37                 ; 242: MR CREAK - BLUE ROOM
  DEFB 52                 ; 243: ASSEMBLY
  DEFB 59                 ; 244: PLAYTIME
  DEFB 42                 ; 245: MR WITHIT - BLUE ROOM
  DEFB 53                 ; 246: REVISION LIBRARY
  DEFB 40                 ; 247: MR CREAK - YELLOW ROOM
  DEFB 57                 ; 248: PLAYTIME (this is an initial playtime)
  DEFB 46                 ; 249: MR WITHIT - SCIENCE LAB
  DEFB 48                 ; 250: MR ROCKITT - SCIENCE LAB
  DEFB 53                 ; 251: REVISION LIBRARY
  DEFB 58                 ; 252: PLAYTIME
  DEFB 39                 ; 253: MR CREAK - YELLOW ROOM
  DEFB 52                 ; 254: ASSEMBLY
  DEFB 49                 ; 255: MR ROCKITT - SCIENCE LAB

; Mirrored values of 0 to 255
;
; Used by the routine at GETTILE to flip sprite graphic data.
  DEFB 0,128,64,192,32,160,96,224,16,144,80,208,48,176,112,240
  DEFB 8,136,72,200,40,168,104,232,24,152,88,216,56,184,120,248
  DEFB 4,132,68,196,36,164,100,228,20,148,84,212,52,180,116,244
  DEFB 12,140,76,204,44,172,108,236,28,156,92,220,60,188,124,252
  DEFB 2,130,66,194,34,162,98,226,18,146,82,210,50,178,114,242
  DEFB 10,138,74,202,42,170,106,234,26,154,90,218,58,186,122,250
  DEFB 6,134,70,198,38,166,102,230,22,150,86,214,54,182,118,246
  DEFB 14,142,78,206,46,174,110,238,30,158,94,222,62,190,126,254
  DEFB 1,129,65,193,33,161,97,225,17,145,81,209,49,177,113,241
  DEFB 9,137,73,201,41,169,105,233,25,153,89,217,57,185,121,249
  DEFB 5,133,69,197,37,165,101,229,21,149,85,213,53,181,117,245
  DEFB 13,141,77,205,45,173,109,237,29,157,93,221,61,189,125,253
  DEFB 3,131,67,195,35,163,99,227,19,147,83,211,51,179,115,243
  DEFB 11,139,75,203,43,171,107,235,27,155,91,219,59,187,123,251
  DEFB 7,135,71,199,39,167,103,231,23,151,87,215,55,183,119,247
  DEFB 15,143,79,207,47,175,111,239,31,159,95,223,63,191,127,255

; Character buffer for little girl no. 1 (183)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTGIRL1.
GIRL1CBUF:
  DEFS 32
  DEFB 72                 ; Initial animatory state: 72 (see PREPGAME)
  DEFB 117,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF183_36:
  DEFB 36                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little girl no. 1 (183)
;
; Used by the routine at NEWLESSON1.
PTGIRL1:
  DEFB 0                  ; Lesson 37 (MR CREAK - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 12                 ; Lesson 38 (MR CREAK - BLUE ROOM): 12 (Dinner hall)
  DEFB 0                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 10                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 10 (Kitchen)
  DEFB 0                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 12                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 12 (Dinner
                          ; hall)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 0                  ; Lesson 53 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 54 (REVISION LIBRARY): 2 (Middle-floor
                          ; classroom)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Skool region identifier table for the top floor
;
; Used by the routine at GETREGION. The skool region identifier table for the
; middle floor is at MIDFLOOR, and the table for the bottom floor is at
; BOTFLOOR.
TOPFLOOR:
  DEFB 72,189             ; Region 189: top floor, left of the left study door
                          ; in the boys' skool (x<=72)
  DEFB 96,194             ; Region 194: top floor, right of the left study door
                          ; in the boys' skool (72<x<=96)
  DEFB 159,191            ; Region 191: mid-air (96<x<=159)
  DEFB 224,196            ; Region 196: top floor in the girls' skool (x>159)

; Unused
  DEFS 8

; Graphic data for animatory states 0-79 (UDG byte 1/8)
;
; Used by the routine at GETTILE.
  DEFB 32,0,69,49,112,16,0,224,224,224,0,1,8,0,0,0
  DEFB 0,0,0,0,0,0,0,255,127,0,128,0,15,7,0,248
  DEFB 240,127,7,240,0,2,127,128,128,0,0,96,0,248,0,0
  DEFB 0,0,127,0,255,192,1,128,20,0,127,79,0,127,0,255
  DEFB 16,7,0,255,0,0,0,248,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,47,0,0,2,0
  DEFB 240,0,1,0,62,64,0,3,0,254,0,0,0,0,0,0
  DEFB 62,63,0,0,0,3,7,0,224,224,63,7,224,0,0,5
  DEFB 0,120,0,126,0,0,0,95,0,128,0,5,0,248,127,0
  DEFB 0,0,3,223,0,0,63,31,0,252,252,0,0,0,0,33
  DEFB 0,163,0,132,0,4,0,127,239,0,127,205,24,60,73,0
  DEFB 0,3,0,254

; Character buffer for little girl no. 2 (184)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTGIRL2.
  DEFS 32
  DEFB 200                ; Initial animatory state: 200 (see PREPGAME)
  DEFB 114,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF184_36:
  DEFB 36                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little girl no. 2 (184)
;
; Used by the routine at NEWLESSON1.
PTGIRL2:
  DEFB 0                  ; Lesson 37 (MR CREAK - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 12                 ; Lesson 38 (MR CREAK - BLUE ROOM): 12 (Dinner hall)
  DEFB 0                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 10                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 10 (Kitchen)
  DEFB 0                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 12                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 12 (Dinner
                          ; hall)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 2                  ; Lesson 53 (REVISION LIBRARY): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 54 (REVISION LIBRARY): 2 (Middle-floor
                          ; classroom)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Skool region identifier table for the middle floor
;
; Used by the routine at GETREGION. The skool region identifier table for the
; top floor is at TOPFLOOR, and the table for the bottom floor is at BOTFLOOR.
MIDFLOOR:
  DEFB 63,190             ; Region 190: middle floor, left of the far wall of
                          ; the Science Lab storeroom (x<=63)
  DEFB 76,191             ; Region 191: mid-air (63<x<=76)
  DEFB 96,193             ; Region 193: near the middle-floor window in the
                          ; boys' skool (76<x<=96)
  DEFB 159,191            ; Region 191: mid-air (96<x<=159)
  DEFB 224,195            ; Region 195: middle floor in the girls' skool
                          ; (x>159)

; Unused
  DEFS 6

; Graphic data for animatory states 0-79 (UDG mask byte 1/8)
;
; Used by the routine at GETTILE.
  DEFB 47,255,69,53,116,215,254,255,255,255,255,255,235,243,255,255
  DEFB 255,223,255,255,255,254,7,255,127,255,191,240,239,199,127,251
  DEFB 247,127,231,247,255,250,127,191,191,255,255,110,255,251,255,255
  DEFB 253,255,127,7,255,223,249,191,212,255,127,79,255,127,7,255
  DEFB 215,247,195,255,255,253,63,251,255,255,0,135,248,127,255,255
  DEFB 255,135,131,248,63,0,255,253,131,255,255,175,127,255,250,255
  DEFB 247,255,253,255,190,95,255,251,135,254,255,255,255,239,249,255
  DEFB 190,191,255,127,255,251,247,255,239,239,191,247,239,255,255,245
  DEFB 255,123,251,126,255,255,195,95,255,191,252,245,63,251,127,255
  DEFB 195,255,251,223,255,224,191,159,15,253,253,255,255,255,255,161
  DEFB 255,171,255,133,255,228,255,127,239,127,127,205,219,189,73,255
  DEFB 252,115,3,254

; Character buffer for little girl no. 3 (185)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTGIRL3.
  DEFS 32
  DEFB 72                 ; Initial animatory state: 72 (see PREPGAME)
  DEFB 120,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF185_36:
  DEFB 36                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little girl no. 3 (185)
;
; Used by the routine at NEWLESSON1.
PTGIRL3:
  DEFB 0                  ; Lesson 37 (MR CREAK - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 38 (MR CREAK - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 10                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 10 (Kitchen)
  DEFB 12                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 12 (Dinner hall)
  DEFB 2                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 10                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 10 (Kitchen)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 0                  ; Lesson 53 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 54 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Skool region identifier table for the bottom floor
;
; Used by the routine at GETREGION. The skool region identifier table for the
; top floor is at TOPFLOOR, and the table for the middle floor is at MIDFLOOR.
BOTFLOOR:
  DEFB 224,191            ; Region 191: Anywhere on the bottom floor

; Locations of doors
;
; Used by the routines at CHKDOOR and SHUTDOORS.
DOORLOCS:
  DEFB 72,3               ; Left study door
  DEFB 83,3               ; Right study door
  DEFB 54,10              ; Science Lab storeroom door
  DEFB 94,17              ; Boys' skool door
  DEFB 133,17             ; Skool gate

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG byte 2/8)
;
; Used by the routine at GETTILE.
  DEFB 32,0,50,203,139,16,1,192,192,192,0,3,20,12,0,0
  DEFB 0,32,0,0,0,0,248,255,126,0,128,15,15,3,128,248
  DEFB 32,126,11,208,0,3,63,128,128,0,0,144,0,248,0,0
  DEFB 2,0,32,248,255,192,7,192,31,0,255,147,0,2,248,127
  DEFB 40,3,60,251,0,3,192,184,0,0,0,120,7,128,0,0
  DEFB 0,120,124,7,192,0,0,2,124,0,0,127,128,0,7,0
  DEFB 248,0,1,0,31,192,0,2,120,126,0,0,0,16,6,0
  DEFB 127,63,0,0,0,7,7,0,240,224,63,7,224,0,0,5
  DEFB 0,252,4,118,0,0,60,91,0,128,3,5,192,184,247,0
  DEFB 60,0,1,219,0,31,79,25,240,252,252,0,0,0,0,66
  DEFB 0,86,0,66,0,1,0,255,102,128,255,126,36,66,42,0
  DEFB 3,7,252,170

; Character buffer for little girl no. 4 (186)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTGIRL4.
  DEFS 32
  DEFB 200                ; Initial animatory state: 200 (see PREPGAME)
  DEFB 160,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF186_36:
  DEFB 38                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little girl no. 4 (186)
;
; Used by the routine at NEWLESSON1.
PTGIRL4:
  DEFB 0                  ; Lesson 37 (MR CREAK - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 38 (MR CREAK - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 10                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 10 (Kitchen)
  DEFB 0                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 10                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 10 (Kitchen)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 2                  ; Lesson 53 (REVISION LIBRARY): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 54 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; ERIC location identifier table for the top floor
;
; Used by the routine at GETREGION_0. The location identifier table for the
; middle floor is at ERICLOCMF, and the table for the bottom floor is at
; ERICLOCBF.
ERICLOCTF:
  DEFB 23,7               ; Location 7: Blue Room (x<=23)
  DEFB 29,2               ; Location 2: Revision Library, outside the Blue Room
                          ; (23<x<=29)
  DEFB 40,5               ; Location 5: Revision Library, outside the Yellow
                          ; Room (29<x<=40)
  DEFB 63,8               ; Location 8: Yellow Room (40<x<=63)
  DEFB 224,0              ; Location 0: anywhere else on the top floor (x>63;
                          ; forbidden)

; Unused
  DEFS 6

; Graphic data for animatory states 0-79 (UDG mask byte 2/8)
;
; Used by the routine at GETTILE.
  DEFB 47,127,178,203,139,214,253,255,255,255,255,255,213,237,255,255
  DEFB 255,161,255,255,254,254,251,255,126,255,191,239,239,251,191,251
  DEFB 47,126,235,215,255,251,191,191,191,254,248,148,63,251,255,255
  DEFB 194,254,160,251,255,223,247,223,223,254,255,147,254,130,251,127
  DEFB 170,251,188,251,255,251,207,187,254,31,243,123,247,191,252,63
  DEFB 131,123,125,247,223,252,31,2,125,255,255,127,191,255,247,255
  DEFB 251,255,253,255,223,223,255,250,123,126,255,255,225,215,246,255
  DEFB 127,191,127,127,255,247,247,255,247,239,191,247,239,255,254,245
  DEFB 255,253,245,118,255,159,189,91,255,191,251,245,223,187,247,255
  DEFB 61,255,253,219,222,223,79,25,247,253,253,255,222,127,255,66
  DEFB 255,86,255,66,255,249,255,255,102,191,255,126,165,66,170,255
  DEFB 139,247,253,170

; Character buffer for little girl no. 5 (187)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTGIRL5.
  DEFS 32
  DEFB 72                 ; Initial animatory state: 72 (see PREPGAME)
  DEFB 163,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF187_36:
  DEFB 38                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little girl no. 5 (187)
;
; Used by the routine at NEWLESSON1.
PTGIRL5:
  DEFB 0                  ; Lesson 37 (MR CREAK - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 38 (MR CREAK - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 2
                          ; (Middle-floor classroom)
  DEFB 10                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 10 (Kitchen)
  DEFB 0                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 12                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 12 (Dinner
                          ; hall)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 2                  ; Lesson 53 (REVISION LIBRARY): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 54 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; ERIC location identifier table for the middle floor
;
; Used by the routine at GETREGION_0. The location identifier table for the top
; floor is at ERICLOCTF, and the table for the bottom floor is at ERICLOCBF.
ERICLOCMF:
  DEFB 30,2               ; Location 2: left of the Science Lab doorway (x<=30)
  DEFB 53,6               ; Location 6: Science Lab (30<x<=53)
  DEFB 224,0              ; Location 0: anywhere else on the middle floor
                          ; (x>53; forbidden)

; Unused
  DEFS 10

; Graphic data for animatory states 0-79 (UDG byte 3/8)
;
; Used by the routine at GETTILE.
  DEFB 160,128,74,4,4,9,2,128,132,128,0,7,36,18,0,0
  DEFB 0,126,0,0,0,0,124,255,42,0,128,7,15,2,192,248
  DEFB 64,42,11,80,0,3,127,128,128,1,7,11,192,252,0,0
  DEFB 46,0,0,124,255,0,11,192,7,0,255,162,0,3,124,255
  DEFB 56,1,79,255,0,4,240,248,1,224,12,252,15,192,3,192
  DEFB 108,252,130,8,32,3,224,90,130,0,0,123,128,0,7,0
  DEFB 184,0,1,0,63,192,0,4,92,62,0,0,22,40,9,0
  DEFB 119,26,0,0,0,7,5,0,112,160,26,3,64,0,0,5
  DEFB 0,236,14,126,0,96,126,95,0,128,7,5,224,248,255,0
  DEFB 198,0,0,223,32,63,79,249,248,252,168,0,33,128,0,66
  DEFB 0,95,0,226,0,0,0,127,87,192,127,95,66,129,42,0
  DEFB 119,7,254,42

; Character buffer for little girl no. 6 (188)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTGIRL6.
  DEFS 32
  DEFB 200                ; Initial animatory state: 200 (see PREPGAME)
  DEFB 166,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF188_36:
  DEFB 38                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little girl no. 6 (188)
;
; Used by the routine at NEWLESSON1.
PTGIRL6:
  DEFB 10                 ; Lesson 37 (MR CREAK - BLUE ROOM): 10 (Kitchen)
  DEFB 2                  ; Lesson 38 (MR CREAK - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 2                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 12                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 12 (Dinner hall)
  DEFB 0                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 2                  ; Lesson 53 (REVISION LIBRARY): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 54 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; ERIC location identifier table for the bottom floor
;
; Used by the routine at GETREGION_0. The location identifier table for the top
; floor is at ERICLOCTF, and the table for the middle floor is at ERICLOCMF.
ERICLOCBF:
  DEFB 42,2               ; Location 2: left of the dining hall (x<=42)
  DEFB 63,4               ; Location 4: dining hall (42<x<=63)
  DEFB 77,3               ; Location 3: assembly hall (63<x<=77)
  DEFB 96,2               ; Location 2: between the assembly hall and the skool
                          ; door (77<x<=96)
  DEFB 159,1              ; Location 1: playground (96<x<=159)
  DEFB 224,0              ; Location 0: girls' skool (x>159; forbidden)

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG mask byte 3/8)
;
; Used by the routine at GETTILE.
  DEFB 175,191,74,52,116,223,250,243,149,255,255,255,165,210,255,251
  DEFB 255,126,223,255,254,254,125,255,170,255,191,247,239,250,223,251
  DEFB 95,170,235,87,255,251,127,191,191,253,247,11,223,253,255,240
  DEFB 46,254,222,125,255,63,235,223,231,254,255,170,254,251,125,255
  DEFB 186,253,79,255,127,244,247,251,253,231,141,253,239,223,251,223
  DEFB 109,253,130,232,47,251,239,90,130,255,255,123,191,255,247,255
  DEFB 187,255,253,255,191,223,255,244,93,62,255,255,214,171,233,255
  DEFB 119,218,127,255,255,151,245,255,119,175,218,251,95,255,254,245
  DEFB 255,237,238,126,255,111,126,95,255,191,247,245,239,251,255,240
  DEFB 198,255,254,223,175,191,79,249,249,253,171,254,45,191,252,66
  DEFB 127,95,255,226,255,252,255,127,87,223,127,95,66,129,170,255
  DEFB 119,247,254,42

; Character buffer for little girl no. 7 (189)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTGIRL7.
  DEFS 32
  DEFB 72                 ; Initial animatory state: 72 (see PREPGAME)
  DEFB 165,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF189_36:
  DEFB 38                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little girl no. 7 (189)
;
; Used by the routine at NEWLESSON1.
PTGIRL7:
  DEFB 10                 ; Lesson 37 (MR CREAK - BLUE ROOM): 10 (Kitchen)
  DEFB 2                  ; Lesson 38 (MR CREAK - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 0                  ; Lesson 53 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 54 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 189
;
; Used by the routine at GOTO. Skool region 189 is on the top floor of the
; boys' skool, anywhere to the left of the left study door (x<=72). It is
; defined by the first entry in the skool region data table at TOPFLOOR.
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 189 to the destination region (189-196). Region 191
; corresponds to anywhere on the bottom floor (see BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR); region 192 is the assembly hall stage (see ONSTAGE).
SCEPIDS189:
  DEFB 189                ; Unused
  DEFB 189                ; Destination region 190: staircase endpoint 189
  DEFB 189                ; Destination region 191: staircase endpoint 189
  DEFB 189                ; Destination region 192: staircase endpoint 189
  DEFB 189                ; Destination region 193: staircase endpoint 189
  DEFB 189                ; Destination region 194: staircase endpoint 189
  DEFB 189                ; Destination region 195: staircase endpoint 189
  DEFB 189                ; Destination region 196: staircase endpoint 189

; Data for staircase endpoint 189
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 189 is the top of
; the staircase leading down from the Revision Library.
  DEFB 26                 ; x-coordinate of the top of the staircase
  DEFB 0                  ; Face left to descend the staircase
  DEFB 77                 ; LSB of DESCEND (descend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG byte 4/8)
;
; Used by the routine at GETTILE.
  DEFB 64,64,90,28,4,6,2,12,42,0,12,0,42,34,0,4
  DEFB 0,31,32,0,0,0,92,255,42,0,128,5,15,4,192,248
  DEFB 160,42,20,72,0,7,95,128,128,2,3,71,224,252,0,15
  DEFB 230,0,0,92,255,0,15,0,2,0,127,162,0,0,92,127
  DEFB 48,0,75,255,0,4,176,248,2,120,82,156,9,192,7,224
  DEFB 198,156,98,6,32,4,16,202,98,0,0,127,128,0,7,0
  DEFB 248,0,3,0,95,192,0,5,4,62,0,0,50,16,0,0
  DEFB 255,26,0,0,0,103,5,0,240,160,26,1,64,0,0,7
  DEFB 0,252,27,254,0,144,78,95,0,128,4,5,224,248,255,15
  DEFB 70,0,0,95,16,57,63,194,186,248,168,1,225,0,3,68
  DEFB 128,95,0,242,0,3,0,255,79,192,255,127,36,153,42,0
  DEFB 239,15,255,42

; Character buffer for little boy no. 1 (190)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY01.
  DEFS 32
  DEFB 64                 ; Initial animatory state: 64 (see PREPGAME)
  DEFB 5,10               ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF190_36:
  DEFB 40                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 1 (190)
;
; Used by the routine at NEWLESSON1.
PTBOY01:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 8                  ; Lesson 38 (MR CREAK - BLUE ROOM): 8 (Science Lab)
  DEFB 4                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 4 (Blue Room)
  DEFB 14                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 4 (Blue Room)
  DEFB 14                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 14 (Revision
                          ; Library)
  DEFB 8                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 6                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 6                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 8                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 4                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 20                 ; Lesson 52 (ASSEMBLY): 20 (Assembly)
  DEFB 6                  ; Lesson 53 (REVISION LIBRARY): 6 (Yellow Room)
  DEFB 4                  ; Lesson 54 (REVISION LIBRARY): 4 (Blue Room)
  DEFB 14                 ; Lesson 55 (PLAYTIME): 14 (Revision Library)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 190
;
; Used by the routine at GOTO. Skool region 190 is on the middle floor of the
; boys' skool, anywhere to the left of the far wall in the Science Lab
; storeroom (x<=63). It is defined by the first entry in the skool region data
; table at MIDFLOOR.
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 190 to the destination region (189-196). Region 191
; corresponds to anywhere on the bottom floor (see BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR); region 192 is the assembly hall stage (see ONSTAGE).
SCEPIDS190:
  DEFB 190                ; Destination region 189: staircase endpoint 190
  DEFB 190                ; Unused
  DEFB 191                ; Destination region 191: staircase endpoint 191
  DEFB 191                ; Destination region 192: staircase endpoint 191
  DEFB 191                ; Destination region 193: staircase endpoint 191
  DEFB 191                ; Destination region 194: staircase endpoint 191
  DEFB 191                ; Destination region 195: staircase endpoint 191
  DEFB 191                ; Destination region 196: staircase endpoint 191

; Data for staircase endpoint 190
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 190 is the bottom
; of the staircase leading up to the Revision Library.
  DEFB 19                 ; x-coordinate of the bottom of the staircase
  DEFB 128                ; Face right to ascend the staircase
  DEFB 56                 ; LSB of ASCEND (ascend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG mask byte 4/8)
;
; Used by the routine at GETTILE.
  DEFB 95,95,90,221,245,246,250,205,42,255,255,255,170,162,63,244
  DEFB 63,159,175,127,254,254,93,255,170,255,191,245,239,244,223,251
  DEFB 175,170,213,75,255,247,95,191,191,234,251,71,239,253,190,15
  DEFB 230,192,254,93,255,127,239,63,250,254,127,170,254,252,93,127
  DEFB 182,254,75,255,127,244,183,251,250,123,82,157,233,223,247,239
  DEFB 198,157,98,246,47,244,23,202,98,255,135,127,191,248,247,127
  DEFB 251,255,251,135,95,223,255,245,5,62,239,128,50,215,246,195
  DEFB 255,218,127,255,252,103,245,63,247,175,218,253,95,255,254,247
  DEFB 15,253,219,254,188,151,78,95,255,191,244,245,239,251,255,15
  DEFB 70,251,255,95,215,185,191,194,186,251,171,253,225,127,251,68
  DEFB 184,95,127,242,252,251,255,255,79,223,255,127,165,153,170,255
  DEFB 239,239,255,42

; Character buffer for little boy no. 2 (191)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY02.
  DEFS 32
  DEFB 192                ; Initial animatory state: 192 (see PREPGAME)
  DEFB 7,10               ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF191_36:
  DEFB 40                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 2 (191)
;
; Used by the routine at NEWLESSON1.
PTBOY02:
  DEFB 4                  ; Lesson 37 (MR CREAK - BLUE ROOM): 4 (Blue Room)
  DEFB 8                  ; Lesson 38 (MR CREAK - BLUE ROOM): 8 (Science Lab)
  DEFB 4                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 4 (Blue Room)
  DEFB 14                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 4                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 8                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 8 (Science
                          ; Lab)
  DEFB 4                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 8                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 4                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 4                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 6                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 20                 ; Lesson 52 (ASSEMBLY): 20 (Assembly)
  DEFB 8                  ; Lesson 53 (REVISION LIBRARY): 8 (Science Lab)
  DEFB 4                  ; Lesson 54 (REVISION LIBRARY): 4 (Blue Room)
  DEFB 14                 ; Lesson 55 (PLAYTIME): 14 (Revision Library)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 191
;
; Used by the routine at GOTO. Skool region 191 corresponds to anywhere on the
; bottom floor (see the skool region data table at BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR).
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 191 to the destination region (189-196). Region 192
; is the assembly hall stage (see ONSTAGE).
SCEPIDS191:
  DEFB 192                ; Destination region 189: staircase endpoint 192
  DEFB 192                ; Destination region 190: staircase endpoint 192
  DEFB 191                ; Unused
  DEFB 193                ; Destination region 192: staircase endpoint 193
  DEFB 193                ; Destination region 193: staircase endpoint 193
  DEFB 193                ; Destination region 194: staircase endpoint 193
  DEFB 194                ; Destination region 195: staircase endpoint 194
  DEFB 194                ; Destination region 196: staircase endpoint 194

; Data for staircase endpoint 191
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 191 is the top of
; the staircase leading down to the bottom floor on the far left of the boys'
; skool.
  DEFB 15                 ; x-coordinate of the top of the staircase
  DEFB 128                ; Face right to descend the staircase
  DEFB 77                 ; LSB of DESCEND (descend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG byte 5/8)
;
; Used by the routine at GETTILE.
  DEFB 64,160,38,34,6,2,7,62,84,0,30,0,36,76,192,15
  DEFB 64,33,112,0,0,0,4,255,42,0,128,0,15,5,64,248
  DEFB 208,26,20,36,0,0,255,0,128,23,2,163,224,252,65,255
  DEFB 174,63,0,244,255,0,19,0,1,0,255,162,0,0,4,63
  DEFB 48,0,99,255,0,6,48,248,2,88,66,20,1,64,4,224
  DEFB 198,20,90,5,160,3,16,218,230,0,120,127,128,7,7,128
  DEFB 248,0,0,120,127,0,0,13,124,62,16,127,214,0,0,60
  DEFB 191,26,0,0,3,159,10,192,240,80,26,2,192,0,1,15
  DEFB 240,252,27,250,67,16,71,111,0,128,4,6,112,248,63,253
  DEFB 142,4,0,111,24,121,47,252,31,248,168,0,255,128,4,64
  DEFB 135,90,128,34,3,4,0,255,78,192,251,126,24,66,42,0
  DEFB 239,18,255,42

; Character buffer for little boy no. 3 (192)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY03.
  DEFS 32
  DEFB 64                 ; Initial animatory state: 64 (see PREPGAME)
  DEFB 10,10              ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF192_36:
  DEFB 40                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 3 (192)
;
; Used by the routine at NEWLESSON1.
PTBOY03:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 38 (MR CREAK - BLUE ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 6 (Yellow Room)
  DEFB 6                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 6 (Yellow Room)
  DEFB 6                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 6 (Yellow
                          ; Room)
  DEFB 14                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 14 (Revision
                          ; Library)
  DEFB 6                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 4                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 4 (Blue Room)
  DEFB 6                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 8                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 4                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 20                 ; Lesson 52 (ASSEMBLY): 20 (Assembly)
  DEFB 14                 ; Lesson 53 (REVISION LIBRARY): 14 (Revision Library)
  DEFB 8                  ; Lesson 54 (REVISION LIBRARY): 8 (Science Lab)
  DEFB 14                 ; Lesson 55 (PLAYTIME): 14 (Revision Library)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 192
;
; Used by the routine at GOTO. Skool region 192 is the assembly hall stage (see
; ONSTAGE).
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 192 to the destination region (189-196). Region 191
; corresponds to anywhere on the bottom floor (see BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR).
SCEPIDS192:
  DEFB 195                ; Destination region 189: staircase endpoint 195
  DEFB 195                ; Destination region 190: staircase endpoint 195
  DEFB 195                ; Destination region 191: staircase endpoint 195
  DEFB 192                ; Unused
  DEFB 196                ; Destination region 193: staircase endpoint 196
  DEFB 196                ; Destination region 194: staircase endpoint 196
  DEFB 195                ; Destination region 195: staircase endpoint 195
  DEFB 195                ; Destination region 196: staircase endpoint 195

; Data for staircase endpoint 192
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 192 is the bottom
; of the staircase leading up to the middle floor on the far left of the boys'
; skool.
  DEFB 22                 ; x-coordinate of the bottom of the staircase
  DEFB 0                  ; Face left to ascend the staircase
  DEFB 56                 ; LSB of ASCEND (ascend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG mask byte 5/8)
;
; Used by the routine at GETTILE.
  DEFB 95,175,166,162,54,250,247,190,85,255,255,255,165,77,195,239
  DEFB 79,161,119,127,254,254,5,255,170,255,191,240,239,245,95,251
  DEFB 215,218,213,165,254,248,255,127,191,215,250,163,239,253,65,255
  DEFB 174,191,254,245,255,127,211,127,253,254,255,170,254,255,5,191
  DEFB 182,254,99,255,127,246,55,251,250,91,66,21,225,95,244,239
  DEFB 198,21,90,245,175,251,23,218,230,255,123,127,191,247,247,191
  DEFB 251,255,252,123,127,63,255,205,125,62,208,127,214,239,254,189
  DEFB 191,218,127,255,251,159,234,223,247,87,218,250,223,254,253,239
  DEFB 247,253,219,250,67,23,71,111,127,191,244,246,119,251,63,253
  DEFB 142,245,255,111,219,121,175,252,31,251,171,192,255,131,244,64
  DEFB 135,90,191,34,251,244,127,255,78,223,251,126,219,66,170,255
  DEFB 239,210,255,170

; Character buffer for little boy no. 4 (193)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY04.
  DEFS 32
  DEFB 192                ; Initial animatory state: 192 (see PREPGAME)
  DEFB 72,17              ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF193_36:
  DEFB 42                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 4 (193)
;
; Used by the routine at NEWLESSON1.
PTBOY04:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 8                  ; Lesson 38 (MR CREAK - BLUE ROOM): 8 (Science Lab)
  DEFB 8                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 8 (Science Lab)
  DEFB 14                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 4 (Blue Room)
  DEFB 4                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 14                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 14                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 14 (Revision
                          ; Library)
  DEFB 6                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 20                 ; Lesson 52 (ASSEMBLY): 20 (Assembly)
  DEFB 6                  ; Lesson 53 (REVISION LIBRARY): 6 (Yellow Room)
  DEFB 8                  ; Lesson 54 (REVISION LIBRARY): 8 (Science Lab)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 193
;
; Used by the routine at GOTO. Skool region 193 is on the middle floor, to the
; left of the window in the boys' skool (76<x<=96). It is defined by the third
; entry in the skool region data table at MIDFLOOR.
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 193 to the destination region (189-196). Region 191
; corresponds to anywhere on the bottom floor (see BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR); region 192 is the assembly hall stage (see ONSTAGE).
SCEPIDS193:
  DEFB 197                ; Destination region 189: staircase endpoint 197
  DEFB 197                ; Destination region 190: staircase endpoint 197
  DEFB 197                ; Destination region 191: staircase endpoint 197
  DEFB 197                ; Destination region 192: staircase endpoint 197
  DEFB 193                ; Unused
  DEFB 198                ; Destination region 194: staircase endpoint 198
  DEFB 197                ; Destination region 195: staircase endpoint 197
  DEFB 197                ; Destination region 196: staircase endpoint 197

; Data for staircase endpoint 193
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 193 is the bottom
; of the staircase leading up to the assembly hall stage.
  DEFB 83                 ; x-coordinate of the bottom of the staircase
  DEFB 0                  ; Face left to ascend the staircase
  DEFB 56                 ; LSB of ASCEND (ascend staircase)
  DEFB 4                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG byte 6/8)
;
; Used by the routine at GETTILE.
  DEFB 128,32,36,66,197,2,8,0,40,0,60,0,80,80,252,3
  DEFB 160,1,56,0,0,1,126,255,62,0,128,7,31,14,224,248
  DEFB 80,30,28,28,1,0,31,0,128,31,0,251,48,236,127,255
  DEFB 238,32,0,70,255,0,12,0,0,0,63,227,0,0,126,63
  DEFB 56,0,14,255,0,0,224,248,3,24,206,134,8,96,0,160
  DEFB 222,134,4,0,64,2,208,210,70,0,92,255,128,5,15,192
  DEFB 248,0,0,92,159,0,0,63,44,126,31,255,246,0,0,78
  DEFB 95,10,0,0,4,9,10,224,240,80,26,2,224,1,3,0
  DEFB 56,252,49,250,127,112,71,127,0,0,4,7,112,240,255,255
  DEFB 254,10,0,127,28,113,31,148,126,252,248,63,189,252,3,64
  DEFB 255,95,0,194,7,3,128,243,79,128,193,95,8,60,44,0
  DEFB 203,12,255,62

; Character buffer for little boy no. 5 (194)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY05.
  DEFS 32
  DEFB 64                 ; Initial animatory state: 64 (see PREPGAME)
  DEFB 74,17              ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF194_36:
  DEFB 42                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 5 (194)
;
; Used by the routine at NEWLESSON1.
PTBOY05:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 8                  ; Lesson 38 (MR CREAK - BLUE ROOM): 8 (Science Lab)
  DEFB 6                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 6 (Yellow Room)
  DEFB 6                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 6 (Yellow Room)
  DEFB 14                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 8                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 8 (Science
                          ; Lab)
  DEFB 6                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 6 (Yellow
                          ; Room)
  DEFB 6                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 6                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 4                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 14                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 88                 ; Lesson 52 (ASSEMBLY): 88 (Assembly)
  DEFB 8                  ; Lesson 53 (REVISION LIBRARY): 8 (Science Lab)
  DEFB 6                  ; Lesson 54 (REVISION LIBRARY): 6 (Yellow Room)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 194
;
; Used by the routine at GOTO. Skool region 194 is on the top floor, to the
; right of the left study door (72<x<=96). It is defined by the second entry in
; the skool region data table at TOPFLOOR.
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 194 to the destination region (189-196). Region 191
; corresponds to anywhere on the bottom floor (see BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR); region 192 is the assembly hall stage (see ONSTAGE).
SCEPIDS194:
  DEFB 199                ; Destination region 189: staircase endpoint 199
  DEFB 199                ; Destination region 190: staircase endpoint 199
  DEFB 199                ; Destination region 191: staircase endpoint 199
  DEFB 199                ; Destination region 192: staircase endpoint 199
  DEFB 199                ; Destination region 193: staircase endpoint 199
  DEFB 199                ; Unused
  DEFB 199                ; Destination region 195: staircase endpoint 199
  DEFB 199                ; Destination region 196: staircase endpoint 199

; Data for staircase endpoint 194
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 194 is the bottom
; of the staircase leading up to the middle floor in the girls' skool.
  DEFB 176                ; x-coordinate of the bottom of the staircase
  DEFB 128                ; Face right to ascend the staircase
  DEFB 56                 ; LSB of ASCEND (ascend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG mask byte 6/8)
;
; Used by the routine at GETTILE.
  DEFB 191,47,165,90,213,250,232,193,171,255,255,255,83,83,252,243
  DEFB 175,221,187,127,254,253,126,255,190,255,191,247,223,238,239,251
  DEFB 87,222,221,221,253,255,31,255,191,223,248,251,55,237,127,255
  DEFB 238,160,254,70,255,127,236,127,254,254,191,235,128,255,126,191
  DEFB 186,254,14,255,255,240,239,251,251,27,206,134,232,111,240,175
  DEFB 222,134,5,240,95,250,215,210,70,254,93,255,191,245,239,223
  DEFB 251,254,255,93,159,255,252,191,45,126,223,255,246,255,255,78
  DEFB 95,234,127,255,244,105,234,239,247,87,218,250,231,253,251,240
  DEFB 59,253,177,250,127,119,71,127,127,127,244,247,119,247,255,255
  DEFB 254,234,255,127,221,113,159,148,126,253,251,191,189,253,251,64
  DEFB 255,95,127,194,247,251,143,243,79,191,193,95,233,61,173,207
  DEFB 203,237,255,190

; Character buffer for little boy no. 6 (195)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY06.
  DEFS 32
  DEFB 192                ; Initial animatory state: 192 (see PREPGAME)
  DEFB 104,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF195_36:
  DEFB 42                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 6 (195)
;
; Used by the routine at NEWLESSON1.
PTBOY06:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 8                  ; Lesson 38 (MR CREAK - BLUE ROOM): 8 (Science Lab)
  DEFB 8                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 8 (Science Lab)
  DEFB 8                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 8 (Science Lab)
  DEFB 14                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 14 (Revision
                          ; Library)
  DEFB 6                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 4 (Blue Room)
  DEFB 8                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 8 (Science
                          ; Lab)
  DEFB 6                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 8                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 4                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 4                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 6                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 88                 ; Lesson 52 (ASSEMBLY): 88 (Assembly)
  DEFB 14                 ; Lesson 53 (REVISION LIBRARY): 14 (Revision Library)
  DEFB 6                  ; Lesson 54 (REVISION LIBRARY): 6 (Yellow Room)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 195
;
; Used by the routine at GOTO. Skool region 195 is the middle floor in the
; girls' skool. It is defined by the fifth entry in the skool region data table
; at MIDFLOOR.
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 195 to the destination region (189-196). Region 191
; corresponds to anywhere on the bottom floor (see BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR); region 192 is the assembly hall stage (see ONSTAGE).
SCEPIDS195:
  DEFB 200                ; Destination region 189: staircase endpoint 200
  DEFB 200                ; Destination region 190: staircase endpoint 200
  DEFB 200                ; Destination region 191: staircase endpoint 200
  DEFB 200                ; Destination region 192: staircase endpoint 200
  DEFB 200                ; Destination region 193: staircase endpoint 200
  DEFB 200                ; Destination region 194: staircase endpoint 200
  DEFB 195                ; Unused
  DEFB 206                ; Destination region 196: staircase endpoint 206

; Data for staircase endpoint 195
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 195 is the top of
; the staircase leading down from the assembly hall stage.
  DEFB 80                 ; x-coordinate of the top of the staircase
  DEFB 128                ; Face right to descend the staircase
  DEFB 77                 ; LSB of DESCEND (descend staircase)
  DEFB 4                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG byte 7/8)
;
; Used by the routine at GETTILE.
  DEFB 0,144,20,71,40,1,8,0,20,0,120,0,80,32,3,4
  DEFB 32,0,60,0,0,2,47,255,62,0,128,2,47,30,240,248
  DEFB 56,14,56,12,2,0,127,0,0,31,3,159,240,234,113,191
  DEFB 252,63,0,231,127,0,0,0,0,0,63,227,127,0,239,63
  DEFB 28,0,116,255,0,7,64,248,0,112,166,255,15,240,4,48
  DEFB 206,255,126,7,224,0,48,252,229,1,4,127,128,0,23,64
  DEFB 248,1,0,4,62,0,3,31,62,252,28,111,254,0,0,78
  DEFB 111,6,0,0,4,7,14,224,224,48,22,3,96,1,7,0
  DEFB 56,252,97,250,67,240,54,127,0,0,3,7,96,240,255,255
  DEFB 252,10,0,127,22,51,31,56,250,252,248,96,219,6,1,32
  DEFB 193,145,0,4,15,1,240,147,153,0,135,153,26,136,28,48
  DEFB 239,0,255,62

; Character buffer for little boy no. 7 (196)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY07.
  DEFS 32
  DEFB 64                 ; Initial animatory state: 64 (see PREPGAME)
  DEFB 107,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF196_36:
  DEFB 42                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 7 (196)
;
; Used by the routine at NEWLESSON1.
PTBOY07:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 38 (MR CREAK - BLUE ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 4 (Blue Room)
  DEFB 14                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 14 (Revision
                          ; Library)
  DEFB 6                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 6 (Yellow Room)
  DEFB 8                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 8 (Science
                          ; Lab)
  DEFB 8                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 8 (Science
                          ; Lab)
  DEFB 14                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 4 (Blue Room)
  DEFB 6                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 4                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 14                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 14 (Revision
                          ; Library)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 88                 ; Lesson 52 (ASSEMBLY): 88 (Assembly)
  DEFB 8                  ; Lesson 53 (REVISION LIBRARY): 8 (Science Lab)
  DEFB 4                  ; Lesson 54 (REVISION LIBRARY): 4 (Blue Room)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Staircase endpoint identifiers for skool region 196
;
; Used by the routine at GOTO. Skool region 196 is the top floor in the girls'
; skool. It is defined by the fourth entry in the skool region data table at
; TOPFLOOR.
;
; Each entry in this table determines the first staircase endpoint to reach on
; the way from skool region 196 to the destination region (189-196). Region 191
; corresponds to anywhere on the bottom floor (see BOTFLOOR) or in mid-air (see
; TOPFLOOR and MIDFLOOR); region 192 is the assembly hall stage (see ONSTAGE).
SCEPIDS196:
  DEFB 207                ; Destination region 189: staircase endpoint 207
  DEFB 207                ; Destination region 190: staircase endpoint 207
  DEFB 207                ; Destination region 191: staircase endpoint 207
  DEFB 207                ; Destination region 192: staircase endpoint 207
  DEFB 207                ; Destination region 193: staircase endpoint 207
  DEFB 207                ; Destination region 194: staircase endpoint 207
  DEFB 207                ; Destination region 195: staircase endpoint 207
  DEFB 207                ; Unused

; Data for staircase endpoint 196
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 196 is the bottom
; of the staircase leading up from the assembly hall stage.
  DEFB 81                 ; x-coordinate of the bottom of the staircase
  DEFB 128                ; Face right to ascend the staircase
  DEFB 56                 ; LSB of ASCEND (ascend staircase)
  DEFB 5                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG mask byte 7/8)
;
; Used by the routine at GETTILE.
  DEFB 127,151,213,87,42,253,235,255,213,255,255,255,87,175,3,244
  DEFB 47,254,189,127,254,250,175,255,190,127,191,250,175,162,247,251
  DEFB 187,238,187,237,250,255,127,255,127,223,251,159,247,234,113,191
  DEFB 252,191,254,231,127,127,243,127,255,254,191,235,127,255,239,191
  DEFB 220,254,116,255,255,247,79,251,248,115,166,255,239,247,244,55
  DEFB 206,255,126,247,231,248,55,253,229,253,5,127,191,240,215,95
  DEFB 251,253,255,5,62,255,251,159,62,252,220,111,254,255,255,78
  DEFB 111,246,127,255,244,247,238,239,239,183,214,251,111,253,247,255
  DEFB 59,253,109,250,67,247,182,127,255,127,251,247,111,247,255,255
  DEFB 252,234,255,127,214,179,159,58,250,253,251,96,219,6,225,160
  DEFB 193,145,7,5,239,225,243,147,153,7,135,153,218,138,221,179
  DEFB 239,243,255,190

; Character buffer for little boy no. 8 (197)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY08.
  DEFS 32
  DEFB 192                ; Initial animatory state: 192 (see PREPGAME)
  DEFB 110,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF197_36:
  DEFB 42                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 8 (197)
;
; Used by the routine at NEWLESSON1.
PTBOY08:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 38 (MR CREAK - BLUE ROOM): 4 (Blue Room)
  DEFB 4                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 4 (Blue Room)
  DEFB 4                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 6 (Yellow Room)
  DEFB 14                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 14 (Revision
                          ; Library)
  DEFB 4                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 4 (Blue Room)
  DEFB 4                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 4 (Blue Room)
  DEFB 14                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 14 (Revision
                          ; Library)
  DEFB 14                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 14 (Revision
                          ; Library)
  DEFB 6                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 4                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 8                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 88                 ; Lesson 52 (ASSEMBLY): 88 (Assembly)
  DEFB 6                  ; Lesson 53 (REVISION LIBRARY): 6 (Yellow Room)
  DEFB 6                  ; Lesson 54 (REVISION LIBRARY): 6 (Yellow Room)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Theme tune data (segment 3/6)
;
; Used by the routine at PLAYTUNE. Segment 2 can be found at OTSEG2.
OTSEG3:
  DEFB 241,128,59,113,51
  DEFB 255                ; Segment end marker
  DEFW OTSEG4             ; Pointer to the next segment

; Data for staircase endpoint 197
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 197 is the top of
; the staircase leading down to the assembly hall stage.
  DEFB 85                 ; x-coordinate of the top of the staircase
  DEFB 0                  ; Face left to descend the staircase
  DEFB 77                 ; LSB of DESCEND (descend staircase)
  DEFB 5                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG byte 8/8)
;
; Used by the routine at GETTILE.
  DEFB 0,144,8,137,40,0,16,0,8,0,240,0,32,0,0,0
  DEFB 0,0,126,0,0,1,127,126,126,128,128,7,23,60,248,232
  DEFB 120,30,120,28,2,0,128,0,0,31,1,7,120,206,127,31
  DEFB 224,79,0,255,127,0,0,0,0,1,63,231,95,0,255,63
  DEFB 15,0,23,126,0,1,112,232,3,176,124,127,7,248,7,248
  DEFB 252,255,127,7,248,3,248,192,255,0,126,254,128,7,15,224
  DEFB 232,1,0,124,64,0,4,14,254,120,31,199,240,0,0,36
  DEFB 52,14,0,0,2,7,28,64,144,96,62,7,192,0,5,0
  DEFB 144,248,32,245,127,96,47,127,0,0,2,7,240,240,254,191
  DEFB 176,7,0,127,7,15,31,61,252,252,248,63,137,252,31,31
  DEFB 34,0,248,248,29,31,124,231,0,248,157,0,12,73,24,124
  DEFB 139,0,255,126

; Character buffer for little boy no. 9 (198)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY09.
  DEFS 32
  DEFB 64                 ; Initial animatory state: 64 (see PREPGAME)
  DEFB 123,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF198_36:
  DEFB 44                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 9 (198)
;
; Used by the routine at NEWLESSON1.
PTBOY09:
  DEFB 6                  ; Lesson 37 (MR CREAK - BLUE ROOM): 6 (Yellow Room)
  DEFB 8                  ; Lesson 38 (MR CREAK - BLUE ROOM): 8 (Science Lab)
  DEFB 8                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 8 (Science Lab)
  DEFB 8                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 8 (Science Lab)
  DEFB 6                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 6 (Yellow Room)
  DEFB 14                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 14 (Revision
                          ; Library)
  DEFB 8                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 8 (Science
                          ; Lab)
  DEFB 8                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 8 (Science
                          ; Lab)
  DEFB 6                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 6                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 6                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 6 (Yellow
                          ; Room)
  DEFB 4                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 4                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 4 (Blue Room)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 88                 ; Lesson 52 (ASSEMBLY): 88 (Assembly)
  DEFB 6                  ; Lesson 53 (REVISION LIBRARY): 6 (Yellow Room)
  DEFB 8                  ; Lesson 54 (REVISION LIBRARY): 8 (Science Lab)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Theme tune data (segment 4/6)
;
; Used by the routine at PLAYTUNE. Segment 3 can be found at OTSEG3.
OTSEG4:
  DEFB 183,57,27,91,123
  DEFB 255                ; Segment end marker
  DEFW OTSEG5             ; Pointer to the next segment

; Data for staircase endpoint 198
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 198 is the bottom
; of the staircase leading up to the head's study.
  DEFB 91                 ; x-coordinate of the bottom of the staircase
  DEFB 0                  ; Face left to ascend the staircase
  DEFB 56                 ; LSB of ASCEND (ascend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFS 4

; Graphic data for animatory states 0-79 (UDG mask byte 8/8)
;
; Used by the routine at GETTILE.
  DEFB 127,151,235,169,170,254,215,255,235,255,255,255,175,223,252,251
  DEFB 223,255,126,127,255,253,127,126,126,191,191,247,215,189,251,235
  DEFB 123,222,123,221,250,255,128,255,255,223,157,7,123,206,127,31
  DEFB 225,79,254,255,127,127,255,127,255,253,191,231,95,255,255,191
  DEFB 239,255,23,126,127,241,119,235,155,183,125,127,247,251,151,251
  DEFB 253,255,127,247,251,155,251,193,255,254,126,254,191,247,239,239
  DEFB 235,253,255,125,65,255,244,238,254,121,223,199,241,255,255,165
  DEFB 180,231,127,255,250,247,221,95,151,111,190,247,223,254,245,255
  DEFB 151,251,174,245,127,111,175,127,127,127,250,247,247,247,254,191
  DEFB 179,247,255,127,231,207,159,189,253,253,251,191,137,253,223,223
  DEFB 42,110,251,251,221,223,124,231,102,251,157,102,237,73,219,125
  DEFB 139,255,255,126

; Character buffer for little boy no. 10 (199)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBOY10.
BOY10CBUF:
  DEFS 32
  DEFB 192                ; Initial animatory state: 192 (see PREPGAME)
  DEFB 126,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF199_36:
  DEFB 44                 ; Random location table identifier (see GOTORAND)

; Personal timetable for little boy no. 10 (199)
;
; Used by the routine at NEWLESSON1.
PTBOY10:
  DEFB 4                  ; Lesson 37 (MR CREAK - BLUE ROOM): 4 (Blue Room)
  DEFB 4                  ; Lesson 38 (MR CREAK - BLUE ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 6 (Yellow Room)
  DEFB 6                  ; Lesson 40 (MR CREAK - YELLOW ROOM): 6 (Yellow Room)
  DEFB 4                  ; Lesson 41 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 4                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 4 (Blue Room)
  DEFB 6                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 6 (Yellow
                          ; Room)
  DEFB 6                  ; Lesson 44 (MR WITHIT - YELLOW ROOM): 6 (Yellow
                          ; Room)
  DEFB 8                  ; Lesson 45 (MR WITHIT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 8                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 8                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 8                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 8                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 8 (Science
                          ; Lab)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 20                 ; Lesson 52 (ASSEMBLY): 20 (Assembly)
  DEFB 14                 ; Lesson 53 (REVISION LIBRARY): 14 (Revision Library)
  DEFB 14                 ; Lesson 54 (REVISION LIBRARY): 14 (Revision Library)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Theme tune data (segment 5/6)
;
; Used by the routine at PLAYTUNE. Segment 4 can be found at OTSEG4.
OTSEG5:
  DEFB 251,191,59,121,123
  DEFB 255                ; Segment end marker
  DEFW OTSEG6             ; Pointer to the next segment

; Data for staircase endpoint 199
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 199 is the top of
; the staircase leading down from the head's study.
  DEFB 84                 ; x-coordinate of the top of the staircase
  DEFB 128                ; Face right to descend the staircase
  DEFB 77                 ; LSB of DESCEND (descend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFB 0

; Graphic data for animatory states 80-127 (UDG byte 1/8)
;
; Used by the routine at GETTILE.
  DEFB 16,1,0,0,8,0,8,128,28,0,0,243,204,0,7,1
  DEFB 0,48,192,0,127,28,0,0,60,94,29,0,252,198,94,0
  DEFB 3,5,1,0,207,236,181,0,192,96,224,29,94,0,5,60
  DEFB 126,0,7,0,0,26,6,0,160,164,0,2,0,0,185,238
  DEFB 228,0,192,72,240,0,11,62,12,0,156,228,231,128,0,228
  DEFB 14,143,0,0,20,0,92,0,80,0,27,68,36,0,192,152
  DEFB 64,0,1,4,1,0,188,73,68,128,0,18,32,2,82,0
  DEFB 4,1,27,100,0,74,0,208,0,0,0,56,176,31,0,252
  DEFB 50,192,0,3,11,3,0,143,3,222,192,32,0,63,1,252
  DEFB 8,56,224,0,80,0,80,0,20,111,55,0,240,158,220,0
  DEFB 6,6,0,79,249,253,0,224,192,55,0,3,125,20,239,0
  DEFB 211,158,127,127,0,192,248

; Character buffer for MR WACKER (200)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTWACKER.
WACKERCBUF:
  DEFS 32
  DEFB 80                 ; Initial animatory state: 80 (see PREPGAME)
  DEFB 76,3               ; Initial location (see PREPGAME)
  DEFB 32                 ; Initial flags for byte 29 (see PREPGAME)
CBUF200_36:
  DEFB 48                 ; Random location table identifier (see GOTORAND)

; Personal timetable for MR WACKER (200)
;
; Used by the routine at NEWLESSON1.
PTWACKER:
  DEFB 74                 ; Lesson 37 (MR CREAK - BLUE ROOM): 74 (Head's study)
  DEFB 70                 ; Lesson 38 (MR CREAK - BLUE ROOM): 70 (Walkabout)
  DEFB 74                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 74 (Head's
                          ; study)
  DEFB 70                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 70 (Walkabout)
  DEFB 74                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 74 (Head's
                          ; study)
  DEFB 70                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 70 (Walkabout)
  DEFB 74                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 74 (Head's
                          ; study)
  DEFB 70                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 70 (Walkabout)
  DEFB 74                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 74 (Head's
                          ; study)
  DEFB 70                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 70 (Walkabout)
  DEFB 74                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 74 (Head's
                          ; study)
  DEFB 70                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 70
                          ; (Walkabout)
  DEFB 74                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 74 (Head's
                          ; study)
  DEFB 30                 ; Lesson 50 (DINNER (MR WACKER)): 30 (Dinner duty)
  DEFB 70                 ; Lesson 51 (DINNER (MR WITHIT)): 70 (Walkabout)
  DEFB 72                 ; Lesson 52 (ASSEMBLY): 72 (Assembly)
  DEFB 74                 ; Lesson 53 (REVISION LIBRARY): 74 (Head's study)
  DEFB 70                 ; Lesson 54 (REVISION LIBRARY): 70 (Walkabout)
  DEFB 66                 ; Lesson 55 (PLAYTIME): 66 (Walkabout)
  DEFB 66                 ; Lesson 56 (PLAYTIME): 66 (Walkabout)
  DEFB 66                 ; Lesson 57 (PLAYTIME): 66 (Walkabout)
  DEFB 66                 ; Lesson 58 (PLAYTIME): 66 (Walkabout)
  DEFB 66                 ; Lesson 59 (PLAYTIME): 66 (Walkabout)

; Theme tune data (segment 6/6)
;
; Used by the routine at PLAYTUNE. Segment 5 can be found at OTSEG5.
OTSEG6:
  DEFB 119,247,134
  DEFB 0                  ; End marker

; Unused
  DEFS 4

; Data for staircase endpoint 200
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 200 is the top of
; the staircase leading down to the bottom floor in the girls' skool.
  DEFB 183                ; x-coordinate of the top of the staircase
  DEFB 0                  ; Face left to descend the staircase
  DEFB 77                 ; LSB of DESCEND (descend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFB 0

; Graphic data for animatory states 80-127 (UDG mask byte 1/8)
;
; Used by the routine at GETTILE.
  DEFB 215,253,255,255,235,255,235,191,156,207,127,243,205,31,247,253
  DEFB 254,55,223,255,127,220,225,225,188,94,221,255,253,198,94,254
  DEFB 251,245,253,31,207,236,181,255,223,111,239,221,94,255,245,60
  DEFB 126,255,247,255,255,90,246,255,175,165,254,250,254,195,185,238
  DEFB 228,255,223,75,247,252,235,190,236,63,157,228,231,191,255,228
  DEFB 238,143,127,255,212,255,92,255,87,227,219,68,164,255,223,155
  DEFB 95,254,253,244,253,63,189,73,69,191,255,210,47,250,82,255
  DEFB 229,253,155,100,255,74,255,215,255,254,224,184,176,223,127,253
  DEFB 50,223,254,251,235,251,7,143,3,222,223,47,255,191,253,253
  DEFB 139,184,224,255,80,255,87,240,212,111,183,127,247,158,221,255
  DEFB 246,246,7,79,249,253,127,239,223,183,255,251,125,212,239,254
  DEFB 211,158,127,127,255,192,251

; Character buffer for MR WITHIT (201)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTWITHIT.
  DEFS 32
  DEFB 216                ; Initial animatory state: 216 (see PREPGAME)
  DEFB 15,17              ; Initial location (see PREPGAME)
  DEFB 32                 ; Initial flags for byte 29 (see PREPGAME)
CBUF201_36:
  DEFB 48                 ; Random location table identifier (see GOTORAND)

; Personal timetable for MR WITHIT (201)
;
; Used by the routine at NEWLESSON1.
PTWITHIT:
  DEFB 60                 ; Lesson 37 (MR CREAK - BLUE ROOM): 60 (Yellow Room)
  DEFB 64                 ; Lesson 38 (MR CREAK - BLUE ROOM): 64 (Walkabout)
  DEFB 58                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 58 (Blue Room)
  DEFB 58                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 58 (Blue Room)
  DEFB 58                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 58 (Blue Room)
  DEFB 58                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 58 (Blue Room)
  DEFB 60                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 60 (Yellow
                          ; Room)
  DEFB 60                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 60 (Yellow
                          ; Room)
  DEFB 62                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 62 (Science
                          ; Lab)
  DEFB 62                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 62 (Science
                          ; Lab)
  DEFB 60                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 60 (Yellow
                          ; Room)
  DEFB 64                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 64
                          ; (Walkabout)
  DEFB 58                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 58 (Blue
                          ; Room)
  DEFB 64                 ; Lesson 50 (DINNER (MR WACKER)): 64 (Walkabout)
  DEFB 30                 ; Lesson 51 (DINNER (MR WITHIT)): 30 (Dinner duty)
  DEFB 80                 ; Lesson 52 (ASSEMBLY): 80 (Assembly)
  DEFB 64                 ; Lesson 53 (REVISION LIBRARY): 64 (Walkabout)
  DEFB 58                 ; Lesson 54 (REVISION LIBRARY): 58 (Blue Room)
  DEFB 68                 ; Lesson 55 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 56 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 57 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 58 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 59 (PLAYTIME): 68 (Walkabout)

; Command list 0: Top-floor classroom - girl
;
; Used by the little girls and HAYLEY in various lessons.
CLIST0:
  DEFW GOTO               ; Go to...
  DEFB 177,3              ; ...the top-floor classroom in the girls' skool
  DEFW MVTILL             ; Move about until...
  DEFB 8                  ; ...the teacher arrives at the top-floor classroom
                          ; in the girls' skool
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 1,0

; Graphic data for animatory states 80-127 (UDG byte 2/8)
;
; Used by the routine at GETTILE.
  DEFB 16,2,0,0,4,0,8,128,113,48,0,226,18,224,7,3
  DEFB 1,32,32,0,126,49,30,30,125,125,29,0,244,46,94,1
  DEFB 7,7,1,224,223,210,151,0,64,224,224,25,94,0,4,253
  DEFB 29,0,4,0,0,144,6,0,224,228,0,2,0,60,255,110
  DEFB 228,0,224,136,112,3,15,38,12,192,254,232,243,128,0,244
  DEFB 15,143,0,0,18,0,78,0,208,28,63,72,20,0,160,144
  DEFB 64,1,3,4,1,192,250,145,140,0,0,18,32,4,137,0
  DEFB 10,0,127,228,0,69,0,80,0,0,31,79,163,31,128,194
  DEFB 84,192,1,4,10,3,248,252,53,207,32,64,0,31,0,252
  DEFB 20,79,59,0,78,0,80,15,55,96,55,128,216,158,220,0
  DEFB 6,6,248,125,9,253,128,224,192,55,0,3,125,55,96,0
  DEFB 175,110,126,127,0,255,36

; Character buffer for MR ROCKITT (202)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTROCKITT.
  DEFS 32
  DEFB 96                 ; Initial animatory state: 96 (see PREPGAME)
  DEFB 37,10              ; Initial location (see PREPGAME)
  DEFB 32                 ; Initial flags for byte 29 (see PREPGAME)
CBUF202_36:
  DEFB 68                 ; Random location table identifier (see GOTORAND)

; Personal timetable for MR ROCKITT (202)
;
; Used by the routine at NEWLESSON1.
PTROCKITT:
  DEFB 68                 ; Lesson 37 (MR CREAK - BLUE ROOM): 68 (Walkabout)
  DEFB 62                 ; Lesson 38 (MR CREAK - BLUE ROOM): 62 (Science Lab)
  DEFB 62                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 62 (Science
                          ; Lab)
  DEFB 62                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 62 (Science
                          ; Lab)
  DEFB 68                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 68 (Walkabout)
  DEFB 60                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 60 (Yellow Room)
  DEFB 62                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 62 (Science
                          ; Lab)
  DEFB 62                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 62 (Science
                          ; Lab)
  DEFB 68                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 68 (Walkabout)
  DEFB 60                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 60 (Yellow
                          ; Room)
  DEFB 62                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 62 (Science
                          ; Lab)
  DEFB 62                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 62 (Science
                          ; Lab)
  DEFB 62                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 62 (Science
                          ; Lab)
  DEFB 68                 ; Lesson 50 (DINNER (MR WACKER)): 68 (Walkabout)
  DEFB 68                 ; Lesson 51 (DINNER (MR WITHIT)): 68 (Walkabout)
  DEFB 80                 ; Lesson 52 (ASSEMBLY): 80 (Assembly)
  DEFB 62                 ; Lesson 53 (REVISION LIBRARY): 62 (Science Lab)
  DEFB 62                 ; Lesson 54 (REVISION LIBRARY): 62 (Science Lab)
  DEFB 68                 ; Lesson 55 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 56 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 57 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 58 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 59 (PLAYTIME): 68 (Walkabout)

; Command list 2: Middle-floor classroom - girl
;
; Used by the little girls and HAYLEY in various lessons.
CLIST2:
  DEFW GOTO               ; Go to...
  DEFB 174,10             ; ...the middle-floor classroom in the girls' skool
  DEFW MVTILL             ; Move about until...
  DEFB 9                  ; ...the teacher arrives at middle-floor classroom in
                          ; the girls' skool
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 1,0

; Graphic data for animatory states 80-127 (UDG mask byte 2/8)
;
; Used by the routine at GETTILE.
  DEFB 215,250,255,135,245,255,235,191,113,183,255,226,18,231,247,251
  DEFB 253,47,47,127,126,177,222,222,125,125,221,255,245,46,94,253
  DEFB 247,247,253,239,223,210,151,255,95,239,239,217,94,255,244,253
  DEFB 157,255,244,191,255,144,246,255,239,229,254,250,254,189,255,110
  DEFB 228,255,239,139,119,251,239,166,236,223,254,232,243,191,255,244
  DEFB 239,143,127,255,210,252,78,255,215,220,191,72,212,255,175,151
  DEFB 95,253,251,244,253,207,250,145,141,127,255,210,47,244,137,127
  DEFB 234,254,127,228,255,69,255,87,255,254,223,79,163,223,159,194
  DEFB 85,223,253,244,234,251,249,252,53,207,47,95,127,223,254,253
  DEFB 213,79,59,248,78,255,87,239,183,96,183,191,219,158,221,254
  DEFB 246,246,251,125,9,253,191,239,223,183,255,251,125,183,96,120
  DEFB 175,110,126,127,255,255,37

; Character buffer for MR CREAK (203)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTCREAK.
  DEFS 32
  DEFB 232                ; Initial animatory state: 232 (see PREPGAME)
  DEFB 43,17              ; Initial location (see PREPGAME)
  DEFB 32                 ; Initial flags for byte 29 (see PREPGAME)
CBUF203_36:
  DEFB 68                 ; Random location table identifier (see GOTORAND)

; Personal timetable for MR CREAK (203)
;
; Used by the routine at NEWLESSON1.
PTCREAK:
  DEFB 58                 ; Lesson 37 (MR CREAK - BLUE ROOM): 58 (Blue Room)
  DEFB 58                 ; Lesson 38 (MR CREAK - BLUE ROOM): 58 (Blue Room)
  DEFB 60                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 60 (Yellow
                          ; Room)
  DEFB 60                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 60 (Yellow
                          ; Room)
  DEFB 60                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 60 (Yellow Room)
  DEFB 68                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 68 (Walkabout)
  DEFB 58                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 58 (Blue Room)
  DEFB 58                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 58 (Blue Room)
  DEFB 60                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 60 (Yellow
                          ; Room)
  DEFB 58                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 58 (Blue Room)
  DEFB 58                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 58 (Blue
                          ; Room)
  DEFB 58                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 58 (Blue
                          ; Room)
  DEFB 60                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 60 (Yellow
                          ; Room)
  DEFB 68                 ; Lesson 50 (DINNER (MR WACKER)): 68 (Walkabout)
  DEFB 68                 ; Lesson 51 (DINNER (MR WITHIT)): 68 (Walkabout)
  DEFB 80                 ; Lesson 52 (ASSEMBLY): 80 (Assembly)
  DEFB 60                 ; Lesson 53 (REVISION LIBRARY): 60 (Yellow Room)
  DEFB 60                 ; Lesson 54 (REVISION LIBRARY): 60 (Yellow Room)
  DEFB 68                 ; Lesson 55 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 56 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 57 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 58 (PLAYTIME): 68 (Walkabout)
  DEFB 68                 ; Lesson 59 (PLAYTIME): 68 (Walkabout)

; Command list 4: Blue Room - little boy
;
; Used by the little boys in various lessons.
CLIST4:
  DEFW GOTO               ; Go to...
  DEFB 22,3               ; ...the Blue Room
  DEFW MVTILL             ; Move about until...
  DEFB 10                 ; ...the teacher arrives at the Blue Room
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 1,0

; Graphic data for animatory states 80-127 (UDG byte 3/8)
;
; Used by the routine at GETTILE.
  DEFB 16,4,0,120,2,0,4,64,177,40,0,182,18,24,7,3
  DEFB 2,96,32,128,123,49,33,33,126,126,31,0,244,30,92,2
  DEFB 7,7,1,16,239,225,149,0,64,224,224,25,92,0,5,254
  DEFB 28,0,4,64,0,141,14,0,96,180,1,1,0,66,110,238
  DEFB 228,0,208,244,112,4,20,30,9,32,237,239,241,64,0,244
  DEFB 15,159,0,0,23,3,71,0,160,3,110,104,20,0,16,112
  DEFB 64,0,6,6,0,48,225,135,142,0,0,18,32,4,136,128
  DEFB 9,0,142,40,0,68,0,32,0,0,15,133,199,31,224,194
  DEFB 140,192,0,8,12,7,254,92,120,207,32,192,128,31,0,126
  DEFB 18,133,35,7,72,0,32,31,111,111,53,192,188,190,92,1
  DEFB 6,7,252,251,251,115,192,224,192,49,0,3,77,111,47,135
  DEFB 222,238,126,127,0,226,132

; Character buffer for MISS TAKE (204)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTTAKE.
TAKECBUF:
  DEFS 32
  DEFB 112                ; Initial animatory state: 112 (see PREPGAME)
  DEFB 188,3              ; Initial location (see PREPGAME)
  DEFB 32                 ; Initial flags for byte 29 (see PREPGAME)
CBUF204_36:
  DEFB 38                 ; Random location table identifier (see GOTORAND)

; Personal timetable for MISS TAKE (204)
;
; Used by the routine at NEWLESSON1.
PTTAKE:
  DEFB 22                 ; Lesson 37 (MR CREAK - BLUE ROOM): 22 (Top-floor
                          ; classroom)
  DEFB 24                 ; Lesson 38 (MR CREAK - BLUE ROOM): 24 (Middle-floor
                          ; classroom)
  DEFB 22                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 22 (Top-floor
                          ; classroom)
  DEFB 24                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 24
                          ; (Middle-floor classroom)
  DEFB 22                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 22 (Top-floor
                          ; classroom)
  DEFB 24                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 24 (Middle-floor
                          ; classroom)
  DEFB 24                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 24
                          ; (Middle-floor classroom)
  DEFB 22                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 22 (Top-floor
                          ; classroom)
  DEFB 24                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 24
                          ; (Middle-floor classroom)
  DEFB 22                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 22 (Top-floor
                          ; classroom)
  DEFB 24                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 24
                          ; (Middle-floor classroom)
  DEFB 24                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 24
                          ; (Middle-floor classroom)
  DEFB 22                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 22 (Top-floor
                          ; classroom)
  DEFB 26                 ; Lesson 50 (DINNER (MR WACKER)): 26 (Kitchen
                          ; walkabout)
  DEFB 26                 ; Lesson 51 (DINNER (MR WITHIT)): 26 (Kitchen
                          ; walkabout)
  DEFB 26                 ; Lesson 52 (ASSEMBLY): 26 (Kitchen walkabout)
  DEFB 22                 ; Lesson 53 (REVISION LIBRARY): 22 (Top-floor
                          ; classroom)
  DEFB 22                 ; Lesson 54 (REVISION LIBRARY): 22 (Top-floor
                          ; classroom)
  DEFB 26                 ; Lesson 55 (PLAYTIME): 26 (Kitchen walkabout)
  DEFB 28                 ; Lesson 56 (PLAYTIME): 28 (Girls' skool walkabout)
  DEFB 26                 ; Lesson 57 (PLAYTIME): 26 (Kitchen walkabout)
  DEFB 28                 ; Lesson 58 (PLAYTIME): 28 (Girls' skool walkabout)
  DEFB 26                 ; Lesson 59 (PLAYTIME): 26 (Kitchen walkabout)

; Command list 6: Yellow Room - little boy
;
; Used by the little boys in various lessons.
CLIST6:
  DEFW GOTO               ; Go to...
  DEFB 56,3               ; ...the Yellow Room
  DEFW MVTILL             ; Move about until...
  DEFB 11                 ; ...the teacher arrives at the Yellow Room
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 1,0

; Graphic data for animatory states 80-127 (UDG mask byte 3/8)
;
; Used by the routine at GETTILE.
  DEFB 215,245,254,121,250,255,245,95,177,168,255,182,18,27,247,251
  DEFB 250,111,47,159,123,177,161,161,126,126,223,127,245,30,93,250
  DEFB 247,247,253,23,239,225,149,255,95,239,239,217,93,255,245,254
  DEFB 220,127,244,95,255,141,206,255,111,181,253,253,254,66,110,238
  DEFB 228,255,215,245,119,244,212,222,233,47,237,239,241,95,127,244
  DEFB 239,159,127,255,215,251,71,127,167,195,110,104,212,127,23,119
  DEFB 95,252,246,246,254,55,225,135,142,127,255,210,47,244,168,191
  DEFB 233,255,142,40,252,68,127,47,254,254,239,133,199,223,239,194
  DEFB 141,223,254,232,236,247,254,92,120,207,47,223,191,223,255,126
  DEFB 210,133,163,247,72,127,47,223,111,111,181,223,189,190,93,253
  DEFB 246,247,253,251,251,115,223,239,223,177,255,251,77,111,175,135
  DEFB 222,238,126,127,255,226,133

; Character buffer for ALBERT (205)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTALBERT.
ALBERTCBUF:
  DEFS 32
  DEFB 120                ; Initial animatory state: 120 (see PREPGAME)
  DEFB 136,17             ; Initial location (see PREPGAME)
  DEFB 32                 ; Initial flags for byte 29 (see PREPGAME)
CBUF205_36:
  DEFB 44                 ; Random location table identifier (see GOTORAND)

; Personal timetable for ALBERT (205)
;
; Used by the routine at NEWLESSON1.
PTALBERT:
  DEFB 76                 ; Lesson 37 (MR CREAK - BLUE ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 38 (MR CREAK - BLUE ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 76 (Close the
                          ; gate and the door)
  DEFB 76                 ; Lesson 50 (DINNER (MR WACKER)): 76 (Close the gate
                          ; and the door)
  DEFB 76                 ; Lesson 51 (DINNER (MR WITHIT)): 76 (Close the gate
                          ; and the door)
  DEFB 76                 ; Lesson 52 (ASSEMBLY): 76 (Close the gate and the
                          ; door)
  DEFB 76                 ; Lesson 53 (REVISION LIBRARY): 76 (Close the gate
                          ; and the door)
  DEFB 76                 ; Lesson 54 (REVISION LIBRARY): 76 (Close the gate
                          ; and the door)
  DEFB 78                 ; Lesson 55 (PLAYTIME): 78 (Open the door and the
                          ; gate)
  DEFB 78                 ; Lesson 56 (PLAYTIME): 78 (Open the door and the
                          ; gate)
  DEFB 78                 ; Lesson 57 (PLAYTIME): 78 (Open the door and the
                          ; gate)
  DEFB 78                 ; Lesson 58 (PLAYTIME): 78 (Open the door and the
                          ; gate)
  DEFB 78                 ; Lesson 59 (PLAYTIME): 78 (Open the door and the
                          ; gate)

; Command list 8: Science Lab - little boy
;
; Used by the little boys in various lessons.
CLIST8:
  DEFW GOTO               ; Go to...
  DEFB 50,10              ; ...the Science Lab
  DEFW MVTILL             ; Move about until...
  DEFB 12                 ; ...the teacher arrives at the Science Lab
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 1,0

; Graphic data for animatory states 80-127 (UDG byte 4/8)
;
; Used by the routine at GETTILE.
  DEFB 16,4,1,134,1,0,4,64,50,47,0,158,33,28,3,3
  DEFB 2,224,16,192,57,50,33,67,114,29,31,128,228,254,92,4
  DEFB 7,1,1,56,46,223,149,0,64,224,144,25,92,0,3,114
  DEFB 29,128,3,96,0,72,61,0,112,178,1,0,0,71,110,241
  DEFB 110,0,208,244,112,4,22,15,9,112,237,31,120,64,128,116
  DEFB 7,23,0,0,13,4,39,128,240,43,68,36,20,128,136,80
  DEFB 64,2,4,2,0,184,72,69,141,0,0,18,32,4,132,64
  DEFB 11,0,132,40,3,39,128,112,0,0,16,133,143,31,112,130
  DEFB 196,192,1,8,8,7,7,88,252,135,32,64,128,31,0,255
  DEFB 22,133,35,8,40,128,240,17,77,183,63,224,188,188,92,1
  DEFB 11,2,30,219,123,251,192,192,192,53,0,3,93,77,55,185
  DEFB 220,238,126,127,0,250,36

; Character buffer for BOY WANDER (206)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTTEARAWAY.
  DEFS 32
  DEFB 144                ; Initial animatory state: 144 (see PREPGAME)
  DEFB 122,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF206_36:
  DEFB 46                 ; Random location table identifier (see GOTORAND)

; Personal timetable for BOY WANDER (206)
;
; Used by the routine at NEWLESSON1.
PTTEARAWAY:
  DEFB 46                 ; Lesson 37 (MR CREAK - BLUE ROOM): 46 (Blue Room)
  DEFB 32                 ; Lesson 38 (MR CREAK - BLUE ROOM): 32 (Science Lab)
  DEFB 50                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 50 (Yellow
                          ; Room)
  DEFB 50                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 50 (Yellow
                          ; Room)
  DEFB 46                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 46 (Blue Room)
  DEFB 36                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 36 (Revision
                          ; Library)
  DEFB 50                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 50 (Yellow
                          ; Room)
  DEFB 50                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 50 (Yellow
                          ; Room)
  DEFB 32                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 32 (Science
                          ; Lab)
  DEFB 36                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 36 (Revision
                          ; Library)
  DEFB 32                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 32 (Science
                          ; Lab)
  DEFB 46                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 46 (Blue
                          ; Room)
  DEFB 32                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 32 (Science
                          ; Lab)
  DEFB 38                 ; Lesson 50 (DINNER (MR WACKER)): 38 (Dinner)
  DEFB 38                 ; Lesson 51 (DINNER (MR WITHIT)): 38 (Dinner)
  DEFB 40                 ; Lesson 52 (ASSEMBLY): 40 (Assembly)
  DEFB 36                 ; Lesson 53 (REVISION LIBRARY): 36 (Revision Library)
  DEFB 46                 ; Lesson 54 (REVISION LIBRARY): 46 (Blue Room)
  DEFB 54                 ; Lesson 55 (PLAYTIME): 54 (Write on the blackboards
                          ; in the boys' skool)
  DEFB 54                 ; Lesson 56 (PLAYTIME): 54 (Write on the blackboards
                          ; in the boys' skool)
  DEFB 56                 ; Lesson 57 (PLAYTIME): 56 (Write on the blackboards
                          ; in the girls' skool)
  DEFB 42                 ; Lesson 58 (PLAYTIME): 42 (Walkabout)
  DEFB 42                 ; Lesson 59 (PLAYTIME): 42 (Walkabout)

; Command list 10: Kitchen - girl
;
; Used by the little girls and HAYLEY in various lessons.
CLIST10:
  DEFW GOTO               ; Go to...
  DEFB 190,17             ; ...the kitchen
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Data for staircase endpoint 206
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 206 is the bottom
; of the staircase leading up to the top floor in the girls' skool.
  DEFB 186                ; x-coordinate of the bottom of the staircase
  DEFB 0                  ; Face left to ascend the staircase
  DEFB 56                 ; LSB of ASCEND (ascend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFB 0

; Graphic data for animatory states 80-127 (UDG mask byte 4/8)
;
; Used by the routine at GETTILE.
  DEFB 215,245,249,134,253,127,245,95,50,175,255,158,33,29,251,251
  DEFB 250,239,23,223,185,178,161,67,114,157,223,191,229,254,93,244
  DEFB 247,249,253,59,46,223,149,255,95,239,151,217,93,255,251,114
  DEFB 221,191,251,96,255,72,61,255,119,178,253,254,255,71,110,241
  DEFB 110,127,215,245,119,244,214,239,233,119,237,31,120,95,191,116
  DEFB 247,23,127,255,237,228,167,191,247,171,68,164,212,191,139,87
  DEFB 95,250,244,250,254,187,72,69,141,127,127,210,47,244,180,95
  DEFB 235,255,132,168,251,167,191,119,254,254,208,133,143,223,119,130
  DEFB 197,223,253,232,232,247,7,88,252,183,47,95,191,223,254,255
  DEFB 214,133,163,232,168,191,247,209,77,183,191,239,189,189,93,253
  DEFB 235,250,30,219,123,251,223,223,223,181,255,251,93,77,183,185
  DEFB 220,238,126,127,255,250,37

; Character buffer for ANGELFACE (207)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTBULLY.
BULLYCBUF:
  DEFS 32
  DEFB 32                 ; Initial animatory state: 32 (see PREPGAME)
  DEFB 116,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF207_36:
  DEFB 46                 ; Random location table identifier (see GOTORAND)

; Personal timetable for ANGELFACE (207)
;
; Used by the routine at NEWLESSON1.
PTBULLY:
  DEFB 48                 ; Lesson 37 (MR CREAK - BLUE ROOM): 48 (Blue Room)
  DEFB 48                 ; Lesson 38 (MR CREAK - BLUE ROOM): 48 (Blue Room)
  DEFB 52                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 52 (Yellow
                          ; Room)
  DEFB 34                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 34 (Science
                          ; Lab)
  DEFB 52                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 52 (Yellow Room)
  DEFB 48                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 48 (Blue Room)
  DEFB 48                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 48 (Blue Room)
  DEFB 36                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 36 (Revision
                          ; Library)
  DEFB 34                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 34 (Science
                          ; Lab)
  DEFB 52                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 52 (Yellow
                          ; Room)
  DEFB 34                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 34 (Science
                          ; Lab)
  DEFB 34                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 34 (Science
                          ; Lab)
  DEFB 36                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 36 (Revision
                          ; Library)
  DEFB 38                 ; Lesson 50 (DINNER (MR WACKER)): 38 (Dinner)
  DEFB 38                 ; Lesson 51 (DINNER (MR WITHIT)): 38 (Dinner)
  DEFB 40                 ; Lesson 52 (ASSEMBLY): 40 (Assembly)
  DEFB 36                 ; Lesson 53 (REVISION LIBRARY): 36 (Revision Library)
  DEFB 36                 ; Lesson 54 (REVISION LIBRARY): 36 (Revision Library)
  DEFB 44                 ; Lesson 55 (PLAYTIME): 44 (Stalk HAYLEY)
  DEFB 42                 ; Lesson 56 (PLAYTIME): 42 (Walkabout)
  DEFB 44                 ; Lesson 57 (PLAYTIME): 44 (Stalk HAYLEY)
  DEFB 42                 ; Lesson 58 (PLAYTIME): 42 (Walkabout)
  DEFB 44                 ; Lesson 59 (PLAYTIME): 44 (Stalk HAYLEY)

; Command list 12: Dinner hall - girl
;
; Used by the little girls and HAYLEY in various lessons.
CLIST12:
  DEFW GOTO               ; Go to...
  DEFB 174,17             ; ...the dinner hall in the girls' skool
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFB 0

; Data for staircase endpoint 207
;
; Used by the routines at GOTO and NEXTMV. Staircase endpoint 207 is the top of
; the staircase leading down to the middle floor in the girls' skool.
  DEFB 179                ; x-coordinate of the top of the staircase
  DEFB 128                ; Face right to descend the staircase
  DEFB 77                 ; LSB of DESCEND (descend staircase)
  DEFB 8                  ; Number of steps in the staircase

; Unused
  DEFB 0

; Graphic data for animatory states 80-127 (UDG byte 5/8)
;
; Used by the routine at GETTILE.
  DEFB 16,8,6,1,0,128,2,0,62,16,0,254,233,92,3,3
  DEFB 3,224,144,192,63,62,53,106,120,29,3,128,230,94,72,6
  DEFB 7,1,0,168,142,213,148,0,96,224,144,9,72,0,1,56
  DEFB 29,64,3,127,0,95,195,0,144,170,1,0,0,111,110,224
  DEFB 36,0,208,240,64,6,22,14,9,240,109,15,4,0,128,12
  DEFB 1,31,0,0,9,31,35,192,144,86,69,44,12,128,200,16
  DEFB 64,5,4,2,0,104,92,193,159,0,128,18,32,5,2,96
  DEFB 6,0,165,36,6,34,128,144,0,0,26,165,159,31,240,146
  DEFB 232,192,1,10,9,7,175,89,254,131,32,128,64,15,0,255
  DEFB 29,133,35,31,39,128,144,26,77,183,7,224,188,162,96,1
  DEFB 11,2,174,219,122,138,192,32,0,13,0,0,222,77,55,193
  DEFB 237,238,126,127,0,204,4

; Character buffer for EINSTEIN (208)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTSWOT.
SWOTCBUF:
  DEFS 32
  DEFB 48                 ; Initial animatory state: 48 (see PREPGAME)
  DEFB 24,3               ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF208_36:
  DEFB 44                 ; Random location table identifier (see GOTORAND)

; Personal timetable for EINSTEIN (208)
;
; Used by the routine at NEWLESSON1.
PTSWOT:
  DEFB 82                 ; Lesson 37 (MR CREAK - BLUE ROOM): 82 (Blue Room)
  DEFB 82                 ; Lesson 38 (MR CREAK - BLUE ROOM): 82 (Blue Room)
  DEFB 84                 ; Lesson 39 (MR CREAK - YELLOW ROOM): 84 (Yellow
                          ; Room)
  DEFB 84                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 84 (Yellow
                          ; Room)
  DEFB 82                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 82 (Blue Room)
  DEFB 82                 ; Lesson 42 (MR WITHIT - BLUE ROOM): 82 (Blue Room)
  DEFB 84                 ; Lesson 43 (MR WITHIT - YELLOW ROOM): 84 (Yellow
                          ; Room)
  DEFB 84                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 84 (Yellow
                          ; Room)
  DEFB 86                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 86 (Science
                          ; Lab)
  DEFB 86                 ; Lesson 46 (MR WITHIT - SCIENCE LAB): 86 (Science
                          ; Lab)
  DEFB 86                 ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 86 (Science
                          ; Lab)
  DEFB 86                 ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 86 (Science
                          ; Lab)
  DEFB 86                 ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 86 (Science
                          ; Lab)
  DEFB 16                 ; Lesson 50 (DINNER (MR WACKER)): 16 (Dinner)
  DEFB 16                 ; Lesson 51 (DINNER (MR WITHIT)): 16 (Dinner)
  DEFB 20                 ; Lesson 52 (ASSEMBLY): 20 (Assembly)
  DEFB 14                 ; Lesson 53 (REVISION LIBRARY): 14 (Revision Library)
  DEFB 14                 ; Lesson 54 (REVISION LIBRARY): 14 (Revision Library)
  DEFB 18                 ; Lesson 55 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 56 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Command list 14: Revision Library - EINSTEIN/little boy
;
; Used by the little boys in various lessons, and by EINSTEIN in lessons 53 and
; 54.
CLIST14:
  DEFW GOTO               ; Go to...
  DEFB 37,3               ; ...the Revision Library
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFS 6

; Graphic data for animatory states 80-127 (UDG mask byte 5/8)
;
; Used by the routine at GETTILE.
  DEFB 215,235,230,121,254,191,250,191,62,208,255,254,233,93,251,251
  DEFB 251,239,151,223,159,190,181,106,120,221,227,191,230,94,75,246
  DEFB 247,253,254,187,142,213,148,255,111,239,151,233,75,255,253,56
  DEFB 221,95,251,127,248,95,195,127,151,170,253,254,255,111,110,224
  DEFB 164,127,215,243,79,246,214,238,233,247,109,15,4,31,191,140
  DEFB 249,31,127,255,233,223,163,223,151,86,69,172,236,191,203,23
  DEFB 95,245,244,250,254,107,92,193,159,127,191,210,47,245,122,111
  DEFB 246,255,165,164,230,162,191,151,254,254,218,165,159,223,247,146
  DEFB 235,223,253,234,233,231,175,89,254,187,47,191,79,239,254,255
  DEFB 221,133,163,223,167,159,151,218,77,183,199,239,189,162,99,253
  DEFB 235,250,174,219,122,138,223,47,63,205,240,252,222,77,183,193
  DEFB 237,238,126,127,63,204,5

; Character buffer for HAYLEY (209)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used. This character's personal timetable follows at PTHAYLEY.
HAYLEYCBUF:
  DEFS 32
  DEFB 56                 ; Initial animatory state: 56 (see PREPGAME)
  DEFB 136,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
CBUF209_36:
  DEFB 36                 ; Random location table identifier (see GOTORAND)

; Personal timetable for HAYLEY (209)
;
; Used by the routine at NEWLESSON1.
PTHAYLEY:
  DEFB 0                  ; Lesson 37 (MR CREAK - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 38 (MR CREAK - BLUE ROOM): 2 (Middle-floor
                          ; classroom)
  DEFB 0                  ; Lesson 39 (MR CREAK - YELLOW ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 10                 ; Lesson 40 (MR CREAK - YELLOW ROOM): 10 (Kitchen)
  DEFB 12                 ; Lesson 41 (MR WITHIT - BLUE ROOM): 12 (Dinner hall)
  DEFB 0                  ; Lesson 42 (MR WITHIT - BLUE ROOM): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 43 (MR WITHIT - YELLOW ROOM): 2
                          ; (Middle-floor classroom)
  DEFB 10                 ; Lesson 44 (MR WITHIT - YELLOW ROOM): 10 (Kitchen)
  DEFB 12                 ; Lesson 45 (MR WITHIT - SCIENCE LAB): 12 (Dinner
                          ; hall)
  DEFB 0                  ; Lesson 46 (MR WITHIT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 2                  ; Lesson 47 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 2                  ; Lesson 48 (MR ROCKITT - SCIENCE LAB): 2
                          ; (Middle-floor classroom)
  DEFB 0                  ; Lesson 49 (MR ROCKITT - SCIENCE LAB): 0 (Top-floor
                          ; classroom)
  DEFB 12                 ; Lesson 50 (DINNER (MR WACKER)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 51 (DINNER (MR WITHIT)): 12 (Dinner hall)
  DEFB 12                 ; Lesson 52 (ASSEMBLY): 12 (Dinner hall)
  DEFB 0                  ; Lesson 53 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 0                  ; Lesson 54 (REVISION LIBRARY): 0 (Top-floor
                          ; classroom)
  DEFB 10                 ; Lesson 55 (PLAYTIME): 10 (Kitchen)
  DEFB 10                 ; Lesson 56 (PLAYTIME): 10 (Kitchen)
  DEFB 18                 ; Lesson 57 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 58 (PLAYTIME): 18 (Walkabout)
  DEFB 18                 ; Lesson 59 (PLAYTIME): 18 (Walkabout)

; Command list 16: Dinner - EINSTEIN/little boy
;
; Used by the little boys and EINSTEIN in lessons 50 and 51.
CLIST16:
  DEFW GOTO               ; Go to...
  DEFB 54,17              ; ...the dinner hall in the boys' skool
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFS 6

; Graphic data for animatory states 80-127 (UDG byte 6/8)
;
; Used by the routine at GETTILE.
  DEFB 16,8,24,0,0,64,2,0,126,8,0,254,233,20,3,3
  DEFB 2,224,144,64,63,62,33,67,120,29,1,128,230,94,72,4
  DEFB 7,1,0,56,142,213,148,0,96,224,112,9,72,0,0,152
  DEFB 29,32,5,112,7,47,55,128,136,238,1,0,0,71,110,228
  DEFB 61,0,208,240,192,4,22,14,30,112,173,79,2,0,128,29
  DEFB 3,231,0,0,9,36,23,32,136,40,68,46,31,128,72,16
  DEFB 64,2,4,2,1,144,68,225,212,0,128,59,160,15,1,224
  DEFB 6,0,164,44,31,19,128,8,0,0,16,165,127,63,80,146
  DEFB 248,192,1,10,7,31,5,89,255,129,32,128,64,31,1,246
  DEFB 4,133,47,47,23,224,8,16,79,119,7,96,188,210,64,1
  DEFB 7,2,6,251,125,137,192,32,0,13,15,0,88,207,55,128
  DEFB 253,230,126,224,192,2,120

; Character buffer for ERIC (210)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used.
ERICCBUF:
  DEFS 32
  DEFB 128                ; Initial animatory state: 128 (see PREPGAME)
  DEFB 133,17             ; Initial location (see PREPGAME)
  DEFB 0                  ; Initial flags for byte 29 (see PREPGAME)
                          ; (irrelevant for ERIC)

; Make a character do nothing
;
; Used by the routines at KISS and PREPGAME, and by command lists 0, 2, 4, 6,
; 8, 32, 34, 46, 48, 50 and 52 to make a character do nothing. The RET
; instruction is located in ERIC's buffer at byte 36, which is otherwise
; unused.
DONOWT:
  RET                     ; Return having done nothing

; Lesson descriptors
;
; When a lesson starts, the routine at NEWLESSON1 picks up the lesson
; descriptor from this table and copies it into LESSONDESC. Each entry in this
; table defines the teacher for the period (if any), and the room. The teacher
; is identified by bits 4-7:
;
; +------+------------+
; | Bits | Teacher    |
; +------+------------+
; | 0000 | None       |
; | 0001 | MR WACKER  |
; | 0010 | MR WITHIT  |
; | 0011 | MR ROCKITT |
; | 0100 | MR CREAK   |
; +------+------------+
;
; The room is identified by bits 0-3:
;
; +------+------------------+
; | Bits | Room             |
; +------+------------------+
; | 0010 | PLAYTIME         |
; | 0011 | ASSEMBLY         |
; | 0100 | DINNER           |
; | 0101 | REVISION LIBRARY |
; | 0110 | SCIENCE LAB      |
; | 0111 | BLUE ROOM        |
; | 1000 | YELLOW ROOM      |
; +------+------------------+
LDESCS:
  DEFB 71                 ; Lesson 37: MR CREAK - BLUE ROOM
  DEFB 71                 ; Lesson 38: MR CREAK - BLUE ROOM
  DEFB 72                 ; Lesson 39: MR CREAK - YELLOW ROOM
  DEFB 72                 ; Lesson 40: MR CREAK - YELLOW ROOM
  DEFB 39                 ; Lesson 41: MR WITHIT - BLUE ROOM
  DEFB 39                 ; Lesson 42: MR WITHIT - BLUE ROOM
  DEFB 40                 ; Lesson 43: MR WITHIT - YELLOW ROOM
  DEFB 40                 ; Lesson 44: MR WITHIT - YELLOW ROOM
  DEFB 38                 ; Lesson 45: MR WITHIT - SCIENCE LAB
  DEFB 38                 ; Lesson 46: MR WITHIT - SCIENCE LAB
  DEFB 54                 ; Lesson 47: MR ROCKITT - SCIENCE LAB
  DEFB 54                 ; Lesson 48: MR ROCKITT - SCIENCE LAB
  DEFB 54                 ; Lesson 49: MR ROCKITT - SCIENCE LAB
  DEFB 20                 ; Lesson 50: DINNER (MR WACKER)
  DEFB 36                 ; Lesson 51: DINNER (MR WITHIT)
  DEFB 35                 ; Lesson 52: ASSEMBLY
  DEFB 5                  ; Lesson 53: REVISION LIBRARY
  DEFB 5                  ; Lesson 54: REVISION LIBRARY
  DEFB 2                  ; Lesson 55: PLAYTIME
  DEFB 2                  ; Lesson 56: PLAYTIME
  DEFB 2                  ; Lesson 57: PLAYTIME
  DEFB 2                  ; Lesson 58: PLAYTIME
  DEFB 2                  ; Lesson 59: PLAYTIME

; Command list 18: Walkabout - EINSTEIN/little boy/girl
;
; Used by the little girls, the little boys, EINSTEIN and HAYLEY in various
; lessons.
CLIST18:
  DEFW GOTORAND           ; Go to a random location
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 0,10               ; ...10 times
  DEFW RESTART            ; Restart the command list

; Unused
  DEFS 5

; Graphic data for animatory states 80-127 (UDG mask byte 6/8)
;
; Used by the routine at GETTILE.
  DEFB 215,235,153,254,255,95,250,255,126,232,255,254,233,21,251,251
  DEFB 250,239,151,95,159,190,161,67,120,221,253,143,230,94,75,244
  DEFB 247,253,254,56,142,213,148,255,111,239,119,233,75,255,254,152
  DEFB 221,175,245,112,247,47,55,191,139,238,253,254,255,71,110,238
  DEFB 189,127,215,247,223,244,214,238,222,119,173,79,250,127,191,221
  DEFB 251,231,127,255,233,164,215,47,139,168,68,174,223,191,75,23
  DEFB 95,250,244,250,253,147,68,225,212,127,191,187,175,239,125,239
  DEFB 246,255,164,172,159,211,159,11,254,254,208,165,127,191,87,146
  DEFB 251,223,253,234,247,223,5,89,255,189,47,191,79,223,253,246
  DEFB 228,133,175,175,215,239,11,208,79,119,247,111,189,210,95,253
  DEFB 247,250,6,251,125,169,223,47,127,237,207,255,89,207,183,128
  DEFB 253,230,126,224,223,2,123

; Character buffer for the bike (211)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used.
BIKECBUF:
  DEFS 32
  DEFB 24                 ; Initial animatory state: 24 (see PREPGAME)
  DEFB 224,17             ; Initial location (see PREPGAME)
  DEFB 64                 ; Initial flags for byte 29 (see PREPGAME)

; Random locations
;
; Used by the routine at GOTORAND. Each location's visitors are indicated in
; brackets. The next set of random locations is at RANDLOCS2.
RANDLOCS1:
  DEFB 129,17             ; Just left of the skool gate (little girls 1-3,
                          ; HAYLEY)
  DEFB 189,17             ; Kitchen in the girls' skool (little girls 4-7, MISS
                          ; TAKE)
  DEFB 38,3               ; Revision Library (little boys 1-3)
  DEFB 38,3               ; Revision Library (little boys 4-8)
  DEFB 112,17             ; Middle of the boys' playground (little boys 9 and
                          ; 10, ALBERT, EINSTEIN)
  DEFB 189,10             ; Near the window on the middle floor in the girls'
                          ; skool (BOY WANDER, ANGELFACE)
  DEFB 157,17             ; Just outside the entrance to the girls' skool (MR
                          ; WACKER, MR WITHIT)

; x-coordinates of the left ends of the classrooms
;
; Used by the routine at BYSEAT. The x-coordinates of the right ends of the
; classrooms can be found at TFRMSMAXX.
TFRMSMINX:
  DEFB 0                  ; x-coordinate of the left end of the Blue Room
  DEFB 40                 ; x-coordinate of the left end of the Yellow Room
  DEFB 159                ; x-coordinate of the left end of the top-floor room
                          ; in the girls' skool
MFRMSMINX:
  DEFB 30                 ; x-coordinate of the left end of the Science Lab
  DEFB 159                ; x-coordinate of the left end of the middle-floor
                          ; room in the girls' skool

; x-coordinates of the left edges of the blackboards
;
; Used by the routine at BOARDID. The blackboard identifiers can be found at
; BOARDIDS, and the y-coordinates of the top rows of the blackboards can be
; found at BBSY.
BBSLEFTXT:
  DEFB 3                  ; x-coordinate of the left edge of the blackboard in
                          ; the Blue Room
  DEFB 41                 ; x-coordinate of the left edge of the blackboard in
                          ; the Yellow Room
  DEFB 161                ; x-coordinate of the left edge of the blackboard in
                          ; the top-floor room in the girls' skool
BBSLEFTXM:
  DEFB 32                 ; x-coordinate of the left edge of the blackboard in
                          ; the Science Lab
  DEFB 161                ; x-coordinate of the left edge of the blackboard in
                          ; the middle-floor room in the girls' skool

; LSBs of addresses of ERIC-handling routines
;
; Used by the routine at HANDLEERIC. The corresponding MSBs can be found at
; EHRMSBS1.
EHRLSBS1:
  DEFB 142                ; LSB of ERICHIT (used when bit 7 is set at STATUS)
  DEFB 0                  ; Unused (when bit 6 is set at STATUS, ERIC is
                          ; incapacitated)
  DEFB 173                ; LSB of MIDFHK (used when bit 5 is set at STATUS)
  DEFB 111                ; LSB of WRITING (used when bit 4 is set at STATUS)
  DEFB 69                 ; LSB of BENDING (used when bit 3 is set at STATUS)
  DEFB 226                ; LSB of ERICSITLIE (used when bit 2 is set at
                          ; STATUS)
  DEFB 0                  ; Unused (when bit 1 is set at STATUS, ERIC's
                          ; secondary status flags at STATUS2 are checked)
  DEFB 99                 ; LSB of JUMPING (used when bit 0 is set at STATUS)

; Random location
;
; This random location is used by MR ROCKITT and MR CREAK (see GOTORAND). The
; next set of random locations is at RANDLOCS3.
RANDLOCS2:
  DEFB 38,3               ; Revision Library

; Unused
  DEFS 3

; Graphic data for animatory states 80-127 (UDG byte 7/8)
;
; Used by the routine at GETTILE.
  DEFB 16,8,96,0,0,32,1,0,255,7,0,254,249,132,1,3
  DEFB 2,224,144,64,31,63,40,53,124,29,1,240,230,94,120,3
  DEFB 7,1,1,95,206,213,244,0,96,224,224,31,112,0,0,156
  DEFB 29,16,0,112,8,142,255,64,200,233,1,0,0,111,6,228
  DEFB 127,0,208,240,192,6,22,14,62,240,205,79,3,0,128,63
  DEFB 7,198,0,0,4,68,14,112,200,33,76,54,53,0,104,16
  DEFB 192,2,4,3,1,16,198,193,117,0,0,110,224,25,1,96
  DEFB 2,0,44,46,100,15,96,72,0,0,30,165,63,66,80,146
  DEFB 224,32,1,10,3,48,229,89,254,130,32,0,192,36,2,20
  DEFB 2,5,63,80,15,112,72,12,79,55,11,64,190,220,192,0
  DEFB 6,7,196,251,125,143,224,192,0,27,63,0,252,79,55,88
  DEFB 251,251,126,160,64,3,152

; Character buffer for the frog or mouse (212)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used.
ANIMALCBUF:
  DEFS 32
  DEFB 47                 ; Initial animatory state: 47 (see PREPGAME)
  DEFB 17,3               ; Initial location (see PREPGAME)
  DEFB 64                 ; Initial flags for byte 29 (see PREPGAME)

; Random locations
;
; Used by the routine at GOTORAND. Each location's visitors are indicated in
; brackets. The next set of random locations is at RANDLOCS4.
RANDLOCS3:
  DEFB 144,17             ; Middle of the girls' playground (little girls 1-3,
                          ; HAYLEY)
  DEFB 189,10             ; Near the window on the middle floor in the girls'
                          ; skool (little girls 4-7, MISS TAKE)
  DEFB 10,10              ; Wash basins (little boys 1-3)
  DEFB 10,17              ; Cloak room at far left of boys' skool (little boys
                          ; 4-8)
  DEFB 145,17             ; Middle of the girls' playground (little boys 9 and
                          ; 10, ALBERT, EINSTEIN)
  DEFB 183,17             ; Kitchen in the girls' skool (BOY WANDER, ANGELFACE)
  DEFB 38,3               ; Revision Library (MR WACKER, MR WITHIT)

; x-coordinates of the right ends of the classrooms
;
; Used by the routine at BYSEAT. The x-coordinates of the left ends of the
; classrooms can be found at TFRMSMINX.
TFRMSMAXX:
  DEFB 23                 ; x-coordinate of the right end of the Blue Room
  DEFB 63                 ; x-coordinate of the right end of the Yellow Room
  DEFB 180                ; x-coordinate of the right end of the top-floor room
                          ; in the girls' skool
MFRMSMAXX:
  DEFB 54                 ; x-coordinate of the right end of the Science Lab
  DEFB 179                ; x-coordinate of the right end of the middle-floor
                          ; room in the girls' skool

; Blackboard identifiers
;
; Used by the routine at BOARDID. Each identifier is the LSB of the
; corresponding blackboard buffer. The x-coordinates of the left edges of the
; blackboards can be found at BBSLEFTXT, and the y-coordinates of the top rows
; of the blackboards can be found at BBSY.
BOARDIDS:
  DEFB 84                 ; ID of the blackboard in the Blue Room (LSB of
                          ; BRBRDBUF)
  DEFB 90                 ; ID of the blackboard in the Yellow Room (LSB of
                          ; YRBRDBUF)
  DEFB 96                 ; ID of the blackboard in the top-floor room in the
                          ; girls' skool (LSB of TFBRDBUF)
  DEFB 102                ; ID of the blackboard in the Science Lab (LSB of
                          ; SLBRDBUF)
  DEFB 108                ; ID of the blackboard in the middle-floor room in
                          ; the girls' skool (LSB of MFBRDBUF)

; MSBs of addresses of ERIC-handling routines
;
; Used by the routine at HANDLEERIC. The corresponding LSBs can be found at
; EHRLSBS1.
EHRMSBS1:
  DEFB 242                ; MSB of ERICHIT (used when bit 7 is set at STATUS)
  DEFB 0                  ; Unused (when bit 6 is set at STATUS, ERIC is
                          ; incapacitated)
  DEFB 94                 ; MSB of MIDFHK (used when bit 5 is set at STATUS)
  DEFB 94                 ; MSB of WRITING (used when bit 4 is set at STATUS)
  DEFB 225                ; MSB of BENDING (used when bit 3 is set at STATUS)
  DEFB 242                ; MSB of ERICSITLIE (used when bit 2 is set at
                          ; STATUS)
  DEFB 0                  ; Unused (when bit 1 is set at STATUS, ERIC's
                          ; secondary status flags at STATUS2 are checked)
  DEFB 93                 ; MSB of JUMPING (used when bit 0 is set at STATUS)

; Random location
;
; This random location is used by MR ROCKITT and MR CREAK (see GOTORAND). The
; next set of random locations is at RANDLOCS5.
RANDLOCS4:
  DEFB 90,3               ; Near the top-floor window in the boys' skool

; Unused
  DEFS 3

; Graphic data for animatory states 80-127 (UDG mask byte 7/8)
;
; Used by the routine at GETTILE.
  DEFB 215,235,103,255,255,175,253,255,255,247,255,254,249,133,253,251
  DEFB 250,239,151,95,223,191,168,181,124,221,253,247,230,94,123,251
  DEFB 247,253,253,95,206,213,244,127,111,239,239,223,119,253,254,156
  DEFB 221,215,250,112,232,142,255,95,203,233,253,254,255,111,6,228
  DEFB 127,127,215,247,223,246,214,238,190,247,205,79,251,127,191,191
  DEFB 247,198,255,247,244,68,238,119,203,161,76,182,181,127,107,23
  DEFB 223,250,244,251,253,23,198,193,117,127,127,110,239,217,125,111
  DEFB 250,255,44,174,100,239,111,75,254,255,222,165,191,66,87,146
  DEFB 231,47,253,234,251,176,229,89,254,186,47,127,207,164,226,20
  DEFB 250,5,191,80,239,119,75,236,79,183,235,95,190,221,223,254
  DEFB 246,247,197,251,125,175,239,223,127,219,191,254,253,79,183,88
  DEFB 251,251,126,160,95,3,155

; Character buffer for the stinkbomb cloud or BOY WANDER's pellet (213)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used.
CBUF213:
  DEFS 32
  DEFB 0                  ; Initial animatory state: 0 (see PREPGAME)
  DEFB 224,17             ; Initial location (see PREPGAME)
  DEFB 64                 ; Initial flags for byte 29 (see PREPGAME)

; Random locations
;
; Used by the routine at GOTORAND. Each location's visitors are indicated in
; brackets. The next set of random locations is at RANDLOCS6.
RANDLOCS5:
  DEFB 136,17             ; Just to the right of the skool gate (little girls
                          ; 1-3, HAYLEY)
  DEFB 170,17             ; Dinner hall in the girls' skool (little girls 4-7,
                          ; MISS TAKE)
  DEFB 10,17              ; Cloak room at the far left of the boys' skool
                          ; (little boys 1-3)
  DEFB 109,17             ; Just to the right of the tree (little boys 4-8)
  DEFB 158,17             ; Just outside the entrance to the girls' skool
                          ; (little boys 9 and 10, ALBERT, EINSTEIN)
  DEFB 137,17             ; Just to the right of the skool gate (BOY WANDER,
                          ; ANGELFACE)
  DEFB 10,17              ; Cloak room at the far left of the boys' skool (MR
                          ; WACKER, MR WITHIT)

; x-coordinates of the leftmost seats in the classrooms
;
; Used by the routine at BYSEAT. The x-coordinates of the rightmost seats in
; the classrooms can be found at SEATSMAXX.
SEATSMINX:
  DEFB 11                 ; x-coordinate of the leftmost seat in the Blue Room
  DEFB 50                 ; x-coordinate of the leftmost seat in the Yellow
                          ; Room
  DEFB 168                ; x-coordinate of the leftmost seat in the top-floor
                          ; room in the girls' skool
  DEFB 40                 ; x-coordinate of the leftmost seat in the Science
                          ; Lab
  DEFB 167                ; x-coordinate of the leftmost seat in the
                          ; middle-floor room in the girls' skool

; y-coordinates of the top rows of the blackboards
;
; Used by the routine at BOARDID. The x-coordinates of the left edges of the
; blackboards can be found at BBSLEFTXT, and the blackboard identifiers can be
; found at BOARDIDS.
BBSY:
  DEFB 3                  ; y-coordinate of the top row of the blackboard in
                          ; the Blue Room
  DEFB 3                  ; y-coordinate of the top row of the blackboard in
                          ; the Yellow Room
  DEFB 3                  ; y-coordinate of the top row of the blackboard in
                          ; the top-floor room in the girls' skool
  DEFB 9                  ; y-coordinate of the top row of the blackboard in
                          ; the Science Lab
  DEFB 9                  ; y-coordinate of the top row of the blackboard in
                          ; the middle-floor room in the girls' skool

; LSBs of addresses of ERIC-handling routines
;
; Used by the routine at HANDLEERIC. The corresponding MSBs can be found at
; EHRMSBS2.
EHRLSBS2:
  DEFB 22                 ; LSB of ONSADDLE (used when bit 7 is set at STATUS2)
  DEFB 35                 ; LSB of OFFSADDLE (used when bit 6 is set at
                          ; STATUS2)
  DEFB 51                 ; LSB of BIGFALL (used when bit 5 is set at STATUS2)
  DEFB 204                ; LSB of FALLING (used when bit 4 is set at STATUS2)
  DEFB 160                ; LSB of LANDING (used when bit 3 is set at STATUS2)
  DEFB 3                  ; LSB of STEPPEDOFF (used when bit 2 is set at
                          ; STATUS2)
  DEFB 60                 ; LSB of ONPLANT (used when bit 1 is set at STATUS2)
  DEFB 94                 ; LSB of RIDINGBIKE (used when bit 0 is set at
                          ; STATUS2)

; Random location
;
; This random location is used by MR ROCKITT and MR CREAK (see GOTORAND). The
; next set of random locations is at RANDLOCS7.
RANDLOCS6:
  DEFB 10,17              ; Cloak room at the far left of the boys' skool

; Unused
  DEFS 3

; Graphic data for animatory states 80-127 (UDG byte 8/8)
;
; Used by the routine at GETTILE.
  DEFB 16,16,128,0,0,16,1,0,127,0,0,252,241,8,1,7
  DEFB 1,192,16,128,31,127,16,33,92,29,3,248,230,94,120,2
  DEFB 5,1,3,31,206,213,245,128,96,224,192,63,112,2,0,92
  DEFB 29,8,0,127,16,126,255,224,232,222,1,0,0,126,232,228
  DEFB 255,0,240,240,192,7,30,14,126,224,239,79,7,0,128,127
  DEFB 7,204,0,8,4,78,14,240,232,61,101,53,72,128,72,224
  DEFB 64,3,6,3,2,216,84,94,30,0,0,68,32,17,2,32
  DEFB 2,0,37,54,69,15,112,200,0,0,8,163,63,127,32,122
  DEFB 224,224,0,10,3,15,130,55,254,135,160,0,128,127,7,247
  DEFB 1,35,63,90,15,240,104,8,111,55,23,64,190,220,192,0
  DEFB 6,15,132,251,125,158,224,192,0,55,61,1,248,111,55,175
  DEFB 31,241,127,175,192,254,240

; Character buffer for water/sherry, plant, conker, desk lid or ERIC's pellet
; (214)
;
; See the character buffer documentation for details of how bytes 0-31 of the
; buffer are used.
CBUF214:
  DEFS 32
  DEFB 0                  ; Initial animatory state: 0 (see PREPGAME)
  DEFB 224,17             ; Initial location (see PREPGAME)
  DEFB 64                 ; Initial flags for byte 29 (see PREPGAME)

; Random locations
;
; Used by the routine at GOTORAND. Each location's visitors are indicated in
; brackets. The next set of random locations is at RANDLOCS8.
RANDLOCS7:
  DEFB 189,17             ; Kitchen in the girls' skool (little girls 1-3,
                          ; HAYLEY)
  DEFB 182,3              ; Just outside MISS TAKE's study (little girls 4-7,
                          ; MISS TAKE)
  DEFB 66,17              ; Left end of the assembly hall (little boys 1-3)
  DEFB 75,17              ; Right end of the assembly hall (little boys 4-8)
  DEFB 37,3               ; Revision Library (little boys 9 and 10, ALBERT,
                          ; EINSTEIN)
  DEFB 72,17              ; Middle of the assembly hall (BOY WANDER, ANGELFACE)
  DEFB 92,17              ; Just to the left of the boys' skool door (MR
                          ; WACKER, MR WITHIT)

; x-coordinates of the rightmost seats in the classrooms
;
; Used by the routine at BYSEAT. The x-coordinates of the leftmost seats in the
; classrooms can be found at SEATSMINX.
SEATSMAXX:
  DEFB 22                 ; x-coordinate of the rightmost seat in the Blue Room
  DEFB 61                 ; x-coordinate of the rightmost seat in the Yellow
                          ; Room
  DEFB 179                ; x-coordinate of the rightmost seat in the top-floor
                          ; room in the girls' skool
  DEFB 53                 ; x-coordinate of the rightmost seat in the Science
                          ; Lab
  DEFB 178                ; x-coordinate of the rightmost seat in the
                          ; middle-floor room in the girls' skool

; Unused
  DEFS 5

; MSBs of addresses of ERIC-handling routines
;
; Used by the routine at HANDLEERIC. The corresponding LSBs can be found at
; EHRLSBS2.
EHRMSBS2:
  DEFB 115                ; MSB of ONSADDLE (used when bit 7 is set at STATUS2)
  DEFB 93                 ; MSB of OFFSADDLE (used when bit 6 is set at
                          ; STATUS2)
  DEFB 93                 ; MSB of BIGFALL (used when bit 5 is set at STATUS2)
  DEFB 251                ; MSB of FALLING (used when bit 4 is set at STATUS2)
  DEFB 251                ; MSB of LANDING (used when bit 3 is set at STATUS2)
  DEFB 251                ; MSB of STEPPEDOFF (used when bit 2 is set at
                          ; STATUS2)
  DEFB 251                ; MSB of ONPLANT (used when bit 1 is set at STATUS2)
  DEFB 114                ; MSB of RIDINGBIKE (used when bit 0 is set at
                          ; STATUS2)

; Random location
;
; This random location is used by MR ROCKITT and MR CREAK (see GOTORAND).
RANDLOCS8:
  DEFB 90,17              ; Just to the left of the boys' skool door

; Unused
  DEFS 3

; Graphic data for animatory states 80-127 (UDG mask byte 8/8)
;
; Used by the routine at GETTILE.
  DEFB 215,215,159,255,255,215,253,255,255,248,255,253,241,11,253,247
  DEFB 253,223,23,159,223,127,208,161,92,221,251,251,230,94,123,250
  DEFB 245,253,251,31,206,213,245,191,111,239,223,191,119,242,255,92
  DEFB 221,232,255,127,208,126,255,239,235,222,253,254,254,126,232,228
  DEFB 255,63,247,247,223,247,222,238,126,227,239,79,247,127,191,127
  DEFB 247,205,255,203,244,78,238,247,235,189,101,181,72,191,75,239
  DEFB 95,251,246,251,250,219,84,94,30,255,255,68,47,209,122,47
  DEFB 250,255,37,182,69,239,119,203,254,255,200,163,191,127,35,122
  DEFB 239,239,252,234,251,143,130,55,254,183,175,255,191,127,247,247
  DEFB 253,35,191,90,239,247,107,232,111,183,215,95,190,221,223,254
  DEFB 246,239,132,251,125,158,239,223,255,183,189,253,251,111,183,175
  DEFB 31,241,127,175,223,254,247

; UDG reference table for the top-floor window when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the top-floor
; window when open is at URTTFWOPEN.
URTTFWSHUT:
  DEFB 2,93,48,132
  DEFB 3,92,33,132,3,93,49,132,3,94,62,132
  DEFB 4,92,34,132,4,93,50,132,4,94,63,132
  DEFB 255                ; End marker

; Unused
  DEFS 3

; Font character bitmap widths for CHR$(32-127)
;
; Used by the routine at CHR2BUF.
FCBWIDTHS:
  DEFB 3,1,3,5,5,3,5,2,2,2,5,5,2,4,2,3
  DEFB 4,3,4,4,4,4,4,4,4,4,2,2,4,4,4,4
  DEFB 5,4,4,4,4,4,4,4,4,3,4,4,4,5,4,4
  DEFB 4,5,4,4,5,4,5,5,5,5,5,2,3,2,5,5
  DEFB 4,4,4,4,4,4,4,4,4,1,3,4,1,5,4,4
  DEFB 4,4,4,4,3,4,3,5,5,4,4,3,1,3,4,5

; UDG references for animatory states 0-127 at row 0, column 0
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 55253   |
; | 93    | 55261   |
; | 100   | 55268   |
; | 101   | 55269   |
; | 109   | 55277   |
; | 117   | 55285   |
; | 125   | 55293   |
; +-------+---------+
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,142,0,142,0,0,0,156,0,203,0,203,76,0,0,0
  DEFB 0,170,0,170,75,0,0,0,0,104,0,104,0,0,0,118
  DEFB 0,232,0,232,0,0,0,0,0,89,0,89,0,0,0,0

; UDG reference table for the top-floor window when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the top-floor
; window when shut is at URTTFWSHUT.
URTTFWOPEN:
  DEFB 2,93,48,132
  DEFB 3,92,202,196,3,93,203,196,3,94,204,196
  DEFB 4,92,205,196,4,93,206,196,4,94,207,196
  DEFB 255                ; End marker

; Unused
  DEFS 3

; Font graphic data for CHR$(32-127) (pixel column 1)
;
; Used by the routine at CHR2BUF.
  DEFB 0,250,192,40,18,78,108,64,124,130,16,8,1,8,3,3
  DEFB 124,66,70,130,56,242,124,128,108,98,54,49,8,20,34,64
  DEFB 76,126,254,124,254,254,254,126,254,130,4,254,254,254,254,124
  DEFB 254,124,254,98,128,252,224,252,198,192,134,255,192,129,32,1
  DEFB 2,28,254,28,28,28,16,24,254,94,1,254,254,62,62,28
  DEFB 63,24,62,18,16,60,56,56,34,56,38,24,255,129,8,124

; UDG references for animatory states 0-127 at row 1, column 0
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 55509   |
; | 93    | 55517   |
; | 100   | 55524   |
; | 101   | 55525   |
; | 109   | 55533   |
; | 117   | 55541   |
; | 125   | 55549   |
; +-------+---------+
  DEFB 96,103,96,103,96,0,0,140,96,136,96,125,240,240,224,225
  DEFB 96,159,96,159,96,0,0,140,0,0,96,125,0,0,91,0
  DEFB 96,152,96,152,96,0,0,0,96,136,0,0,0,0,0,0
  DEFB 0,145,0,145,0,0,0,0,0,214,0,214,0,0,0,221
  DEFB 0,169,0,169,0,0,0,82,0,192,0,192,201,0,0,0
  DEFB 131,143,131,143,77,0,0,157,193,204,193,204,0,0,0,217
  DEFB 0,171,0,171,0,0,0,185,0,105,0,105,74,0,122,119
  DEFB 0,171,0,171,0,0,0,217,0,88,0,88,0,0,243,82

; UDG reference table for the middle-floor window when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the
; middle-floor window when open is at URTMFWOPEN.
URTMFWSHUT:
  DEFB 8,95,80,142
  DEFB 9,94,68,132,9,95,81,133
  DEFB 10,94,69,133,10,95,82,133
  DEFB 11,94,70,133,11,95,83,133
  DEFB 255                ; End marker

; Unused
  DEFS 3

; Font graphic data for CHR$(32-127) (pixel column 2)
;
; Used by the routine at CHR2BUF. The following addresses are unused, because
; the corresponding font character bitmaps are less than 2 pixels wide:
;
; +------------+---------+
; | ASCII code | Address |
; +------------+---------+
; | 33 [!]     | 55585   |
; | 105 [i]    | 55657   |
; | 108 [l]    | 55660   |
; | 124 [|]    | 55676   |
; +------------+---------+
  DEFB 0,0,0,254,42,16,146,128,130,124,124,8,2,8,3,56
  DEFB 138,254,138,146,72,146,146,134,146,146,54,50,20,20,20,138
  DEFB 82,144,146,130,130,146,144,130,16,254,2,16,2,64,96,130
  DEFB 144,130,144,146,128,2,28,2,40,32,138,129,56,255,64,1
  DEFB 126,34,34,34,34,42,126,37,32,0,1,16,0,32,16,34
  DEFB 36,36,16,42,126,2,6,6,20,5,42,102,0,102,16,146

; UDG references for animatory states 0-127 at row 2, column 0
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 55765   |
; | 93    | 55773   |
; | 100   | 55780   |
; | 101   | 55781   |
; | 109   | 55789   |
; | 117   | 55797   |
; | 125   | 55805   |
; +-------+---------+
  DEFB 97,104,97,104,112,0,0,141,132,137,130,126,241,241,141,226
  DEFB 97,104,97,104,112,0,0,141,0,234,130,126,0,0,0,0
  DEFB 97,104,97,104,112,0,0,252,132,137,0,0,87,87,87,0
  DEFB 97,104,97,104,112,0,0,0,0,215,0,215,202,0,0,222
  DEFB 165,170,165,170,173,178,0,81,186,193,186,193,202,0,0,0
  DEFB 132,144,132,144,0,0,0,132,194,205,194,205,0,0,0,0
  DEFB 0,172,0,172,0,0,0,186,0,106,0,106,0,0,123,0
  DEFB 194,233,194,233,0,0,0,0,0,87,0,87,0,0,251,0

; UDG reference table for the middle-floor window when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the
; middle-floor window when shut is at URTMFWSHUT.
URTMFWOPEN:
  DEFB 8,95,80,142
  DEFB 9,94,208,196,9,95,209,197
  DEFB 10,94,210,197,10,95,211,197
  DEFB 11,94,212,197,11,95,213,197
  DEFB 255                ; End marker

; Unused
  DEFS 3

; Font graphic data for CHR$(32-127) (pixel column 3)
;
; Used by the routine at CHR2BUF. The following addresses are unused, because
; the corresponding font character bitmaps are less than 3 pixels wide:
;
; +---------------+-------------+
; | ASCII code(s) | Address(es) |
; +---------------+-------------+
; | 33 [!]        | 55841       |
; | 39-41 ['()]   | 55847-55849 |
; | 44 [,]        | 55852       |
; | 46 [.]        | 55854       |
; | 58-59 [:;]    | 55866-55867 |
; | 91 [[]        | 55899       |
; | 93 []]        | 55901       |
; | 105 [i]       | 55913       |
; | 108 [l]       | 55916       |
; | 124 [|]       | 55932       |
; +---------------+-------------+
  DEFB 0,0,192,40,127,228,146,0,0,0,56,62,0,8,0,192
  DEFB 146,2,146,178,254,146,146,152,146,146,0,0,20,20,20,144
  DEFB 94,144,146,130,130,146,144,138,16,130,2,40,2,48,24,130
  DEFB 144,134,152,146,254,2,2,28,16,30,146,0,3,0,191,1
  DEFB 146,34,34,34,34,42,144,37,32,0,94,40,0,30,32,34
  DEFB 36,36,32,42,16,2,56,56,8,5,50,129,0,24,8,170

; UDG references for animatory states 0-127 at row 3, column 0
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 56021   |
; | 93    | 56029   |
; | 100   | 56036   |
; | 101   | 56037   |
; | 109   | 56045   |
; | 117   | 56053   |
; | 125   | 56061   |
; +-------+---------+
  DEFB 0,105,0,110,113,117,122,0,133,133,0,0,235,235,0,227
  DEFB 0,105,0,110,113,117,122,0,231,235,0,0,0,0,0,0
  DEFB 0,105,0,110,113,117,122,253,133,133,0,0,0,0,0,0
  DEFB 0,105,0,110,113,117,122,0,0,194,0,199,203,206,208,0
  DEFB 0,105,0,110,174,179,182,0,0,194,0,199,203,206,208,0
  DEFB 133,145,133,153,0,0,124,133,0,206,0,215,0,0,124,0
  DEFB 0,173,0,182,0,0,124,0,0,107,0,107,0,0,124,0
  DEFB 0,234,0,244,0,0,248,0,0,206,0,215,0,0,252,0

; UDG reference table for the drinks cabinet door when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the drinks
; cabinet door when open is at URTDCDOPEN.
URTDCDSHUT:
  DEFB 2,190,136,198,2,191,150,204
  DEFB 3,190,137,206,3,191,151,204
  DEFB 255                ; End marker

; UDG reference table for the three cups (empty) on the shelf in the boys'
; skool
;
; Used by the routine at ALTERUDGS.
URTBSCUPSE:
  DEFB 14,25,186,7        ; Leftmost cup
  DEFB 14,27,191,7        ; Middle cup
  DEFB 14,30,186,7        ; Rightmost cup
  DEFB 255                ; End marker

; Unused
  DEFS 2

; Font graphic data for CHR$(32-127) (pixel column 4)
;
; Used by the routine at CHR2BUF. The following addresses are unused, because
; the corresponding font character bitmaps are less than 4 pixels wide:
;
; +---------------+-------------+
; | ASCII code(s) | Address(es) |
; +---------------+-------------+
; | 32-34 [ !"]   | 56096-56098 |
; | 37 [%]        | 56101       |
; | 39-41 ['()]   | 56103-56105 |
; | 44 [,]        | 56108       |
; | 46-47 [./]    | 56110-56111 |
; | 49 [1]        | 56113       |
; | 58-59 [:;]    | 56122-56123 |
; | 73 [I]        | 56137       |
; | 91-93 [[\]]   | 56155-56157 |
; | 105-106 [ij]  | 56169-56170 |
; | 108 [l]       | 56172       |
; | 116 [t]       | 56180       |
; | 118 [v]       | 56182       |
; | 123-125 [{|}] | 56187-56189 |
; +---------------+-------------+
  DEFB 0,0,0,254,42,0,109,0,0,0,124,8,0,8,0,0
  DEFB 124,0,98,204,8,140,140,224,108,124,0,0,34,20,8,96
  DEFB 66,126,108,130,124,130,128,78,254,0,252,198,2,64,254,124
  DEFB 96,124,102,140,128,252,28,2,40,32,162,0,0,0,64,1
  DEFB 130,62,28,34,254,24,64,62,30,0,0,6,0,32,30,28
  DEFB 24,63,16,36,0,62,0,6,20,62,34,0,0,0,16,130

; UDG references for animatory states 0-127 at row 0, column 1
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 56277   |
; | 93    | 56285   |
; | 100   | 56292   |
; | 101   | 56293   |
; | 109   | 56301   |
; | 117   | 56309   |
; | 125   | 56317   |
; +-------+---------+
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 134,146,134,146,0,0,0,134,195,207,195,207,78,0,0,195
  DEFB 162,174,162,174,76,0,0,162,96,108,96,108,75,0,0,96
  DEFB 224,235,224,235,0,0,0,224,95,86,95,86,0,0,0,95

; UDG reference table for the drinks cabinet door when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the drinks
; cabinet door when shut is at URTDCDSHUT.
URTDCDOPEN:
  DEFB 2,190,162,198,2,191,163,198
  DEFB 3,190,164,206,3,191,165,198
  DEFB 255                ; End marker

; UDG reference table for the cup (empty) on the shelf in the girls' kitchen
;
; Used by the routine at INITDOORS.
URTCUP4E:
  DEFB 14,186,102,199
  DEFB 255                ; End marker

; Unused
  DEFS 10

; Font graphic data for CHR$(32-43) (pixel column 5)
;
; Used by the routine at CHR2BUF. The following addresses are unused, because
; the corresponding font character bitmaps are less than 5 pixels wide:
;
; +---------------+-------------+
; | ASCII code(s) | Address(es) |
; +---------------+-------------+
; | 32-34 [ !"]   | 56352-56354 |
; | 37 [%]        | 56357       |
; | 39-41 ['()]   | 56359-56361 |
; +---------------+-------------+
  DEFB 0,0,0,40,36,0,2,0,0,0,16,8

; Up-a-year tune data (segment 1/3)
;
; Used by the routine at PLAYTUNE.
UAYTUNE1:
  DEFB 55,55,55,119,59,127,59,119
  DEFB 55,121,57,121,55,117,51,177
  DEFB 55
  DEFB 255                ; Segment end marker
  DEFW UAYTUNE2           ; Pointer to the next segment

; Font graphic data for CHR$(64) (pixel column 5)
;
; Used by the routine at CHR2BUF.
  DEFB 60

; Theme tune data (segment 1/6)
;
; Used by the routine at PLAYTUNE.
OTSEG1:
  DEFB 59,113,51,183,57,27,91,123,251
  DEFB 255                ; Segment end marker
  DEFW OTSEG2             ; Pointer to the next segment

; Font graphic data for CHR$(77-95) (pixel column 5)
;
; Used by the routine at CHR2BUF. The following addresses are unused, because
; the corresponding font character bitmaps are less than 5 pixels wide:
;
; +---------------+-------------+
; | ASCII code(s) | Address(es) |
; +---------------+-------------+
; | 78-80 [NOP]   | 56398-56400 |
; | 82-83 [RS]    | 56402-56403 |
; | 85 [U]        | 56405       |
; | 91-93 [[\]]   | 56411-56413 |
; +---------------+-------------+
  DEFB 254,0,0
  DEFB 0,2,0,0,128,0,224,252,198,192,194,0,0,0,32,1

; Up-a-year tune data (segment 2/3)
;
; Used by the routine at PLAYTUNE. Segment 1 can be found at UAYTUNE1.
UAYTUNE2:
  DEFB 55,55,119,59,127,59,119,55,121,57
  DEFB 255                ; Segment end marker
  DEFW UAYTUNE3           ; Pointer to the next segment

; Font graphic data for CHR$(109) (pixel column 5)
;
; Used by the routine at CHR2BUF.
  DEFB 30

; Theme tune data (segment 2/6)
;
; Used by the routine at PLAYTUNE. Segment 1 can be found at OTSEG1.
OTSEG2:
  DEFB 49,113,51,119,119,115
  DEFB 255                ; Segment end marker
  DEFW OTSEG3             ; Pointer to the next segment

; Font graphic data for CHR$(119-120) (pixel column 5)
;
; Used by the routine at CHR2BUF.
  DEFB 56,34

; Up-a-year tune data (segment 3/3)
;
; Used by the routine at PLAYTUNE. Segment 2 can be found at UAYTUNE2.
UAYTUNE3:
  DEFB 49,66,53,183,183
  DEFB 0                  ; End marker

; Font graphic data for CHR$(127) (pixel column 5)
;
; Used by the routine at CHR2BUF.
  DEFB 124

; UDG references for animatory states 0-127 at row 1, column 1
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 56533   |
; | 93    | 56541   |
; | 100   | 56548   |
; | 101   | 56549   |
; | 109   | 56557   |
; | 117   | 56565   |
; | 125   | 56573   |
; +-------+---------+
  DEFB 98,106,98,106,98,0,0,98,98,138,98,127,242,242,98,228
  DEFB 158,160,158,160,158,0,0,158,0,0,158,164,0,0,90,0
  DEFB 151,153,151,153,151,0,0,0,151,157,0,249,86,86,86,0
  DEFB 142,146,142,146,142,0,0,0,210,216,210,216,210,0,0,210
  DEFB 166,171,166,171,175,0,0,80,187,195,187,195,204,0,0,185
  DEFB 135,147,135,147,78,0,158,135,196,208,196,208,77,0,220,218
  DEFB 163,175,163,175,0,0,189,187,97,109,97,109,0,0,125,120
  DEFB 225,236,225,236,74,0,224,246,94,85,94,85,73,0,253,81

; UDG reference table for the Science Lab storeroom door when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the Science Lab
; storeroom door when open is at URTSLSOPEN.
URTSLSSHUT:
  DEFB 9,54,50,71,9,55,55,69,9,56,61,69
  DEFB 10,54,51,71,10,55,56,69,10,56,62,69
  DEFB 11,54,52,71,11,55,57,69,11,56,63,78
  DEFB 12,54,53,71,12,55,58,69,12,56,64,78
  DEFB 13,54,54,71,13,55,59,69,13,56,208,14
  DEFB 255                ; End marker

; UDG reference table for the left study door when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the left study
; door when open is at URTLSDOPEN.
URTLSDSHUT:
  DEFB 2,72,128,71,2,73,135,71
  DEFB 3,71,243,6,3,72,129,70,3,73,136,71
  DEFB 4,71,238,6,4,72,130,70,4,73,137,71
  DEFB 5,71,225,14,5,72,131,78,5,73,138,71
  DEFB 6,71,84,14,6,72,132,78,6,73,139,71
  DEFB 255                ; End marker

; UDG reference table for the leftmost cup (containing water) on the shelf in
; the boys' skool
;
; Used by the routine at ALTERUDGS.
URTCUP1W:
  DEFB 14,25,186,5
  DEFB 255                ; End marker

; UDG reference table for the leftmost cup (containing sherry) on the shelf in
; the boys' skool
;
; Used by the routine at ALTERUDGS.
URTCUP1S:
  DEFB 14,25,186,11
  DEFB 255                ; End marker

; UDG references for animatory states 0-127 at row 2, column 1
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 56789   |
; | 93    | 56797   |
; | 100   | 56804   |
; | 101   | 56805   |
; | 109   | 56813   |
; | 117   | 56821   |
; | 125   | 56829   |
; +-------+---------+
  DEFB 99,107,99,107,114,118,0,99,134,139,128,128,243,246,99,229
  DEFB 99,107,99,107,114,161,0,99,0,236,128,128,0,93,0,0
  DEFB 99,107,99,107,114,154,0,254,134,139,248,250,85,84,83,0
  DEFB 143,147,143,147,114,148,0,0,211,217,211,217,218,210,0,223
  DEFB 167,172,167,172,176,180,0,79,188,196,188,196,205,187,0,0
  DEFB 136,148,136,148,79,0,159,136,197,209,197,209,0,0,221,219
  DEFB 164,176,164,176,0,0,190,188,98,110,98,110,0,0,126,121
  DEFB 226,237,226,237,73,0,225,247,93,84,93,84,73,0,0,93

; UDG reference table for the Science Lab storeroom door when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the Science Lab
; storeroom door when shut is at URTSLSSHUT.
URTSLSOPEN:
  DEFB 9,54,166,199,9,55,167,199,9,56,168,199
  DEFB 10,54,51,71,10,55,85,5,10,56,169,197
  DEFB 11,54,170,207,11,55,81,15,11,56,171,207
  DEFB 12,54,172,207,12,55,81,15,12,56,87,15
  DEFB 13,54,173,207,13,55,83,207,13,56,174,207
  DEFB 255                ; End marker

; UDG reference table for the left study door when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the left study
; door when shut is at URTLSDSHUT.
URTLSDOPEN:
  DEFB 2,72,128,71,2,73,175,192
  DEFB 3,71,176,207,3,72,177,207,3,73,178,196
  DEFB 4,71,179,207,4,72,180,207,4,73,181,199
  DEFB 5,71,182,207,5,72,81,15,5,73,183,199
  DEFB 6,71,103,15,6,72,81,15,6,73,184,199
  DEFB 255                ; End marker

; UDG reference table for the middle cup (containing water) on the shelf in the
; boys' skool
;
; Used by the routine at ALTERUDGS.
URTCUP2W:
  DEFB 14,27,191,5
  DEFB 255                ; End marker

; UDG reference table for the middle cup (containing sherry) on the shelf in
; the boys' skool
;
; Used by the routine at ALTERUDGS.
URTCUP2S:
  DEFB 14,27,191,11
  DEFB 255                ; End marker

; UDG references for animatory states 0-127 at row 3, column 1
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 57045   |
; | 93    | 57053   |
; | 100   | 57060   |
; | 101   | 57061   |
; | 109   | 57069   |
; | 117   | 57077   |
; | 125   | 57085   |
; +-------+---------+
  DEFB 100,108,109,111,115,119,123,100,135,135,100,100,244,247,100,230
  DEFB 100,108,109,111,115,119,123,100,232,237,100,100,94,0,0,89
  DEFB 100,108,109,111,115,119,123,255,135,135,0,0,0,0,0,251
  DEFB 100,108,109,111,115,119,123,88,189,197,198,200,0,207,219,189
  DEFB 100,108,109,111,177,181,183,78,189,197,198,200,0,207,219,0
  DEFB 137,149,152,154,0,0,127,137,198,210,214,216,0,0,127,198
  DEFB 165,177,180,183,0,0,127,165,99,111,116,111,0,0,127,99
  DEFB 227,238,242,245,73,0,249,227,198,210,214,216,73,0,254,198

; UDG reference table for the right study door when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the right study
; door when open is at URTRSDOPEN.
URTRSDSHUT:
  DEFB 2,84,211,68,2,85,103,6,2,86,81,6
  DEFB 3,84,212,70,3,85,103,6,3,86,81,6
  DEFB 4,84,213,70,4,85,218,70,4,86,225,70
  DEFB 5,84,214,70,5,85,219,78,5,86,226,78
  DEFB 6,84,214,70,6,85,220,78,6,86,227,78
  DEFB 255                ; End marker

; UDG reference table for the boys' skool door when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the boys' skool
; door when open is at URTBSDOPEN.
URTBSDSHUT:
  DEFB 16,93,57,134,16,94,152,78,16,95,86,140
  DEFB 17,93,57,134,17,94,152,78,17,95,87,140
  DEFB 18,93,57,134,18,94,152,78,18,95,88,140
  DEFB 19,93,58,134,19,94,73,142,19,95,80,12
  DEFB 20,94,80,6,20,95,80,12
  DEFB 255                ; End marker

; UDG reference table for the rightmost cup (containing water) on the shelf in
; the boys' skool
;
; Used by the routine at ALTERUDGS.
URTCUP3W:
  DEFB 14,30,186,5
  DEFB 255                ; End marker

; UDG reference table for the rightmost cup (containing sherry) on the shelf in
; the boys' skool
;
; Used by the routine at ALTERUDGS.
URTCUP3S:
  DEFB 14,30,186,11
  DEFB 255                ; End marker

; UDG references for animatory states 0-127 at row 0, column 2
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 57301   |
; | 93    | 57309   |
; | 100   | 57316   |
; | 101   | 57317   |
; | 109   | 57325   |
; | 117   | 57333   |
; | 125   | 57341   |
; +-------+---------+
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
  DEFB 138,0,138,0,0,0,0,138,199,138,199,138,0,0,0,199
  DEFB 166,0,166,0,78,0,0,166,100,112,100,112,76,0,0,100
  DEFB 228,0,228,0,0,0,0,228,92,0,92,0,0,0,0,92

; UDG reference table for the right study door when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the right study
; door when shut is at URTRSDSHUT.
URTRSDOPEN:
  DEFB 2,84,185,196,2,85,186,198,2,86,187,198
  DEFB 3,84,188,198,3,85,189,198,3,86,118,70
  DEFB 4,84,190,198,4,85,191,198,4,86,192,198
  DEFB 5,84,58,198,5,85,193,206,5,86,194,206
  DEFB 6,84,58,198,6,85,81,14,6,86,195,206
  DEFB 255                ; End marker

; UDG reference table for the boys' skool door when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the boys' skool
; door when shut is at URTBSDSHUT.
URTBSDOPEN:
  DEFB 16,93,196,198,16,94,197,206,16,95,198,196
  DEFB 17,93,199,198,17,94,200,206,17,95,81,4
  DEFB 18,93,173,70,18,94,182,78,18,95,81,4
  DEFB 19,93,80,6,19,94,182,78,19,95,81,4
  DEFB 20,94,208,6,20,95,201,196
  DEFB 255                ; End marker

; UDG reference table for the cup (containing water) on the shelf in the girls'
; skool
;
; Used by the routine at ALTERUDGS.
URTCUP4W:
  DEFB 14,186,102,197
  DEFB 255                ; End marker

; UDG reference table for the cup (containing sherry) on the shelf in the
; girls' skool
;
; Used by the routine at ALTERUDGS.
URTCUP4S:
  DEFB 14,186,102,203
  DEFB 255                ; End marker

; UDG references for animatory states 0-127 at row 1, column 2
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 57557   |
; | 93    | 57565   |
; | 100   | 57572   |
; | 101   | 57573   |
; | 109   | 57581   |
; | 117   | 57589   |
; | 125   | 57597   |
; +-------+---------+
  DEFB 101,0,101,0,101,0,0,101,101,101,101,101,0,0,101,0
  DEFB 101,0,101,0,101,0,0,101,0,0,101,101,0,0,0,0
  DEFB 101,0,101,0,101,0,0,0,101,101,0,0,0,0,0,0
  DEFB 144,0,144,0,144,0,0,0,212,0,212,0,212,0,0,212
  DEFB 0,0,0,0,0,0,0,77,0,0,0,0,0,0,0,0
  DEFB 139,138,139,138,0,0,160,139,200,211,200,211,78,0,222,200
  DEFB 167,151,167,151,77,0,191,167,101,113,101,113,0,0,128,101
  DEFB 229,239,229,239,0,0,228,229,91,138,91,138,0,0,0,91

; UDG reference table for the tree with no bike attached
;
; Used by the routine at ALTERUDGS. The UDG reference table for the tree with
; the bike attached is at URTBIKE.
URTTREE:
  DEFB 18,100,153,132,18,101,171,132,18,102,189,132
  DEFB 19,100,154,132,19,101,172,132,19,102,190,132
  DEFB 20,100,81,4,20,101,81,4,20,102,81,4
  DEFB 255                ; End marker

; 'C' pressed - catch a mouse or the frog
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'C' is
; pressed.
CATCH:
  LD A,39                 ; 39=ERIC bending over, facing left
  LD HL,CATCHMORF         ; The routine at CATCHMORF deals with ERIC after 'C'
                          ; has been pressed
; This entry point is used by the routines at THROW, KISS, DROPSBOMB, RELEASE
; and FIREWP with ERIC's new animatory state in A (bit 7 reset) and an
; appropriate routine address in HL.
CATCH_0:
  LD (ERICDATA1),HL       ; Place into ERICDATA1 the address of the routine
                          ; that will deal with ERIC after we've finished here
  LD HL,STATUS            ; Set bit 3 at STATUS: ERIC is bending over, firing
  LD (HL),8               ; the water pistol, moving forward as if to kiss, or
                          ; dropping a stinkbomb
; This entry point is used by the routine at STARTFHK with ERIC's new animatory
; state in A (bit 7 reset).
CATCH_1:
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC's buffer
  LD BC,3
  BIT 7,(HL)              ; Is ERIC facing left?
  JR Z,CATCH_2            ; Jump if so
  ADD A,128               ; Set bit 7 of ERIC's new animatory state so he's
                          ; still facing right
CATCH_2:
  LD D,H                  ; Point DE at byte 3 of ERIC's buffer
  LD E,C                  ;
  LDIR                    ; Copy bytes 0-2 of ERIC's buffer (which hold ERIC's
                          ; current animatory state and location) into bytes
                          ; 3-5 for retrieval when ERIC has completed this
                          ; action
  JP SITDOWN_1            ; Update ERIC's animatory state and update the SRB

; Deal with ERIC when he's bending over, dropping a stinkbomb etc.
;
; This routine is called by the routine at HANDLEERIC when bit 3 at STATUS is
; set by the routine at CATCH, indicating that ERIC is bending over (to throw
; away the water pistol, release a mouse, or catch a mouse or the frog),
; dropping a stinkbomb, firing the water pistol, or moving forward as if to
; kiss.
BENDING:
  XOR A                   ; Reset all flags at STATUS, indicating that ERIC has
  LD (STATUS),A           ; finished bending over, dropping a stinkbomb etc.
  LD HL,5126              ; Set ERIC's main action timer at ERICTIMER to 20
  LD (ERICTIMER2),HL      ; (the delay before ERIC completes this action), and
                          ; the mid-action timer at ERICTIMER2 to 6 (the delay
                          ; after ERIC completes this action)
; This entry point is used by the routine at MIDFHK (when bit 5 at STATUS is
; set).
BENDING_0:
  LD HL,(ERICDATA1)       ; Collect in HL the routine address stored in
  PUSH HL                 ; ERICDATA1, and push it onto the stack ready for an
                          ; indirect jump
  LD H,210                ; 210=ERIC
  RET                     ; Jump to the routine whose address is stored in
                          ; ERICDATA1

; Addresses of closed door/window UDG reference tables
;
; Used by the routines at INITDOORS and MVDOORWIN. The door/gate/cups/bike
; initialisation table follows at URTINIT.
DWURTINIT:
  DEFW URTLSDSHUT         ; Left study door (closed)
  DEFW URTRSDSHUT         ; Right study door (closed)
  DEFW URTSLSSHUT         ; Science Lab storeroom door (closed)
  DEFW URTBSDSHUT         ; Boys' skool door (closed)
  DEFW URTSGSHUT          ; Skool gate (closed)
  DEFW URTDCDSHUT         ; Drinks cabinet door (closed)
  DEFW URTTFWSHUT         ; Top-floor window (closed)
  DEFW URTMFWSHUT         ; Middle-floor window (closed)

; Door/gate/cups/bike initialisation table
;
; Used by the routine at INITDOORS. Follows on from the table at DWURTINIT
; (which is used to close all the doors and windows first).
URTINIT:
  DEFW URTBIKE            ; Tree with bike attached
  DEFW URTSGOPEN          ; Skool gate (open)
  DEFW URTBSDOPEN         ; Boys' skool door (open)
  DEFW URTBSCUPSE         ; Cups on the shelf in the boys' skool (empty)
  DEFW URTCUP4E           ; Cup on the shelf in the girls' skool (empty)

; UDG back buffer
;
; Used by the routines at GETTILE and PRINTTILE.
BACKBUF:
  DEFS 8

; SRB bit translation table
;
; Used by the routines at UPDATEAS and SHOWBUBBLE. Given an integer N between 0
; and 7, this table is used to obtain a byte with bit 7-N set and all other
; bits reset.
SRBBITS:
  DEFB 128                ; 10000000
  DEFB 64                 ; 01000000
  DEFB 32                 ; 00100000
  DEFB 16                 ; 00010000
  DEFB 8                  ; 00001000
  DEFB 4                  ; 00000100
  DEFB 2                  ; 00000010
  DEFB 1                  ; 00000001

; UDG references for animatory states 0-127 at row 2, column 2
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 57813   |
; | 93    | 57821   |
; | 100   | 57828   |
; | 101   | 57829   |
; | 109   | 57837   |
; | 117   | 57845   |
; | 125   | 57853   |
; +-------+---------+
  DEFB 102,0,102,0,116,120,0,102,102,102,131,129,245,245,102,0
  DEFB 102,0,102,0,116,162,0,102,0,238,131,129,0,92,0,0
  DEFB 102,0,102,0,116,155,0,95,102,102,0,0,0,0,0,0
  DEFB 102,0,102,0,116,149,0,0,213,0,213,0,213,212,0,213
  DEFB 168,0,168,0,0,0,0,76,190,0,190,0,0,0,0,0
  DEFB 140,150,140,150,0,0,161,140,201,212,201,212,77,0,223,201
  DEFB 168,178,168,178,0,0,192,168,102,114,102,114,0,0,129,102
  DEFB 230,240,230,240,0,0,229,230,90,83,90,83,0,0,0,90

; UDG reference table for the tree with the bike attached
;
; Used by the routine at ALTERUDGS. The UDG reference table for the tree with
; no bike attached is at URTTREE.
URTBIKE:
  DEFB 18,100,214,196,18,101,215,196,18,102,216,196
  DEFB 19,100,217,196,19,101,218,196,19,102,219,196
  DEFB 20,100,220,196,20,101,221,196,20,102,222,196
  DEFB 255                ; End marker

; 'R' pressed - release mice
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'R' is
; pressed.
;
; H 210 (ERIC)
RELEASE:
  CALL ONTMBFLOOR         ; Is ERIC on a staircase or the assembly hall stage?
  RET NZ                  ; Return if so
  DEC L                   ; L=1
  LD A,(HL)               ; A=ERIC's x-coordinate
  CP 160                  ; Is ERIC in the girls' skool?
  RET C                   ; Return if not
  LD A,39                 ; 39: animatory state of ERIC bending over
  LD HL,DROPMICE          ; Routine at DROPMICE: release some mice
  JP CATCH_0              ; Set ERIC's new animatory state

; Prepare buffer 213 for use by a stinkbomb cloud
;
; Used by the routines at DROPSBOMB and CLOUD. Prepares buffer 213 (normally
; used by BOY WANDER's catapult pellet) for use by a stinkbomb cloud. Returns
; to the main loop if the buffer is already in use by a stinkbomb cloud;
; otherwise returns to the caller with buffer 213 ready to go.
PREPSBBUF:
  LD HL,54529             ; Point HL at byte 1 of buffer 213
  LD A,(HL)               ; A=x-coordinate of the object that last used or is
                          ; now using this buffer
  CP 192                  ; Is this buffer being used now?
  JR NC,PREPSBBUF_0       ; Jump if not
  DEC L                   ; L=0
  LD A,(HL)               ; A=animatory state of the object that is using this
                          ; buffer
  POP BC                  ; Pop the return address from the stack into BC
  AND 127                 ; Drop the 'direction' bit (bit 7) of the object's
                          ; animatory state
  CP 79                   ; 79: Is this buffer being used by BOY WANDER's
                          ; catapult pellet?
  RET NZ                  ; Return to the main loop if not
  PUSH BC                 ; Restore the return address (to the caller of this
                          ; routine) to the stack
  CALL OBJFALL_0          ; Terminate BOY WANDER's catapult pellet
PREPSBBUF_0:
  LD H,210                ; 210=ERIC
  RET

; Unused
  DEFS 2

; Check whether buffer 214 is being used
;
; Used by the routines at FIRE and FIREWP. Returns to the main loop if buffer
; 214 is already in use (by ERIC's catapult pellet, water or sherry fired from
; the pistol or falling from a cup, a plant, the conker, or a desk lid);
; otherwise returns to the caller.
CHECK214:
  LD A,(54802)            ; Is there an uninterruptible subcommand routine
  AND A                   ; address in bytes 17 and 18 of the buffer in page
                          ; 214?
  RET Z                   ; Return if not (the buffer is free)
  POP BC                  ; Otherwise drop the return address from the stack
  RET                     ; and return to the main loop

; Command list 20: Assembly - EINSTEIN/little boy
;
; Used by little boys 1-4 and 10 and also by EINSTEIN in lesson 52.
CLIST20:
  DEFW GOTO               ; Go to...
  DEFB 71,17              ; ...the assembly hall
  DEFW MVTILL             ; Move about until...
  DEFB 7                  ; ...it's time to start assembly
  DEFW INASSEMBLY         ; Sit down in the assembly hall until assembly is
                          ; finished
  DEFW GOTORAND           ; Go to a random location
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Command list 28: Girls' skool walkabout - MISS TAKE
;
; Used by MISS TAKE in lessons 56 and 58.
CLIST28:
  DEFW GOTO               ; Go to...
  DEFB 188,3              ; ...the drinks cabinet
  DEFW MOVEDOOR           ; Move the...
  DEFB 32,0               ; ...drinks cabinet door (close it)
  DEFW CLSTART            ; Make the next command be the start of the command
                          ; list
  DEFW GOTORAND           ; Go to a random location
  DEFW RESTART            ; Restart the command list

; Command list 30: Dinner duty
;
; Used by MR WACKER in lesson 50, and by MR WITHIT in lesson 51.
CLIST30:
  DEFW GOTO               ; Go to...
  DEFB 61,17              ; ...the far right end of the dinner hall in the
                          ; boys' skool
  DEFW DINDUTY            ; Go after ERIC if necessary
  DEFW GOTO               ; Go to...
  DEFB 40,17              ; ...the far left end of the dinner hall in the boys'
                          ; skool
  DEFW DINDUTY            ; Go after ERIC if necessary
  DEFW RESTART            ; Restart the command list

; UDG references for animatory states 0-127 at row 3, column 2
;
; Used by the routine at GETTILE. The following animatory states and
; corresponding addresses are unused:
;
; +-------+---------+
; | State | Address |
; +-------+---------+
; | 85    | 58069   |
; | 93    | 58077   |
; | 100   | 58084   |
; | 101   | 58085   |
; | 109   | 58093   |
; | 117   | 58101   |
; | 125   | 58109   |
; +-------+---------+
  DEFB 0,0,0,0,0,121,124,0,102,102,0,0,239,239,0,0
  DEFB 0,0,0,0,0,121,163,0,233,239,0,0,0,0,0,0
  DEFB 0,0,0,0,0,121,156,0,102,102,0,0,0,0,0,0
  DEFB 0,0,0,0,0,121,150,0,191,0,191,0,0,0,220,191
  DEFB 0,0,0,0,0,0,184,0,191,0,191,0,0,0,209,0
  DEFB 141,151,141,155,80,0,130,141,202,213,202,151,0,0,130,202
  DEFB 169,179,181,184,0,0,130,169,103,115,117,115,0,0,130,103
  DEFB 231,241,231,241,0,0,250,231,202,213,202,151,0,0,255,202

; Message 12: score/lines total/hi-score/number of lines being given
;
; Used by the routines at PRINTNUM2 and NUM2ASCII. The character codes of the
; digits of the number are stored here; the corresponding graphic data is
; generated and stored in the buffer at NBUFG.
MSG012:
  DEFS 11

; Number graphic buffer
;
; Used by the routine at PRINTNUM2. Holds the graphic data for the number
; stored in the buffer at MSG012.
NBUFG:
  DEFS 21

; Unused
;
; The 8 bytes here are skool graphic data that is copied into page 128 by the
; startup routine after the game has loaded; however, this slice of RAM remains
; unused thereafter.
  DEFB 255,255,255,255,255,255,255,255

; Temporary store for the part of the screen overwritten by a message box
;
; Used by the routines at SCR2BUF and GIVELINES.
OVERBUF:
  DEFS 24                 ; The attributes go here
  DEFS 192                ; The graphic data goes here

; Command list 22: Top-floor classroom - MISS TAKE
;
; Used by MISS TAKE in lessons 37, 39, 41, 44, 46, 49, 53 and 54.
CLIST22:
  DEFW GOTO               ; Go to...
  DEFB 183,3              ; ...the doorway of MISS TAKE's study
  DEFW GOTO               ; Go to...
  DEFB 188,3              ; ...the drinks cabinet
  DEFW RSTDROPEN          ; Close the drinks cabinet door if it's open, and
                          ; then restart the command list unless the boys'
                          ; skool door is closed
  DEFW MOVEDOOR           ; Move the...
  DEFB 32,1               ; ...drinks cabinet door (open it)
  DEFW CLSTART            ; Make the next command be the start of the command
                          ; list
  DEFW GOTO               ; Go to...
  DEFB 184,3              ; ...the doorway of MISS TAKE's study
  DEFW GOTO               ; Go to...
  DEFB 180,3              ; ...the doorway of the top-floor classroom in the
                          ; girls' skool
  DEFW RSTTELLSIT         ; Restart the command list, or tell the kids to sit
                          ; down and signal that...
  DEFB 8                  ; ...the teacher has arrived at the top-floor
                          ; classroom in the girls' skool
  DEFW GOTO               ; Go to...
  DEFB 169,3              ; ...the blackboard in the top-floor classroom in the
                          ; girls' skool
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Unused
;
; Remnants of the message 'Culloden' in Skool Daze.
  DEFM "den"
  DEFB 0                  ; End marker
  DEFS 3

; Buffer holding attributes and graphic data for a message box
;
; Used by the routine at PRTMSGBOX.
MSGBOX:
  DEFS 24                 ; The attributes go here
MSGBOXG:
  DEFS 192                ; The graphic data goes here

; Keypress offset table
;
; Used by the routine at KEYOFFSET. Each non-zero entry in this table
; corresponds to a game key, and points to an entry in the table of
; keypress-handling routine addresses at K_LEFTF. For more details, see the
; keypress table.
KEYTABLE:
  DEFB 104                ; '0': Fire catapult
  DEFB 0                  ; '1': unused
  DEFB 0                  ; '2': unused
  DEFB 0                  ; '3': unused
  DEFB 0                  ; '4': unused
  DEFB 80                 ; '5': Left
  DEFB 86                 ; '6': Down
  DEFB 84                 ; '7': Up
  DEFB 82                 ; '8': Right
  DEFB 0                  ; '9': unused
  DEFB 0                  ; ':': unused
  DEFB 0                  ; ';': unused
  DEFB 0                  ; '<': unused
  DEFB 0                  ; '=': unused
  DEFB 0                  ; '>': unused
  DEFB 0                  ; '?': unused
  DEFB 0                  ; '@': unused
  DEFB 86                 ; 'A': Down
  DEFB 106                ; 'B': Mount bike
  DEFB 100                ; 'C': Catch mouse/frog
  DEFB 108                ; 'D': Drop stinkbomb
  DEFB 0                  ; 'E': unused
  DEFB 104                ; 'F': Fire catapult
  DEFB 110                ; 'G': Fire water pistol
  DEFB 112                ; 'H': Hit
  DEFB 0                  ; 'I': unused
  DEFB 114                ; 'J': Jump
  DEFB 116                ; 'K': Kiss
  DEFB 114                ; 'L': Jump
  DEFB 106                ; 'M': Mount bike
  DEFB 0                  ; 'N': unused
  DEFB 80                 ; 'O': Left
  DEFB 82                 ; 'P': Right
  DEFB 84                 ; 'Q': Up
  DEFB 102                ; 'R': Release mice
  DEFB 98                 ; 'S': Sit
  DEFB 118                ; 'T': Throw away water pistol
  DEFB 108                ; 'U': Drop stinkbomb
  DEFB 0                  ; 'V': unused
  DEFB 120                ; 'W': Write
  DEFB 0                  ; 'X': unused
  DEFB 0                  ; 'Y': unused
  DEFB 0                  ; 'Z': unused
  DEFB 0                  ; '[': unused
  DEFB 0                  ; '\': unused
  DEFB 0                  ; ']': unused
  DEFB 0                  ; '↑': unused
  DEFB 0                  ; '_': unused
  DEFB 0                  ; '£': unused
  DEFB 94                 ; 'a': Down
  DEFB 106                ; 'b': Mount bike
  DEFB 100                ; 'c': Catch mouse/frog
  DEFB 108                ; 'd': Drop stinkbomb
  DEFB 0                  ; 'e': unused
  DEFB 104                ; 'f': Fire catapult
  DEFB 110                ; 'g': Fire water pistol
  DEFB 112                ; 'h': Hit
  DEFB 0                  ; 'i': unused
  DEFB 114                ; 'j': Jump
  DEFB 116                ; 'k': Kiss
  DEFB 114                ; 'l': Jump
  DEFB 106                ; 'm': Mount bike
  DEFB 0                  ; 'n': unused
  DEFB 88                 ; 'o': Left
  DEFB 90                 ; 'p': Right
  DEFB 92                 ; 'q': Up
  DEFB 102                ; 'r': Release mice
  DEFB 98                 ; 's': Sit
  DEFB 118                ; 't': Throw away water pistol
  DEFB 108                ; 'u': Drop stinkbomb
  DEFB 0                  ; 'v': unused
  DEFB 120                ; 'w': Write
  DEFB 0                  ; 'x': unused
  DEFB 0                  ; 'y': unused
  DEFB 0                  ; 'z': unused
  DEFB 0                  ; '{': unused
  DEFB 0                  ; '|': unused
  DEFB 0                  ; '}': unused
  DEFB 0                  ; '~': unused
  DEFB 0                  ; '©': unused

; Addresses of keypress handling routines
;
; Used by the main loop at MAINLOOP, and by the routine at KEYOFFSET. Each
; non-zero entry in the keypress offset table at KEYTABLE points to a routine
; address in this table. For more details, see the keypress table.
K_LEFTF:
  DEFW LEFT               ; 80: Left (fast)
K_RIGHTF:
  DEFW RIGHT              ; 82: Right (fast)
K_UPF:
  DEFW UP                 ; 84: Up (fast)
K_DOWNF:
  DEFW DOWN               ; 86: Down (fast)
K_LEFT:
  DEFW LEFT               ; 88: Left (slow)
K_RIGHT:
  DEFW RIGHT              ; 90: Right (slow)
K_UP:
  DEFW UP                 ; 92: Up (slow)
K_DOWN:
  DEFW DOWN               ; 94: Down (slow)
  DEFW 0                  ; 96: Unused
K_SIT:
  DEFW SIT                ; 98: Sit
K_CATCH:
  DEFW CATCH              ; 100: Catch mouse/frog
K_RELEASE:
  DEFW RELEASE            ; 102: Release mice
K_FIRE:
  DEFW FIRE               ; 104: Fire catapult
K_MOUNT:
  DEFW MOUNTBIKE          ; 106: Mount bike
K_SBOMB:
  DEFW DROPSBOMB          ; 108: Drop stinkbomb
K_WPISTOL:
  DEFW FIREWP             ; 110: Fire water pistol
K_HIT:
  DEFW HIT                ; 112: Hit
K_JUMP:
  DEFW JUMP               ; 114: Jump
K_KISS:
  DEFW KISS               ; 116: Kiss
K_THROWWP:
  DEFW THROW              ; 118: Throw away water pistol
K_WRITE:
  DEFW WRITE              ; 120: Write

; Unused
  DEFS 22

; Command list 32: Science Lab - BOY WANDER
;
; Used by BOY WANDER in lessons 38, 45, 47 and 49.
CLIST32:
  DEFW ADDR2CBUF          ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 35,10              ; ...the Science Lab blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 12                 ; ...the teacher has arrived at the Science Lab
; This command list continues at CLIST34.

; Command list 34: Science Lab - ANGELFACE
;
; Used by ANGELFACE in lessons 40, 45, 47 and 48. Command list 32 also
; continues here.
CLIST34:
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW GOTO               ; Go to...
  DEFB 48,10              ; ...the Science Lab
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 12                 ; ...the teacher arrives at the Science Lab
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Command list 36: Revision Library - ANGELFACE/BOY WANDER
;
; Used by BOY WANDER in lessons 42, 46 and 53, and by ANGELFACE in lessons 44,
; 49, 53 and 54.
CLIST36:
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW GOTO               ; Go to...
  DEFB 36,3               ; ...the Revision Library
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the end of the lesson

; Command list 38: Dinner - ANGELFACE/BOY WANDER
;
; Used by BOY WANDER and ANGELFACE in lessons 50 and 51.
CLIST38:
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW GOTO               ; Go to...
  DEFB 53,17              ; ...the dinner hall in the boys' skool
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Command list 42: Walkabout - ANGELFACE/BOY WANDER
;
; Used by BOY WANDER in lessons 58 and 59, and by ANGELFACE in lessons 56 and
; 58.
CLIST42:
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW GOTORAND           ; Go to a random location
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 0,9                ; ...9 times
  DEFW RESTART            ; Restart the command list

; Command list 44: Stalk HAYLEY - ANGELFACE
;
; Used by ANGELFACE in lessons 55, 57 and 59.
CLIST44:
  DEFW ADDR2CBUF          ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW HITHAYLEY          ; ...stalk HAYLEY
  DEFW GOTO               ; Go to...
  DEFB 120,17             ; ...wherever HAYLEY's going (HAYLEY's coordinates
                          ; are placed here by the routine at HITHAYLEY)
  DEFW ADDR2CBUF          ; Put the next address in ANGELFACE's buffer, making
                          ; him...
  DEFW HITHAYLEY          ; ...stalk HAYLEY
  DEFW WALKABOUT          ; Walk up and down...
  DEFB 0,9                ; ...9 times
  DEFW RESTART            ; Restart the command list

; Command list 88: Assembly - little boy
;
; Used by little boys 5-9 in lesson 52.
CLIST88:
  DEFW GOTO               ; Go to...
  DEFB 75,17              ; ...the assembly hall
  DEFW MVTILL             ; Move about until...
  DEFB 7                  ; ...it's time to start assembly
  DEFW INASSEMBLY         ; Sit down in the assembly hall until assembly is
                          ; finished
  DEFW GOTORAND           ; Go to a random location
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFS 4

; Command list 24: Middle-floor classroom - MISS TAKE
;
; Used by MISS TAKE in lessons 38, 40, 42, 43, 45, 47 and 48.
CLIST24:
  DEFW GOTO               ; Go to...
  DEFB 183,3              ; ...the doorway of MISS TAKE's study
  DEFW GOTO               ; Go to...
  DEFB 188,3              ; ...the drinks cabinet
  DEFW RSTDROPEN          ; Close the drinks cabinet door (if it's open), and
                          ; then restart the command list unless the boys'
                          ; skool door is closed
  DEFW MOVEDOOR           ; Move the...
  DEFB 32,1               ; ...drinks cabinet door (open it)
  DEFW CLSTART            ; Make the next command be the start of the command
                          ; list
  DEFW GOTO               ; Go to...
  DEFB 188,10             ; ...the window on the middle floor in the girls'
                          ; skool
  DEFW GOTO               ; Go to...
  DEFB 179,10             ; ...the doorway of the middle-floor classroom in the
                          ; girls' skool
  DEFW RSTTELLSIT         ; Restart the command list, or tell the kids to sit
                          ; down and signal that...
  DEFB 9                  ; ...the teacher has arrived at the middle-floor
                          ; classroom in the girls' skool
  DEFW GOTO               ; Go to...
  DEFB 169,10             ; ...the blackboard in the middle-floor classroom in
                          ; the girls' skool
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Unused
;
; Remnants of the message 'QUADRATIC EQUATIONS' in Skool Daze.
  DEFM "UADRATI"

; 'Score - 0 Lines - 0 Hi-Sc - 0' box graphic
;
; Used by the routine at PREPGAME.
SCOREBOX:
  DEFB 31,31,31,31,31,31,31,31 ; Attributes (PAPER 3: INK 7)
  DEFB 31,31,31,31,31,31,31,31 ;
  DEFB 31,31,31,31,31,31,31,31 ;
  DEFB 0,0,0,0,0,0,0,0         ; Graphic data
  DEFB 56,0,0,0,0,0,0,96       ;
  DEFB 64,0,0,0,0,0,0,144      ;
  DEFB 65,204,163,0,0,0,0,144  ;
  DEFB 50,18,212,128,0,0,0,176 ;
  DEFB 10,18,135,158,0,0,0,208 ;
  DEFB 10,18,132,0,0,0,0,144   ;
  DEFB 113,216,131,0,0,0,0,96  ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 64,0,0,0,0,0,0,96       ;
  DEFB 66,0,0,0,0,0,0,144      ;
  DEFB 64,163,28,0,0,0,0,144   ;
  DEFB 66,212,160,0,0,0,0,176  ;
  DEFB 66,151,152,30,0,0,0,208 ;
  DEFB 66,148,4,0,0,0,0,144    ;
  DEFB 122,147,56,0,0,0,0,96   ;
  DEFB 0,0,0,0,0,0,0,0         ;
  DEFB 72,7,0,0,0,0,0,96       ;
  DEFB 74,8,0,0,0,0,0,144      ;
  DEFB 72,8,56,0,0,0,0,144     ;
  DEFB 122,6,64,0,0,0,0,176    ;
  DEFB 74,225,64,30,0,0,0,208  ;
  DEFB 74,1,64,0,0,0,0,144     ;
  DEFB 74,14,56,0,0,0,0,96     ;

; Command list 26: Kitchen walkabout - MISS TAKE
;
; Used by MISS TAKE in lessons 50, 51, 52, 55, 57 and 59.
CLIST26:
  DEFW GOTO               ; Go to...
  DEFB 188,3              ; ...the drinks cabinet
  DEFW MOVEDOOR           ; Move the...
  DEFB 32,0               ; ...drinks cabinet door (close it)
  DEFW GOTO               ; Go to...
  DEFB 189,17             ; ...the kitchen
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Command list 40: Assembly - ANGELFACE/BOY WANDER
;
; Used by BOY WANDER and ANGELFACE in lesson 52.
CLIST40:
  DEFW GOTO               ; Go to...
  DEFB 73,17              ; ...the assembly hall
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 7                  ; ...it's time to start assembly
  DEFW INASSEMBLY         ; Sit down in the assembly hall until assembly is
                          ; finished
  DEFW GOTORAND           ; Go to a random location
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
;
; Remnants of the message 'Splitting The Atom' in Skool Daze.
  DEFM "plittin"

; Back to Skool logo
;
; Used by the routine at PREPGAME.
LOGO:
  DEFB 31,31,31,31,31,31,31,31 ; Attributes (PAPER 3: INK 7)
  DEFB 31,31,31,31,31,31,31,31 ;
  DEFB 31,31,31,31,31,31,31,31 ;
  DEFB 0,0,0,0,0,0,64,0        ; Graphic data
  DEFB 0,0,57,0,0,0,32,0       ;
  DEFB 0,0,64,136,0,0,16,0     ;
  DEFB 0,0,64,72,0,0,8,0       ;
  DEFB 0,0,64,40,0,0,4,0       ;
  DEFB 0,0,66,31,0,0,242,0     ;
  DEFB 0,0,60,8,0,1,9,0        ;
  DEFB 0,0,0,36,60,1,68,0      ;
  DEFB 0,0,28,48,66,0,132,0    ;
  DEFB 0,0,34,40,65,0,136,0    ;
  DEFB 0,0,33,0,33,30,112,0    ;
  DEFB 0,0,33,160,34,33,0,0    ;
  DEFB 0,128,33,20,28,40,128,0 ;
  DEFB 0,79,146,8,0,16,128,0   ;
  DEFB 0,40,76,20,40,17,0,0    ;
  DEFB 0,24,64,2,68,78,0,0     ;
  DEFB 0,8,64,1,130,64,0,0     ;
  DEFB 0,5,128,0,1,64,0,0      ;
  DEFB 0,2,0,0,56,248,0,0      ;
  DEFB 0,0,0,0,64,64,0,0       ;
  DEFB 0,0,0,0,78,32,0,0       ;
  DEFB 0,0,0,0,82,0,0,0        ;
  DEFB 0,0,0,0,34,0,0,0        ;
  DEFB 0,0,0,0,12,0,0,0        ;

  ORG 59392

; Addresses of command lists
;
; Used by the routine at NEWLESSON1.
CLISTADDR:
  DEFW CLIST0             ; 0: Top-floor classroom - girl
  DEFW CLIST2             ; 2: Middle-floor classroom - girl
  DEFW CLIST4             ; 4: Blue Room - little boy
  DEFW CLIST6             ; 6: Yellow Room - little boy
  DEFW CLIST8             ; 8: Science Lab - little boy
  DEFW CLIST10            ; 10: Kitchen - girl
  DEFW CLIST12            ; 12: Dinner hall - girl
  DEFW CLIST14            ; 14: Revision Library - little boy
  DEFW CLIST16            ; 16: Dinner - EINSTEIN/little boy
  DEFW CLIST18            ; 18: Walkabout - EINSTEIN/little boy/girl
  DEFW CLIST20            ; 20: Assembly - EINSTEIN/little boy
  DEFW CLIST22            ; 22: Top-floor classroom - MISS TAKE
  DEFW CLIST24            ; 24: Middle-floor classroom - MISS TAKE
  DEFW CLIST26            ; 26: Kitchen walkabout - MISS TAKE
  DEFW CLIST28            ; 28: Girls' skool walkabout - MISS TAKE
  DEFW CLIST30            ; 30: Dinner duty
  DEFW CLIST32            ; 32: Science Lab - BOY WANDER
  DEFW CLIST34            ; 34: Science Lab - ANGELFACE
  DEFW CLIST36            ; 36: Revision Library - ANGELFACE/BOY WANDER
  DEFW CLIST38            ; 38: Dinner - ANGELFACE/BOY WANDER
  DEFW CLIST40            ; 40: Assembly - ANGELFACE/BOY WANDER
  DEFW CLIST42            ; 42: Walkabout - ANGELFACE/BOY WANDER
  DEFW CLIST44            ; 44: Stalk HAYLEY - ANGELFACE
  DEFW CLIST46            ; 46: Blue Room - BOY WANDER
  DEFW CLIST48            ; 48: Blue Room - ANGELFACE
  DEFW CLIST50            ; 50: Yellow Room - BOY WANDER
  DEFW CLIST52            ; 52: Yellow Room - ANGELFACE
  DEFW CLIST54            ; 54: Write on the blackboards in the boys' skool -
                          ; BOY WANDER
  DEFW CLIST56            ; 56: Write on the blackboards in the girls' skool -
                          ; BOY WANDER
  DEFW CLIST58            ; 58: Blue Room - teacher
  DEFW CLIST60            ; 60: Yellow Room - teacher
  DEFW CLIST62            ; 62: Science Lab - teacher
  DEFW CLIST64            ; 64: Walkabout - WITHIT
  DEFW CLIST66            ; 66: Walkabout - WACKER
  DEFW CLIST68            ; 68: Walkabout - teacher
  DEFW CLIST70            ; 70: Walkabout - WACKER
  DEFW CLIST72            ; 72: Assembly - WACKER
  DEFW CLIST74            ; 74: Head's study - WACKER
  DEFW CLIST76            ; 76: Close the gate and the door - ALBERT
  DEFW CLIST78            ; 78: Open the door and the gate - ALBERT
  DEFW CLIST80            ; 80: Assembly - teacher
  DEFW CLIST82            ; 82: Blue Room - EINSTEIN
  DEFW CLIST84            ; 84: Yellow Room - EINSTEIN
  DEFW CLIST86            ; 86: Science Lab - EINSTEIN
  DEFW CLIST88            ; 88: Assembly - little boy

; Unused
  DEFS 6

; Command list 46: Blue Room - BOY WANDER
;
; Used by BOY WANDER in lessons 37, 41, 48 and 54.
CLIST46:
  DEFW ADDR2CBUF          ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 6,3                ; ...the Blue Room blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 10                 ; ...the teacher has arrived at the Blue Room
; This command list continues at CLIST48.

; Command list 48: Blue Room - ANGELFACE
;
; Used by ANGELFACE in lessons 37, 38, 42 and 43. Command list 46 also
; continues here.
CLIST48:
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW GOTO               ; Go to...
  DEFB 21,3               ; ...the Blue Room
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 10                 ; ...the teacher arrives at the Blue Room
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 50: Yellow Room - BOY WANDER
;
; Used by BOY WANDER in lessons 39, 40, 43 and 44.
CLIST50:
  DEFW ADDR2CBUF          ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW CATTY              ; ...fire the catapult now and then
  DEFW GOTO               ; Go to...
  DEFB 44,3               ; ...the Yellow Room blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 11                 ; ...the teacher has arrived at the Yellow Room
; This command list continues at CLIST52.

; Command list 52: Yellow Room - ANGELFACE
;
; Used by ANGELFACE in lessons 39, 41 and 46. Command list 50 also continues
; here.
CLIST52:
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW GOTO               ; Go to...
  DEFB 55,3               ; ...the Yellow Room
  DEFW ADDR2CBUF          ; Put the next address in the character's buffer,
                          ; making him...
  DEFW HITFIRE            ; ...fire the catapult or hit now and then
  DEFW MVTILL             ; Move about until...
  DEFB 11                 ; ...the teacher arrives at the Yellow Room
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW DONOWT             ; Sit still

; Unused
  DEFB 0

; Command list 54: Write on the blackboards in the boys' skool - BOY WANDER
;
; Used by BOY WANDER in lessons 55 and 56.
CLIST54:
  DEFW GOTO               ; Go to...
  DEFB 6,3                ; ...the Blue Room blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 0                  ; ...playtime is over
  DEFW GOTO               ; Go to...
  DEFB 44,3               ; ...the Yellow Room blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 0                  ; ...playtime is over
  DEFW GOTO               ; Go to...
  DEFB 35,10              ; ...the Science Lab blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 0                  ; ...playtime is over
  DEFW ADDR2CBUF          ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW HITFIRE            ; ...fire the catapult now and then
  DEFW GOTORAND           ; Go to a random location
  DEFW ADDR2CBUF          ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW HITFIRE            ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...playtime is over

; Command list 56: Write on the blackboards in the girls' skool - BOY WANDER
;
; Used by BOY WANDER in lesson 57.
CLIST56:
  DEFW GOTO               ; Go to...
  DEFB 164,3              ; ...the top-floor classroom blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 0                  ; ...playtime is over
  DEFW GOTO               ; Go to...
  DEFB 164,10             ; ...the middle-floor classroom blackboard
  DEFW BWWRITE            ; Write on the board unless...
  DEFB 0                  ; ...playtime is over
  DEFW ADDR2CBUF          ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW HITFIRE            ; ...fire the catapult now and then
  DEFW GOTORAND           ; Go to a random location
  DEFW ADDR2CBUF          ; Put the next address in BOY WANDER's buffer, making
                          ; him...
  DEFW HITFIRE            ; ...fire the catapult now and then
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...playtime is over

; Unused
  DEFB 0

; Command list 58: Blue Room - teacher
;
; Used by MR WITHIT in lessons 39, 40, 41, 42, 49 and 54, and by MR CREAK in
; lessons 37, 38, 43, 44, 46, 47 and 48.
CLIST58:
  DEFW GOTO               ; Go to...
  DEFB 32,3               ; ...the Revision Library
  DEFW GOTO               ; Go to...
  DEFB 23,3               ; ...the Blue Room doorway
  DEFW RSTTELLSIT         ; Restart the command list, or tell the kids to sit
                          ; down and signal that...
  DEFB 10                 ; ...the teacher has arrived at the Blue Room
  DEFW GOTO               ; Go to...
  DEFB 11,3               ; ...the Blue Room blackboard
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Command list 60: Yellow Room - teacher
;
; Used by MR WITHIT in lessons 37, 43, 44 and 47, by MR ROCKITT in lessons 42
; and 46, and by MR CREAK in lessons 39, 40, 41, 45, 49, 53 and 54.
CLIST60:
  DEFW GOTO               ; Go to...
  DEFB 31,3               ; ...the Revision Library
  DEFW GOTO               ; Go to...
  DEFB 38,3               ; ...the Yellow Room doorway
  DEFW RSTTELLSIT         ; Restart the command list, or tell the kids to sit
                          ; down and signal that...
  DEFB 11                 ; ...the teacher has arrived at the Yellow Room
  DEFW GOTO               ; Go to...
  DEFB 49,3               ; ...the Yellow Room blackboard
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Command list 62: Science Lab - teacher
;
; Used by MR WITHIT in lessons 45 and 46, and by MR ROCKITT in lessons 38, 39,
; 40, 43, 44, 47, 48, 49, 53 and 54.
CLIST62:
  DEFW GOTO               ; Go to...
  DEFB 18,10              ; ...the trophy cabinet on the middle floor
  DEFW GOTO               ; Go to...
  DEFB 28,10              ; ...the Science Lab doorway
  DEFW RSTTELLSIT         ; Restart the command list, or tell the kids to sit
                          ; down and signal that...
  DEFB 12                 ; ...the teacher has arrived at the Science Lab
  DEFW GOTO               ; Go to...
  DEFB 40,10              ; ...the Science Lab blackboard
  DEFW DOCLASS            ; Wipe the board, conduct the class

; Command list 64: Walkabout - WITHIT
;
; Used by MR WITHIT in lessons 38, 48, 50 and 53.
CLIST64:
  DEFW GOTO               ; Go to...
  DEFB 77,14              ; ...the assembly hall stage
  DEFW GOTO               ; Go to...
  DEFB 32,3               ; ...the Revision Library
  DEFW RESTART            ; Restart the command list

; Command list 66: Walkabout - WACKER
;
; Used by MR WACKER when it's PLAYTIME.
CLIST66:
  DEFW GOTO               ; Go to...
  DEFB 76,3               ; ...the head's study
; This command list continues at CLIST68.

; Command list 68: Walkabout - teacher
;
; Used by MR WITHIT when it's PLAYTIME, by MR ROCKITT in lessons 37, 41 and 45
; (and when it's DINNER or PLAYTIME), and by MR CREAK in lesson 42 (and when
; it's DINNER or PLAYTIME). Command list 66 also continues here.
CLIST68:
  DEFW GOTORAND           ; Go to a random location
  DEFW RESTART            ; Restart the command list

; Command list 70: Walkabout - WACKER
;
; Used by MR WACKER in lessons 38, 40, 42, 44, 46, 48, 51 and 54.
CLIST70:
  DEFW GOTO               ; Go to...
  DEFB 76,3               ; ...the head's study
  DEFW GOTO               ; Go to...
  DEFB 79,14              ; ...the assembly hall stage
  DEFW GOTO               ; Go to...
  DEFB 3,17               ; ...the cloak room
  DEFW GOTO               ; Go to...
  DEFB 79,14              ; ...the assembly hall stage
  DEFW RESTART            ; Restart the command list

; Command list 72: Assembly - WACKER
;
; Used by MR WACKER in lesson 52.
CLIST72:
  DEFW GOTO               ; Go to...
  DEFB 76,3               ; ...the head's study
  DEFW GOTO               ; Go to...
  DEFB 85,10              ; ...the top of the stairs leading down to the stage
  DEFW RSTORASSEM         ; Restart the command list unless it's time for
                          ; assembly
  DEFW GOTO               ; Go to...
  DEFB 77,14              ; ...the assembly hall stage
  DEFW DETENTION          ; Tell the kids they're in detention
; This command list continues at CLIST74.

; Command list 74: Head's study - WACKER
;
; Used by MR WACKER in lessons 37, 39, 41, 43, 45, 47, 49 and 53. Command list
; 72 also continues here.
CLIST74:
  DEFW GOTO               ; Go to...
  DEFB 81,3               ; ...the head's study
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Unused
  DEFS 2

; Command list 76: Close the gate and the door - ALBERT
;
; Used by ALBERT when it's not PLAYTIME.
CLIST76:
  DEFW GOTO               ; Go to...
  DEFB 112,17             ; ...the middle of the boys' playground
  DEFW CLISTJR10          ; Move forward 10 places in the command list (to
                          ; GATESHUT) unless...
  DEFB 16                 ; ...the skool gate is open
  DEFW GOTO               ; Go to...
  DEFB 130,17             ; ...the skool gate
  DEFW WAITDOOR           ; Wait till everybody has gone past the skool gate
  DEFW MOVEDOOR           ; Move the...
  DEFB 16,0               ; ...skool gate (close it)
GATESHUT:
  DEFW CLISTJR10          ; Move forward 10 places in the command list (to
                          ; DOORSHUT) unless...
  DEFB 8                  ; ...the boys' skool door is open
  DEFW GOTO               ; Go to...
  DEFB 95,17              ; ...the boys' skool door
  DEFW WAITDOOR           ; Wait till everybody has gone past the skool door
  DEFW MOVEDOOR           ; Move the...
  DEFB 8,0                ; ...boys' skool door (close it)
DOORSHUT:
  DEFW GOTO               ; Go to...
  DEFB 104,17             ; ...the position just right of the tree
  DEFW ADDR2CBUF          ; Put the next address in ALBERT's buffer, making
                          ; him...
  DEFW WATCHERIC          ; ...keep an eye out for an escaping ERIC
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Command list 78: Open the door and the gate - ALBERT
;
; Used by ALBERT when it's PLAYTIME.
CLIST78:
  DEFW GOTO               ; Go to...
  DEFB 95,17              ; ...the boys' skool door
  DEFW MOVEDOOR           ; Move the...
  DEFB 8,1                ; ...boys' skool door (open it)
  DEFW GOTO               ; Go to...
  DEFB 132,17             ; ...the skool gate
  DEFW MOVEDOOR           ; Move the...
  DEFB 16,1               ; ...skool gate (open it)
  DEFW CLSTART            ; Make the next command be the start of the command
                          ; list
  DEFW GOTORAND           ; Go to a random location
  DEFW RESTART            ; Restart the command list

; Unused
  DEFB 0

; Command list 80: Assembly - teacher
;
; Used by MR WITHIT, MR ROCKITT and MR CREAK in lesson 52.
CLIST80:
  DEFW GOTO               ; Go to...
  DEFB 62,17              ; ...the position just left of where the teachers
                          ; stand during assembly
  DEFW GOTO               ; Go to...
  DEFB 65,17              ; ...the position where the teachers stand during
                          ; assembly
  DEFW ASSEMDUTY          ; Stand still or perform assembly duty
  DEFW MVTILL             ; Move about until...
  DEFB 0                  ; ...the bell rings

; Command list 82: Blue Room - EINSTEIN
;
; Used by EINSTEIN in lessons 37, 38, 41 and 42.
CLIST82:
  DEFW GOTO               ; Go to...
  DEFB 21,3               ; ...the Blue Room
  DEFW MVTILL             ; Move about until...
  DEFB 10                 ; ...the teacher arrives at the Blue Room
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW GRASSETC           ; Grass and answer questions

; Command list 84: Yellow Room - EINSTEIN
;
; Used by EINSTEIN in lessons 39, 40, 43 and 44.
CLIST84:
  DEFW GOTO               ; Go to...
  DEFB 56,3               ; ...the Yellow Room
  DEFW MVTILL             ; Move about until...
  DEFB 11                 ; ...the teacher arrives at the Yellow Room
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW GRASSETC           ; Grass and answer questions

; Command list 86: Science Lab - EINSTEIN
;
; Used by EINSTEIN in lessons 45, 46, 47, 48 and 49.
CLIST86:
  DEFW GOTO               ; Go to...
  DEFB 51,10              ; ...the Science Lab
  DEFW MVTILL             ; Move about until...
  DEFB 12                 ; ...the teacher arrives at the Science Lab
  DEFW FINDSEAT1          ; Find a seat and sit down
  DEFW GRASSETC           ; Grass and answer questions

; Unused
  DEFS 6

; Message 3: 'Please Sir I cannot tell a lie . . '
;
; Used as a submessage of messages 18, 81, 83, 87, 88 and 137.
MSG003:
  DEFM "Please Sir I cannot tell a lie . . "
  DEFB 0                  ; End marker

; Message 4: '         ' (9 spaces)
;
; Used as a submessage of messages 18, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82,
; 83, 84, 87, 88, 89, 90, 91, 92, 93, 94, 96 and 138.
MSG004:
  DEFM "         "        ; 9 spaces
  DEFB 0                  ; End marker

; Message 13: '[12]0 LINES^[5]'
;
; Used by the routine at GIVELINES.
MSG013:
  DEFB 12                 ; 12: number of lines being given (divided by 10)
  DEFM "0 LINES"
  DEFB 2                  ; Newline
  DEFB 5                  ; 5: '{lines recipient}'
  DEFB 0                  ; End marker

; Message 14: '[5]^ '
;
; Used by the routine at OBJFALL.
MSG014:
  DEFB 5                  ; 5: combination number or letter
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 145: 'ATE'
;
; Used by the routine at DETENTION.
MSG145:
  DEFM "ATE"
  DEFB 0                  ; End marker

; Message 15: 'NOW {DON'T }^DO IT AGAIN'
;
; Used by the routine at KNOCKED.
MSG015:
  DEFM "NOW "
  DEFB 9                  ; 9: 'DON'T '
  DEFB 2                  ; Newline
  DEFM "DO IT AGAIN"
  DEFB 0                  ; End marker

; Message 8: 'THE '
;
; Used as a submessage of messages 33, 35, 49, 55, 64, 66, 69, 101, 102, 103,
; 104, 105, 106, 107, 108, 109, 110 and 248, and the unused message at
; XM_LINES0.
MSG008:
  DEFM "THE "
  DEFB 0                  ; End marker

; Message 16: '{{teacher}}^{{room}}'
;
; Used by the routine at NEWLESSON2.
MSG016:
  DEFB 6                  ; 6: '{teacher}'
  DEFB 2                  ; Newline
  DEFB 5                  ; 5: '{room}'
  DEFB 0                  ; End marker

; Message 63: 'DEMO.MODE^ '
;
; Used by the routine at NEWLESSON2.
MSG063:
  DEFM "DEMO.MODE"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 18: '{Please Sir I cannot tell a lie . . }{ERIC} is not here{9sp}'
;
; Used by the routine at DOCLASS.
MSG018:
  DEFB 3                  ; 3: 'Please Sir I cannot tell a lie . . '
  DEFB 31                 ; 31: 'ERIC'
  DEFM " is not"
  DEFM " here"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 19: '{DON'T }BE^LATE AGAIN'
;
; Used by the routine at DOCLASS.
MSG019:
  DEFB 9                  ; 9: 'DON'T '
  DEFM "BE"
  DEFB 2                  ; Newline
  DEFM "LATE AGAIN"
  DEFB 0                  ; End marker

; Message 9: 'DON'T '
;
; Used as a submessage of messages 15, 19, 65, 72, 85, 86 and 97, and the
; unused message at XM_LINES0.
MSG009:
  DEFM "DON'T "
  DEFB 0                  ; End marker

; Message 20: 'STAY TILL I^DISMISS YOU'
;
; Used by the routine at DOCLASS.
MSG020:
  DEFM "STAY TILL I"
  DEFB 2                  ; Newline
  DEFM "DISMISS YOU"
  DEFB 0                  ; End marker

; Message 142: ' THE '
;
; Used as a submessage of messages 0, 44, 45, 46, 47, 48 and 96.
MSG142:
  DEFM " THE "
  DEFB 0                  ; End marker

; Message 143: 'i hate ^'
;
; Used as a submessage of messages 56, 57, 58, 59 and 60.
MSG143:
  DEFM "i hate "
  DEFB 2                  ; Newline
  DEFB 0                  ; End marker

; Message 32: 'ARTESIAN^WELLS'
;
; Used by the routine at WRITEBRD.
MSG032:
  DEFM "ARTESIAN"
  DEFB 2                  ; Newline
  DEFM "WELLS"
  DEFB 0                  ; End marker

; Message 33: '{THE }DOLDRUMS^ '
;
; Used by the routine at WRITEBRD.
MSG033:
  DEFB 8                  ; 8: 'THE '
  DEFM "DOLDRUMS"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 34: 'TASTY^GEYSERS'
;
; Used by the routine at WRITEBRD.
MSG034:
  DEFM "TASTY"
  DEFB 2                  ; Newline
  DEFM "GEYSERS"
  DEFB 0                  ; End marker

; Message 35: '{THE }GREEN^REVOLUTION'
;
; Used by the routine at WRITEBRD.
MSG035:
  DEFB 8                  ; 8: 'THE '
  DEFM "GREEN"
  DEFB 2                  ; Newline
  DEFM "REVOLUTION"
  DEFB 0                  ; End marker

; Message 36: 'TREACLE^MINING'
;
; Used by the routine at WRITEBRD.
MSG036:
  DEFM "TREACLE"
  DEFB 2                  ; Newline
  DEFM "MINING"
  DEFB 0                  ; End marker

; Message 37: 'FROG FARMING^ '
;
; Used by the routine at WRITEBRD.
MSG037:
  DEFM "FROG FARMING"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 38: 'HEAVY WATER^ '
;
; Used by the routine at WRITEBRD.
MSG038:
  DEFM "HEAVY WATER"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 39: 'HOLOGRAMS &^LASERS'
;
; Used by the routine at WRITEBRD.
MSG039:
  DEFM "HOLOGRAMS &"
  DEFB 2                  ; Newline
  DEFM "LASERS"
  DEFB 0                  ; End marker

; Message 40: 'DNA^ '
;
; Used by the routine at WRITEBRD.
MSG040:
  DEFM "DNA"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 41: 'VAMPIRE^BATS'
;
; Used by the routine at WRITEBRD.
MSG041:
  DEFM "VAMPIRE"
  DEFB 2                  ; Newline
  DEFM "BATS"
  DEFB 0                  ; End marker

; Message 42: 'NUCLEAR^FUSION'
;
; Used by the routine at WRITEBRD.
MSG042:
  DEFM "NUCLEAR"
  DEFB 2                  ; Newline
  DEFM "FUSION"
  DEFB 0                  ; End marker

; Message 43: 'BACTERIA^AS PETS'
;
; Used by the routine at WRITEBRD.
MSG043:
  DEFM "BACTERIA"
  DEFB 2                  ; Newline
  DEFM "AS PETS"
  DEFB 0                  ; End marker

; Message 44: 'ATTILA{ THE }^HUN'
;
; Used by the routine at WRITEBRD.
MSG044:
  DEFM "ATTILA"
  DEFB 142                ; 142: ' THE '
  DEFB 2                  ; Newline
  DEFM "HUN"
  DEFB 0                  ; End marker

; Message 45: 'ERIC{ THE }RED^ '
;
; Used by the routine at WRITEBRD.
MSG045:
  DEFM "ERIC"
  DEFB 142                ; 142: ' THE '
  DEFM "RED"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 46: 'NOGGIN{ THE }^NOG'
;
; Used by the routine at WRITEBRD.
MSG046:
  DEFM "NOGGIN"
  DEFB 142                ; 142: ' THE '
  DEFB 2                  ; Newline
  DEFM "NOG"
  DEFB 0                  ; End marker

; Message 47: 'IVAN{ THE }^TERRIBLE'
;
; Used by the routine at WRITEBRD.
MSG047:
  DEFM "IVAN"
  DEFB 142                ; 142: ' THE '
  DEFB 2                  ; Newline
  DEFM "TERRIBLE"
  DEFB 0                  ; End marker

; Message 48: 'ETHELRED{ THE }^UNREADY'
;
; Used by the routine at WRITEBRD.
MSG048:
  DEFM "ETHELRED"
  DEFB 142                ; 142: ' THE '
  DEFB 2                  ; Newline
  DEFM "UNREADY"
  DEFB 0                  ; End marker

; Message 49: '{THE }LUDDITES^ '
;
; Used by the routine at WRITEBRD.
MSG049:
  DEFB 8                  ; 8: 'THE '
  DEFM "LUDDITES"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 50: 'IAMBIC^PENTAMETERS'
;
; Used by the routine at WRITEBRD.
MSG050:
  DEFM "IAMBIC"
  DEFB 2                  ; Newline
  DEFM "PENTAMETERS"
  DEFB 0                  ; End marker

; Message 51: 'ELOCUTION^AINT ARF FUN'
;
; Used by the routine at WRITEBRD.
MSG051:
  DEFM "ELOCUTION"
  DEFB 2                  ; Newline
  DEFM "AINT ARF FUN"
  DEFB 0                  ; End marker

; Message 52: 'SUGAR AND^SPICE'
;
; Used by the routine at WRITEBRD.
MSG052:
  DEFM "SUGAR AND"
  DEFB 2                  ; Newline
  DEFM "SPICE"
  DEFB 0                  ; End marker

; Message 53: 'TONE POEMS^ '
;
; Used by the routine at WRITEBRD.
MSG053:
  DEFM "TONE POEMS"
  DEFB 2                  ; Newline
  DEFM " "
  DEFB 0                  ; End marker

; Message 54: 'ELEMENTARY^ASTROPHYSICS'
;
; Used by the routine at WRITEBRD.
MSG054:
  DEFM "ELEMENTARY"
  DEFB 2                  ; Newline
  DEFM "ASTROPHYSICS"
  DEFB 0                  ; End marker

; Message 55: '{THE }BARD OF^AVON'
;
; Used by the routine at WRITEBRD.
MSG055:
  DEFB 8                  ; 8: 'THE '
  DEFM "BARD OF"
  DEFB 2                  ; Newline
  DEFM "AVON"
  DEFB 0                  ; End marker

; Message 56: '{i hate ^}girls'
;
; Used by the routine at WRITEBRD.
MSG056:
  DEFB 143                ; 143: 'i hate ^'
  DEFM "girls"
  DEFB 0                  ; End marker

; Message 57: '{i hate ^}skool'
;
; Used by the routine at WRITEBRD.
MSG057:
  DEFB 143                ; 143: 'i hate ^'
  DEFM "skool"
  DEFB 0                  ; End marker

; Message 58: '{i hate ^}mafs'
;
; Used by the routine at WRITEBRD.
MSG058:
  DEFB 143                ; 143: 'i hate ^'
  DEFM "mafs"
  DEFB 0                  ; End marker

; Message 59: '{i hate ^}{MR WACKER}'
;
; Used by the routine at WRITEBRD.
MSG059:
  DEFB 143                ; 143: 'i hate ^'
  DEFB 21                 ; 21: 'MR WACKER'
  DEFB 0                  ; End marker

; Message 60: '{i hate ^}groan-ups'
;
; Used by the routine at WRITEBRD.
MSG060:
  DEFB 143                ; 143: 'i hate ^'
  DEFM "groan-ups"
  DEFB 0                  ; End marker

; Message 61: 'who's Sam^Cruise?'
;
; Used by the routine at WRITEBRD.
MSG061:
  DEFM "who's Sam"
  DEFB 2                  ; Newline
  DEFM "Cruise?"
  DEFB 0                  ; End marker

; Message 139: 'SIT DOWN '
;
; Used as a submessage of messages 70, 92, 93, 94 and 95.
MSG139:
  DEFM "SIT DOWN "
  DEFB 0                  ; End marker

; Message 140: 'SIT '
;
; Used as a submessage of message 69 and the unused message at XM_LINES0.
MSG140:
  DEFM "SIT "
  DEFB 0                  ; End marker

; Message 141: 'GET '
;
; Used as a submessage of messages 64, 66, 67 and 68.
MSG141:
  DEFM "GET "
  DEFB 0                  ; End marker

; Message 62: 'YOU ARE NOT^ALLOWED HERE'
;
; Used by the routine at CHKERIC2.
MSG062:
  DEFM "YOU ARE NOT"
  DEFB 2                  ; Newline
  DEFM "ALLOWED HERE"
  DEFB 0

; '{DON'T }{SIT }ON^{THE }STAIRS' (unused)
;
; ERIC can't sit on the stairs in Back to Skool.
XM_LINES0:
  DEFB 9                  ; 9: 'DON'T '
  DEFB 140                ; 140: 'SIT '
  DEFM "ON"
  DEFB 2                  ; Newline
  DEFB 8                  ; 8: 'THE '
  DEFM "STAIRS"
  DEFB 0                  ; End marker

; Message 64: '{GET }OFF^{THE }PLANTS'
;
; Used by the routine at CHKERIC2.
MSG064:
  DEFB 141                ; 141: 'GET '
  DEFM "OFF"
  DEFB 2                  ; Newline
  DEFB 8                  ; 8: 'THE '
  DEFM "PLANTS"
  DEFB 0                  ; End marker

; Message 65: '{DON'T }RIDE^BIKES IN HERE'
;
; Used by the routine at CHKERIC2.
MSG065:
  DEFB 9                  ; 9: 'DON'T '
  DEFM "RIDE"
  DEFB 2                  ; Newline
  DEFM "BIKES IN HERE"
  DEFB 0                  ; End marker

; Message 66: '{GET }OFF^{THE }FLOOR'
;
; Used by the routine at CHKERIC2.
MSG066:
  DEFB 141                ; 141: 'GET '
  DEFM "OFF"
  DEFB 2                  ; Newline
  DEFB 8                  ; 8: 'THE '
  DEFM "FLOOR"
  DEFB 0                  ; End marker

; Message 67: '{GET }BACK^TO SCHOOL'
;
; Used by the routine at CHKERIC2.
MSG067:
  DEFB 141                ; 141: 'GET '
  DEFM "BACK"
  DEFB 2                  ; Newline
  DEFM "TO SCHOOL"
  DEFB 0                  ; End marker

; Message 68: '{GET }ALONG^NOW'
;
; Used by the routine at CHKERIC2.
MSG068:
  DEFB 141                ; 141: 'GET '
  DEFM "ALONG"
  DEFB 2                  ; Newline
  DEFM "NOW"
  DEFB 0                  ; End marker

; Message 69: '{SIT }FACING^{THE }STAGE'
;
; Used by the routine at CHKERIC2.
MSG069:
  DEFB 140                ; 140: 'SIT '
  DEFM "FACING"
  DEFB 2                  ; Newline
  DEFB 8                  ; 8: 'THE '
  DEFM "STAGE"
  DEFB 0                  ; End marker

; Message 70: 'NOW^{SIT DOWN }'
;
; Used by the routine at CHKERIC2.
MSG070:
  DEFM "NOW"
  DEFB 2                  ; Newline
  DEFB 139                ; 139: 'SIT DOWN '
  DEFB 0                  ; End marker

; Message 71: 'COME ALONG^YOU MONSTER'
;
; Used by the routine at CHKERIC2.
MSG071:
  DEFM "COME ALONG"
  DEFB 2                  ; Newline
  DEFM "YOU MONSTER"
  DEFB 0                  ; End marker

; Message 72: '{DON'T }KEEP^ME WAITING'
;
; Used by the routine at CHKERIC2.
MSG072:
  DEFB 9                  ; 9: 'DON'T '
  DEFM "KEEP"
  DEFB 2                  ; Newline
  DEFM "ME WAITING"
  DEFB 0                  ; End marker

; Message 85: '{DON'T }TELL^TALES'
;
; Used by the routine at SWOTLINES.
MSG085:
  DEFB 9                  ; 9: 'DON'T '
  DEFM "TELL"
  DEFB 2                  ; Newline
  DEFM "TALES"
  DEFB 0                  ; End marker

; Message 86: '{DON'T }TOUCH^BLACKBOARDS'
;
; Used by the routines at WRITING and DOCLASS.
MSG086:
  DEFB 9                  ; 9: 'DON'T '
  DEFM "TOUCH"
  DEFB 2                  ; Newline
  DEFM "BLACKBOARDS"
  DEFB 0                  ; End marker

; Message 87: '{Please Sir I cannot tell a lie . . }{ERIC} hit me{9sp}'
;
; Used by the routine at DOCLASS.
MSG087:
  DEFB 3                  ; 3: 'Please Sir I cannot tell a lie . . '
  DEFB 31                 ; 31: 'ERIC'
  DEFM " hit me"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 88: '{Please Sir I cannot tell a lie . . }[7] wrote on the
; board{9sp}'
;
; Used by the routine at DOCLASS. This message is never actually used in the
; game, because of a bug in the section of code at DOCLASS_12.
MSG088:
  DEFB 3                  ; 3: 'Please Sir I cannot tell a lie . . '
  DEFB 7                  ; 7: '{grassee}'
  DEFM " wrote on"
  DEFM " the board"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 89: 'START REVISING FOR YOUR EXAMS{9sp}'
;
; Used by the routine at TELLCLASS.
MSG089:
  DEFM "START REVISING "
  DEFM "FOR YOUR EXAMS"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 90: 'START READING AT THE NEXT CHAPTER IN YOUR BOOKS{9sp}'
;
; Used by the routine at TELLCLASS.
MSG090:
  DEFM "START READING AT THE "
  DEFM "NEXT CHAPTER IN YOUR BOOKS"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 91: 'WRITE AN ESSAY TITLED 'WHY I LOVE SCHOOL'{9sp}'
;
; Used by the routine at TELLCLASS.
MSG091:
  DEFM "WRITE AN ESSAY TITLED "
  DEFM "'WHY I LOVE SCHOOL'"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 92: '{SIT DOWN }CHAPS{9sp}'
;
; Used by the routine at RSTTELLSIT.
MSG092:
  DEFB 139                ; 139: 'SIT DOWN '
  DEFM "CHAPS"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 93: '{SIT DOWN }MY CHERUBS{9sp}'
;
; Used by the routine at RSTTELLSIT.
MSG093:
  DEFB 139                ; 139: 'SIT DOWN '
  DEFM "MY CHERUBS"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 94: '{SIT DOWN }YOU LITTLE ANARCHISTS{9sp}'
;
; Used by the routine at RSTTELLSIT.
MSG094:
  DEFB 139                ; 139: 'SIT DOWN '
  DEFM "YOU LITTLE ANARCHISTS"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 95: '{SIT DOWN }'
;
; Used by the routine at RSTTELLSIT.
MSG095:
  DEFB 139                ; 139: 'SIT DOWN '
  DEFB 0                  ; End marker

; Message 96: 'YOU'RE ALL IN DETENTION UNTIL I FIND OUT WHO [10]{ THE
; }[11]{9sp}'
;
; Used by the routine at DETENTION.
MSG096:
  DEFM "YOU'RE ALL IN DETENTION"
  DEFM " UNTIL I FIND OUT WHO "
  DEFB 10                 ; 10: '{verb}'
  DEFB 142                ; 142: ' THE '
  DEFB 11                 ; 11: '{noun}'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 144: 'KIDNAPPED'
;
; Used by the routine at DETENTION.
MSG144:
  DEFM "KIDNAPPED"
  DEFB 0                  ; End marker

; Message 97: '{DON'T }HIT^YOUR MATES'
;
; Used by the routines at HIT and DOCLASS.
MSG097:
  DEFB 9                  ; 9: 'DON'T '
  DEFM "HIT"
  DEFB 2                  ; Newline
  DEFM "YOUR MATES"
  DEFB 0                  ; End marker

; Message 98: '{MR WACKER}^HE'S ESCAPING'
;
; Used by the routine at WATCHERIC.
MSG098:
  DEFB 21                 ; 21: 'MR WACKER'
  DEFB 2                  ; Newline
  DEFM "HE'S ESCAPING"
  DEFB 0                  ; End marker

; Message 99: 'YOU HAVE 10000 LINES{ {ERIC}{9sp}YOU'RE EXPELLED{9sp}}'
;
; Used by the routine at FINDEXPEL.
MSG099:
  DEFM "YOU HAVE 10000 LINES"
  DEFB 138                ; 138: ' {ERIC}{9sp}YOU'RE EXPELLED{9sp}'
  DEFB 0                  ; End marker

; Message 100: 'YOU ARE NOT A BIRD{ {ERIC}{9sp}YOU'RE EXPELLED{9sp}}'
;
; Used by the routine at FINDEXPEL.
MSG100:
  DEFM "YOU ARE NOT A BIRD"
  DEFB 138                ; 138: ' {ERIC}{9sp}YOU'RE EXPELLED{9sp}'
  DEFB 0                  ; End marker

; Message 138: ' {ERIC}{9sp}YOU'RE EXPELLED{9sp}'
;
; Used as a submessage of messages 99 and 100.
MSG138:
  DEFM " "
  DEFB 31                 ; 31: 'ERIC'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFM "YOU'RE EXPELLED"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 128: 'PLAYTIME'
;
; Used by the routine at NEWLESSON2.
MSG128:
  DEFM "PLAYTIME"
  DEFB 0                  ; End marker

; Message 129: 'ASSEMBLY'
;
; Used by the routine at NEWLESSON2.
MSG129:
  DEFM "ASSEMBLY"
  DEFB 0                  ; End marker

; Message 130: 'DINNER'
;
; Used by the routine at NEWLESSON2.
MSG130:
  DEFM "DINNER"
  DEFB 0                  ; End marker

; Message 131: 'LIBRARY'
;
; Used by the routine at NEWLESSON2.
MSG131:
  DEFM "LIBRARY"
  DEFB 0                  ; End marker

; Message 132: 'SCIENCE LAB'
;
; Used by the routine at NEWLESSON2.
MSG132:
  DEFM "SCIENCE LAB"
  DEFB 0                  ; End marker

; Message 133: 'BLUE ROOM'
;
; Used by the routine at NEWLESSON2.
MSG133:
  DEFM "BLUE ROOM"
  DEFB 0                  ; End marker

; Message 134: 'YELLOW ROOM'
;
; Used by the routine at NEWLESSON2.
MSG134:
  DEFM "YELLOW ROOM"
  DEFB 0                  ; End marker

; Message 135: 'REVISION'
;
; Used by the routine at NEWLESSON2.
MSG135:
  DEFM "REVISION"
  DEFB 0                  ; End marker

; Message 146: 'SET FIRE TO'
;
; Used by the routine at DETENTION.
MSG146:
  DEFM "SET FIRE TO"
  DEFB 0                  ; End marker

; Message 147: 'BLEW UP'
;
; Used by the routine at DETENTION.
MSG147:
  DEFM "BLEW UP"
  DEFB 0                  ; End marker

; Message 148: 'IS MAKING RUDE PHONE CALLS TO'
;
; Used by the routine at DETENTION.
MSG148:
  DEFM "IS MAKING RUDE PHONE CALLS TO"
  DEFB 0                  ; End marker

; Message 149: 'IS BLACKMAILING'
;
; Used by the routine at DETENTION.
MSG149:
  DEFM "IS BLACKMAILING"
  DEFB 0                  ; End marker

; Message 150: 'SQUASHED'
;
; Used by the routine at DETENTION.
MSG150:
  DEFM "SQUASHED"
  DEFB 0                  ; End marker

; Message 151: 'POISONED'
;
; Used by the routine at DETENTION.
MSG151:
  DEFM "POISONED"
  DEFB 0                  ; End marker

; Message 152: 'GOLDFISH'
;
; Used by the routine at DETENTION.
MSG152:
  DEFM "GOLDFISH"
  DEFB 0                  ; End marker

; Message 153: 'SCHOOL CAT'
;
; Used by the routine at DETENTION.
MSG153:
  DEFM "SCHOOL CAT"
  DEFB 0                  ; End marker

; Message 154: 'LATIN MASTER'
;
; Used by the routine at DETENTION.
MSG154:
  DEFM "LATIN MASTER"
  DEFB 0                  ; End marker

; Message 155: 'LOLLIPOP LADY'
;
; Used by the routine at DETENTION.
MSG155:
  DEFM "LOLLIPOP LADY"
  DEFB 0                  ; End marker

; Message 156: 'PTA'
;
; Used by the routine at DETENTION.
MSG156:
  DEFM "PTA"
  DEFB 0                  ; End marker

; Message 157: 'CARETAKER'S BUDGIE'
;
; Used by the routine at DETENTION.
MSG157:
  DEFM "CARETAKER'S BUDGIE"
  DEFB 0                  ; End marker

; Message 158: 'MILK MONITOR'
;
; Used by the routine at DETENTION.
MSG158:
  DEFM "MILK MONITOR"
  DEFB 0                  ; End marker

; Message 159: 'HEAD BOY'
;
; Used by the routine at DETENTION.
MSG159:
  DEFM "HEAD BOY"
  DEFB 0                  ; End marker

; Message 21: 'MR WACKER'
;
; Used by the routine at GETNAMES. Also used as a submessage of messages 59 and
; 98.
MSG021:
  DEFM "MR WACKER"
  DEFB 0                  ; End marker
  DEFS 3

; Message 22: 'MR WITHIT'
;
; Used by the routines at GETNAMES and NEWLESSON2.
MSG022:
  DEFM "MR WITHIT"
  DEFB 0                  ; End marker
  DEFS 3

; Message 23: 'MR ROCKITT'
;
; Used by the routines at GETNAMES and NEWLESSON2.
MSG023:
  DEFM "MR ROCKITT"
  DEFB 0                  ; End marker
  DEFS 2

; Message 24: 'MR CREAK'
;
; Used by the routines at GETNAMES and NEWLESSON2.
MSG024:
  DEFM "MR CREAK"
  DEFB 0                  ; End marker
  DEFS 4

; Message 25: 'MISS TAKE'
;
; Used by the routine at GETNAMES.
MSG025:
  DEFM "MISS TAKE"
  DEFB 0                  ; End marker
  DEFS 3

; Message 26: 'ALBERT'
;
; Used by the routine at GETNAMES.
MSG026:
  DEFM "ALBERT"
  DEFB 0                  ; End marker
  DEFS 6

; Message 27: 'BOY WANDER'
;
; Used by the routines at GETNAMES and GIVELINES.
MSG027:
  DEFM "BOY WANDER"
  DEFB 0                  ; End marker
  DEFS 2

; Message 28: 'ANGELFACE'
;
; Used by the routines at GETNAMES and GIVELINES.
MSG028:
  DEFM "ANGELFACE"
  DEFB 0                  ; End marker
  DEFS 3

; Message 29: 'EINSTEIN'
;
; Used by the routines at GETNAMES and GIVELINES.
MSG029:
  DEFM "EINSTEIN"
  DEFB 0                  ; End marker
  DEFS 4

; Message 30: 'HAYLEY'
;
; Used by the routines at GETNAMES and GIVELINES.
MSG030:
  DEFM "HAYLEY"
  DEFB 0                  ; End marker
  DEFS 6

; Message 31: 'ERIC'
;
; Used by the routines at GETNAMES and GIVELINES. Also used as a submessage of
; messages 18, 87 and 138.
MSG031:
  DEFM "ERIC"
  DEFB 0                  ; End marker
  DEFS 8

; Message 137: '{Please Sir I cannot tell a lie . . }it is '
;
; Used as a submessage of messages 73, 75, 77 and 79.
MSG137:
  DEFB 3                  ; 3: 'Please Sir I cannot tell a lie . . '
  DEFM "it is "
  DEFB 0                  ; End marker

; Message 75: '{{Please Sir I cannot tell a lie . . }it is }MT.[10]{9sp}'
;
; Used by the routine at DOCLASS.
MSG075:
  DEFB 137                ; 137: '{Please Sir I cannot tell a lie . . }it is '
  DEFM "MT."
  DEFB 10                 ; 10: '{mountain}'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 81: '{Please Sir I cannot tell a lie . . }it was in 1[11]{9sp}'
;
; Used by the routine at DOCLASS.
MSG081:
  DEFB 3                  ; 3: 'Please Sir I cannot tell a lie . . '
  DEFM "it was in 1"
  DEFB 11                 ; 11: '{year}'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Control EINSTEIN during class
;
; Used by command lists 82, 84 and 86. Controls EINSTEIN from the moment after
; he sits down until the lesson ends.
;
; H 208 (EINSTEIN)
GRASSETC:
  LD A,(LFLAGS)           ; Bit 3 of LFLAGS is set by the routine at SWOTSPK
  BIT 3,A                 ; when it's EINSTEIN's turn to speak in class; is it
                          ; his turn now?
  RET Z                   ; Return if not
  LD BC,SPEAK_0           ; Direct control of EINSTEIN to the routine at
  CALL CALLSUBCMD         ; SPEAK_0 (make character speak) and then return to
                          ; GRASSETC_0 (below)
GRASSETC_0:
  LD L,3                  ; Reset the address of the primary command routine in
  LD (HL),0               ; bytes 3 and 4 of EINSTEIN's buffer from GRASSETC_0
                          ; to GRASSETC
  LD HL,LFLAGS            ; Signal: EINSTEIN has finished speaking
  RES 3,(HL)              ;
  RET

; Unused
  DEFB 0

; Make EINSTEIN speak
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of the buffer of the teacher who is taking ERIC's class by the routine
; at DOCLASS. It is used to make EINSTEIN tell a tale.
;
; E Message number
; H Teacher's character number (201-203)
SWOTSPK:
  LD A,E                  ; A=message number for EINSTEIN
  LD (53259),A            ; Place this in byte 11 of EINSTEIN's buffer
; The address of this entry point is placed into bytes 9 and 10 of the buffer
; of the teacher who is taking ERIC's class by the routine at DOCLASS. It is
; used to make EINSTEIN answer the teacher's question.
SWOTSPK_0:
  EX DE,HL
  LD HL,LFLAGS            ; Set bit 3 at LFLAGS to indicate that it's
  SET 3,(HL)              ; EINSTEIN's turn to speak
  EX DE,HL
  LD L,9                  ; Replace the address of this routine in bytes 9 and
  LD (HL),38              ; 10 of the teacher's buffer with SWOTSPK0 (below)
; This entry point is used while the teacher is waiting for EINSTEIN to finish
; speaking.
SWOTSPK0:
  LD A,(LFLAGS)           ; Has EINSTEIN finished speaking yet?
  BIT 3,A                 ;
  RET NZ                  ; Return if not
  JP RMSUBCMD             ; Otherwise terminate this interruptible subcommand,
                          ; thus restoring control to the main routine at
                          ; DOCLASS

; Make a teacher give lines to EINSTEIN or the kid he grassed up
;
; Used by the routine at DOCLASS. Makes a teacher give lines to EINSTEIN (for
; telling tales) 84 times out of 256, or give lines to the kid EINSTEIN grassed
; up (if any) the rest of the time.
;
; B Reprimand message number
; C Character number of the kid to give lines to (206, 208 or 210)
; H Teacher's character number (201-203)
SWOTLINES:
  CALL GETRANDOM          ; A=random number
  CP 84                   ; Shall we give lines to EINSTEIN for telling tales?
  JR C,SWOTLINES_0        ; Jump if so
  LD A,C                  ; A=character number of the kid to give lines to
  CP 208                  ; Was EINSTEIN telling the teacher that ERIC is not
                          ; in class?
  RET Z                   ; Return if so (there is no scapegoat to give lines
                          ; to)
  JR SWOTLINES_1
SWOTLINES_0:
  LD A,208                ; 208=EINSTEIN
  LD B,85                 ; Message 85: DON'T TELL TALES
; This entry point also is used by the routine at DOCLASS.
SWOTLINES_1:
  PUSH HL
  CALL GIVELINES          ; Give lines to EINSTEIN or the kid he grassed up
  POP HL
  RET

; Unused
  DEFS 2

; Check whether ERIC and EINSTEIN are in class
;
; Used by the routine at DOCLASS. If EINSTEIN is in class, this routine returns
; to the caller with the zero flag set if and only if ERIC is present too. If
; EINSTEIN is not yet sitting down in class, it makes the teacher wait until he
; shows up.
;
; H Teacher's character number (201-203)
INCLASS:
  LD A,(SWOTCBUF)         ; Pick up EINSTEIN's current animatory state in A
  CP 52                   ; 52: Is EINSTEIN sitting down (i.e. in class)?
  JP Z,PRESENT            ; Check on ERIC if so
  LD DE,65533             ; DE=-3
  POP BC                  ; Drop the return address from the stack into BC
  EX DE,HL                ; Set DE to the address of the CALL INCLASS
  ADD HL,BC               ; instruction in the calling routine
  EX DE,HL                ;
  LD L,3                  ; Place this address into bytes 3 and 4 of the
  LD (HL),E               ; teacher's buffer, so that we keep returning here
  INC L                   ; (and the teacher stands still) until EINSTEIN
  LD (HL),D               ; arrives in class
  RET                     ; Return to the character-moving routine at MVCHARS

; Make a teacher tell the class what to do
;
; Used by the routine at DOCLASS. Makes the teacher choose between messages 89,
; 90 and 91 (in a lesson without ERIC and EINSTEIN, or a lesson with no
; question-and-answer session).
;
; H Teacher's character number (201-204)
TELLCLASS:
  CALL GETRANDOM          ; A=random number
  CP 240                  ; Set the carry flag if A<240, and set the zero flag
  BIT 7,A                 ; if A<128
  LD E,91
  JR NC,TELLCLASS_1       ; Jump if it's essay time (A>=240)
  JR Z,TELLCLASS_0        ; Jump if it's book-reading time (A<128)
  DEC E                   ; It's revision time (128<=A<240)
TELLCLASS_0:
  DEC E
; Now E holds the appropriate message number:
;
; +----+-------------------------------------------------+
; | E  | Message                                         |
; +----+-------------------------------------------------+
; | 89 | START REVISING FOR YOUR EXAMS                   |
; | 90 | START READING AT THE NEXT CHAPTER IN YOUR BOOKS |
; | 91 | WRITE AN ESSAY TITLED 'WHY I LOVE SCHOOL'       |
; +----+-------------------------------------------------+
TELLCLASS_1:
  LD BC,SPEAK             ; Redirect control to the routine at SPEAK (make
  JP CALLSUBCMD           ; character speak), then return control to the
                          ; calling routine

; Unused
  DEFB 0

; Restart the command list or make a teacher tell the kids to sit down
;
; Used by the teachers' command lists 22, 24, 58, 60 and 62. Restarts the
; teacher's command list if it's not time to start the class yet; otherwise
; makes the teacher tell the kids to sit down.
;
; H Teacher's character number (201-204)
RSTTELLSIT:
  LD A,(32740)            ; Collect the MSB of the lesson clock (which starts
                          ; at 16)
  CP 12                   ; Is it time to tell the kids to sit down yet?
  JR C,RSTTELLSIT_0       ; Jump if so
  LD L,29                 ; Otherwise signal that the command list is to be
  SET 0,(HL)              ; restarted
  JR RSTTELLSIT_1
RSTTELLSIT_0:
  CALL GETPARAM           ; Collect the event indicator for the start of the
                          ; lesson (8, 9, 10, 11 or 12) from the command list
  LD C,A                  ; Save the event indicator in C briefly
  CALL CHECKSIG           ; Has the lesson already started?
  JR NZ,RSTTELLSIT_1      ; Jump if so (the teacher must have just returned to
                          ; the classroom doorway after hunting down the truant
                          ; ERIC)
  LD A,C                  ; Restore the event indicator to A, and signal that
  CALL SIGRAISE           ; the lesson has started
  LD A,H                  ; A=teacher's character number
  SUB 109                 ; E=92, 93, 94 or 95 (appropriate SIT DOWN message
  LD E,A                  ; for this teacher)
  LD BC,SPEAK             ; Redirect control to the routine at SPEAK (make
  CALL CALLSUBCMD         ; character speak) and then return to RSTTELLSIT_1
                          ; (below)
RSTTELLSIT_1:
  JP NEXTCMD              ; Move to the next command in the command list

; Unused
  DEFB 0

; Make a teacher find the truant ERIC
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of a teacher's buffer by the routine at DINDUTY. It makes the teacher
; run after and stalk ERIC until he goes to wherever he should be (the dinner
; hall, the assembly hall, or the classroom).
;
; H Teacher's character number (201-203)
SEEKERIC:
  LD L,29                 ; Set bit 7 of byte 29 of the teacher's buffer,
  SET 7,(HL)              ; making him run
  LD L,0                  ; Byte 0 of the teacher's buffer holds his animatory
                          ; state
  BIT 0,(HL)              ; Is the teacher midstride?
  JP NZ,FINDERIC_0        ; Finish the stride if so
  LD L,29
; Now check whether a command list restart has been requested. This request
; will have been made for one of two reasons: the lesson in which the teacher
; started chasing ERIC has ended, and the next lesson has just begun (see
; NEWLESSON1); or ERIC is now where he should be (see below). In either case,
; the chase is over.
SEEKERIC_0:
  BIT 0,(HL)              ; Has the teacher's command list been marked for a
                          ; restart?
  JP NZ,HERDERIC2_0       ; If so, do some post-chase cleanup, remove the
                          ; address of this routine from bytes 9 and 10 of the
                          ; teacher's buffer, and restart the command list
  CALL PRESENT            ; Is ERIC where he should be?
  JP NZ,FINDERIC          ; Continue the chase if not
  SET 0,(HL)              ; Otherwise set bit 0 of byte 29 of the teacher's
                          ; buffer, indicating that the command list should be
                          ; restarted
  JR SEEKERIC_0           ; Restart the command list now that the chase is over

; Unused
  DEFB 0

; Restart the command list
;
; Used by the routines at RSTDROPEN and RSTORASSEM, and by command lists 18,
; 28, 30, 42, 44, 64, 66, 68, 70 and 78.
;
; H Character number (183-209)
RESTART:
  LD L,29                 ; Set bit 0 of byte 29 of the character's buffer,
  SET 0,(HL)              ; indicating to the routine at MVCHARS that the
                          ; command list should be restarted
  JP NEXTCMD              ; Move to the next command in the command list (which
                          ; will be the first command)

; Make a teacher find ERIC if he's absent during dinner
;
; Used by command list 30. Makes the teacher run after and stalk ERIC until he
; goes to the dinner hall.
;
; H 200 (MR WACKER) or 201 (MR WITHIT)
DINDUTY:
  LD DE,32740             ; Point DE at the MSB of the lesson clock (which
                          ; starts off at 16)
  LD A,(DE)               ; Pick this up in A
  CP 12                   ; Is ERIC supposed to be in the dinner hall by now?
  JR NC,DINDUTY_1         ; Jump if not
  LD E,128                ; Set bit 6 at LFLAGS, indicating that ERIC should be
  EX DE,HL                ; in the dinner hall now
  SET 6,(HL)              ;
  EX DE,HL                ;
  CALL PRESENT            ; Reset the zero flag if ERIC is not in the dinner
                          ; hall
; This entry point is used by the routines at DOCLASS and ASSEMDUTY.
DINDUTY_0:
  LD BC,SEEKERIC          ; Redirect control to the routine at SEEKERIC (find
  CALL NZ,CALLSUBCMD      ; ERIC) if ERIC is not where he should be, then
                          ; return to DINDUTY_1 (below)
DINDUTY_1:
  JP NEXTCMD              ; Move to the next command in the command list

; Unused
  DEFB 0

; Control a teacher during assembly
;
; Used by command list 80. Makes the teacher stand still until the period is
; nearly over, and also perform assembly duty (if the teacher is MR WITHIT).
;
; H Teacher's character number (201-203)
ASSEMDUTY:
  LD A,(32740)            ; Collect the MSB of the lesson clock (which starts
                          ; off at 16)
  CP 2                    ; Has the teacher been standing still long enough
                          ; (the routine at DETENTION sets the MSB of the
                          ; lesson clock to 1 after MR WACKER has finished his
                          ; detention speech)?
  JP C,NEXTCMD            ; Move to the next command in the command list if so
  CP 11                   ; Is it time for the kids to sit down for assembly
                          ; yet?
  RET NC                  ; Return if not
  LD A,H                  ; A=teacher's character number
  CP 201                  ; Is this MR WITHIT (who does assembly duty)?
  RET NZ                  ; Return if not
  LD DE,LFLAGS            ; Set bits 6 (ERIC should be here now) and 7 (time to
  LD A,(DE)               ; sit down: this is checked by command lists 20, 40
  OR 192                  ; and 88) at LFLAGS
  LD (DE),A               ;
  LD A,(32740)            ; A=MSB of the lesson clock
  CP 10                   ; Is it time to find ERIC if he's absent?
  RET NC                  ; Return if not
  CALL PRESENT            ; Is ERIC in the assembly hall?
  RET Z                   ; Return if so
  JP DINDUTY_0            ; Go and find him otherwise

; Unused
  DEFS 3

; Make a teacher conduct a class
;
; Used by command lists 22, 24, 58, 60 and 62 to make a teacher conduct a class
; (after having walked to the edge of the blackboard).
;
; H Teacher's character number (201-204)
DOCLASS:
  LD A,(LESSONDESC)       ; LESSONDESC holds the current lesson descriptor
  AND 240                 ; Keep only the teacher-identifying bits and shift
  RLCA                    ; them into bits 0-3
  RLCA                    ;
  RLCA                    ;
  RLCA                    ;
  ADD A,199               ; Now A=character number of the teacher taking ERIC's
                          ; class (201-203)
  CP H                    ; Is this teacher conducting ERIC's class?
  JR Z,DOCLASS_5          ; Jump if so
; This teacher is not conducting ERIC's class, so he has it easy: wipe the
; board, write on the board (maybe), tell the class what to do, and pace up and
; down.
  LD BC,WIPE              ; Redirect control to the routine at WIPE (wipe
  CALL CALLSUBCMD         ; board) and return to DOCLASS_0 (below) when done
DOCLASS_0:
  LD L,1                  ; Point HL at byte 1 of the teacher's buffer
  LD A,(HL)               ; A=teacher's x-coordinate
  ADD A,3                 ; A=x-coordinate of the middle of the blackboard
  LD L,11                 ; Place this in byte 11 ready for the routine at WALK
  LD (HL),A               ;
  LD BC,WALK              ; Redirect control to the routine at WALK (walk to a
  CALL CALLSUBCMD         ; location) and return to DOCLASS_1 (below) when done
DOCLASS_1:
  CALL GETRANDOM          ; A=random number
  LD BC,WRITEBRD          ; Routine at WRITEBRD: write on board
  CP 160                  ; Should the teacher write on the board?
  CALL NC,CALLSUBCMD      ; If so, redirect control to the routine at WRITEBRD
                          ; and return to DOCLASS_2 (below) when done
DOCLASS_2:
  CALL TELLCLASS          ; Tell the class what to do
DOCLASS_3:
  LD L,1                  ; Point HL at byte 1 of the teacher's buffer
  LD A,(HL)               ; A=teacher's x-coordinate
  XOR 3                   ; Now A=x-coordinate of the location 3 paces behind
                          ; the teacher
  LD L,11                 ; Place this in byte 11 of the teacher's buffer,
  LD (HL),A               ; ready for the routine at WALK
  LD BC,WALK              ; Redirect control to the routine at WALK (walk to a
  CALL CALLSUBCMD         ; location) and return to DOCLASS_4 (below) when done
DOCLASS_4:
  JR DOCLASS_3            ; Make the teacher pace up (or down) again
; The teacher is taking ERIC's class. This is a far more difficult task.
DOCLASS_5:
  LD A,(LFLAGS)           ; Set bit 6 at LFLAGS, indicating that ERIC should be
  SET 6,A                 ; in class now
  LD (LFLAGS),A           ;
  CALL INCLASS            ; Are ERIC and EINSTEIN both in class?
  JR Z,DOCLASS_10         ; Jump if so
; ERIC is not in class, but EINSTEIN is. EINSTEIN must therefore grass.
DOCLASS_6:
  LD BC,SWOTSPK           ; Routine at SWOTSPK: make EINSTEIN talk
  LD E,18                 ; 18: Please Sir...ERIC is not here
  CALL CALLSUBCMD         ; Make EINSTEIN grass on ERIC, and return to
                          ; DOCLASS_7 (below) when done
DOCLASS_7:
  LD C,208                ; 208=EINSTEIN
  CALL SWOTLINES          ; Give lines to EINSTEIN (maybe)
  CALL PRESENT            ; Set the zero flag if ERIC is in class
  LD A,(LFLAGS)           ; LFLAGS holds various game status flags
  JR Z,DOCLASS_8          ; Jump if ERIC is in class
  SET 7,A                 ; Set bit 7 at LFLAGS: the teacher's next absence
  LD (LFLAGS),A           ; reprimand should be 'STAY TILL I DISMISS YOU'
  JP DINDUTY_0            ; Go and find ERIC
; ERIC is in class now, having shown up while EINSTEIN was grassing on him for
; being absent.
DOCLASS_8:
  RLCA                    ; Push bit 7 of LFLAGS into the carry flag
  LD B,20                 ; 20: STAY TILL I DISMISS YOU
  LD A,210                ; 210=ERIC
  JR C,DOCLASS_9          ; Jump if the teacher's already had to chase or give
                          ; lines to ERIC for being late or leaving early
  DEC B                   ; B=19: DON'T BE LATE AGAIN
DOCLASS_9:
  CALL SWOTLINES_1        ; Give ERIC lines
DOCLASS_10:
  LD A,(LFLAGS)           ; Set bits 6 (ERIC should be in class) and 7 (the
  OR 192                  ; teacher's next absence reprimand should be 'STAY
  LD (LFLAGS),A           ; TILL I DISMISS YOU') at LFLAGS
  CALL INCLASS            ; Is ERIC in class?
  JR NZ,DOCLASS_6         ; Jump if not
; Now is EINSTEIN's chance to grass on ERIC for hitting him.
  CALL GETRANDOM          ; A=random number
  CP 220                  ; Should EINSTEIN tell tales?
  JR C,DOCLASS_12         ; Jump if not
  LD E,87                 ; 87: Please Sir...ERIC hit me
  LD BC,SWOTSPK           ; Redirect control to the routine at SWOTSPK (make
  CALL CALLSUBCMD         ; EINSTEIN talk) and return to DOCLASS_11 (below)
                          ; when done
DOCLASS_11:
  LD BC,25042             ; B=97: DON'T HIT YOUR MATES; C=210 (ERIC)
  CALL SWOTLINES          ; Give EINSTEIN or ERIC lines
  CALL INCLASS            ; Is ERIC in class?
  JR NZ,DOCLASS_6         ; Jump if not
; Now is EINSTEIN's opportunity to grass on someone for writing on the
; blackboard. However, the opportunity is wasted, because the CALL to BOARDID
; (at DOCLASS_12 below) returns a valid blackboard ID only if the character is
; standing within four spaces of the left edge of the blackboard, which is not
; the case when the teacher is waiting for EINSTEIN to grass (teachers stand at
; the right edge of the blackboard for that, more than four spaces away). This
; is a bug.
DOCLASS_12:
  CALL BOARDID            ; Collect information about the blackboard
  INC B                   ; The intent here is to point BC at the second byte
  LD C,B                  ; of the blackboard's buffer, which holds the
  LD B,127                ; character number of the person who last wrote on
                          ; it; instead, BC points at 32516 if the teacher is
                          ; on the top floor, or 32515 if he's on the middle
                          ; floor
  LD A,(BC)               ; Now A is supposed to hold the character number of
                          ; the person who last wrote on the board; instead it
                          ; holds the contents of 32515 or 32516 (bytes 3 and 4
                          ; of the SRB), which will never be 206 or 210
  CP 210                  ; Did ERIC write on the board?
  JR Z,DOCLASS_13         ; Jump if so (this jump is never made)
  CP 206                  ; Or did BOY WANDER?
  JR NZ,DOCLASS_15        ; Jump if not (this jump is always made)
  CALL GETRANDOM          ; A=random number
  CP 195                  ; Should EINSTEIN blame BOY WANDER for writing on the
                          ; board?
  LD A,206                ; 206=BOY WANDER
  JR C,DOCLASS_13         ; Jump if so
  LD A,210                ; Blame ERIC instead
DOCLASS_13:
  LD (MSG007),A           ; Place the character number of the scapegoat into
                          ; MSG007
  LD BC,SWOTSPK           ; Routine at SWOTSPK: make EINSTEIN talk
  LD E,88                 ; 88: Please Sir...[7] wrote on the board
  CALL CALLSUBCMD         ; Redirect control to the routine at SWOTSPK and
                          ; return to DOCLASS_14 (below) when done
DOCLASS_14:
  LD A,(MSG007)           ; A=206 (BOY WANDER) or 210 (ERIC)
  LD C,A                  ; Copy the scapegoat's character number to C
  LD B,86                 ; 86: DON'T TOUCH BLACKBOARDS
  CALL SWOTLINES          ; Give lines to EINSTEIN, ERIC or BOY WANDER
; The next section of code makes the teacher wipe the board. Note that the
; teacher may already have wiped the board at this point, and therefore will
; not be standing at the right edge of the board (the correct location to begin
; wiping); this is a bug.
DOCLASS_15:
  LD BC,WIPE              ; Redirect control to the routine at WIPE (wipe
  CALL CALLSUBCMD         ; board) and return to DOCLASS_16 (below) when done
DOCLASS_16:
  LD L,1                  ; Point HL at byte 1 of the teacher's buffer
  LD A,(HL)               ; A=teacher's x-coordinate
  ADD A,3                 ; A=x-coordinate of the middle of the blackboard
  LD L,11                 ; Place this in byte 11 of the teacher's buffer,
  LD (HL),A               ; ready for the routine at WALK
  LD BC,WALK              ; Redirect control to the routine at WALK (walk to
  CALL CALLSUBCMD         ; location) and return to DOCLASS_17 (below) when
                          ; done
; The teacher has wiped the board, and is ready to start the class.
DOCLASS_17:
  CALL INCLASS            ; Is ERIC in class?
DOCLASS_18:
  JP NZ,DOCLASS_6         ; Jump if not
  CALL GETRANDOM          ; A=random number
  LD BC,WRITEBRD          ; Routine at WRITEBRD: write on board
  CP 184                  ; Should the teacher write on the board?
  CALL NC,CALLSUBCMD      ; Redirect control to WRITEBRD if so, and return to
                          ; DOCLASS_19 (below) when done
DOCLASS_19:
  CALL INCLASS            ; Is ERIC in class?
  JR NZ,DOCLASS_18        ; Jump if not
  CALL GETRANDOM          ; A=random number
  CP 232                  ; Should this be a questions-and-answers lesson?
  JR C,DOCLASS_22         ; Jump if so
; This is not going to be a questions-and-answers lesson.
  CALL TELLCLASS          ; Tell the class what to do
DOCLASS_20:
  CALL INCLASS            ; Is ERIC in class?
  JR NZ,DOCLASS_18        ; Jump if not
  LD L,1                  ; Point HL at byte 1 of the teacher's buffer
  LD A,(HL)               ; A=teacher's x-coordinate
  XOR 3                   ; Now A=x-coordinate of location three paces behind
                          ; the teacher
  LD L,11                 ; Place this in byte 11 of the teacher's buffer ready
  LD (HL),A               ; for the routine at WALK
  LD BC,WALK              ; Redirect control to the routine at WALK (walk to
  CALL CALLSUBCMD         ; location) and return to DOCLASS_21 (below) when
                          ; done
DOCLASS_21:
  JR DOCLASS_20           ; Check whether ERIC's in class, then pace up (or
                          ; down) again
; This is going to be a questions-and-answers lesson. Pick a question-answer
; word pair, and decide which word will be in the question, and which in the
; answer.
DOCLASS_22:
  CALL GETRANDOM          ; A=random number
  AND 135                 ; Keep only bits 0-2 (the word pair identifier) and
                          ; bit 7 (indicates which word will be in the
                          ; question)
  PUSH AF                 ; Save bit 7 for now
  LD C,A
  LD DE,MSG010            ; Point DE at the first byte of message 10
  LD A,H                  ; Set A=160 (if the teacher is MR WITHIT), 176 (MR
  SUB D                   ; ROCKITT), or 192 (MR CREAK)
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  ADD A,A                 ;
  OR C                    ; Now A holds the message number of a random
                          ; mountain, animal or king
  LD (DE),A               ; Place this into MSG010 (message 10)
  ADD A,8                 ; Set A to the message number of the corresponding
                          ; country, habitat or date
  LD E,142                ; Place this into MSG011 (the first byte of message
  LD (DE),A               ; 11)
  POP AF                  ; Restore the random bit 7 to A
  RLCA                    ; Push it into the carry flag
  SBC A,A                 ; Set A to 73 or 75 (MR WITHIT), 77 or 79 (MR
  ADD A,H                 ; ROCKITT), 81 or 83 (MR CREAK); this is the message
  ADD A,H                 ; number for EINSTEIN's reply
  ADD A,A                 ;
  ADD A,39                ;
  LD (53259),A            ; Store it in byte 11 of EINSTEIN's buffer, ready for
                          ; the routine at SWOTSPK_0
  INC A                   ; Set E to 74 or 76 (MR WITHIT), 78 or 80 (MR
  LD E,A                  ; ROCKITT), 82 or 84 (MR CREAK); this is the message
                          ; number for the teacher's question
  LD BC,SPEAK             ; Redirect control to the routine at SPEAK (make
  CALL CALLSUBCMD         ; character speak) and return to DOCLASS_23 (below)
                          ; when done
DOCLASS_23:
  LD BC,SWOTSPK_0         ; Redirect control to the routine at SWOTSPK_0 (make
  CALL CALLSUBCMD         ; EINSTEIN answer) and return to DOCLASS_24 (below)
                          ; when done
DOCLASS_24:
  CALL INCLASS            ; Is ERIC in class?
  JR Z,DOCLASS_22         ; Jump if so
  JP DOCLASS_6            ; Otherwise make EINSTEIN grass on the absent ERIC

; Unused
  DEFS 4

; Make MR WACKER put the kids in detention
;
; Used by command list 72. Builds a detention message (using a random noun and
; verb) and makes MR WACKER deliver it.
;
; H 200 (MR WACKER)
DETENTION:
  LD A,(LEFTCOL)          ; A=leftmost column of the play area on screen
  CP 48                   ; Is the stage off-screen to the right?
  RET C                   ; Return if so
  CP 88                   ; Is the stage off-screen to the left?
  RET NC                  ; Return if so
  CALL GETRANDOM          ; A=random number
  LD DE,MSG010            ; Point DE at the first byte of message 10, which
                          ; will be the verb
  LD L,A                  ; Save the random number in L briefly
  AND 7                   ; Store the verb message number (144, 145, 146, 147,
  ADD A,144               ; 148, 149, 150 or 151) at MSG010
  LD (DE),A               ;
  LD A,L                  ; Restore the random number to A
  RRCA                    ; Set A to another random number from 0 to 7
  RRCA                    ;
  RRCA                    ;
  AND 7                   ;
  LD E,142                ; Point DE at the first byte of message 11, which
                          ; will be the noun
  ADD A,152               ; Store the noun message number (152, 153, 154, 155,
  LD (DE),A               ; 156, 157, 158 or 159) at MSG011
  LD E,96                 ; Message 96: YOU'RE ALL IN DETENTION...
  LD BC,SPEAK             ; Redirect control to the routine at SPEAK (make
  CALL CALLSUBCMD         ; character speak) and then return to DETENTION_0
                          ; (below)
; Control of MR WACKER resumes here when he has finished delivering the
; detention message.
DETENTION_0:
  EX DE,HL                ; Save MR WACKER's character number in D briefly
  LD HL,LFLAGS            ; Reset bit 6 (ERIC no longer has to be in the
  RES 7,(HL)              ; assembly hall) and bit 7 (the kids can stand up
  RES 6,(HL)              ; now) at LFLAGS
  NOP                     ;
  NOP                     ;
  NOP                     ;
  NOP                     ;
  LD L,228                ; Set the MSB of the lesson clock to 1 so that the
  LD (HL),1               ; bell will ring soon
  EX DE,HL                ; Restore MR WACKER's character number (200) to H
  JP NEXTCMD              ; Move to the next command in the command list

; Unused
  DEFS 2

; Deal with ERIC when he's been knocked over
;
; Used by the routine at HANDLEERIC when bit 7 at STATUS is set (by the routine
; at OBJFALL). Note that the routine at CLEARSEAT also sets bit 7 at STATUS
; when someone sits in the seat occupied by ERIC, but in that case this routine
; is not called because bit 2 at STATUS is still set (indicating that ERIC is
; sitting) and takes precedence; this is a bug.
ERICHIT:
  LD HL,KODELAY           ; KODELAY holds ERIC's knockout delay counter
  LD A,(HL)               ; Is this the first time this routine's been called
  AND A                   ; since ERIC was downed?
  JR Z,ERICHIT_0          ; Jump if so
  DEC (HL)                ; Otherwise wait a while before enabling ERIC to
  RET NZ                  ; stand up again
  LD L,243                ; HL=ERICTIMER (ERIC's main action timer)
  LD (HL),1               ; Set this to 1 so that the keyboard is checked on
                          ; the next pass through the main loop
  LD L,251                ; HL=STATUS (ERIC's status flags)
  LD (HL),4               ; Set bit 2: ERIC is sitting or lying down; the
                          ; routine at ERICSITLIE handles ERIC from this point
  RET
; ERIC has just been hit. Determine the manner of his descent to the floor.
ERICHIT_0:
  LD (HL),40              ; Initialise ERIC's knockout delay counter at KODELAY
                          ; to 40
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  PUSH DE                 ; Save ERIC's coordinates briefly
  PUSH AF                 ; Save ERIC's current animatory state briefly
  CP 4                    ; 4: Was ERIC sitting on a chair?
  JR NZ,ERICHIT_1         ; Jump if not
  CALL ERICLOCID          ; Get the identifier of ERIC's location
  LD B,5                  ; 5: ERIC sitting on the floor
  CP 6                    ; Is ERIC in a classroom in the boys' skool (A=6, 7
                          ; or 8)?
  JR NC,ERICHIT_2         ; Jump if so
ERICHIT_1:
  LD B,6                  ; 6: ERIC lying on his back
ERICHIT_2:
  POP AF                  ; Restore ERIC's current animatory state to A
  POP DE                  ; Restore ERIC's coordinates to DE
  AND 128                 ; Keep only the 'direction' bit (bit 7) of ERIC's
                          ; current animatory state
  ADD A,B                 ; A=ERIC's new animatory state (sitting on the floor
                          ; or lying on his back)
  CALL UPDATEAS           ; Update ERIC's animatory state and update the SRB
  CALL UPDATESCR          ; Update the display
; Now to initialise the sound effect parameters.
ERICHIT_3:
  LD HL,275               ; H=1 (pitch adjustment), L=19 (border colour XOR
                          ; mask)
; This entry point is used by the routine at JUMPING with H=2 and L=22.
ERICHIT_4:
  LD DE,0                 ; D=0 (duration), E=0 (initial pitch)
  LD A,2                  ; A=2 (initial border colour)
; This entry point is used by the routines at ERICSITLIE (with H=255, L=23,
; D=255, E=255) and PREPCATTY (with H=248, L=18, D=128, E=0, A=4).
ERICHIT_5:
  XOR L                   ; Make a sound effect
  OUT (254),A             ;
  LD B,E                  ;
ERICHIT_6:
  DJNZ ERICHIT_6          ;
  LD B,A                  ;
  LD A,E                  ;
  ADD A,H                 ;
  LD E,A                  ;
  LD A,B                  ;
  DEC D                   ;
  JR NZ,ERICHIT_5         ;
; This entry point is used by the routine at PLAYTUNE.
ERICHIT_7:
  EI                      ; Re-enable interrupts
  LD A,1                  ; Reset the border colour to blue
  OUT (254),A             ;
  RET

; Unused
  DEFS 3

; Deal with ERIC when he's sitting or lying down
;
; Used by the routine at HANDLEERIC when bit 2 at STATUS is set (by the routine
; at ERICHIT, SIT or FALLING). Makes ERIC stand up if 'S' is pressed, or open a
; desk (and collect any contents) if 'O' is pressed.
ERICSITLIE:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  LD A,(GAMEMODE)         ; GAMEMODE holds 255 if we're in demo mode, 0
                          ; otherwise
  INC A                   ; Are we in demo mode?
  JR NZ,ERICSITLIE_5      ; Jump if not
; We're in demo mode. Figure out ERIC's next move depending on what little boy
; no. 10 is doing.
  CALL EXITDEMO           ; Exit demo mode and start a new game if a key was
                          ; pressed; otherwise return here with ERIC's
                          ; animatory state in A
  LD HL,BOY10CBUF         ; Point HL at byte 0 of little boy no. 10's buffer
  CP 4                    ; 4: Is ERIC sitting in a chair?
  JR Z,ERICSITLIE_0       ; Jump if so
  CP 133                  ; 133: Is ERIC sitting on the floor facing right?
  JR NZ,ERICSITLIE_2      ; Jump if not
ERICSITLIE_0:
  ADD A,64                ; A=68 (little boy sitting on a chair) or 197 (little
                          ; boy sitting on the floor facing right)
  CP (HL)                 ; Compare ERIC's animatory state with that of little
                          ; boy no. 10
  LD HL,UPDELAY           ; UPDELAY holds the counter that determines the delay
                          ; between little boy no. 10 standing up and ERIC
                          ; standing up
  JR NZ,ERICSITLIE_1      ; Jump if ERIC's not doing what little boy no. 10 is
                          ; doing
  LD (HL),45              ; Initialise the stand-up delay counter at UPDELAY to
                          ; 45
  RET
ERICSITLIE_1:
  DEC (HL)                ; Decrement the stand-up delay counter at UPDELAY
  RET NZ                  ; Return if it's not time for ERIC to stand up
ERICSITLIE_2:
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  LD (HL),0               ; Clear all of them (ERIC is no longer sitting or
                          ; lying down)
; This entry point is used by the routine at WRITING.
ERICSITLIE_3:
  CALL WALKSOUND          ; Make a sound effect
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  AND 128                 ; A=0/128: ERIC standing up
; This entry point is used by the routines at JUMPING, WRITE, STEPOFF and
; LANDING.
ERICSITLIE_4:
  CALL UPDATEAS           ; Update ERIC's animatory state and location and
                          ; update the SRB
  LD A,6                  ; Set ERIC's main action timer (at ERICTIMER) to 6
  LD (ERICTIMER),A        ;
  RET
; We're not in demo mode.
ERICSITLIE_5:
  CALL READKEY            ; Collect the code of the last key pressed in A
  RET Z                   ; Return if no keys were pressed
  RES 5,A                 ; Normalise the code to upper case
  CP 83                   ; Was 'S' pressed?
  JR Z,ERICSITLIE_2       ; Make ERIC stand up if so
  CP 79                   ; Was 'O' pressed?
  RET NZ                  ; Return if not
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  CP 4                    ; 4: Is ERIC sitting in a chair?
  RET NZ                  ; Return if not
; ERIC is sitting in a chair and 'O' (open desk) was pressed. Compute the ID of
; the desk ERIC is sitting at so we can determine whether he's found the water
; pistol or the stinkbombs.
  LD A,(54785)            ; Pick up the x-coordinate of whatever object is
                          ; using buffer 214
  CP 192                  ; Is this buffer being used at the moment?
  RET C                   ; Return if so
  LD DE,(53761)           ; Pick up ERIC's coordinates in DE
  LD A,D                  ; A=ERIC's y-coordinate
  CP 10                   ; Is ERIC on the middle floor?
  JR Z,ERICSITLIE_7       ; Jump if so
  CP 3                    ; Is ERIC on the top floor?
  RET NZ                  ; Return if not
  LD A,E                  ; A=ERIC's x-coordinate
  CP 22                   ; Is ERIC in the Blue Room?
  JR C,ERICSITLIE_6       ; Jump if so
  SUB 27
  CP 34                   ; Is ERIC in the Yellow Room?
  JR C,ERICSITLIE_6       ; Jump if so
  SUB 106
ERICSITLIE_6:
  SUB 9
  JR ERICSITLIE_9
ERICSITLIE_7:
  LD A,E                  ; A=ERIC's x-coordinate
  CP 53                   ; Is ERIC in the Science lab?
  JR C,ERICSITLIE_8       ; Jump if so
  SUB 113
ERICSITLIE_8:
  SUB 2
ERICSITLIE_9:
  RRA
  LD B,A
; At this point B holds the identifier of the desk that ERIC's sitting at.
;
; +----------+---------------------------------------+
; | Desk IDs | Room                                  |
; +----------+---------------------------------------+
; | 1-6      | Blue Room                             |
; | 7-12     | Yellow Room                           |
; | 13-18    | Top-floor room in the girls' skool    |
; | 19-25    | Science Lab                           |
; | 26-31    | Middle-floor room in the girls' skool |
; +----------+---------------------------------------+
;
; Now we check whether the water pistol has been placed in a desk yet.
  LD HL,WPDESKID          ; WPDESKID holds the ID of the desk containing the
                          ; water pistol
  LD A,(HL)               ; Has the water pistol been placed in a desk yet?
  AND A                   ;
  JR NZ,ERICSITLIE_11     ; Jump if so
; The water pistol hasn't been placed in a desk yet. Pick one at random that
; doesn't match the desk containing the stinkbombs.
ERICSITLIE_10:
  CALL GETRANDOM          ; A=random desk ID from 1 to 31
  AND 31                  ;
  JR Z,ERICSITLIE_10      ;
  LD L,219                ; HL=SBDESKID (which holds the ID of the desk
                          ; containing the stinkbombs)
  CP (HL)                 ; Is this the desk containing the stinkbombs?
  JR Z,ERICSITLIE_10      ; Get another random desk ID if so
  DEC L                   ; Otherwise place the newly determined ID of the desk
  LD (HL),A               ; containing the water pistol into WPDESKID
; Check whether ERIC has found the water pistol.
ERICSITLIE_11:
  CP B                    ; Is ERIC sitting at the desk that contains the water
                          ; pistol?
  JR NZ,ERICSITLIE_12     ; Jump if not
  LD (HL),0               ; Set WPDESKID to 0 (the water pistol is not in any
                          ; desk now)
  LD L,235                ; HL=INVENTORY (inventory flags)
  LD A,(HL)               ; Pick these up in A
  AND 24                  ; Keep only bits 3 and 4 (the pistol bits)
  LD A,44                 ; 44: animatory state of a desk lid
  JR NZ,ERICSITLIE_15     ; Jump if ERIC already has a water pistol
  SET 3,(HL)              ; Give ERIC a water pistol (full of water)
  INC A                   ; 45: animatory state of a desk lid with a water
                          ; pistol
  JR ERICSITLIE_15
; There was no water pistol in the desk. Check whether there are stinkbombs.
ERICSITLIE_12:
  INC L                   ; HL=SBDESKID (which holds the ID of the desk
                          ; containing the stinkbombs)
  LD A,(HL)               ; Have the stinkbombs been placed in a desk yet?
  AND A                   ;
  JR NZ,ERICSITLIE_14     ; Jump if so
; The stinkbombs haven't been placed in a desk yet. Pick one at random that
; doesn't match the desk containing the water pistol.
ERICSITLIE_13:
  CALL GETRANDOM          ; A=random desk ID from 1 to 31
  AND 31                  ;
  JR Z,ERICSITLIE_13      ;
  LD L,218                ; HL=WPDESKID (which holds the ID of the desk
                          ; containing the water pistol)
  CP (HL)                 ; Is this the desk containing the water pistol?
  JR Z,ERICSITLIE_13      ; Get another random desk ID if so
  INC L                   ; Otherwise place the newly determined ID of the desk
  LD (HL),A               ; containing the stinkbombs into SBDESKID
; Check whether ERIC has found the stinkbombs.
ERICSITLIE_14:
  CP B                    ; Compare the ID of the desk ERIC's sitting at (A)
                          ; with the ID of the desk containing the stinkbombs
                          ; (B)
  LD A,44                 ; 44: animatory state of a desk lid
  JR NZ,ERICSITLIE_15     ; Jump if ERIC isn't sitting at the desk that
                          ; contains the stinkbombs
  LD L,235                ; HL=INVENTORY (inventory flags)
  LD A,(HL)               ; Set bits 5-7, giving ERIC three stinkbombs
  OR 224                  ;
  LD (HL),A               ;
  LD A,46                 ; 46: animatory state of a desk lid with stinkbombs
; Now A holds the animatory state of the desk lid: 44 (empty desk), 45 (with
; pistol), or 46 (with stinkbombs).
ERICSITLIE_15:
  LD HL,54803             ; Point HL at byte 19 of the desk lid's buffer
  LD (HL),8               ; Initialise the counter determining how long the
                          ; desk lid stays up
  DEC E                   ; E=x-coordinate of the desk lid in front of ERIC
  CALL SUBCMD             ; Update the SRB for the desk lid's appearance and
                          ; place address DESKLID (below) into bytes 17 and 18
                          ; of its buffer
; The address of this entry point is placed into bytes 17 and 18 of the desk
; lid's buffer by the instruction above.
DESKLID:
  LD L,19                 ; Byte 19 of the desk lid's buffer holds the delay
                          ; counter that determines when the desk lid will shut
  DEC (HL)                ; Decrement this counter
  LD A,(HL)               ; Copy its current value to A
  JP Z,OBJFALL_0          ; Jump if it's time for the desk lid to close
  SUB 6                   ; Is it time for ERIC to collect the contents of the
                          ; desk?
  RET NZ                  ; Return if not
  LD L,A                  ; L=0
  LD A,(HL)               ; A=animatory state of a desk lid (with contents, if
                          ; any)
  CP 44                   ; 44: Is the desk empty?
  RET Z                   ; Return if so
; This entry point is used by the routines at CHKCOMBOS, ONSADDLE and FROG2INV.
ERICSITLIE_16:
  CALL PRINTINV           ; Print the inventory
; This entry point is used by the routine at KISS.
ERICSITLIE_17:
  LD C,5                  ; Make a celebratory sound effect (ERIC has achieved
ERICSITLIE_18:
  LD B,3                  ; something rather handy)
  LD HL,65303             ;
  LD D,H                  ;
  LD E,H                  ;
  CALL ERICHIT_5          ;
  DEC C                   ;
  JR NZ,ERICSITLIE_18     ;
  RET

; Unused
  DEFS 3

; 'S' pressed - sit (1)
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'S' is
; pressed.
SIT:
  CALL ONSTAIRS           ; Is ERIC on a staircase?
  RET C                   ; Return if so
  LD A,4                  ; Set bit 2 of ERIC's status flags at STATUS: ERIC is
  LD (STATUS),A           ; sitting
  JR SIT2                 ; Skip over the routine at SITDOWN

; Unused
  DEFS 4

; Make ERIC sit in a chair or on the floor
;
; Used by the routine at SIT2.
SITDOWN:
  LD A,4                  ; 4: ERIC sitting in a chair
; This entry point is used by the routine at SIT2 (with A=5/133) to make ERIC
; sit on the floor.
SITDOWN_0:
  PUSH AF                 ; Save ERIC's new animatory state briefly
  CALL WALKSOUND          ; Make a sitting-down sound effect
  POP AF                  ; Restore ERIC's new animatory state to A
  LD HL,ERICTIMER         ; Initialise ERIC's main action timer at ERICTIMER to
  LD (HL),5               ; 5
  LD H,210                ; 210=ERIC
; This entry point is used by the routines at CATCH and WATCHERIC.
SITDOWN_1:
  PUSH AF                 ; Save the character's new animatory state briefly
  CALL UPDATESRB          ; Update the SRB for the character's current
                          ; animatory state
  POP AF                  ; Restore the character's new animatory state to A
  JP UPDATEAS             ; Update the character's animatory state and update
                          ; the SRB

; 'S' pressed - sit (2)
;
; Continues from SIT. Makes ERIC sit on a chair (if he's standing beside one)
; or on the floor.
;
; H 210 (ERIC)
SIT2:
  CALL BYSEAT             ; Check for chairs next to ERIC
  AND A                   ; Is ERIC standing beside one, facing left?
  JR Z,SIT2_0             ; Jump if so
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  AND 128                 ; Keep only the 'direction' bit (bit 7)
  ADD A,5                 ; A=5/133: ERIC sitting on the floor
  JR SITDOWN_0            ; Sit ERIC on the floor
SIT2_0:
  CALL CLEARSEAT          ; Knock anybody who's sitting in this chair out of
                          ; the way
  JR SITDOWN              ; Sit ERIC on the chair

; Unused
  DEFS 3

; Collect a keypress during the game (or simulate one in demo mode)
;
; Called from the main loop at MAINLOOP. Returns with A holding the offset from
; the keypress table corresponding to the last (actual or simulated) keypress,
; or 0 if there was no (actual or simulated) keypress.
GETINPUT:
  LD A,(GAMEMODE)         ; A=255 if we're in demo mode, 0 otherwise
  INC A                   ; Are we in demo mode?
  JP NZ,KEYOFFSET         ; Read the keyboard if not
; We're in demo mode.
  LD HL,0                 ; Set the score (held at SCORE) and the number of
  LD (SCORE),HL           ; lines (held at LINES) to 0
  LD (LINES),HL           ;
  CALL READKEY            ; Check for keypresses
  JP NZ,START             ; Start a new game if there was one
; No keys have been pressed, so figure out ERIC's next move by seeing what
; little boy no. 10 is doing.
  LD DE,(50945)           ; Pick up the coordinates of little boy no. 10 in DE
  LD H,210                ; 210=ERIC
  CALL NEXTMV             ; Compare the coordinates of ERIC and little boy no.
                          ; 10
  AND A                   ; Are they the same?
  JR Z,GETINPUT_1         ; Jump if so
  DEC A                   ; Now A=0 (ERIC is on a staircase facing up), 1 (on a
                          ; staircase facing down), 2 (to the right of little
                          ; boy no. 10), or 3 (to his left)
  LD HL,23672             ; Point HL at the LSB of the system variable FRAMES
  XOR 2                   ; Flip bit 1 of A
; Now A holds a value that will be translated into a simulated keypress
; depending on ERIC's location:
;
; +---+-----------------------------------+----------+
; | A | ERIC's location                   | Keypress |
; +---+-----------------------------------+----------+
; | 0 | To the right of little boy no. 10 | Left     |
; | 1 | To the left of little boy no. 10  | Right    |
; | 2 | On a staircase, facing up         | Up       |
; | 3 | On a staircase, facing down       | Down     |
; +---+-----------------------------------+----------+
  BIT 7,(HL)              ; Is the LSB of FRAMES < 128?
  JR Z,GETINPUT_0         ; Jump if so: this will be an upper case (fast)
                          ; keypress
  ADD A,4                 ; 4<=A<=7: force a lower case (slow) keypress
GETINPUT_0:
  ADD A,A                 ; A=80+2n where 0<=n<=7 (simulated keypress for LEFT,
  ADD A,80                ; RIGHT, UP, DOWN or left, right, up, down)
  RET
; ERIC's coordinates match those of little boy no. 10 exactly.
GETINPUT_1:
  LD A,(BOY10CBUF)        ; A=animatory state of little boy no. 10
  CP 68                   ; 68: little boy sitting in a chair
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC's buffer
  JR Z,GETINPUT_2         ; Jump if little boy no. 10 is sitting in a chair
  CP 197                  ; 197: Is little boy no. 10 sitting on the floor
                          ; facing right (as in assembly)?
  JR Z,GETINPUT_2         ; Jump if so
  XOR A                   ; No simulated keypress (ERIC stays still)
  RET
; Little boy no. 10 is sitting in a chair or sitting on the floor facing right
; (as in assembly), and ERIC is standing. Make ERIC face the same way and sit
; too (in the same spot as little boy no. 10).
GETINPUT_2:
  XOR (HL)                ; Set the carry flag if ERIC and little boy no. 10
  RLCA                    ; are facing in opposite directions
  LD A,98                 ; 98=keypress code for 'S' (sit)
  RET NC                  ; Return if ERIC and little boy no. 10 are facing the
                          ; same way
  LD A,(HL)               ; A=ERIC's animatory state
  RLCA                    ; Set A to 1 if ERIC is facing left (leading to a
  SBC A,A                 ; simulated keypress code of 82, or RIGHT), 0 if he's
  INC A                   ; facing right (leading to a simulated keypress code
  JR GETINPUT_0           ; of 80, or LEFT); that is, make ERIC turn round

; Make the next command be the start of the command list
;
; Used by command lists 22, 24, 28 and 78. Copies the address of the point
; reached in the current command list (stored in bytes 25 and 26 of the
; character's buffer) into bytes 27 and 28 (where the start address of the
; current command list is stored). This means that whenever the command list is
; restarted, it will restart from this point instead of the actual beginning.
;
; H 204 (MISS TAKE) or 205 (ALBERT)
CLSTART:
  LD L,25                 ; Copy the address reached in the current command
  LD D,H                  ; list from bytes 25 and 26 into bytes 27 and 28
  LD E,27                 ; (where the command list's start address is usually
  LDI                     ; stored)
  LDI                     ;
  JP NEXTCMD              ; Move to the next command in the command list

; Restart the command list unless the boys' skool door is closed
;
; Used by command lists 22 and 24. If the boys' skool door is open, makes MISS
; TAKE close the drinks cabinet door (if it's open) and restart the command
; list; otherwise moves to the next command in the command list.
;
; H 204 (MISS TAKE)
RSTDROPEN:
  LD A,(DOORFLAGS)        ; DOORFLAGS holds the door/window status flags
  BIT 3,A                 ; Is the boys' skool door closed?
  JP Z,NEXTCMD            ; Move to the next command in the command list if so
  BIT 5,A                 ; Is the drinks cabinet door closed?
  JP Z,RESTART            ; Restart the command list if so
  LD L,20                 ; Point HL at byte 20 of MISS TAKE's buffer
  LD (HL),0               ; Place 0 (close door) in byte 20, and 32 (bit 5 set:
  DEC L                   ; drinks cabinet door) in byte 19, ready for the
  LD (HL),32              ; routine at CHRMVDOOR
  JP DOOROPEN_2           ; Close the drinks cabinet door

; Unused
  DEFS 4

; Jump forward in the command list if the boys' skool door or the gate is
; closed
;
; Used by command list 76. Saves ALBERT a trip to the skool gate or the skool
; door if it's already closed.
;
; H 205 (ALBERT)
CLISTJR10:
  LD DE,DOORFLAGS         ; DOORFLAGS holds the door/window status flags
  CALL GETPARAM           ; Collect the next byte (the door identifier) from
                          ; the command list
  EX DE,HL
  AND (HL)                ; Set the zero flag if this door is closed
  EX DE,HL
  JR NZ,CLISTJR10_1       ; Jump if the specified door is open
  LD B,10                 ; Move along 10 places in the command list
CLISTJR10_0:
  CALL GETPARAM           ;
  DJNZ CLISTJR10_0        ;
CLISTJR10_1:
  JP NEXTCMD              ; Move to the next command in the command list

; Wait till everyone has gone past the gate or the boys' skool door
;
; Used by command list 76. Makes ALBERT wait at the skool gate or the boys'
; skool door until all the female characters are on the girls' side of the gate
; and all the male characters are to his left.
;
; H 205 (ALBERT)
WAITDOOR:
  LD L,1                  ; Point HL at byte 1 of ALBERT's buffer
  LD E,(HL)               ; E=ALBERT's x-coordinate
  LD H,209                ; 209=HAYLEY
  LD A,(HL)               ; A=HAYLEY's x-coordinate
  LD H,182
  LD B,4                  ; There are 4 little girls (183-186) who sometimes
                          ; venture past the skool gate
  JR WAITDOOR_1
WAITDOOR_0:
  LD A,(HL)               ; A=girl's x-coordinate
WAITDOOR_1:
  CP 136                  ; Is this girl on the right side of the gate?
  RET C                   ; Return if not
  INC H                   ; Next girl
  DJNZ WAITDOOR_0         ; Jump back until HAYLEY and girls 1-4 have been
                          ; checked
; The female characters are safely on the right side of the gate. Now check the
; male characters.
  LD A,E                  ; A=ALBERT's x-coordinate
  SUB 2
  LD H,210                ; 210=ERIC
  CP (HL)                 ; Is ERIC at least 2 spaces to the left of ALBERT?
  RET C                   ; Return if not
  LD H,193                ; 193=little boy no. 4
  LD B,11                 ; There are 7 little boys and 4 male teachers who may
                          ; venture out of the boys' skool
WAITDOOR_2:
  CP (HL)                 ; Is this character at least 2 spaces to the left of
                          ; ALBERT?
  RET C                   ; Return if not
  INC H                   ; Next teacher or little boy
  DJNZ WAITDOOR_2         ; Jump back until little boys 4-10 and the male
                          ; teachers have been checked
; ERIC, the male teachers and all the little boys are safely to ALBERT's left.
; Now check the remaining boys.
  LD H,206                ; 206=BOY WANDER
  LD B,3                  ; 3 main boys: BOY WANDER, ANGELFACE and EINSTEIN
WAITDOOR_3:
  CP (HL)                 ; Is this character at least 2 spaces to the left of
                          ; ALBERT?
  RET C                   ; Return if not
  INC H                   ; Next main boy
  DJNZ WAITDOOR_3         ; Jump back until the three main boys have been
                          ; checked
  JP NEXTCMD              ; Move to the next command in the command list now
                          ; it's safe to shut the gate or door

; Unused
  DEFB 0

; Make ALBERT keep an eye out for ERIC during lessons
;
; The address of this continual subcommand routine is placed into bytes 23 and
; 24 of ALBERT's buffer by command list 76. Makes ALBERT raise his arm and
; alert MR WACKER if ERIC enters the 'danger zone' (within 6 spaces to the left
; of ALBERT). Also makes ALBERT lower his arm if ERIC leaves the danger zone.
;
; H 205 (ALBERT)
WATCHERIC:
  LD L,0                  ; Point HL at byte 0 of ALBERT's buffer
  LD A,(HL)               ; A=ALBERT's animatory state
  OR 250                  ; Is ALBERT midstride or lying on his back?
  RET PO                  ; Return if so
  LD DE,(53761)           ; Collect ERIC's coordinates in DE
  LD A,D                  ; A=ERIC's y-coordinate
  CP 14                   ; Is ERIC above the level of the stage?
  JR C,WATCHERIC_0        ; Jump if so
  LD A,E                  ; A=ERIC's x-coordinate
  CP 96                   ; Is ERIC inside the boys' skool?
  JR C,WATCHERIC_0        ; Jump if so
  LD L,1                  ; Point HL at byte 1 of ALBERT's buffer
  CP (HL)                 ; Is ERIC to the left of ALBERT?
  JR NC,WATCHERIC_0       ; Jump if not
  ADD A,6                 ; Is ERIC within 6 spaces to the left of ALBERT?
  CP (HL)                 ;
  JR NC,WATCHERIC_1       ; Jump if so
; ERIC's not in the danger zone. Make ALBERT lower his arm if he has it raised.
WATCHERIC_0:
  LD L,0                  ; Point HL at byte 0 of ALBERT's buffer
  LD A,(HL)               ; A=ALBERT's animatory state
  CP 127                  ; 127: Is ALBERT facing left with his arm up?
  RET NZ                  ; Return if not
  POP BC                  ; Drop the return address (MVCHARS_11) from the stack
  POP BC                  ; Drop the character buffer reference used by the
                          ; routine at MVCHARS
  LD A,120                ; 120: ALBERT facing left, standing, with his arm
                          ; down
  JP SITDOWN_1            ; Update ALBERT's animatory state and SRB
; ERIC is in the danger zone near ALBERT.
WATCHERIC_1:
  DEC L                   ; L=0
  POP BC                  ; Drop the return address (MVCHARS_11) from the stack
  POP BC                  ; Drop the character buffer reference used by the
                          ; routine at MVCHARS
  BIT 7,(HL)              ; Is ALBERT facing right?
  JP NZ,WALK_7            ; Turn ALBERT round if so
  LD A,127                ; 127: ALBERT facing left with his arm up
  CP (HL)                 ; Is ALBERT already holding ERIC back?
  RET Z                   ; Return if so
  CALL SITDOWN_1          ; Make ALBERT put his arm up and update the SRB
  LD HL,LFLAGS            ; LFLAGS holds various game status flags
  BIT 1,(HL)              ; Is MR WACKER already looking for ERIC in order to
                          ; expel him?
  JR NZ,WATCHERIC_2       ; Jump if so
  BIT 2,(HL)              ; Is MR WACKER already looking for the truant ERIC?
  JR NZ,WATCHERIC_2       ; Jump if so
  SET 2,(HL)              ; Signal: MR WACKER is looking for the truant ERIC
  LD HL,51227             ; Place the address of the command list at CLGETERIC
  LD (HL),46              ; into bytes 27 and 28 of MR WACKER's buffer; this
  INC L                   ; command list contains a single entry: the address
  LD (HL),245             ; of the routine at STALKERIC
  INC L                   ; Set bit 0 of byte 29 of MR WACKER's buffer,
  SET 0,(HL)              ; triggering a command list restart (at CLGETERIC)
WATCHERIC_2:
  LD H,205                ; 205=ALBERT
  CALL BOXCOORDS          ; Determine the coordinates at which to print
                          ; ALBERT's exclamation
  CALL SCR2BUF            ; Save the area of the screen that will be
                          ; overwritten by the message box
  LD BC,25138             ; B=98 (MR WACKER^HE'S ESCAPING), C=50 (INK 2: PAPER
                          ; 6)
  LD A,2                  ; The border colour will be red
  JP GIVELINES_2          ; Make ALBERT sound the alarm

; Command list used to make MR WACKER find the truant ERIC
;
; Used by the routine at WATCHERIC.
CLGETERIC:
  DEFW STALKERIC          ; Make MR WACKER find the truant ERIC

; Command list used to make MR WACKER find and expel ERIC
;
; Used by the routine at EXPEL.
CLEXPEL:
  DEFW FINDEXPEL          ; Make MR WACKER find and expel ERIC

; Set MR WACKER on his way to expel ERIC
;
; Used by the routines at ADDLINES (when ERIC has 10000 or more lines) and
; FALLING (when ERIC has jumped out of the top-floor window).
EXPEL:
  LD HL,LFLAGS            ; LFLAGS holds various game status flags
  BIT 1,(HL)              ; Is MR WACKER already looking for ERIC (to expel
                          ; him)?
  RET NZ                  ; Return if so
  SET 1,(HL)              ; Signal: MR WACKER is looking for ERIC (to expel
                          ; him)
  LD L,228                ; Set the MSB of the lesson clock at CLOCK to 255, so
  LD (HL),255             ; that the lesson will not end until ERIC's expelled
  LD HL,51227             ; Point HL at byte 27 of MR WACKER's buffer
  LD (HL),48              ; Place the address of the command list at CLEXPEL
  INC L                   ; (which contains the single routine address
  LD (HL),245             ; FINDEXPEL) into bytes 27 and 28 of MR WACKER's
                          ; buffer
  INC L                   ; Set bit 0 of byte 29 of MR WACKER's buffer,
  SET 0,(HL)              ; triggering a command list restart (at CLEXPEL)
  RET

; Make MR WACKER find the truant ERIC
;
; The address of this interruptible subcommand routine is placed into bytes 9
; and 10 of MR WACKER's buffer by the routine at STALKERIC after ALBERT has
; spotted ERIC trying to escape and raised the alarm.
;
; H 200 (MR WACKER)
FINDTRUANT:
  LD L,29                 ; Set bit 7 of byte 29 of MR WACKER's buffer, making
  SET 7,(HL)              ; him run
  LD L,0                  ; Point HL at byte 0 of MR WACKER's buffer
  BIT 0,(HL)              ; Is MR WACKER midstride?
  JP NZ,FINDERIC_0        ; Finish the stride if so
; Now check whether a command list restart has been requested. This request
; will have been made if the lesson in which MR WACKER started chasing ERIC has
; ended, and the next lesson has just begun (see NEWLESSON1).
  LD L,29                 ; Has a command list restart been requested (i.e.
  BIT 0,(HL)              ; should MR WACKER end the chase now)?
  JP NZ,HERDERIC2_0       ; Jump if so
  JP FINDERIC             ; Otherwise continue the hunt for ERIC

; Make MR WACKER find and expel ERIC
;
; The address of this routine is found in the command list at CLEXPEL, the
; address of which is placed into bytes 27 and 28 of MR WACKER's buffer by the
; routine at EXPEL.
;
; H 200 (MR WACKER)
FINDEXPEL:
  LD L,29                 ; Set bit 7 of byte 29 of MR WACKER's buffer, making
  SET 7,(HL)              ; him run
  LD A,255                ; Set the MSB of the lesson clock at CLOCK to 255
  LD (32740),A            ; (this lesson will not end until ERIC's expelled)
  CALL FINDERIC           ; Move MR WACKER one step closer to ERIC (if he's not
                          ; yet close enough)
  LD HL,WACKERCBUF        ; Point HL at byte 0 of MR WACKER's buffer
  BIT 0,(HL)              ; Is MR WACKER now midstride?
  RET NZ                  ; Return if so
  INC L                   ; L=1
  LD DE,(53761)           ; E=ERIC's x-coordinate, D=ERIC's y-coordinate
  LD A,(HL)               ; A=MR WACKER's x-coordinate
  SUB E                   ; Is MR WACKER within 3 x-coordinates of ERIC?
  ADD A,3                 ;
  CP 7                    ;
  RET NC                  ; Return if not
  INC L                   ; L=2
  LD A,(HL)               ; A=MR WACKER's y-coordinate
  SUB D                   ; Is MR WACKER within 3 y-coordinates of ERIC?
  ADD A,3                 ;
  CP 7                    ;
  RET NC                  ; Return if not
; MR WACKER has found ERIC.
  LD HL,STATUS            ; STATUS holds ERIC's status flags
  LD (HL),64              ; Set bit 6: MR WACKER is expelling ERIC (who is now
                          ; paralysed)
  LD L,237                ; HL=STATUS2 (ERIC's other status flags)
  BIT 5,(HL)              ; Bit 5 is set if ERIC jumped out of the top-floor
                          ; window
  LD H,200                ; 200=MR WACKER
  LD E,99                 ; Message 99: YOU HAVE 10000 LINES...
  JR Z,FINDEXPEL_0        ; Jump if ERIC didn't jump out of the top-floor
                          ; window (i.e. he has 10000 lines)
  INC E                   ; E=100: YOU ARE NOT A BIRD...
FINDEXPEL_0:
  LD BC,SPEAK             ; Redirect control to the routine at SPEAK (make
  CALL CALLSUBCMD         ; character speak) and return to FINDEXPEL_1 (below)
                          ; when done
FINDEXPEL_1:
  LD DE,(SCORE)           ; Collect the score from SCORE into DE
  LD HL,(HISCORE)         ; Collect the current hi-score from HISCORE into HL
  AND A                   ; Clear the carry flag ready for subtraction
  SBC HL,DE               ; Do we have a new hi-score?
  JR NC,FINDEXPEL_2       ; Jump if not
  LD (HISCORE),DE         ; Insert the new hi-score
FINDEXPEL_2:
  LD HL,0                 ; Reset the score (at SCORE) and the lines total (at
  LD (SCORE),HL           ; LINES) to 0
  LD (LINES),HL           ;
  JP START                ; Enter demo mode

; Unused
  DEFS 7

; Prepare for a new game
;
; Used by the routine at START. First of all, wipe the blackboards clean.
PREPGAME:
  LD H,135                ; The blackboards use UDGs 0-79 (0-15 for the Blue
  LD C,8                  ; Room board, 16-31 for the Yellow Room, 32-47 for
  XOR A                   ; the top-floor room in the girls' skool, 48-63 for
PREPGAME_0:
  LD L,A                  ; the Science Lab, and 64-79 for the middle-floor
  LD B,80                 ; room in the girls' skool) with base page 128; wipe
PREPGAME_1:
  LD (HL),255             ; them clean by setting every byte of graphic data
  INC L                   ; for these UDGs to 255
  DJNZ PREPGAME_1         ;
  DEC H                   ;
  DEC C                   ;
  JR NZ,PREPGAME_0        ;
; Next, clean up the game status buffer.
  LD L,0                  ; Clear the first 222 bytes of the game status buffer
  LD B,222                ;
PREPGAME_2:
  LD (HL),A               ;
  INC L                   ;
  DJNZ PREPGAME_2         ;
; Reset various counters, flags and identifiers.
  LD L,224                ; Set the lesson descriptor at LESSONDESC to 0 (for
  LD (HL),A               ; no good reason: it will be set again by the routine
                          ; at NEWLESSON1 when demo mode or a new game starts)
  INC L                   ; Reset the number of mice caught at MOUSETALLY
  LD (HL),A               ;
  INC L                   ; Reset the number of kisses available from HAYLEY at
  LD (HL),40              ; KISSCOUNT
  INC L                   ; Set the lesson clock at CLOCK to 1 (so the bell
  LD (HL),1               ; will ring straight away)
  INC L                   ;
  LD (HL),A               ;
  LD L,235                ; Reset the inventory flags at INVENTORY
  LD (HL),A               ;
  LD L,237                ; Clear the game status buffer from STATUS2 to STATUS
  LD B,15                 ;
PREPGAME_3:
  LD (HL),A               ;
  INC L                   ;
  DJNZ PREPGAME_3         ;
  LD (HL),214             ; LASTCHAR holds the number of the last character
                          ; moved; set this to 214 so it immediately rewinds to
                          ; 183 (little boy no. 1)
  LD L,255                ; HL=LEFTCOL (leftmost column of the play area on
                          ; screen)
  LD (HL),A               ; Set this to 0
  LD L,223                ; HL=LESSONNO (current lesson number)
  LD A,(HL)               ; Pick this up in A (192-255)
  OR 7                    ; Make sure the first lesson of the new game will be
  LD (HL),A               ; PLAYTIME
  CALL INITDOORS          ; Prepare the doors, windows, cups and bike
  LD L,243                ; HL=ERICTIMER (ERIC's main action timer)
  LD (HL),1               ; Initialise this to 1
; Set up the bike and storeroom combinations.
  LD C,8                  ; 4 numbers in each of the 2 combinations
  LD L,156                ; HL=COMBOS (combinations)
PREPGAME_4:
  CALL GETRANDOM          ; Generate new bike and Science Lab storeroom
  LD B,A                  ; combinations
PREPGAME_5:
  CALL GETRANDOM          ;
  DJNZ PREPGAME_5         ;
  LD (HL),A               ;
  INC L                   ;
  DEC C                   ;
  JR NZ,PREPGAME_4        ;
  LD L,156                ; HL=COMBOS (combinations)
  LD B,4                  ; There are 4 digits in the bike combination
PREPGAME_6:
  LD C,47                 ; 47+1=48 (ASCII code for '0')
  LD A,(HL)               ; A=random number collected earlier
PREPGAME_7:
  INC C                   ; C=ASCII code of the number INT(A/26)
  SUB 26                  ;
  JR NC,PREPGAME_7        ;
  LD (HL),C               ; Put the bike combination digit in place
  INC L                   ; Point HL at the next digit slot
  DJNZ PREPGAME_6         ; Jump back until all four digits are done
  LD B,4                  ; There are 4 letters in the storeroom combination
PREPGAME_8:
  LD C,64                 ; 64+1=65 (ASCII code for 'A')
  LD A,(HL)               ; A=random number collected earlier
PREPGAME_9:
  INC C                   ; C=ASCII code of the Nth letter of the alphabet
  SUB 10                  ; (where N=1+INT(A/10))
  JR NC,PREPGAME_9        ;
  LD (HL),C               ; Put the storeroom combination letter in place
  INC L                   ; Point HL at the next letter slot
  DJNZ PREPGAME_8         ; Jump back until all four letters are done
  LD DE,COMBOS            ; Set HL to COMBOS and DE to COMBOS2
  EX DE,HL                ;
  LD C,8                  ; Copy the bike combination to COMBOS2 and the
  LDIR                    ; storeroom combination to COMBOS2SLS
; Place the game characters in their initial positions.
  LD B,32                 ; There are 32 game characters (183-214)
  LD H,183                ; 183=little girl no. 1
PREPGAME_10:
  LD D,H
  PUSH BC
  LD L,32                 ; Collect the initial animatory state and location of
  XOR A                   ; the character from bytes 32-34 of the character's
  LD E,A                  ; buffer and copy them into bytes 0-2
  LD BC,3                 ;
  LDIR                    ;
  EX DE,HL
  LD (HL),36              ; Place the address of the routine at DONOWT (RET)
  INC L                   ; into bytes 3 and 4 of the character's buffer (thus
  LD (HL),210             ; making it the character's primary command; this is
                          ; useful only for the non-human characters, which do
                          ; not use command lists and therefore need a default
                          ; primary command)
  INC L                   ; L=5
  EX DE,HL
  LD B,24                 ; Zero out bytes 5-28 of the character's buffer
PREPGAME_11:
  LD (DE),A               ;
  INC E                   ;
  DJNZ PREPGAME_11        ;
  LDI                     ; Copy byte 35 into byte 29
  LD A,H                  ; A=character number (183-214)
  AND 15                  ; Initialise the walking speed change delay counter
  LD (DE),A               ; at byte 30 of the character's buffer
  POP BC
  INC H                   ; Next character
  DJNZ PREPGAME_10        ; Jump back until all the characters have been
                          ; initialised
; Set up a free mouse.
  LD HL,MVMOUSE           ; Routine at MVMOUSE: control mouse
  LD (54289),HL           ; Place this uninterruptible subcommand routine
                          ; address into bytes 17 and 18 of the mouse's buffer
  LD HL,257               ; Set bytes 20 and 21 of the mouse's buffer to 1;
  LD (54292),HL           ; this has the effect of making the mouse hide
                          ; initially
; Clear the screen.
  LD HL,16384             ; Clear the display file
  LD (HL),L               ;
  LD DE,16385             ;
  LD BC,6144              ;
  LDIR                    ;
  LD (HL),7               ; PAPER 0: INK 7
  LD B,3                  ;
  LDIR                    ;
; Prepare the bottom three lines of the screen.
  LD HL,LOGO              ; The graphic data for the BTS logo is at LOGO
  LD DE,23224             ; Point DE at the appropriate attribute file address
  CALL BUF2SCR            ; Print the BTS logo
  LD HL,SCOREBOX          ; The graphic data for the score box is at SCOREBOX
  LD DE,23200             ; Point DE at the appropriate attribute file address
  CALL BUF2SCR            ; Print the Score/Lines/Hi-score box
  LD HL,(HISCORE)         ; Collect the hi-score from HISCORE into DE
  EX DE,HL                ;
  LD HL,20964             ; Point HL at the appropriate display file address
  CALL PRINTNUM           ; Print the hi-score
  XOR A                   ; Print the score (0)
  CALL ADDPTS             ;
  XOR A                   ; Print the lines total (0)
  CALL ADDLINES           ;
  LD A,88                 ; When the game begins, the leftmost column of the
  LD (LEFTCOL),A          ; play area on screen will be 88+32=120
; Scroll the play area onto the screen and play the theme tune.
  LD B,4                  ; There are 4 quarters to scroll on
PREPGAME_12:
  PUSH BC
  CALL LSCROLL8           ; Scroll on a quarter
  POP BC
  DJNZ PREPGAME_12        ; Jump back until all 4 quarters have been scrolled
                          ; on
  CALL PLAYTUNE           ; Play the theme tune
  RET

; Move the characters, close doors, and give ERIC lines if necessary
;
; Called from the main loop at MAINLOOP.
MAINLOOP2:
  CALL MVCHARS            ; Move the game characters
  CALL SHUTDOORS          ; Close any temporarily open doors that need closing
  CALL CHKERIC1           ; Make a teacher give ERIC lines if he's up to
                          ; something he shouldn't be
  RET                     ; Return to the main loop

; Start a new game if a key is pressed while ERIC's sitting or lying down in
; demo mode
;
; Used by the routine at ERICSITLIE. Returns with ERIC's animatory state in A
; if no key was pressed.
EXITDEMO:
  CALL READKEY            ; Check for keypresses
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  RET Z                   ; Return if no key was pressed
  JP START                ; Otherwise start a new game

; 'D.S.REIDY 85 '
  DEFM "D.S.REIDY 85 "

; Start a new game or enter demo mode
;
; Used by the routines at INPUTDEV2, JUMPING, GETINPUT, FINDEXPEL and EXITDEMO.
START:
  LD SP,23806             ; Put the stack somewhere safe
  LD HL,GAMEMODE          ; A=255 if we're in demo mode, 0 if a game has just
  LD A,(HL)               ; ended, or 1 if ERIC has just gone up a year
  INC A
  PUSH HL
  NOP                     ; Who knows what used to happen here?
  NOP                     ;
  NOP                     ;
  POP HL
  LD A,(HL)               ; A=255 if we're in demo mode, 0 if a game has just
                          ; ended, or 1 if ERIC has just gone up a year
  RRCA                    ; 0 becomes 255 (demo mode), and 1 and 255 become 0
  CCF                     ;
  SBC A,A                 ;
  LD (HL),A               ; Store this in GAMEMODE
  CALL PREPGAME           ; Prepare for a new game or demo mode, then enter the
                          ; main loop at MAINLOOP

; Main loop
;
; The routine at START continues here.
MAINLOOP:
  LD HL,CLOCK             ; CLOCK holds the lesson clock
  DEC (HL)                ; Decrement the LSB
  JR NZ,MAINLOOP_0        ; Jump if it's non-zero
  INC L                   ; Point HL at the MSB of the lesson clock
  DEC (HL)                ; Decrement the MSB
  LD A,(HL)               ; Copy the new MSB to A
  INC A                   ; Have we reached the end of the lesson?
  CALL Z,NEWLESSON1       ; Ring the bell and start the next lesson if so
MAINLOOP_0:
  CALL MAINLOOP2          ; Move the characters, close any temporarily open
                          ; doors, and make the nearest teacher give ERIC lines
                          ; if he's up to something he shouldn't be
  LD HL,STATUS            ; STATUS holds ERIC's primary status flags
  LD A,(HL)               ; Pick these up in A
  AND A                   ; Are any of the status flags set?
  JR Z,MAINLOOP_1         ; Jump if not
  CALL HANDLEERIC         ; Deal with ERIC if any of the status flags at STATUS
                          ; are set
  JR MAINLOOP_3
; No status flags at STATUS are set, which means ERIC is standing, or
; midstride, or just about to finish a 'short action' (bending over, dropping a
; stinkbomb, firing the water pistol, or moving forward as if to kiss). Here we
; finish ERIC's stride if he's midstride, finish any action he's just about to
; finish, or check for keypresses.
MAINLOOP_1:
  LD L,243                ; Decrement ERIC's main action timer at ERICTIMER
  DEC (HL)                ;
  JR NZ,MAINLOOP_3        ; Jump unless it's 0
  DEC L                   ; Collect the mid-action timer from ERICTIMER2 into
  LD A,(HL)               ; A; it will be non-zero if ERIC is midstride or
                          ; mid-action
  LD (HL),0               ; Reset the mid-action timer to 0
  INC L                   ; Copy the previous contents of the mid-action timer
  LD (HL),A               ; into the main action timer at ERICTIMER
  AND A                   ; Is ERIC midstride or mid-action?
  JR NZ,MAINLOOP_2        ; Jump if so
; ERIC is neither midstride nor just about to finish a short action, so check
; the keyboard.
  CALL GETINPUT           ; Check for keypresses
  JR Z,MAINLOOP_3         ; Jump if there haven't been any
  LD (KEYCODE),A          ; Store the offset of the last keypress in KEYCODE
  LD H,229                ; HL will index the table of keypress handling
                          ; routines at K_LEFTF
  LD DE,MAINLOOP_3        ; Push the address MAINLOOP_3 (see below) onto the
  PUSH DE                 ; stack so we return there after dealing with the
                          ; keypress
  LD L,A                  ; Point HL at the appropriate entry
  LD C,(HL)               ; Copy the address of the routine for dealing with
  INC L                   ; the keypress into BC
  LD B,(HL)               ;
  PUSH BC                 ; Push this address onto the stack
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC's buffer
  LD A,(HL)               ; Collect ERIC's animatory state into A, and his
  INC L                   ; coordinates into DE
  LD E,(HL)               ;
  INC L                   ;
  LD D,(HL)               ;
  RET                     ; Make an indirect jump to the relevant
                          ; keypress-handling routine, and then return to
                          ; MAINLOOP_3
; ERIC is midstride, or just about to finish an action (such as firing the
; water pistol or catching a mouse). Make him finish the stride or action.
MAINLOOP_2:
  CALL MVERIC2            ; Update ERIC's animatory state and location, update
                          ; the SRB, and scroll the screen if necessary
; Now that ERIC's movements have been dealt with, the main loop continues.
MAINLOOP_3:
  CALL UPDATESCR          ; Update the display
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  LD A,(HL)               ; Pick this up in A
  AND A                   ; Did we check for keypresses on this pass?
  JR NZ,MAINLOOP_4        ; Jump if not
  LD (HL),2               ; Otherwise reset ERIC's main action timer to 2
; This next section of code ensures that we don't pass through the main loop
; more than once every 1/50th of a second.
MAINLOOP_4:
  LD L,217                ; HL=LFRAMES (which holds the LSB of the system
                          ; variable FRAMES as it was when the last pass
                          ; through the main loop was completed)
MAINLOOP_5:
  LD A,(23672)            ; A=LSB of the system variable FRAMES, which is
                          ; incremented every 1/50th of a second
  SUB (HL)                ; Now A=0 if FRAMES hasn't been incremented since the
                          ; last pass through the main loop
  CP 1                    ; Was FRAMES incremented?
  JR C,MAINLOOP_5         ; Jump back if not to check again
  ADD A,(HL)              ; Store the current value of the LSB of FRAMES at
  LD (HL),A               ; LFRAMES
  JR MAINLOOP             ; Jump back to the start of the main loop

; Change the lesson
;
; This routine is called from the main loop at MAINLOOP when the lesson clock
; has counted down to zero. It sets each character up with the appropriate
; command list for the next lesson, and teleports some of the minor characters
; (specifically, little boys 1-8 and all the little girls) to their initial
; destinations if possible.
;
; HL 32740 (MSB of the lesson clock)
NEWLESSON1:
  LD (HL),16              ; Reset the MSB of the lesson clock to 16
  CALL CLRFLAGS           ; Clear the flags at LFLAGS and LSIGS
  LD L,223                ; HL=LESSONNO (which holds the current lesson number)
  LD A,(HL)               ; Pick this up in A (192-255)
  INC A                   ; Next lesson
  OR 192                  ; Roll over from 255 to 192 if necessary
  LD (HL),A               ; Store the new lesson number
  LD E,A                  ; Point DE at the main timetable entry for this
  LD D,181                ; lesson
  LD A,(DE)               ; Pick up the lesson identifier (37-59) in A
  LD E,A                  ; Copy this to E
  LD D,210                ; The lesson descriptor table is at LDESCS
  LD A,(DE)               ; Pick up the lesson descriptor in A
  INC L                   ; Store a copy of the lesson descriptor in LESSONDESC
  LD (HL),A               ;
  DEC D                   ; D=209 (HAYLEY)
; We now enter a loop to transfer the start address of a command list into
; bytes 27 and 28 of each character's buffer.
NEWLESSON1_0:
  LD H,D
  LD A,(DE)               ; A=command list number for this lesson from the
                          ; character's personal timetable
  EXX
  LD H,232                ; Page 232 holds the start addresses of the command
                          ; lists
  LD L,A                  ; Pick up the LSB of the command list start address
  LD A,(HL)               ; in A
  INC L                   ; Point HL' at the MSB
  EXX
  LD L,27                 ; The LSB of the command list start address goes into
  LD (HL),A               ; byte 27 of the character's buffer
  INC L                   ; Point HL at byte 28 of the character's buffer
  EXX
  LD A,(HL)               ; A=MSB of the command list start address
  EXX
  LD (HL),A               ; Place this into byte 28 of the character's buffer
  INC L                   ; Set bit 0 of byte 29 of the character's buffer,
  SET 0,(HL)              ; which will trigger a command list restart at the
                          ; next available opportunity (i.e. after any
                          ; subcommand routine whose address is currently in
                          ; bytes 17 and 18 or bytes 9 and 10 of the
                          ; character's buffer has finished its job; see
                          ; MVCHARS)
  LD A,H                  ; Copy the character number to A
  CP 198                  ; Are we dealing with one of the characters 198-209
                          ; (little boys nos. 9 and 10, the adults and the main
                          ; kids)?
  JR NC,NEWLESSON1_6      ; Jump if so
; We are dealing with one of the characters 183-197 (all the little girls, and
; little boys 1-8), who get special treatment at the start of a new lesson. Any
; of them who never venture into the area that's currently on-screen will be
; 'teleported' to the first destination in their new command list.
  SET 3,(HL)              ; Set bit 3 of byte 29, signalling to the routine at
                          ; RSCROLL8_3 (called later) that this character may
                          ; be teleported
  EXX
  LD B,1                  ; We let the routine at RSCROLL8_3 consider only one
                          ; character at a time
  LD H,A                  ; H'=character number (183-197)
  LD A,(LEFTCOL)          ; A=X (leftmost column of the play area on screen)
  CP 80                   ; Is X at least 80 (the middle of the assembly hall
                          ; stage)?
  JR NC,NEWLESSON1_1      ; Jump if so
  LD A,H                  ; A=character number (183-197)
  CP 190                  ; Now X<80, so set the carry flag if we're dealing
                          ; with a little girl (none of whom should be
                          ; on-screen)
  JR NEWLESSON1_4
NEWLESSON1_1:
  CP 120                  ; Set the carry flag if X<120
  LD A,H                  ; A=character number (183-197)
  JR NC,NEWLESSON1_2      ; Jump if X>=120 (which means none of the little boys
                          ; 1-8 should be on-screen)
  CP 193                  ; Are we dealing with little boys 4-8?
  JR NC,NEWLESSON1_5      ; Jump if so (they do venture this far into the
                          ; playground)
  CP 186                  ; Now 80<=X<=112, so reset the carry flag if we're
                          ; dealing with little girls 4-7 (who never leave the
                          ; girls' skool) or little boys 1-3 (who never venture
                          ; beyond the assembly hall stage)
  JR NEWLESSON1_3
NEWLESSON1_2:
  CP 190                  ; Now X>=120, so reset the carry flag if we're
                          ; dealing with a little boy
NEWLESSON1_3:
  CCF                     ; Set the carry flag if this character should be
                          ; considered for teleportation
NEWLESSON1_4:
  CALL C,RSCROLL8_3       ; Teleport this character if appropriate
NEWLESSON1_5:
  EXX
; The loop that prepares each character for the new lesson continues here.
NEWLESSON1_6:
  DEC D                   ; Next character
  LD A,D                  ; Copy this character's number to A
  CP 182                  ; Have we done all the characters yet?
  JR NZ,NEWLESSON1_0      ; Jump back if not
  JP NEWLESSON2           ; Print the lesson and ring the bell

; Unused
  DEFB 0

; Deal with ERIC
;
; Used by the routine at MAINLOOP. Deals with ERIC when any of the bits at
; STATUS (ERIC's primary status flags) are set, indicating that ERIC is doing
; something other than standing or walking:
;
; +-----+------------------------------+-------------------------+------------+
; | Bit | Set by                       | Meaning if set          | Handler    |
; +-----+------------------------------+-------------------------+------------+
; | 0   | JUMP                         | ERIC is jumping         | JUMPING    |
; | 1   | JUMPING, OFFSTAGE, MOUNTBIKE | Examine the secondary   |            |
; |     |                              | status flags at STATUS2 |            |
; | 2   | ERICHIT, SIT, FALLING        | ERIC is sitting or      | ERICSITLIE |
; |     |                              | lying down              |            |
; | 3   | CATCH                        | ERIC is bending over,   | BENDING    |
; |     |                              | dropping a stinkbomb    |            |
; |     |                              | etc.                    |            |
; | 4   | WRITE                        | ERIC is writing on a    | WRITING    |
; |     |                              | blackboard              |            |
; | 5   | STARTFHK                     | ERIC is firing, hitting | MIDFHK     |
; |     |                              | or kissing              |            |
; | 6   | FINDEXPEL, FALLING           | MR WACKER is expelling  |            |
; |     |                              | ERIC                    |            |
; | 7   | CLEARSEAT, OBJFALL           | ERIC has been knocked   | ERICHIT    |
; |     |                              | over                    |            |
; +-----+------------------------------+-------------------------+------------+
;
; If bit 1 at STATUS is set, we examine ERIC's secondary status flags at
; STATUS2:
;
; +-----+---------------------------------+----------------------+------------+
; | Bit | Set by                          | Meaning if set       | Handler    |
; +-----+---------------------------------+----------------------+------------+
; | 0   | ONSADDLE                        | ERIC is riding the   | RIDINGBIKE |
; |     |                                 | bike                 |            |
; | 1   | JUMPING                         | ERIC is standing on  | ONPLANT    |
; |     |                                 | a plant or plant pot |            |
; | 2   | STEPOFF, STEPPEDOFF, ONPLANT    | ERIC is stepping off | STEPPEDOFF |
; |     |                                 | a plant, a plant     |            |
; |     |                                 | pot, or the stage    |            |
; | 3   | STEPPEDOFF, ONPLANT             | ERIC is falling and  | LANDING    |
; |     |                                 | will land on his     |            |
; |     |                                 | feet                 |            |
; | 4   | WHEELBIKE, RIDINGBIKE, CHKWATER | ERIC is falling and  | FALLING    |
; |     |                                 | will not land on his |            |
; |     |                                 | feet                 |            |
; | 5   | STEPPEDOFF                      | ERIC has stepped out | BIGFALL    |
; |     |                                 | of the top-floor     |            |
; |     |                                 | window               |            |
; | 6   | WHEELBIKE                       | ERIC is falling from | OFFSADDLE  |
; |     |                                 | the saddle of the    |            |
; |     |                                 | bike                 |            |
; | 7   | RIDINGBIKE                      | ERIC is standing on  | ONSADDLE   |
; |     |                                 | the saddle of the    |            |
; |     |                                 | bike                 |            |
; +-----+---------------------------------+----------------------+------------+
;
; H 127
HANDLEERIC:
  LD L,237                ; HL=STATUS2 (ERIC's secondary status flags)
  BIT 5,(HL)              ; Has ERIC stepped out of the top-floor window?
  JR Z,HANDLEERIC_0       ; Jump if not
  LD A,(ERICCBUF)         ; A=ERIC's animatory state
  CP 134                  ; 134: Is ERIC lying on his back, facing right?
  JR NZ,HANDLEERIC_1      ; Jump if not
HANDLEERIC_0:
  LD L,251                ; HL=STATUS (ERIC's status flags)
  BIT 6,(HL)              ; Is MR WACKER expelling ERIC?
  RET NZ                  ; Return if so (ERIC is incapacitated)
  BIT 1,(HL)              ; Bit 1 at STATUS is set if we need to look at
                          ; STATUS2
  LD D,211
  JR Z,HANDLEERIC_2       ; Jump unless we need to look at STATUS2
  LD L,237                ; HL=STATUS2 (ERIC's secondary status flags)
HANDLEERIC_1:
  LD D,213
HANDLEERIC_2:
  LD A,(HL)               ; Copy the contents of STATUS or STATUS2 to A
; Now D=211 and A=(STATUS) if bit 1 at STATUS is reset; otherwise D=213 and
; A=(STATUS2).
  LD E,68                 ; Point DE at the appropriate entry in the table of
HANDLEERIC_3:
  DEC E                   ; handler routine address LSBs (at EHRLSBS1 or
  RRCA                    ; EHRLSBS2), depending on which bit of A is set
  JR NC,HANDLEERIC_3      ;
  EX DE,HL                ; Transfer the table entry address to HL
  LD E,(HL)               ; Collect the handler routine address from the table
  INC H                   ; entry into DE
  LD D,(HL)               ;
  EX DE,HL                ; Transfer this handler routine address to HL
  JP (HL)                 ; Jump to the handler routine

; Unused
  DEFB 0

; Place a continual subcommand routine address into a character's buffer
;
; Used by command lists 32, 34, 36, 38, 40, 42, 44, 46, 48, 50, 52, 54, 56 and
; 76. Collects a continual subcommand routine address from the command list and
; places it into bytes 23 and 24 of the character's buffer. The routine address
; will be one of the following:
;
; +-----------+-------------------------------------------------------+
; | Address   | Description                                           |
; +-----------+-------------------------------------------------------+
; | CATTY     | Make BOY WANDER fire his catapult now and then        |
; | WATCHERIC | Make ALBERT keep an eye out for ERIC during lessons   |
; | HITHAYLEY | Make ANGELFACE stalk HAYLEY                           |
; | HITFIRE   | Make ANGELFACE or BOY WANDER hit or fire now and then |
; +-----------+-------------------------------------------------------+
;
; H Character number (205-207)
ADDR2CBUF:
  LD L,23                 ; Collect the routine address from the command list
  CALL GETPARAMS          ; and place it into bytes 23 and 24 of the
                          ; character's buffer
  JP MVCHARS_15           ; Move to the next command in the command list

; Make ANGELFACE stalk HAYLEY
;
; The address of this continual subcommand routine is placed into bytes 23 and
; 24 of ANGELFACE's buffer by command list 44. Sets ANGELFACE's destination to
; match HAYLEY's, presumably so he has a better chance of knocking her out with
; a random punch.
;
; H 207 (ANGELFACE)
HITHAYLEY:
  LD DE,53533             ; Point DE at byte 29 of HAYLEY's buffer
  LD A,(DE)               ; Pick this up in A
  RRCA                    ; Is HAYLEY's command list due to be restarted?
  JR C,HITHAYLEY_0        ; Jump if so
  LD E,4                  ; Pick up the MSB of the routine address of the
  LD A,(DE)               ; current command in HAYLEY's command list
  CP 100                  ; Is it the routine at GOTO (go to a location)?
  JR NZ,HITHAYLEY_0       ; Jump if not
  LD DE,(53509)           ; Copy the coordinates of HAYLEY's destination from
                          ; bytes 5 and 6 of HAYLEY's buffer to DE
  LD (58850),DE           ; Place these coordinates directly into command list
                          ; 44
  LD L,4                  ; Pick up the MSB of the routine address of the
  LD A,(HL)               ; current command in ANGELFACE's command list
  CP 100                  ; Is it the routine at GOTO (go to a location)?
  JR NZ,HITHAYLEY_0       ; Jump if not
  LD (52997),DE           ; Copy HAYLEY's destination into bytes 5 and 6 of
                          ; ANGELFACE's buffer (i.e. make it his destination
                          ; too)
HITHAYLEY_0:
  JP VIOLENT              ; Continue into VIOLENT (make ANGELFACE hit now and
                          ; then)

; Unused
  DEFB 0

; Make ANGELFACE or BOY WANDER hit or fire now and then
;
; The address of this continual subcommand routine is placed into bytes 23 and
; 24 of BOY WANDER's or ANGELFACE's buffer by command lists 32, 34, 36, 38, 40,
; 42, 46, 48, 50, 52, 54 and 56.
;
; H 206 (BOY WANDER) or 207 (ANGELFACE)
HITFIRE:
  LD A,H                  ; Is this ANGELFACE?
  CP 207                  ;
  JP Z,VIOLENT            ; Jump if so
  JP CATTY                ; Jump if it's BOY WANDER

; Restart the command list unless it's time for assembly
;
; Used by command list 72. Restarts the command list (making MR WACKER go back
; up to his study) unless it's time to start assembly.
;
; H 200 (MR WACKER)
RSTORASSEM:
  LD A,(32740)            ; A=MSB of the lesson clock (which starts off at 16)
  CP 9                    ; Is it time to start assembly yet?
  JP NC,RESTART           ; Restart the command list if not
  JP NEXTCMD              ; Otherwise move to the next command in the command
                          ; list

; Scroll the display file left or right one column
;
; Used by the routine at RSCROLL. Returns with the column of the play area that
; was at the far left of the screen before this routine was called in A.
;
; DE 16384 or 16415
LRSCROLL:
  LD B,0                  ; BC will be used as the counter for the LDIR/LDDR
                          ; instruction at SCRL2; initialise the MSB to 0
  EXX
  LD C,21                 ; There are 21 rows to scroll (the bottom 3 rows of
                          ; the screen are fixed)
LRSCROLL_0:
  LD B,8                  ; 8 pixel lines per row of the screen
LRSCROLL_1:
  EXX
  LD H,D                  ; Copy the display file address from DE to HL
  LD L,E                  ;
SCRL1:
  INC L                   ; This instruction is set to INC L if we're scrolling
                          ; to the left, or DEC L if we're scrolling to the
                          ; right (see RSCROLL)
  LD A,E                  ; Save the LSB of the display file address in A
                          ; briefly
  LD C,31                 ; 31 bytes to shift per pixel line
SCRL2:
  LDIR                    ; This instruction is set to LDIR if we're shifting
                          ; to the left, or LDDR if we're shifting to the right
                          ; (see RSCROLL)
  LD E,A                  ; Restore the LSB of the display file address to E
  INC D                   ; Point DE at the first (or last) byte of the next
                          ; pixel line in this row
  EXX
  DJNZ LRSCROLL_1         ; Shift the remaining pixel lines in this row
  EXX
  LD A,E                  ; Set E to the LSB of the display file address for
  ADD A,32                ; the first (or last) byte of the first pixel line in
  LD E,A                  ; the next row down
  JR C,LRSCROLL_2         ; Jump if we just finished shifting the eighth or
                          ; sixteenth row
  LD A,D                  ; Adjust D appropriately in the case where we haven't
  SUB 8                   ; finished with the current 8-row third of the screen
  LD D,A                  ;
LRSCROLL_2:
  EXX
  DEC C                   ; Next row
  JR NZ,LRSCROLL_0        ; Jump back until all 21 rows have been shifted
  EXX
  LD A,(LEFTCOL)          ; A=column of the play area (that was) at the far
                          ; left of the screen
  RET

; Scroll the display file left one column
;
; Used by the routine at LSCROLL8.
LSCROLL:
  LD BC,45100             ; C=44 (INC L), B=176 (LDIR after 237)
  LD DE,16384             ; Initialise DE to the appropriate display file
                          ; address
  JR RSCROLL_0

; Scroll the display file right one column
;
; Used by the routine at RSCROLL8.
RSCROLL:
  LD BC,47149             ; C=45 (DEC L), B=184 (LDDR after 237)
  LD DE,16415             ; Initialise DE to the appropriate display file
                          ; address
; This entry point is used by the routine at LSCROLL with C=44 (INC L), B=176
; (LDIR after 237), and DE=16384.
RSCROLL_0:
  LD HL,SCRL1             ; Set the instruction at SCRL1 to INC L or DEC L, and
  LD (HL),C               ; the instruction at SCRL2 to LDIR or LDDR
  LD L,39                 ;
  LD (HL),B               ;
  JR LRSCROLL             ; Scroll the display file one column left or right

; Clear various game and lesson flags
;
; Used by the routine at NEWLESSON1. Clears the flags in LFLAGS and LSIGS, and
; also sets the 6 unused bytes at GSBUNUSED to 0.
;
; H 127
CLRFLAGS:
  LD L,128                ; HL=LFLAGS
  LD B,8                  ; Zero out the bytes
CLRFLAGS_0:
  LD (HL),0               ;
  INC L                   ;
  DJNZ CLRFLAGS_0         ;
  RET

; Deal with a stinkbomb when dropped
;
; The address of this routine is placed into ERICDATA1 by the routine at
; DROPSBOMB when ERIC drops a stinkbomb. It creates a stinkbomb cloud and deals
; with it until it has dissipated, which includes checking whether MR WACKER
; and a closed window are nearby, and making the former set off for the latter
; if so.
CLOUD:
  LD HL,INVENTORY         ; INVENTORY holds the inventory flags
  LD A,(HL)               ; Pick these up in A
  AND 224                 ; Keep only the stinkbomb bits (bits 5-7)
  ADD A,A                 ; Shift these bits left, losing the leftmost bit
  LD C,A                  ; Store the new stinkbomb bits in C briefly
  LD A,(HL)               ; Pick up the inventory flags in A again
  AND 31                  ; Discard the old stinkbomb bits
  OR C                    ; Superimpose the new stinkbomb bits
  LD (HL),A               ; Restore the inventory flags, minus one stinkbomb
  CALL PRINTINV           ; Print the inventory
  CALL PREPSBBUF          ; Prepare buffer 213 for the stinkbomb cloud (or
                          ; return to the main loop if the buffer is already in
                          ; use by a stinkbomb cloud)
  LD L,0                  ; Point HL at byte 0 of ERIC's buffer
  LD A,(HL)               ; A=ERIC's animatory state
  INC L                   ; L=1
  RLCA                    ; Set A to -1 if ERIC is facing left, 1 if he's
  SBC A,A                 ; facing right
  ADD A,A                 ;
  CPL                     ;
  ADD A,(HL)              ; Add ERIC's x-coordinate to get the x-coordinate for
                          ; the stinkbomb cloud
  LD E,A                  ; Copy this to E
  INC L                   ; L=2
  LD D,(HL)               ; D=ERIC's y-coordinate, which will also be the
                          ; y-coordinate for the stinkbomb cloud
  LD HL,54547             ; Point HL at byte 19 of the stinkbomb cloud's buffer
  LD (HL),29              ; Initialise the counter that determines when the
                          ; cloud will disappear
  LD A,71                 ; 71: stinkbomb cloud (phase 1)
  CALL SUBCMD             ; Update the SRB for the stinkbomb cloud's appearance
                          ; and place address SBCLOUD (below) into bytes 17 and
                          ; 18 of the cloud's buffer
; The address of the entry point below is placed into bytes 17 and 18 of the
; cloud's buffer by the instruction above.
SBCLOUD:
  LD L,19                 ; Byte 19 of the cloud's buffer holds the counter
                          ; that determines how much longer the cloud will
                          ; stick around (which starts off at 29)
  DEC (HL)                ; Is it time for the cloud to disappear?
  JP Z,OBJFALL_0          ; Jump if so
  LD A,(HL)               ; Pick up the counter's value in A
  CP 28                   ; Is it time for a teacher to give lines?
  JR NZ,CLOUD_1           ; Jump if not
  LD A,1                  ; Message 1: NO STINKBOMBS
; This entry point is used by the routines at WRITING, HIT, FIRE and WATER with
; A holding a reprimand message number.
CLOUD_0:
  SUB 62                  ; Subtract 62 now (it's added back later on)
  CP A                    ; Set the zero flag to indicate that any teacher may
                          ; give ERIC lines (including the one who last gave
                          ; him lines)
  JP CHKERIC2_14          ; Make any teacher within range give ERIC lines
; Now deal with animating the stinkbomb cloud and checking whether it's near a
; window.
CLOUD_1:
  CALL WALK_7             ; Toggle between cloud animation phases 1 (71) and 2
                          ; (199) and update the SRB accordingly
  LD L,0                  ; Point HL at byte 0 of the cloud's buffer
  BIT 0,(HL)              ; Bit 0 of the cloud's animatory state (71 or 199) is
  RET Z                   ; always set, so this RET never happens
  INC L                   ; L=1
  LD A,(HL)               ; A=stinkbomb cloud's x-coordinate
  CP 84                   ; The head's right study door is at x-coordinate 84
  RET C                   ; Return if the cloud is to the left of this
  CP 96                   ; The boys' skool door is at x-coordinate 96
  RET NC                  ; Return if the cloud is to the right of this
  INC L                   ; L=2
  LD A,(HL)               ; A=stinkbomb cloud's y-coordinate
  CP 17                   ; Is the cloud on the bottom floor?
  RET Z                   ; Return if so
; The stinkbomb cloud is on the top floor or middle floor, somewhere between
; the head's right study door and the boys' skool door. Check the windows.
  EX DE,HL                ; Point DE at byte 2 of the cloud's buffer
  LD HL,DOORFLAGS         ; DOORFLAGS holds the door/window status flags
  CP 3                    ; Is the cloud on the top floor?
  JR Z,CLOUD_2            ; Jump if so
  BIT 7,(HL)              ; Is the middle-floor window already open?
  RET NZ                  ; Return if so
  LD BC,32861             ; B=128 (middle floor window), C=93 (x-coordinate)
  JR CLOUD_3
CLOUD_2:
  BIT 6,(HL)              ; Is the top-floor window already open?
  RET NZ                  ; Return if so
  LD BC,16475             ; B=64 (top-floor window), C=91 (x-coordinate)
; Now B holds the identifier of a closed window on the same floor as the
; stinkbomb cloud, and C holds its x-coordinate. Is MR WACKER in the vicinity?
CLOUD_3:
  LD HL,51202             ; Point HL at byte 2 of MR WACKER's buffer
  CP (HL)                 ; Is MR WACKER on the same floor as the stinkbomb
                          ; cloud?
  RET NZ                  ; Return if not
  DEC L                   ; L=1
  DEC E                   ; E=1
  LD A,(DE)               ; A=x-coordinate of the stinkbomb cloud
  SUB 3                   ; Return if MR WACKER is more than 2 spaces to the
  CP (HL)                 ; left of the cloud
  RET NC                  ;
  ADD A,5                 ; Return if MR WACKER is more than 2 spaces to the
  CP (HL)                 ; right of the cloud
  RET C                   ;
; MR WACKER is close enough to the stinkbomb cloud to be affected by its
; stench. Check whether he's available for a spot of window-opening.
  LD E,(HL)               ; E=MR WACKER's x-coordinate
  LD L,18                 ; Is there an uninterruptible subcommand routine
  LD A,(HL)               ; address in bytes 17 and 18 of MR WACKER's buffer
  AND A                   ; (meaning he is otherwise occupied at the moment)?
  RET NZ                  ; Return if so
  LD L,A                  ; L=0
  LD A,(HL)               ; A=MR WACKER's animatory state
  RRCA                    ; Is MR WACKER midstride?
  RET C                   ; Return if so
  RLCA                    ; A=MR WACKER's animatory state
  LD L,19                 ; Store MR WACKER's current x-coordinate and
  LD (HL),E               ; animatory state in bytes 19 and 20 of his buffer
  INC L                   ; for later retrieval
  LD (HL),A               ;
  INC L                   ; Store the x-coordinate of the window in byte 21
  LD (HL),C               ;
  INC L                   ; Byte 22 holds 128 (middle-floor window) or 64
  LD (HL),B               ; (top-floor window)
  CALL UPDATESRB          ; Update the SRB for MR WACKER's current animatory
                          ; state and location
  SET 7,A                 ; A=animatory state of MR WACKER facing right
  CALL SUBCMD             ; Place address HEAD2WIN (below) into bytes 17 and 18
                          ; of MR WACKER's buffer and update his animatory
                          ; state
; The address of the entry point below is placed into bytes 17 and 18 of MR
; WACKER's buffer by the instruction above.
HEAD2WIN:
  LD L,0                  ; Point HL at byte 0 of MR WACKER's buffer
  BIT 0,(HL)              ; Is MR WACKER midstride?
  JP NZ,WALK_0            ; Finish his stride if so
  JP TOWINDOW             ; Otherwise set MR WACKER off on his journey to the
                          ; window

; Unused
  DEFB 0

  ORG 63744

; UDG reference table for the skool gate when shut
;
; Used by the routine at ALTERUDGS. The UDG reference table for the skool gate
; when open is at URTSGOPEN.
URTSGSHUT:
  DEFB 17,132,81,4,17,133,81,4,17,134,254,132,17,135,3,196
  DEFB 18,132,81,4,18,133,81,4,18,134,254,132,18,135,3,196
  DEFB 19,132,81,4,19,133,81,4,19,134,207,68,19,135,4,196
  DEFB 20,134,122,4,20,135,5,196
  DEFB 255                ; End marker

; Deal with MR WACKER's journey to a window
;
; Continues from CLOUD. Controls MR WACKER as he walks from the vicinity of the
; stinkbomb cloud over to the nearest closed window, opens it, and walks back
; again.
;
; H 200 (MR WACKER)
TOWINDOW:
  LD L,21                 ; Pick up the x-coordinate of the window to be opened
  LD A,(HL)               ; from byte 21 of MR WACKER's buffer
  LD L,1                  ; Byte 1 of MR WACKER's buffer holds his current
                          ; x-coordinate
  CP (HL)                 ; Has MR WACKER reached the window yet?
  JP NZ,WALK_4            ; Keep going if not
  CALL UPDATESRB          ; Update the SRB for MR WACKER's current animatory
                          ; state and location
  OR 7                    ; A=animatory state of MR WACKER with his arm up
  CALL SUBCMD             ; Place address TOWINDOW_0 (below) into bytes 17 and
                          ; 18 of MR WACKER's buffer and update his animatory
                          ; state
; The address of this entry point is placed into bytes 17 and 18 of MR WACKER's
; buffer by the instruction above when he has reached the window that needs
; opening.
TOWINDOW_0:
  LD L,17                 ; Replace the address of this entry point in bytes 17
  LD (HL),86              ; and 18 of MR WACKER's buffer with TOWINDOW0
  LD B,1                  ; B=1 (open window)
  LD L,22                 ; Pick up the window identifier (64/128) from byte 22
  LD A,(HL)               ; of MR WACKER's buffer
  JP CHRMVDOOR_0          ; Open the window and lower MR WACKER's arm
; This entry point is used after the window has been opened.
TOWINDOW0:
  CALL UPDATESRB          ; Update the SRB for MR WACKER's current animatory
                          ; state and location
  XOR 128                 ; A=animatory state of MR WACKER facing left
  CALL SUBCMD             ; Place address TOWINDOW_1 (below) into bytes 17 and
                          ; 18 of MR WACKER's buffer and update his animatory
                          ; state
; This entry point is used after MR WACKER has turned round to begin his return
; journey from the window.
TOWINDOW_1:
  LD L,0                  ; Point HL at byte 0 of MR WACKER's buffer
  BIT 0,(HL)              ; Is MR WACKER midstride?
  JP NZ,WALK_0            ; Finish his stride if so
  LD L,19                 ; Byte 19 stores MR WACKER's x-coordinate before he
  LD A,(HL)               ; went off to the  window; pick this up in A
  LD L,1                  ; Byte 1 of MR WACKER's buffer holds his x-coordinate
  CP (HL)                 ; Is MR WACKER at his pre-window-opening position?
  JP NZ,WALK_4            ; Continue his return journey if not
; MR WACKER has now returned to the spot where the stinkbomb first offended his
; nostrils.
  LD L,18                 ; Remove the routine address (TOWINDOW_1 above) from
  LD (HL),0               ; bytes 17 and 18 of MR WACKER's buffer
  JP KNOCKED_0            ; Restore MR WACKER's original animatory state and
                          ; update SRB

; Prepare the buffer for ERIC's catapult pellet and make a sound effect
;
; Used by the routine at FIRE.
;
; B 214 (ERIC's catapult pellet)
PREPCATTY:
  CALL CATTY_2            ; Prepare buffer 214 for the pellet
; This entry point is used by the routine at WATER.
PREPCATTY_0:
  PUSH DE
  LD HL,63506             ; Initialise the sound effect parameters
  LD DE,32768             ;
  LD A,4                  ;
  CALL ERICHIT_5          ; Make a catapult/water pistol sound effect
  POP DE
  RET

; Check for the presence of a plant pot
;
; Used by the routines at JUMPING and CHKWATER. Returns with the zero flag set
; if either ERIC or the recently ejected contents of the water pistol are on
; top of a plant pot:
;
; +----------+--------------------------------+
; | Location | Plant pot                      |
; +----------+--------------------------------+
; | 91,2     | Near the top-floor window      |
; | 93,9     | Near the middle-floor window   |
; | 132,16   | To the left of the skool gate  |
; | 135,16   | To the right of the skool gate |
; +----------+--------------------------------+
;
; H 210 (ERIC) or 214 (water/sherry)
CHECKPOT:
  LD DE,POTSY             ; Point DE at the plant-pot y-coordinate table at
                          ; POTSY
CHECKPOT_0:
  LD L,2                  ; Point HL at byte 2 of the character's buffer
  LD A,(DE)               ; A=y-coordinate of the plant pot
  CP (HL)                 ; Does this match the character's y-coordinate?
  JR NZ,CHECKPOT_1        ; Jump if not
  DEC L                   ; L=1
  DEC D                   ; Point DE at the plant-pot x-coordinate table at
                          ; POTSX
  LD A,(DE)               ; A=x-coordinate of the plant pot
  CP (HL)                 ; Does this match the character's x-coordinate?
  RET Z                   ; Return with the zero flag set if so
  INC D                   ; Point DE back at the plant-pot y-coordinate table
CHECKPOT_1:
  INC E                   ; Next plant pot
  JR NZ,CHECKPOT_0        ; Jump back until all four plant pots have been
                          ; checked
  INC E                   ; Reset the zero flag to indicate the lack of any
                          ; plant pot here
  RET

; 'G' pressed - fire water pistol
;
; The address of this routine is found in the table of keypress handling
; routines at K_LEFTF. It is called from the main loop at MAINLOOP when 'G' is
; pressed.
FIREWP:
  LD A,(INVENTORY)        ; INVENTORY holds the inventory flags
  AND 24                  ; Keep only bits 3 and 4 (the pistol bits)
  RET Z                   ; Return if ERIC hasn't got the water pistol
  CALL CHECK214           ; Give up now if buffer 214 is already being used
  LD HL,WATER             ; The routine at WATER will control the water fired
                          ; from the pistol
  LD A,14                 ; 14: animatory state of ERIC firing the water pistol
  JP CATCH_0              ; Begin the pistol-firing action

; Control water fired from the pistol (1)
;
; The address of this routine is placed into ERICDATA1 and ERICDATA2 by the
; routine at FIREWP. In the following, 'water' may refer to either water or
; sherry.
;
; H 210 (ERIC)
WATER:
  LD B,84                 ; 84: water fired from the pistol, phase 1 (facing
                          ; left)
  LD L,0                  ; Point HL at byte 0 of ERIC's buffer
  LD A,(HL)               ; A=ERIC's animatory state
  INC L                   ; L=1
  RLCA                    ; Is ERIC facing left?
  JR NC,WATER_0           ; Jump if so
  LD B,212                ; 212: water fired from the pistol, phase 1 (facing
                          ; right)
WATER_0:
  SBC A,A                 ; Set A=-2 if ERIC's facing left, 2 if he's facing
  ADD A,A                 ; right
  CPL                     ;
  ADD A,A                 ;
  ADD A,(HL)              ; Add ERIC's x-coordinate to get the initial
                          ; x-coordinate of the water
  CP 191                  ; Is ERIC too close to (and facing) the far left wall
                          ; of the boys' skool or the far right wall of the
                          ; girls' skool?
  RET NC                  ; Return if so
  LD E,A                  ; Copy the water's initial x-coordinate to E
  INC L                   ; L=2
  LD D,(HL)               ; D=ERIC's y-coordinate
  DEC D                   ; Subtract 2 to get the initial y-coordinate of the
  DEC D                   ; water
  LD A,B                  ; A=initial animatory state of the water
  PUSH AF                 ; Save the water's initial animatory state briefly
  PUSH DE                 ; Save the water's initial coordinates briefly
  CALL PREPCATTY_0        ; Make a water pistol sound effect
  LD A,136                ; Message 136: NO WATERPISTOLS
  CALL CLOUD_0            ; Give ERIC lines if any teacher is nearby
  POP DE                  ; Restore the water's initial coordinates to DE
  POP AF                  ; Restore the water's initial animatory state to A
  LD HL,54803             ; Point HL at byte 19 of the water's buffer
  LD (HL),151             ; Initialise the water animation phase identifier
  CALL SUBCMD             ; Place address WATERFALL (below) into bytes 17 and
                          ; 18 of the water's buffer and update the SRB for the
                          ; water's appearance
; The address of this entry point is placed into bytes 17 and 18 of the water's
; buffer by the instruction above just after the water has been fired from the
; pistol, and is used throughout the water's lifespan. First, check whether the
; water has hit a cup, a plant, or the floor.
WATERFALL:
  LD L,19                 ; Increment the water animation phase identifier
  INC (HL)                ; (which starts off at 151) held in byte 19 of the
                          ; buffer
  LD B,(HL)               ; Pick up this identifier (152-156) in B
  LD C,255                ; Point BC at the appropriate entry in the water
                          ; animation table (see below)
  LD A,(BC)               ; Pick up the entry in A
  AND A                   ; Deal with the water at phase 3 (when it can hit a
  CALL NZ,CHKWATER        ; cup), 6 (when it can hit a plant) or 7+ (when it
                          ; hits the floor)
; Next, determine the water's new animatory state and coordinates as it moves
; through the air. For this we use the water animation table, which comprises
; five 4-byte entries located in bytes 252-255 of pages 152-156, corresponding
; to the phases of animation of the water as it flies from the pistol to the
; ground. Each entry contains the animatory state (AS), the x-coordinate
; increment (x+), the y-coordinate increment (y+), and a parameter (P) that is
; passed to the routine at CHKWATER when non-zero. When P=1, the water is at
; the right phase to fill a cup; when P=2, the water may have hit a plant or
; the ground.
;
; +---------+-------+-----+----+----+---+
; | Address | Phase | AS  | x+ | y+ | P |
; +---------+-------+-----+----+----+---+
; | WANIM2  | 2     | 92  | -2 | -1 | 0 |
; | WANIM3  | 3     | 108 | -2 | 0  | 1 |
; | WANIM4  | 4     | 116 | -1 | 0  | 0 |
; | WANIM5  | 5     | 124 | 0  | 1  | 0 |
; | WANIM6  | 6+    | 124 | 0  | 1  | 2 |
; +---------+-------+-----+----+----+---+
  DEC C                   ; C=254
  PUSH BC                 ; Save the water animation table pointer
  CALL UPDATESRB          ; Update the SRB for the water's current animatory
                          ; state and location
  LD A,D                  ; A=water's current y-coordinate
  POP HL                  ; Restore the water animation table pointer to HL
  ADD A,(HL)              ; Add the entry from the table to obtain the water's
                          ; new y-coordinate
  LD D,A                  ; Copy this to D
  DEC L                   ; L=253
  LD C,(HL)               ; Pick up the x-coordinate increment in C
  DEC L                   ; L=252
  LD A,(HL)               ; A=water's new animatory state (facing left)
  LD HL,CBUF214           ; Point HL at byte 0 of the water's buffer
  RL (HL)                 ; Set the carry flag if the water's travelling to the
                          ; right
  RLA                     ; Copy the carry flag into the 'direction' bit (bit
  RRCA                    ; 7) of the animatory state
  LD B,A                  ; B=water's new animatory state (facing the correct
                          ; way)
  LD A,E                  ; A=water's current x-coordinate
  JP WATER2               ; Jump over the data table at URTSGOPEN to continue
                          ; this routine

; Unused
  DEFS 4

  ORG 64000

; UDG reference table for the skool gate when open
;
; Used by the routine at ALTERUDGS. The UDG reference table for the skool gate
; when shut is at URTSGSHUT.
URTSGOPEN:
  DEFB 17,132,223,196,17,133,240,196
  DEFB 17,134,241,196,17,135,242,196
  DEFB 18,132,243,196,18,133,244,196
  DEFB 18,134,245,196,18,135,117,4
  DEFB 19,132,246,196,19,133,247,196
  DEFB 19,134,248,196,19,135,249,196
  DEFB 20,134,81,4,20,135,81,4
  DEFB 255                ; End marker

; Control water fired from the pistol (2)
;
; Continues from WATER. On entry the carry flag is set if the water is
; travelling to the right. In the following, 'water' may refer to either water
; or sherry.
;
; A Water's current x-coordinate
; B Water's new animatory state
; C x-coordinate increment (-2, -1 or 0)
; D Water's new y-coordinate
; H 214 (water)
WATER2:
  JR NC,WATER2_0          ; Jump if the water is travelling to the left
  SUB C                   ; Add 0, 1 or 2 to the water's current x-coordinate
  SUB C                   ; Do the same again to compensate for the ADD A,C
                          ; instruction below
WATER2_0:
  ADD A,C                 ; Now A=new x-coordinate for the water
  LD E,A                  ; Copy this to E
  CP 191                  ; Reset the carry flag if the water has hit the far
                          ; left wall of the boys' skool or the far right wall
                          ; of the girls' skool
  LD A,B                  ; Transfer the water's new animatory state to A
  JP C,UPDATEAS           ; Update the water's animatory state and location and
                          ; update the SRB (if the water hasn't hit one of the
                          ; far walls, i.e. it's still entirely on-screen)
  LD L,18                 ; Otherwise remove the uninterruptible subcommand
  LD (HL),0               ; routine address (WATERFALL) from bytes 17 and 18 of
                          ; the water's buffer
  JP OBJFALL_2            ; Place the water out of sight

; Unused
  DEFB 0

; Deal with water fired from the pistol at certain phases
;
; Used by the routine at WATER. On entry, A is equal to 1 if the water is at
; phase 3 of its trajectory (at which point it may hit a cup), or 2 if it's at
; phase 6 or above (at which point it may hit a plant or the floor). In the
; following, 'water' may refer to either water or sherry.
;
; A 1 or 2
; H 214 (water)
; L 19
CHKWATER:
  DEC A                   ; Is the water at the right phase to fill a cup?
  JR NZ,CHKWATER_3        ; Jump if not
  LD L,2                  ; Point HL at byte 2 of the water's buffer
  LD A,(HL)               ; A=water's y-coordinate
  SUB 14                  ; All cups have a y-coordinate of 14
  RET NZ                  ; Return if the water's not at the right height
  LD L,A                  ; L=0
  DEC A                   ; A=-1
  BIT 7,(HL)              ; Is the water travelling to the left?
  JR Z,CHKWATER_0         ; Jump if so
  LD A,3
CHKWATER_0:
  INC L                   ; L=1
  ADD A,(HL)              ; Now A=x-coordinate of the spot the water is hitting
  CALL CHKHITCUP_1        ; Is this the x-coordinate of one of the cups?
  RET NZ                  ; Return if not
  LD HL,INVENTORY         ; INVENTORY holds the inventory flags
  BIT 4,(HL)              ; Set the zero flag if there's water (not sherry) in
                          ; the pistol
  LD HL,56439             ; H=220, L=119
  JR Z,CHKWATER_1         ; Jump if there's water in the pistol
  LD L,124
CHKWATER_1:
  INC H                   ; Point HL at a cup graphic data table
  CP (HL)                 ; Is this the right data table for the cup?
  JR NZ,CHKWATER_1        ; Jump back to check the next table if not
  DEC L                   ; Point HL at the appropriate UDG reference table
; Now HL points at the start of the appropriate UDG reference table for the
; cup:
;
; +----------+----------------------------------+----------+
; | HL       | Cup                              | Contents |
; +----------+----------------------------------+----------+
; | URTCUP1W | Leftmost cup in the boys' skool  | Water    |
; | URTCUP1S | Leftmost cup in the boys' skool  | Sherry   |
; | URTCUP2W | Middle cup in the boys' skool    | Water    |
; | URTCUP2S | Middle cup in the boys' skool    | Sherry   |
; | URTCUP3W | Rightmost cup in the boys' skool | Water    |
; | URTCUP3S | Rightmost cup in the boys' skool | Sherry   |
; | URTCUP4W | Cup in the girls' skool          | Water    |
; | URTCUP4S | Cup in the girls' skool          | Sherry   |
; +----------+----------------------------------+----------+
  CALL ALTERUDGS          ; Alter the appropriate UDG and attribute references
                          ; in the play area to show the cup filled with water
                          ; or sherry
CHKWATER_2:
  POP HL                  ; Drop the return address (to the routine at WATER)
                          ; from the stack
  LD H,214                ; 214=water
  JP OBJFALL_0            ; Remove the water from sight
; The water is at phase 6 or above of its trajectory. Has it hit the floor?
CHKWATER_3:
  DEC (HL)                ; Decrement the water animation phase identifier in
                          ; byte 19 of the water's buffer from 156 to 155; this
                          ; has the effect of bringing us back through this
                          ; section of code (via the routine at WATER) until
                          ; the water has hit either a plant or the floor
  CALL ONTMBFLOOR         ; Is the water's y-coordinate 3 (top floor), 10
                          ; (middle floor) or 17 (bottom floor)?
  JR NZ,CHKWATER_4        ; Jump if not
  CP 17                   ; Did it hit the bottom floor?
  JR Z,CHKWATER_2         ; Terminate the water if so
; The water has reached the level of the top or middle floor. We terminate it
; here if it's inside the boys' skool or the girls' skool. (Note that it hasn't
; necessarily hit the floor at this point; for example, it may have been fired
; from the top of the stairs above the stage down onto the stage, in which case
; it will terminate in mid-air above the stage, at the level of the middle
; floor.) Otherwise we let it continue till it hits the ground.
  DEC L                   ; L=1
  LD A,(HL)               ; A=water's x-coordinate
  CP 96                   ; Terminate the water if A<96 (it's inside the boys'
  JR C,CHKWATER_2         ; skool) or A>=160 (it's inside the girls' skool)
  CP 160                  ;
  JR NC,CHKWATER_2        ;
  RET
; The water hasn't hit a cup or the floor. Has it hit a plant pot?
CHKWATER_4:
  CALL CHECKPOT           ; Has the water hit a plant pot?
  RET NZ                  ; Return if not
  LD A,(INVENTORY)        ; INVENTORY holds the inventory flags
  BIT 4,A                 ; Has the pistol got sherry in it?
  JR NZ,CHKWATER_2        ; Terminate the sherry if so (sherry does not make
                          ; plants grow)
  POP DE                  ; Drop the return address (to the routine at WATER)
                          ; from the stack
  CALL UPDATESRB          ; Update the SRB for the water's current animatory
                          ; state and location
  INC D                   ; Now D=y-coordinate of the plant
  LD L,19                 ; Initialise the counter that determines the phases
  LD (HL),22              ; of growth of the watered plant
  INC L                   ; Store the x-coordinate of the plant in byte 20 of
  LD (HL),E               ; the buffer for retrieval later when it starts
                          ; growing
  LD E,0                  ; The plant's initial x-coordinate will be 0 (which
                          ; is certainly off-screen at the moment, since the
                          ; leftmost plant pot is at x-coordinate 91)
  LD A,E                  ; And its initial animatory state will be 0 (ERIC
                          ; standing, facing left), so it's a good thing it
                          ; will be off-screen!
  CALL SUBCMD             ; Place address GROWPLANT (below) into bytes 17 and
                          ; 18 of the plant's buffer and update the SRB for the
                          ; plant's appearance
; The address of this entry point is placed into bytes 17 and 18 of the plant's
; buffer by the instruction above just after the plant has been watered.
GROWPLANT:
  LD L,19                 ; Byte 19 holds the counter that determines the
                          ; phases of growth (and death) of the plant
  DEC (HL)                ; Is the plant ready to die?
  JR NZ,CHKWATER_6        ; Jump if not
  LD A,(STATUS2)          ; STATUS2 holds ERIC's secondary status flags
  CP 2                    ; Is ERIC standing on a plant or plant pot?
  JR NZ,CHKWATER_5        ; Jump if not
  LD A,16                 ; Set bit 4 at STATUS2, indicating that ERIC is going
  LD (STATUS2),A          ; to take a fall (even if ERIC's standing on a
                          ; different plant pot; this is a bug)
CHKWATER_5:
  JP OBJFALL_0            ; Kill the plant
; The plant is not dead yet. Plan its next move.
CHKWATER_6:
  LD A,(HL)               ; Pick up the plant lifespan counter in A
  CP 8                    ; Is it time for the plant to start growing?
  JR NZ,CHKWATER_7        ; Jump if not
  INC L                   ; E=x-coordinate of the plant (stored in byte 20
  LD E,(HL)               ; earlier)
  LD L,2                  ; Point HL at byte 2 of the plant's buffer
  LD D,(HL)               ; D=y-coordinate of the plant
  LD A,42                 ; 42: flower, half-grown
  JR CHKWATER_8
CHKWATER_7:
  CP 4                    ; Is it time for the plant to reach full height?
  RET NZ                  ; Return if not
  CALL UPDATESRB          ; Update the SRB for the plant's current animatory
                          ; state
  INC A                   ; A=43: flower, fully grown
CHKWATER_8:
  CALL UPDATEAS           ; Update the plant's animatory state and update the
                          ; SRB
  LD A,(STATUS2)          ; STATUS2 holds ERIC's status flags
  CP 2                    ; Is ERIC standing on a plant?
  RET NZ                  ; Return if not
  LD A,(53761)            ; A=ERIC's x-coordinate
  LD L,1                  ; Point HL at byte 1 of the plant's buffer (which
                          ; holds its x-coordinate)
  CP (HL)                 ; Is ERIC standing on this plant?
  RET NZ                  ; Return if not
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  DEC D                   ; ERIC will rise one level as the plant grows
  JP UPDATEAS             ; Update ERIC's location and update the SRB

; Unused
  DEFB 0

; Begin ERIC's descent from a plant, a plant pot, or the assembly hall stage
;
; Used by the routines at OFFSTAGE and ONPLANT.
STEPOFF:
  LD A,12                 ; Bits 2 and 3 set (ERIC is stepping off a fully
                          ; grown plant in the direction of an open window or
                          ; the closed skool gate, or stepping off the stage)
; This entry point is also used by the routine at ONPLANT with A=4 (bit 2 set,
; indicating that ERIC has stepped off a plant).
STEPOFF_0:
  LD (STATUS2),A          ; Set ERIC's secondary status flags at STATUS2 as
                          ; appropriate
; This entry point is used by the routine at STEPPEDOFF.
STEPOFF_1:
  CALL WALKSOUND          ; Make a sound effect
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  INC A                   ; A=ERIC's next animatory state (midstride)
  JP ERICSITLIE_4         ; Update ERIC's animatory state and update the SRB

; Deal with ERIC when he's stepping off a plant, a plant pot, or the assembly
; hall stage
;
; Used by the routine at HANDLEERIC when bit 2 at STATUS2 is set (by the
; routine at STEPOFF or ONPLANT, or by this routine on the first pass through).
; On entry, bit 3 at STATUS2 is also set if ERIC is stepping out of an open
; window, over the closed skool gate, or off the assembly hall stage. On exit,
; ERIC's status flags at STATUS2 are set as follows:
;
; +-----+---------------------------------------------------------------------+
; | Bit | Meaning                                                             |
; +-----+---------------------------------------------------------------------+
; | 2   | ERIC has completed one step towards an open window, over the closed |
; |     | skool gate, or off the stage                                        |
; | 3   | ERIC is now falling (but not from the top-floor window)             |
; | 5   | ERIC is now falling out of the top-floor window                     |
; +-----+---------------------------------------------------------------------+
STEPPEDOFF:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  LD (HL),5               ; Reset ERIC's main action timer to 5
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC' buffer
  BIT 0,(HL)              ; Is ERIC midstride?
  JR Z,STEPOFF_1          ; Make him so if not (with an accompanying sound
                          ; effect)
  LD A,(STATUS2)          ; Set the zero flag if bit 3 of ERIC's secondary
  CP 12                   ; status flags at STATUS2 is also set
  LD A,4                  ; Set bit 2 (only) in A
  JR Z,STEPPEDOFF_0       ; Reset bit 3 at STATUS2 if ERIC has just stepped off
                          ; a fully grown plant towards an open window or the
                          ; closed skool gate, or just stepped off the stage
  INC L                   ; L=1
  LD A,(HL)               ; A=ERIC's x-coordinate
  CP 92                   ; This is the x-coordinate of the plant pot on the
                          ; top floor
  LD A,32                 ; Set bit 5 in A
  JR Z,STEPPEDOFF_0       ; Jump if ERIC is going to fall from the top-floor
                          ; window
  LD A,8                  ; Set bit 3 in A
STEPPEDOFF_0:
  LD (STATUS2),A          ; Set the appropriate bit (2, 3 or 5) in ERIC's
                          ; secondary status flags at STATUS2
  CALL WALKSOUND          ; Make a sound effect
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  INC A                   ; A=ERIC's new animatory state (standing)
  AND 130                 ;
  DEC E                   ; Decrement ERIC's x-coordinate
  BIT 7,A                 ; Is ERIC facing left?
  JR Z,STEPPEDOFF_1       ; Jump if so
  INC E                   ; Increment ERIC's x-coordinate (if he's facing
  INC E                   ; right)
STEPPEDOFF_1:
  JP MVERIC2_0            ; Update ERIC's animatory state and location, make a
                          ; sound effect, and scroll the screen if necessary

; Deal with ERIC when he's standing on a plant or plant pot
;
; Used by the routine at HANDLEERIC when bit 1 at STATUS2 is set (by the
; routine at JUMPING). If 'down', 'left' or 'right' is pressed while ERIC is
; standing on a plant or plant pot, this routine will set bit 2 or 3 of ERIC's
; status flags at STATUS2 as follows:
;
; +--------+-----------------------------------------------------------------+
; | Bit(s) | Meaning                                                         |
; +--------+-----------------------------------------------------------------+
; | 2      | ERIC is stepping off a plant or plant pot straight to the floor |
; | 3      | ERIC is falling and will land on his feet ('down' pressed)      |
; | 2 & 3  | ERIC is stepping off a fully grown plant in the direction of an |
; |        | open window or the closed skool gate                            |
; +--------+-----------------------------------------------------------------+
ONPLANT:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  INC (HL)                ; Set ERIC's main action timer to 1, ensuring that we
                          ; pass through the following section of code on the
                          ; next call to this routine if no keypress is
                          ; detected this time
  CALL KEYOFFSET          ; Get the value from the keypress offset table
                          ; corresponding to the last key pressed (if any)
  RET Z                   ; Return if no keys were pressed
; A key was pressed while ERIC was standing on a plant or plant pot.
  LD HL,ERICTIMER         ; Reset ERIC's main action timer at ERICTIMER to 5
  LD (HL),5               ;
  RES 3,A                 ; Reset bit 3 of the keypress code to make it lower
                          ; case
  CP 86                   ; Was 'down' pressed?
  JR NZ,ONPLANT_0         ; Jump if not
; 'Down' was pressed, so make ERIC descend gracefully.
  LD L,237                ; HL=STATUS2 (ERIC's secondary status flags)
  LD (HL),8               ; Set bit 3: ERIC is falling and will land on his
                          ; feet
  RET
; It wasn't 'down'. Was it 'left' or 'right'?
ONPLANT_0:
  EX DE,HL
  LD HL,ERICCBUF          ; Point HL at byte 0 of ERIC's buffer
  CP 82                   ; Was 'right' pressed?
  JR NZ,ONPLANT_8         ; Jump if not
; 'Right' was pressed.
  BIT 7,(HL)              ; Is ERIC facing left?
  JP Z,TURNERIC           ; Turn ERIC round if so
  INC L                   ; L=1
  LD A,(HL)               ; A=ERIC's x-coordinate
  EX DE,HL
  INC L                   ; HL=DOORFLAGS (which holds the door/window status
                          ; flags)
  CP 132                  ; This is the x-coordinate of the plant pot to the
                          ; left of the gate
  JR C,ONPLANT_5          ; Jump if ERIC is standing on a plant pot in the
                          ; boys' skool
ONPLANT_1:
  JR Z,ONPLANT_3          ; Jump if ERIC is standing on one of the plant pots
                          ; beside the gate
ONPLANT_2:
  LD A,4                  ; Make a sound effect, update the SRB, and set bit 2
  JP STEPOFF_0            ; at STATUS2 (ERIC is stepping off a plant/plant pot)
; ERIC is standing on one of the plant pots on either side of the skool gate,
; and has tried to step off the plant or plant pot in the direction of the
; gate.
ONPLANT_3:
  BIT 4,(HL)              ; Is the skool gate open?
  JR NZ,ONPLANT_2         ; Jump if so
  LD B,14                 ; This is the y-coordinate of ERIC if he's standing
                          ; on a fully grown plant on the bottom floor
ONPLANT_4:
  INC E                   ; Point DE at byte 2 of ERIC's buffer
  LD A,(DE)               ; A=ERIC's y-coordinate
  CP B                    ; Is ERIC on top of a fully grown plant?
  RET NZ                  ; Return if not
  JP STEPOFF              ; Otherwise make a sound effect, update the SRB, and
                          ; set bits 2 and 3 at STATUS2
; ERIC is standing on one of the plant pots in the boys' skool, and has tried
; to step off in the direction of the window.
ONPLANT_5:
  CP 91                   ; Is ERIC standing on the top-floor plant pot?
  JR NZ,ONPLANT_7         ; Jump if not
  LD B,0                  ; This is the y-coordinate of ERIC if he's standing
                          ; on a fully-grown plant on the top floor
  BIT 6,(HL)              ; Is the top-floor window closed?
ONPLANT_6:
  RET Z                   ; Return if so
  JR ONPLANT_4
ONPLANT_7:
  LD B,7                  ; This is the y-coordinate of ERIC if he's standing
                          ; on a fully-grown plant on the middle floor
  BIT 7,(HL)              ; Set the zero flag if the middle-floor window is
                          ; closed
  JR ONPLANT_6
; 'Right' wasn't pressed. Was it 'left'?
ONPLANT_8:
  CP 80                   ; Was 'left' pressed?
  RET NZ                  ; Return if not
; 'Left' was pressed.
  BIT 7,(HL)              ; Is ERIC facing right?
  JP NZ,TURNERIC          ; Turn ERIC round if so
  INC L                   ; Point HL at byte 1 of ERIC's buffer
  LD A,(HL)               ; A=ERIC's x-coordinate
  EX DE,HL
  INC L                   ; HL=DOORFLAGS (which holds the door/window status
                          ; flags)
  CP 135                  ; This is the x-coordinate of the plant pot to the
                          ; right of the gate; set the carry flag if ERIC is to
                          ; the left of this, or set the zero flag if he's
                          ; standing on it
  JR ONPLANT_1

; Deal with ERIC when he's falling to the floor to land on his feet
;
; Used by the routine at HANDLEERIC when bit 3 at STATUS2 is set (by the
; routine at STEPPEDOFF or ONPLANT).
LANDING:
  LD HL,ERICTIMER         ; ERICTIMER holds ERIC's main action timer
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  EX DE,HL
  LD HL,53762             ; Point HL at byte 2 of ERIC's buffer
  INC (HL)                ; Increment ERIC's y-coordinate temporarily (for
                          ; testing purposes)
  CALL ONTMBFLOOR         ; Has ERIC reached the floor yet?
  JR NZ,LANDING_2         ; Jump if not
  CP 17                   ; This is the y-coordinate of the bottom floor
  DEC HL                  ; Point HL at byte 1 of ERIC's buffer
  JR Z,LANDING_0          ; Jump if ERIC has landed on the bottom floor
  LD A,(HL)               ; A=ERIC's x-coordinate
  CP 95                   ; This is the x-coordinate of the rightmost end of
                          ; the boys' skool
  JR NC,LANDING_1         ; Jump if ERIC is outside the boys' skool (and
                          ; therefore cannot land on the top or middle floors)
LANDING_0:
  EX DE,HL                ; Reset all of ERIC's status flags at STATUS2 and
  LD L,237                ; STATUS, indicating that ERIC has landed safely
  LD (HL),0               ;
  LD L,251                ;
  LD (HL),0               ;
  EX DE,HL                ;
LANDING_1:
  INC L                   ; L=2
LANDING_2:
  DEC (HL)                ; Restore ERIC's y-coordinate (we've finished
                          ; testing)
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  INC D                   ; Increment ERIC's y-coordinate (i.e. make him drop
                          ; one level)
  JP ERICSITLIE_4         ; Update ERIC's location, update the SRB, and reset
                          ; ERIC's main action timer at ERICTIMER to 6

; Deal with ERIC while he's falling (1)
;
; Used by the routine at HANDLEERIC when bit 4 at STATUS2 is set (by the
; routine at WHEELBIKE, RIDINGBIKE or CHKWATER). On entry, bit 6 at STATUS2 is
; also set if ERIC has already begun his descent (i.e. we've already made the
; first pass through this routine).
FALLING:
  LD HL,STATUS2           ; STATUS2 holds ERIC's secondary status flags
  BIT 6,(HL)              ; Has ERIC already begun his descent?
  JR NZ,FALLING_4         ; Jump if so
; ERIC has not begun his descent yet, which means we need to figure out what
; his trajectory will be first.
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  AND 128                 ; A=0 if ERIC's facing left, 128 if facing right
; This entry point is used by the routine at ONSADDLE when ERIC has jumped from
; the saddle of the bike (with A=2 or 130, and D holding the y-coordinate of
; the spot above ERIC's hand).
FALLING_0:
  LD B,A                  ; Now B=0 (or 2) if ERIC's facing left, 128 (or 130)
                          ; if facing right
  LD A,D                  ; A=y-coordinate of ERIC (or the spot above his hand)
  CP 17                   ; Is ERIC on the bottom floor?
  JR NZ,FALLING_1         ; Jump if not
  DEC D                   ; D=16
  DEC A                   ; A=16
FALLING_1:
  ADD A,4                 ; Set A=-7+(y-3)%7 if y<17, or -1 if y=17 (where y is
FALLING_2:
  SUB 7                   ; ERIC's y-coordinate); this is the distance
  JR NC,FALLING_2         ; (negated) left to travel till ERIC reaches the
                          ; floor
  ADD A,166               ; A=159-165 (normal descent table entry pointer)
  PUSH AF                 ; Save this briefly
  LD A,5                  ; Now A=animatory state of ERIC sitting on the floor
  ADD A,B                 ; (5/133) or with his arm up (7/135)
  CALL UPDATEAS           ; Update ERIC's animatory state and location and
                          ; update the SRB
  POP AF                  ; Restore the normal descent table entry pointer to A
  LD H,A                  ; Point HL at the appropriate entry (H=159-165) in
  LD L,252                ; the normal descent table (L=252) for ERIC's current
                          ; height above the floor
; This entry point is used by the routines at OFFSADDLE (with H=159, L=253 or
; 254) and BIGFALL (with H=159, L=255).
FALLING_3:
  LD (ERICDATA1),HL       ; Store the descent table pointer at ERICDATA1
  LD HL,STATUS2           ; STATUS2 holds ERIC's secondary status flags
  LD (HL),80              ; Set bits 4 (ERIC is still falling) and 6 (ERIC has
                          ; begun his descent)
  LD L,243                ; Set ERIC's main action timer at ERICTIMER to 7
  LD (HL),7               ;
; Deal with the next phase of ERIC's descent.
FALLING_4:
  LD L,243                ; HL=ERICTIMER (which holds ERIC's main action timer)
  DEC (HL)                ; Is it time to deal with ERIC yet?
  RET NZ                  ; Return if not
  LD (HL),3               ; Reset ERIC's action timer to 3
  LD L,216                ; HL=ERICDATA1 (which holds the descent table
                          ; pointer)
  INC (HL)                ; Collect the descent table pointer into DE
  LD D,(HL)               ;
  DEC L                   ;
  LD E,(HL)               ;
  LD A,(DE)               ; Pick up an entry from the descent table in A
  AND A                   ; Has ERIC landed yet?
  JR NZ,FALLING_5         ; Jump if not
; ERIC has landed. Time to set ERIC's status flags at STATUS and STATUS2
; appropriately.
  LD L,251                ; HL=STATUS (ERIC's main status flags)
  INC D                   ; Collect the first set of flags from the descent
  LD A,(DE)               ; table
  LD (HL),A               ; Copy them into STATUS
  INC D                   ; Collect the second set of flags from the descent
  LD A,(DE)               ; table
  LD L,237                ; Copy them into STATUS2
  LD (HL),A               ;
  AND A                   ; Did ERIC jump from the top floor window (bit 5
                          ; set)?
  CALL NZ,EXPEL           ; Set MR WACKER on his way to expel ERIC if so
  JP ERICHIT_3            ; Make a sound effect for ERIC's landing
; ERIC hasn't landed yet. Determine his next move through the air.
FALLING_5:
  PUSH AF                 ; Save the descent table entry briefly
  LD H,210                ; 210=ERIC
  CALL UPDATESRB          ; Update the SRB for ERIC's current animatory state
                          ; and location
  AND 128                 ; B=0 if ERIC's facing left, 128 if facing right
  LD B,A                  ;
  POP AF                  ; Restore the descent table entry to A
  LD C,A                  ; Copy it to C
  JP FALLING2             ; The routine continues at FALLING2
; This routine uses a descent table to guide ERIC to the floor. The bits in
; each table entry have the following significance:
;
; +--------+----------------------------------+
; | Bit(s) | Meaning                          |
; +--------+----------------------------------+
; | 0-3    | ERIC's next animatory state      |
; | 4      | If set, move backwards one space |
; | 5      | If set, move forwards one space  |
; | 6      | If set, descend one level        |
; | 7      | If set, ascend one level         |
; +--------+----------------------------------+
;
; The descent tables themselves contain the following entries:
;
; +-----------------------------------------------------------------------+
; | E=252 (normal descent)                                                |
; +-----+---------+------+------------------------------------------------+
; | D   | DE      | (DE) | Meaning                                        |
; +-----+---------+------+------------------------------------------------+
; | 160 | DT252E0 | 69   | Animatory state 5 or 133; descend              |
; | 161 | DT252E1 | 69   | Animatory state 5 or 133; descend              |
; | 162 | DT252E2 | 69   | Animatory state 5 or 133; descend              |
; | 163 | DT252E3 | 69   | Animatory state 5 or 133; descend              |
; | 164 | DT252E4 | 69   | Animatory state 5 or 133; descend              |
; | 165 | DT252E5 | 69   | Animatory state 5 or 133; descend              |
; | 166 | DT252E6 | 69   | Animatory state 5 or 133; descend              |
; | 167 | DT252E7 | 0    | ERIC has landed                                |
; | 168 | DT252E8 | 4    | Set bit 2 at STATUS (ERIC is sitting)          |
; | 169 | DT252E9 | 0    | Reset all bits at STATUS2 (ERIC landed safely) |
; +-----+---------+------+------------------------------------------------+
;
; +-------------------------------------------------------------------------+
; | E=253 (from the bike saddle over the closed skool gate)                 |
; +-----+----------+------+-------------------------------------------------+
; | D   | DE       | (DE) | Meaning                                         |
; +-----+----------+------+-------------------------------------------------+
; | 160 | DT253E0  | 160  | Animatory state 0 or 128; ascend; move forward  |
; | 161 | DT253E1  | 160  | Animatory state 0 or 128; ascend; move forward  |
; | 162 | DT253E2  | 160  | Animatory state 0 or 128; ascend; move forward  |
; | 163 | DT253E3  | 96   | Animatory state 0 or 128; descend; move forward |
; | 164 | DT253E4  | 100  | Animatory state 4 or 132; descend; move forward |
; | 165 | DT253E5  | 37   | Animatory state 5 or 133; move forward          |
; | 166 | DT253E6  | 69   | Animatory state 5 or 133; descend               |
; | 167 | DT253E7  | 69   | Animatory state 5 or 133; descend               |
; | 168 | DT253E8  | 0    | ERIC has landed                                 |
; | 169 | DT253E9  | 4    | Set bit 2 at STATUS (ERIC is sitting)           |
; | 170 | DT253E10 | 0    | Reset all bits at STATUS2 (ERIC landed safely)  |
; +-----+----------+------+-------------------------------------------------+
;
; +-----------------------------------------------------------------------+
; | E=254 (from the bike saddle into a wall or closed door)               |
; +-----+---------+------+------------------------------------------------+
; | D   | DE      | (DE) | Meaning                                        |
; +-----+---------+------+------------------------------------------------+
; | 160 | DT254E0 | 160  | Animatory state 0 or 128; ascend; move forward |
; | 161 | DT254E1 | 160  | Animatory state 0 or 128; ascend; move forward |
; | 162 | DT254E2 | 2    | Animatory state 2 or 130; ascend               |
; | 163 | DT254E3 | 64   | Animatory state 0 or 128; descend              |
; | 164 | DT254E4 | 64   | Animatory state 0 or 128; descend              |
; | 165 | DT254E5 | 64   | Animatory state 0 or 128; descend              |
; | 166 | DT254E6 | 22   | Animatory state 6 or 134; move backwards       |
; | 167 | DT254E7 | 0    | ERIC has landed                                |
; | 168 | DT254E8 | 4    | Set bit 2 at STATUS (ERIC is sitting)          |
; | 169 | DT254E9 | 0    | Reset all bits at STATUS2 (ERIC landed safely) |
; +-----+---------+------+------------------------------------------------+
;
; +---------------------------------------------------------------------------+
; | E=255 (from the top-floor window)                                         |
; +-----+----------+------+---------------------------------------------------+
; | D   | DE       | (DE) | Meaning                                           |
; +-----+----------+------+---------------------------------------------------+
; | 160 | DT255E0  | 96   | Animatory state 0 or 128; descend; move forwards  |
; | 161 | DT255E1  | 96   | Animatory state 0 or 128; descend; move forwards  |
; | 162 | DT255E2  | 96   | Animatory state 0 or 128; descend; move forwards  |
; | 163 | DT255E3  | 96   | Animatory state 0 or 128; descend; move forwards  |
; | 164 | DT255E4  | 64   | Animatory state 0 or 128; descend                 |
; | 165 | DT255E5  | 64   | Animatory state 0 or 128; descend                 |
; | 166 | DT255E6  | 64   | Animatory state 0 or 128; descend                 |
; | 167 | DT255E7  | 64   | Animatory state 0 or 128; descend                 |
; | 168 | DT255E8  | 64   | Animatory state 0 or 128; descend                 |
; | 169 | DT255E9  | 64   | Animatory state 0 or 128; descend                 |
; | 170 | DT255E10 | 68   | Animatory state 4 or 132; descend                 |
; | 171 | DT255E11 | 68   | Animatory state 4 or 132; descend                 |
; | 172 | DT255E12 | 68   | Animatory state 4 or 132; descend                 |
; | 173 | DT255E13 | 5    | Animatory state 5 or 133                          |
; | 174 | DT255E14 | 69   | Animatory state 5 or 133; descend                 |
; | 175 | DT255E15 | 69   | Animatory state 5 or 133; descend                 |
; | 176 | DT255E16 | 69   | Animatory state 5 or 133; descend                 |
; | 177 | DT255E17 | 70   | Animatory state 6 or 134; descend                 |
; | 178 | DT255E18 | 0    | ERIC has landed                                   |
; | 179 | DT255E19 | 64   | Set bit 6 at STATUS (MR WACKER should expel ERIC) |
; | 180 | DT255E20 | 32   | Set bit 5 at STATUS2 (ERIC fell out of the        |
; |     |          |      | top-floor window)                                 |
; +-----+----------+------+---------------------------------------------------+

; Unused
  DEFB 32,32,32,32

; Message 1: 'NO^STINKBOMBS'
;
; Used by the routine at CLOUD.
MSG001:
  DEFM "NO"
  DEFB 2                  ; Newline
  DEFM "STINKBOMBS"
  DEFB 0                  ; End marker

; Message 2: 'NO^CATAPULTS'
;
; Used by the routine at FIRE.
MSG002:
  DEFM "NO"
  DEFB 2                  ; Newline
  DEFM "CATAPULTS"
  DEFB 0                  ; End marker

; Message 136: 'NO^WATERPISTOLS'
;
; Used by the routine at WATER.
MSG136:
  DEFM "NO"
  DEFB 2                  ; Newline
  DEFM "WATERPISTOLS"
  DEFB 0                  ; End marker

; Unused
  DEFB 0

; Message 82: 'WHEN DID [10] BECOME KING?{9sp}'
;
; Used by the routine at DOCLASS.
MSG082:
  DEFM "WHEN DID "
  DEFB 10                 ; 10: '{king}'
  DEFM " BECOME KING?"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 78: 'WHERE DOES A[10] LIVE?{9sp}'
;
; Used by the routine at DOCLASS.
MSG078:
  DEFM "WHERE DOES A"
  DEFB 10                 ; 10: '{animal}'
  DEFM " LIVE?"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 160: 'KILIMANJARO'
;
; Used by the routine at DOCLASS.
MSG160:
  DEFM "KILIMANJARO"
  DEFB 0                  ; End marker

; Message 161: 'KOSCIUSKO'
;
; Used by the routine at DOCLASS.
MSG161:
  DEFM "KOSCIUSKO"
  DEFB 0                  ; End marker

; Message 162: 'McKINLEY'
;
; Used by the routine at DOCLASS.
MSG162:
  DEFM "McKINLEY"
  DEFB 0                  ; End marker

; Message 163: 'KOMMUNISMA'
;
; Used by the routine at DOCLASS.
MSG163:
  DEFM "KOMMUNISMA"
  DEFB 0                  ; End marker

; Message 164: 'HUASCARAN'
;
; Used by the routine at DOCLASS.
MSG164:
  DEFM "HUASCARAN"
  DEFB 0                  ; End marker

; Message 165: 'SAJAMA'
;
; Used by the routine at DOCLASS.
MSG165:
  DEFM "SAJAMA"
  DEFB 0                  ; End marker

; Message 166: 'COOK'
;
; Used by the routine at DOCLASS.
MSG166:
  DEFM "COOK"
  DEFB 0                  ; End marker

; Message 167: 'SNOWDON'
;
; Used by the routine at DOCLASS.
MSG167:
  DEFM "SNOWDON"
  DEFB 0                  ; End marker

; Message 168: 'TANZANIA'
;
; Used by the routine at DOCLASS.
MSG168:
  DEFM "TANZANIA"
  DEFB 0                  ; End marker

; Message 169: 'AUSTRALIA'
;
; Used by the routine at DOCLASS.
MSG169:
  DEFM "AUSTRALIA"
  DEFB 0                  ; End marker

; Message 170: 'ALASKA'
;
; Used by the routine at DOCLASS.
MSG170:
  DEFM "ALASKA"
  DEFB 0                  ; End marker

; Message 171: 'THE USSR'
;
; Used by the routine at DOCLASS.
MSG171:
  DEFM "THE USSR"
  DEFB 0                  ; End marker

; Message 172: 'PERU'
;
; Used by the routine at DOCLASS.
MSG172:
  DEFM "PERU"
  DEFB 0                  ; End marker

; Message 173: 'BOLIVIA'
;
; Used by the routine at DOCLASS.
MSG173:
  DEFM "BOLIVIA"
  DEFB 0                  ; End marker

; Message 174: 'NEW ZEALAND'
;
; Used by the routine at DOCLASS.
MSG174:
  DEFM "NEW ZEALAND"
  DEFB 0                  ; End marker

; Message 175: 'WALES'
;
; Used by the routine at DOCLASS.
MSG175:
  DEFM "WALES"
  DEFB 0                  ; End marker

; Message 192: 'JOHN'
;
; Used by the routine at DOCLASS.
MSG192:
  DEFM "JOHN"
  DEFB 0                  ; End marker

; Message 193: 'STEPHEN'
;
; Used by the routine at DOCLASS.
MSG193:
  DEFM "STEPHEN"
  DEFB 0                  ; End marker

; Message 194: 'JAMES I'
;
; Used by the routine at DOCLASS.
MSG194:
  DEFM "JAMES I"
  DEFB 0                  ; End marker

; Message 195: 'GEORGE V'
;
; Used by the routine at DOCLASS.
MSG195:
  DEFM "GEORGE V"
  DEFB 0                  ; End marker

; Message 196: 'HENRY V'
;
; Used by the routine at DOCLASS.
MSG196:
  DEFM "HENRY V"
  DEFB 0                  ; End marker

; Message 197: 'HENRY VII'
;
; Used by the routine at DOCLASS.
MSG197:
  DEFM "HENRY VII"
  DEFB 0                  ; End marker

; Message 198: 'GEORGE I'
;
; Used by the routine at DOCLASS.
MSG198:
  DEFM "GEORGE I"
  DEFB 0                  ; End marker

; Message 199: 'CHARLES I'
;
; Used by the routine at DOCLASS.
MSG199:
  DEFM "CHARLES I"
  DEFB 0                  ; End marker

; Unused
  DEFB 0

; Message 200: '199'
;
; Used by the routine at DOCLASS.
MSG200:
  DEFM "199"
  DEFB 0                  ; End marker

; Message 201: '135'
;
; Used by the routine at DOCLASS.
MSG201:
  DEFM "135"
  DEFB 0                  ; End marker

; Message 202: '603'
;
; Used by the routine at DOCLASS.
MSG202:
  DEFM "603"
  DEFB 0                  ; End marker

; Message 203: '910'
;
; Used by the routine at DOCLASS.
MSG203:
  DEFM "910"
  DEFB 0                  ; End marker

; Message 204: '413'
;
; Used by the routine at DOCLASS.
MSG204:
  DEFM "413"
  DEFB 0                  ; End marker

; Message 205: '485'
;
; Used by the routine at DOCLASS.
MSG205:
  DEFM "485"
  DEFB 0                  ; End marker

; Message 206: '714'
;
; Used by the routine at DOCLASS.
MSG206:
  DEFM "714"
  DEFB 0                  ; End marker

; Message 207: '625'
;
; Used by the routine at DOCLASS.
MSG207:
  DEFM "625"
  DEFB 0                  ; End marker

; Message 176: ' BADGER'
;
; Used by the routine at DOCLASS.
MSG176:
  DEFM " BADGER"
  DEFB 0                  ; End marker

; Message 177: ' SQUIRREL'
;
; Used by the routine at DOCLASS.
MSG177:
  DEFM " SQUIRREL"
  DEFB 0                  ; End marker

; Message 178: ' BEAVER'
;
; Used by the routine at DOCLASS.
MSG178:
  DEFM " BEAVER"
  DEFB 0                  ; End marker

; Message 179: ' HARE'
;
; Used by the routine at DOCLASS.
MSG179:
  DEFM " HARE"
  DEFB 0                  ; End marker

; Message 180: 'N EAGLE'
;
; Used by the routine at DOCLASS.
MSG180:
  DEFM "N EAGLE"
  DEFB 0                  ; End marker

; Message 181: 'N OTTER'
;
; Used by the routine at DOCLASS.
MSG181:
  DEFM "N OTTER"
  DEFB 0                  ; End marker

; Message 182: ' BEE'
;
; Used by the routine at DOCLASS.
MSG182:
  DEFM " BEE"
  DEFB 0                  ; End marker

; Message 183: ' BUG'
;
; Used by the routine at DOCLASS.
MSG183:
  DEFM " BUG"
  DEFB 0                  ; End marker

; Message 184: ' SET'
;
; Used by the routine at DOCLASS.
MSG184:
  DEFM " SET"
  DEFB 0                  ; End marker

; Message 185: ' DREY'
;
; Used by the routine at DOCLASS.
MSG185:
  DEFM " DREY"
  DEFB 0                  ; End marker

; Message 186: ' LODGE'
;
; Used by the routine at DOCLASS.
MSG186:
  DEFM " LODGE"
  DEFB 0                  ; End marker

; Message 187: ' FORM'
;
; Used by the routine at DOCLASS.
MSG187:
  DEFM " FORM"
  DEFB 0                  ; End marker

; Message 188: 'N EYRIE'
;
; Used by the routine at DOCLASS.
MSG188:
  DEFM "N EYRIE"
  DEFB 0                  ; End marker

; Message 189: ' HOLT'
;
; Used by the routine at DOCLASS.
MSG189:
  DEFM " HOLT"
  DEFB 0                  ; End marker

; Message 190: ' HIVE'
;
; Used by the routine at DOCLASS.
MSG190:
  DEFM " HIVE"
  DEFB 0                  ; End marker

; Message 191: ' BAD PROGRAM'
;
; Used by the routine at DOCLASS.
MSG191:
  DEFM " BAD PROGRAM"
  DEFB 0                  ; End marker

; Message 0: 'ONTO{ THE }^NEXT YEAR'
;
; Used by the routine at JUMPING.
MSG000:
  DEFM "ONTO"
  DEFB 142                ; 142: ' THE '
  DEFB 2                  ; Newline
  DEFM "NEXT YEAR"
  DEFB 0                  ; End marker

; Unused
  DEFB 32

  ORG 65024

; Addresses of messages 0-111 (LSBs)
;
; Used by the routine at NEXTCHR. The LSBs of the addresses of messages 128-207
; can be found at MSGLSBS2.
MSGLSBS1:
  DEFB 239                ; Message 0: 'ONTO{ THE }^NEXT YEAR'
  DEFB 52                 ; Message 1: 'NO^STINKBOMBS'
  DEFB 66                 ; Message 2: 'NO^CATAPULTS'
  DEFB 192                ; Message 3: 'Please Sir I cannot tell a lie . . '
  DEFB 228                ; Message 4: '<9 spaces>'
  DEFB 138                ; Message 5: '{lines recipient}/combination number or
                          ; letter/{room}'
  DEFB 136                ; Message 6: '{teacher}'
  DEFB 144                ; Message 7: '{character name}'
  DEFB 19                 ; Message 8: 'THE '
  DEFB 71                 ; Message 9: 'DON'T '
  DEFB 140                ; Message 10: '{mountain}/{king}/{animal}/{verb}'
  DEFB 142                ; Message 11: '{country}/{date}/{habitat}/{noun}'
  DEFB 0                  ; Message 12: score/lines total/hi-score/number of
                          ; lines being given
  DEFB 238                ; Message 13: '[12]0 LINES^[5]'
  DEFB 249                ; Message 14: '[5]^ '
  DEFB 1                  ; Message 15: 'NOW {DON'T }^DO IT AGAIN'
  DEFB 24                 ; Message 16: '[6]^[5]'
  DEFB 249                ; Message 17: 'MASTER'
  DEFB 40                 ; Message 18: '{Please Sir I cannot tell a lie . .
                          ; }{ERIC} is not here{9sp}'
  DEFB 56                 ; Message 19: '{DON'T }BE^LATE AGAIN'
  DEFB 78                 ; Message 20: 'STAY TILL I^DISMISS YOU'
  DEFB 83                 ; Message 21: 'MR WACKER'
  DEFB 96                 ; Message 22: 'MR WITHIT'
  DEFB 109                ; Message 23: 'MR ROCKITT'
  DEFB 122                ; Message 24: 'MR CREAK'
  DEFB 135                ; Message 25: 'MISS TAKE'
  DEFB 148                ; Message 26: 'ALBERT'
  DEFB 161                ; Message 27: 'BOY WANDER'
  DEFB 174                ; Message 28: 'ANGELFACE'
  DEFB 187                ; Message 29: 'EINSTEIN'
  DEFB 200                ; Message 30: 'HAYLEY'
  DEFB 213                ; Message 31: 'ERIC'
  DEFB 117                ; Message 32: 'ARTESIAN^WELLS'
  DEFB 132                ; Message 33: '{THE }DOLDRUMS^ '
  DEFB 144                ; Message 34: 'TASTY^GEYSERS'
  DEFB 158                ; Message 35: '{THE }GREEN^REVOLUTION'
  DEFB 176                ; Message 36: 'TREACLE^MINING'
  DEFB 191                ; Message 37: 'FROG FARMING^ '
  DEFB 206                ; Message 38: 'HEAVY WATER^ '
  DEFB 220                ; Message 39: 'HOLOGRAMS &^LASERS'
  DEFB 239                ; Message 40: 'DNA^ '
  DEFB 245                ; Message 41: 'VAMPIRE^BATS'
  DEFB 2                  ; Message 42: 'NUCLEAR^FUSION'
  DEFB 17                 ; Message 43: 'BACTERIA^AS PETS'
  DEFB 34                 ; Message 44: 'ATTILA{ THE }^HUN'
  DEFB 46                 ; Message 45: 'ERIC{ THE }RED^ '
  DEFB 57                 ; Message 46: 'NOGGIN{ THE }^NOG'
  DEFB 69                 ; Message 47: 'IVAN{ THE }^TERRIBLE'
  DEFB 84                 ; Message 48: 'ETHELRED{ THE }^UNREADY'
  DEFB 102                ; Message 49: '{THE }LUDDITES^ '
  DEFB 114                ; Message 50: 'IAMBIC^PENTAMETERS'
  DEFB 133                ; Message 51: 'ELOCUTION^AINT ARF FUN'
  DEFB 156                ; Message 52: 'SUGAR AND^SPICE'
  DEFB 172                ; Message 53: 'TONE POEMS^ '
  DEFB 185                ; Message 54: 'ELEMENTARY^ASTROPHYSICS'
  DEFB 209                ; Message 55: '{THE }BARD OF^AVON'
  DEFB 223                ; Message 56: '{i hate ^}girls'
  DEFB 230                ; Message 57: '{i hate ^}skool'
  DEFB 237                ; Message 58: '{i hate ^}mafs'
  DEFB 243                ; Message 59: '{i hate ^}{MR WACKER}'
  DEFB 246                ; Message 60: '{i hate ^}groan-ups'
  DEFB 1                  ; Message 61: 'who's Sam^Cruise?'
  DEFB 39                 ; Message 62: 'YOU ARE NOT^ALLOWED HERE'
  DEFB 28                 ; Message 63: 'DEMO.MODE^ '
  DEFB 77                 ; Message 64: '{GET }OFF^{THE }PLANTS'
  DEFB 90                 ; Message 65: '{DON'T }RIDE^BIKES IN HERE'
  DEFB 110                ; Message 66: '{GET }OFF^{THE }FLOOR'
  DEFB 122                ; Message 67: '{GET }BACK^TO SCHOOL'
  DEFB 138                ; Message 68: '{GET }ALONG^NOW'
  DEFB 149                ; Message 69: '{SIT }FACING^{THE }STAGE'
  DEFB 164                ; Message 70: 'NOW^{SIT DOWN }'
  DEFB 170                ; Message 71: 'COME ALONG^YOU MONSTER'
  DEFB 193                ; Message 72: '{DON'T }KEEP^ME WAITING'
  DEFB 244                ; Message 73: '{{Please Sir I cannot tell a lie . .
                          ; }it is }in [11]{9sp}'
  DEFB 112                ; Message 74: 'WHERE IS MT.[10]?{9sp}'
  DEFB 234                ; Message 75: '{{Please Sir I cannot tell a lie . .
                          ; }it is }MT.[10]{9sp}'
  DEFB 208                ; Message 76: 'WHAT IS THE HIGHEST MOUNTAIN IN
                          ; [11]?{9sp}'
  DEFB 251                ; Message 77: '{{Please Sir I cannot tell a lie . .
                          ; }it is }A[11]{9sp}'
  DEFB 121                ; Message 78: 'WHERE DOES A[10] LIVE?{9sp}'
  DEFB 251                ; Message 79: '{{Please Sir I cannot tell a lie . .
                          ; }it is }A[10]{9sp}'
  DEFB 232                ; Message 80: 'WHAT LIVES IN A[11]?{9sp}'
  DEFB 241                ; Message 81: '{Please Sir I cannot tell a lie . .
                          ; }it was in 1[11]{9sp}'
  DEFB 96                 ; Message 82: 'WHEN DID [10] BECOME KING?{9sp}'
  DEFB 112                ; Message 83: '{Please Sir I cannot tell a lie . .
                          ; }it was KING [10]{9sp}'
  DEFB 208                ; Message 84: 'WHO BECAME KING IN 1[11]?{9sp}'
  DEFB 210                ; Message 85: '{DON'T }TELL^TALES'
  DEFB 222                ; Message 86: '{DON'T }TOUCH^BLACKBOARDS'
  DEFB 241                ; Message 87: '{Please Sir I cannot tell a lie . .
                          ; }{ERIC} hit me{9sp}'
  DEFB 252                ; Message 88: '{Please Sir I cannot tell a lie . .
                          ; }[7] wrote on the board{9sp}'
  DEFB 19                 ; Message 89: 'START REVISING FOR YOUR EXAMS{9sp}'
  DEFB 50                 ; Message 90: 'START READING AT THE NEXT CHAPTER IN
                          ; YOUR BOOKS{9sp}'
  DEFB 99                 ; Message 91: 'WRITE AN ESSAY TITLED 'WHY I LOVE
                          ; SCHOOL'{9sp}'
  DEFB 142                ; Message 92: '{SIT DOWN }CHAPS{9sp}'
  DEFB 150                ; Message 93: '{SIT DOWN }MY CHERUBS{9sp}'
  DEFB 163                ; Message 94: '{SIT DOWN }YOU LITTLE ANARCHISTS{9sp}'
  DEFB 187                ; Message 95: '{SIT DOWN }'
  DEFB 189                ; Message 96: 'YOU'RE ALL IN DETENTION UNTIL I FIND
                          ; OUT WHO [10]{ THE }[11]{9sp}'
  DEFB 249                ; Message 97: '{DON'T }HIT^YOUR MATES'
  DEFB 9                  ; Message 98: '{MR WACKER}^HE'S ESCAPING'
  DEFB 25                 ; Message 99: 'YOU HAVE 10000 LINES{
                          ; {ERIC}{9sp}YOU'RE EXPELLED{9sp}}'
  DEFB 47                 ; Message 100: 'YOU ARE NOT A BIRD{ {ERIC}{9sp}YOU'RE
                          ; EXPELLED{9sp}}'
  DEFB 244                ; Message 101: '{THE }HEADMASTER'
  DEFB 160                ; Message 102: '{THE }GEOGRAPHY {MASTER}'
  DEFB 173                ; Message 103: '{THE }SCIENCE {MASTER}'
  DEFB 184                ; Message 104: '{THE }HISTORY {MASTER}'
  DEFB 195                ; Message 105: '{THE }HEADMISTRESS '
  DEFB 210                ; Message 106: '{THE }CARETAKER'
  DEFB 221                ; Message 107: '{THE }TEARAWAY'
  DEFB 200                ; Message 108: '{THE }BULLY'
  DEFB 231                ; Message 109: '{THE }SWOT'
  DEFB 237                ; Message 110: '{THE }HEROINE'
  DEFB 246                ; Message 111: 'OUR HERO'

; Message 74: 'WHERE IS MT.[10]?{9sp}'
;
; Used by the routine at DOCLASS.
MSG074:
  DEFM "WHERE IS MT."
  DEFB 10                 ; 10: '{mountain}'
  DEFM "?"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

  ORG 65152

; Addresses of messages 128-207 (LSBs)
;
; Used by the routine at NEXTCHR. The LSBs of the addresses of messages 0-111
; can be found at MSGLSBS1.
MSGLSBS2:
  DEFB 87                 ; Message 128: 'PLAYTIME'
  DEFB 96                 ; Message 129: 'ASSEMBLY'
  DEFB 105                ; Message 130: 'DINNER'
  DEFB 112                ; Message 131: 'LIBRARY'
  DEFB 120                ; Message 132: 'SCIENCE LAB'
  DEFB 132                ; Message 133: 'BLUE ROOM'
  DEFB 142                ; Message 134: 'YELLOW ROOM'
  DEFB 154                ; Message 135: 'REVISION'
  DEFB 79                 ; Message 136: 'NO^WATERPISTOLS'
  DEFB 226                ; Message 137: '{Please Sir I cannot tell a lie . .
                          ; }it is '
  DEFB 67                 ; Message 138: ' {ERIC}{9sp}YOU'RE EXPELLED{9sp}'
  DEFB 19                 ; Message 139: 'SIT DOWN '
  DEFB 29                 ; Message 140: 'SIT '
  DEFB 34                 ; Message 141: 'GET '
  DEFB 102                ; Message 142: ' THE '
  DEFB 108                ; Message 143: 'i hate ^'
  DEFB 239                ; Message 144: 'KIDNAPPED'
  DEFB 253                ; Message 145: 'ATE'
  DEFB 163                ; Message 146: 'SET FIRE TO'
  DEFB 175                ; Message 147: 'BLEW UP'
  DEFB 183                ; Message 148: 'IS MAKING RUDE PHONE CALLS TO'
  DEFB 213                ; Message 149: 'IS BLACKMAILING'
  DEFB 229                ; Message 150: 'SQUASHED'
  DEFB 238                ; Message 151: 'POISONED'
  DEFB 247                ; Message 152: 'GOLDFISH'
  DEFB 0                  ; Message 153: 'SCHOOL CAT'
  DEFB 11                 ; Message 154: 'LATIN MASTER'
  DEFB 24                 ; Message 155: 'LOLLIPOP LADY'
  DEFB 38                 ; Message 156: 'PTA'
  DEFB 42                 ; Message 157: 'CARETAKER'S BUDGIE'
  DEFB 61                 ; Message 158: 'MILK MONITOR'
  DEFB 74                 ; Message 159: 'HEAD BOY'
  DEFB 142                ; Message 160: 'KILIMANJARO'
  DEFB 154                ; Message 161: 'KOSCIUSKO'
  DEFB 164                ; Message 162: 'McKINLEY'
  DEFB 173                ; Message 163: 'KOMMUNISMA'
  DEFB 184                ; Message 164: 'HUASCARAN'
  DEFB 194                ; Message 165: 'SAJAMA'
  DEFB 201                ; Message 166: 'COOK'
  DEFB 206                ; Message 167: 'SNOWDON'
  DEFB 214                ; Message 168: 'TANZANIA'
  DEFB 223                ; Message 169: 'AUSTRALIA'
  DEFB 233                ; Message 170: 'ALASKA'
  DEFB 240                ; Message 171: 'THE USSR'
  DEFB 249                ; Message 172: 'PERU'
  DEFB 254                ; Message 173: 'BOLIVIA'
  DEFB 6                  ; Message 174: 'NEW ZEALAND'
  DEFB 18                 ; Message 175: 'WALES'
  DEFB 124                ; Message 176: ' BADGER'
  DEFB 132                ; Message 177: ' SQUIRREL'
  DEFB 142                ; Message 178: ' BEAVER'
  DEFB 150                ; Message 179: ' HARE'
  DEFB 156                ; Message 180: 'N EAGLE'
  DEFB 164                ; Message 181: 'N OTTER'
  DEFB 172                ; Message 182: ' BEE'
  DEFB 177                ; Message 183: ' BUG'
  DEFB 182                ; Message 184: ' SET'
  DEFB 187                ; Message 185: ' DREY'
  DEFB 193                ; Message 186: ' LODGE'
  DEFB 200                ; Message 187: ' FORM'
  DEFB 206                ; Message 188: 'N EYRIE'
  DEFB 214                ; Message 189: ' HOLT'
  DEFB 220                ; Message 190: ' HIVE'
  DEFB 226                ; Message 191: ' BAD PROGRAM'
  DEFB 24                 ; Message 192: 'JOHN'
  DEFB 29                 ; Message 193: 'STEPHEN'
  DEFB 37                 ; Message 194: 'JAMES I'
  DEFB 45                 ; Message 195: 'GEORGE V'
  DEFB 54                 ; Message 196: 'HENRY V'
  DEFB 62                 ; Message 197: 'HENRY VII'
  DEFB 72                 ; Message 198: 'GEORGE I'
  DEFB 81                 ; Message 199: 'CHARLES I'
  DEFB 92                 ; Message 200: '199'
  DEFB 96                 ; Message 201: '135'
  DEFB 100                ; Message 202: '603'
  DEFB 104                ; Message 203: '910'
  DEFB 108                ; Message 204: '413'
  DEFB 112                ; Message 205: '485'
  DEFB 116                ; Message 206: '714'
  DEFB 120                ; Message 207: '625'

; Message 76: 'WHAT IS THE HIGHEST MOUNTAIN IN [11]?{9sp}'
;
; Used by the routine at DOCLASS.
MSG076:
  DEFM "WHAT IS THE HIGHEST"
  DEFM " MOUNTAIN IN "
  DEFB 11                 ; 11: '{country}'
  DEFM "?"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 73: '{{Please Sir I cannot tell a lie . . }it is }in [11]{9sp}'
;
; Used by the routine at DOCLASS.
MSG073:
  DEFB 137                ; 137: '{Please Sir I cannot tell a lie . . }it is '
  DEFM "in "
  DEFB 11                 ; 11: '{country}'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 77: '{{Please Sir I cannot tell a lie . . }it is }A[11]{9sp}'
;
; Used by the routine at DOCLASS.
MSG077:
  DEFB 137                ; 137: '{Please Sir I cannot tell a lie . . }it is '
  DEFM "A"
  DEFB 11                 ; 11: '{habitat}'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

  ORG 65280

; Addresses of messages 0-111 (MSBs)
;
; Used by the routine at NEXTCHR. The MSBs of the addresses of messages 128-207
; can be found at MSGMSBS2.
MSGMSBS1:
  DEFB 253                ; Message 0: 'ONTO{ THE }^NEXT YEAR'
  DEFB 252                ; Message 1: 'NO^STINKBOMBS'
  DEFB 252                ; Message 2: 'NO^CATAPULTS'
  DEFB 233                ; Message 3: 'Please Sir I cannot tell a lie . . '
  DEFB 233                ; Message 4: '<9 spaces>'
  DEFB 127                ; Message 5: '{lines recipient}/combination number or
                          ; letter/{room}'
  DEFB 127                ; Message 6: '{teacher}'
  DEFB 127                ; Message 7: '{character name}'
  DEFB 234                ; Message 8: 'THE '
  DEFB 234                ; Message 9: 'DON'T '
  DEFB 127                ; Message 10: '{mountain}/{king}/{animal}/{verb}'
  DEFB 127                ; Message 11: '{country}/{date}/{habitat}/{noun}'
  DEFB 227                ; Message 12: score/lines total/hi-score/number of
                          ; lines being given
  DEFB 233                ; Message 13: '[12]0 LINES^[5]'
  DEFB 233                ; Message 14: '[5]^ '
  DEFB 234                ; Message 15: 'NOW {DON'T }^DO IT AGAIN'
  DEFB 234                ; Message 16: '[6]^[5]'
  DEFB 86                 ; Message 17: 'MASTER'
  DEFB 234                ; Message 18: '{Please Sir I cannot tell a lie . .
                          ; }{ERIC} is not here{9sp}'
  DEFB 234                ; Message 19: '{DON'T }BE^LATE AGAIN'
  DEFB 234                ; Message 20: 'STAY TILL I^DISMISS YOU'
  DEFB 239                ; Message 21: 'MR WACKER'
  DEFB 239                ; Message 22: 'MR WITHIT'
  DEFB 239                ; Message 23: 'MR ROCKITT'
  DEFB 239                ; Message 24: 'MR CREAK'
  DEFB 239                ; Message 25: 'MISS TAKE'
  DEFB 239                ; Message 26: 'ALBERT'
  DEFB 239                ; Message 27: 'BOY WANDER'
  DEFB 239                ; Message 28: 'ANGELFACE'
  DEFB 239                ; Message 29: 'EINSTEIN'
  DEFB 239                ; Message 30: 'HAYLEY'
  DEFB 239                ; Message 31: 'ERIC'
  DEFB 234                ; Message 32: 'ARTESIAN^WELLS'
  DEFB 234                ; Message 33: '{THE }DOLDRUMS^ '
  DEFB 234                ; Message 34: 'TASTY^GEYSERS'
  DEFB 234                ; Message 35: '{THE }GREEN^REVOLUTION'
  DEFB 234                ; Message 36: 'TREACLE^MINING'
  DEFB 234                ; Message 37: 'FROG FARMING^ '
  DEFB 234                ; Message 38: 'HEAVY WATER^ '
  DEFB 234                ; Message 39: 'HOLOGRAMS &^LASERS'
  DEFB 234                ; Message 40: 'DNA^ '
  DEFB 234                ; Message 41: 'VAMPIRE^BATS'
  DEFB 235                ; Message 42: 'NUCLEAR^FUSION'
  DEFB 235                ; Message 43: 'BACTERIA^AS PETS'
  DEFB 235                ; Message 44: 'ATTILA{ THE }^HUN'
  DEFB 235                ; Message 45: 'ERIC{ THE }RED^ '
  DEFB 235                ; Message 46: 'NOGGIN{ THE }^NOG'
  DEFB 235                ; Message 47: 'IVAN{ THE }^TERRIBLE'
  DEFB 235                ; Message 48: 'ETHELRED{ THE }^UNREADY'
  DEFB 235                ; Message 49: '{THE }LUDDITES^ '
  DEFB 235                ; Message 50: 'IAMBIC^PENTAMETERS'
  DEFB 235                ; Message 51: 'ELOCUTION^AINT ARF FUN'
  DEFB 235                ; Message 52: 'SUGAR AND^SPICE'
  DEFB 235                ; Message 53: 'TONE POEMS^ '
  DEFB 235                ; Message 54: 'ELEMENTARY^ASTROPHYSICS'
  DEFB 235                ; Message 55: '{THE }BARD OF^AVON'
  DEFB 235                ; Message 56: '{i hate ^}girls'
  DEFB 235                ; Message 57: '{i hate ^}skool'
  DEFB 235                ; Message 58: '{i hate ^}mafs'
  DEFB 235                ; Message 59: '{i hate ^}{MR WACKER}'
  DEFB 235                ; Message 60: '{i hate ^}groan-ups'
  DEFB 236                ; Message 61: 'who's Sam^Cruise?'
  DEFB 236                ; Message 62: 'YOU ARE NOT^ALLOWED HERE'
  DEFB 234                ; Message 63: 'DEMO.MODE^ '
  DEFB 236                ; Message 64: '{GET }OFF^{THE }PLANTS'
  DEFB 236                ; Message 65: '{DON'T }RIDE^BIKES IN HERE'
  DEFB 236                ; Message 66: '{GET }OFF^{THE }FLOOR'
  DEFB 236                ; Message 67: '{GET }BACK^TO SCHOOL'
  DEFB 236                ; Message 68: '{GET }ALONG^NOW'
  DEFB 236                ; Message 69: '{SIT }FACING^{THE }STAGE'
  DEFB 236                ; Message 70: 'NOW^{SIT DOWN }'
  DEFB 236                ; Message 71: 'COME ALONG^YOU MONSTER'
  DEFB 236                ; Message 72: '{DON'T }KEEP^ME WAITING'
  DEFB 254                ; Message 73: '{{Please Sir I cannot tell a lie . .
                          ; }it is }in [11]{9sp}'
  DEFB 254                ; Message 74: 'WHERE IS MT.[10]?{9sp}'
  DEFB 239                ; Message 75: '{{Please Sir I cannot tell a lie . .
                          ; }it is }MT.[10]{9sp}'
  DEFB 254                ; Message 76: 'WHAT IS THE HIGHEST MOUNTAIN IN
                          ; [11]?{9sp}'
  DEFB 254                ; Message 77: '{{Please Sir I cannot tell a lie . .
                          ; }it is }A[11]{9sp}'
  DEFB 252                ; Message 78: 'WHERE DOES A[10] LIVE?{9sp}'
  DEFB 255                ; Message 79: '{{Please Sir I cannot tell a lie . .
                          ; }it is }A[10]{9sp}'
  DEFB 255                ; Message 80: 'WHAT LIVES IN A[11]?{9sp}'
  DEFB 239                ; Message 81: '{Please Sir I cannot tell a lie . .
                          ; }it was in 1[11]{9sp}'
  DEFB 252                ; Message 82: 'WHEN DID [10] BECOME KING?{9sp}'
  DEFB 255                ; Message 83: '{Please Sir I cannot tell a lie . .
                          ; }it was KING [10]{9sp}'
  DEFB 255                ; Message 84: 'WHO BECAME KING IN 1[11]?{9sp}'
  DEFB 236                ; Message 85: '{DON'T }TELL^TALES'
  DEFB 236                ; Message 86: '{DON'T }TOUCH^BLACKBOARDS'
  DEFB 236                ; Message 87: '{Please Sir I cannot tell a lie . .
                          ; }{ERIC} hit me{9sp}'
  DEFB 236                ; Message 88: '{Please Sir I cannot tell a lie . .
                          ; }[7] wrote on the board{9sp}'
  DEFB 237                ; Message 89: 'START REVISING FOR YOUR EXAMS{9sp}'
  DEFB 237                ; Message 90: 'START READING AT THE NEXT CHAPTER IN
                          ; YOUR BOOKS{9sp}'
  DEFB 237                ; Message 91: 'WRITE AN ESSAY TITLED 'WHY I LOVE
                          ; SCHOOL'{9sp}'
  DEFB 237                ; Message 92: '{SIT DOWN }CHAPS{9sp}'
  DEFB 237                ; Message 93: '{SIT DOWN }MY CHERUBS{9sp}'
  DEFB 237                ; Message 94: '{SIT DOWN }YOU LITTLE ANARCHISTS{9sp}'
  DEFB 237                ; Message 95: '{SIT DOWN }'
  DEFB 237                ; Message 96: 'YOU'RE ALL IN DETENTION UNTIL I FIND
                          ; OUT WHO [10]{ THE }[11]{9sp}'
  DEFB 237                ; Message 97: '{DON'T }HIT^YOUR MATES'
  DEFB 238                ; Message 98: '{MR WACKER}^HE'S ESCAPING'
  DEFB 238                ; Message 99: 'YOU HAVE 10000 LINES{
                          ; {ERIC}{9sp}YOU'RE EXPELLED{9sp}}'
  DEFB 238                ; Message 100: 'YOU ARE NOT A BIRD{ {ERIC}{9sp}YOU'RE
                          ; EXPELLED{9sp}}'
  DEFB 86                 ; Message 101: '{THE }HEADMASTER'
  DEFB 87                 ; Message 102: '{THE }GEOGRAPHY {MASTER}'
  DEFB 87                 ; Message 103: '{THE }SCIENCE {MASTER}'
  DEFB 87                 ; Message 104: '{THE }HISTORY {MASTER}'
  DEFB 87                 ; Message 105: '{THE }HEADMISTRESS '
  DEFB 87                 ; Message 106: '{THE }CARETAKER'
  DEFB 87                 ; Message 107: '{THE }TEARAWAY'
  DEFB 86                 ; Message 108: '{THE }BULLY'
  DEFB 87                 ; Message 109: '{THE }SWOT'
  DEFB 87                 ; Message 110: '{THE }HEROINE'
  DEFB 87                 ; Message 111: 'OUR HERO'

; Message 83: '{Please Sir I cannot tell a lie . . }it was KING [10]{9sp}'
;
; Used by the routine at DOCLASS.
MSG083:
  DEFB 3                  ; 3: 'Please Sir I cannot tell a lie . . '
  DEFM "it was"
  DEFM " KING "
  DEFB 10                 ; 10: '{king}'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

  ORG 65408

; Addresses of messages 128-207 (MSBs)
;
; Used by the routine at NEXTCHR. The MSBs of the addresses of messages 0-111
; can be found at MSGMSBS1.
MSGMSBS2:
  DEFB 238                ; Message 128: 'PLAYTIME'
  DEFB 238                ; Message 129: 'ASSEMBLY'
  DEFB 238                ; Message 130: 'DINNER'
  DEFB 238                ; Message 131: 'LIBRARY'
  DEFB 238                ; Message 132: 'SCIENCE LAB'
  DEFB 238                ; Message 133: 'BLUE ROOM'
  DEFB 238                ; Message 134: 'YELLOW ROOM'
  DEFB 238                ; Message 135: 'REVISION'
  DEFB 252                ; Message 136: 'NO^WATERPISTOLS'
  DEFB 239                ; Message 137: '{Please Sir I cannot tell a lie . .
                          ; }it is '
  DEFB 238                ; Message 138: ' {ERIC}{9sp}YOU'RE EXPELLED{9sp}'
  DEFB 236                ; Message 139: 'SIT DOWN '
  DEFB 236                ; Message 140: 'SIT '
  DEFB 236                ; Message 141: 'GET '
  DEFB 234                ; Message 142: ' THE '
  DEFB 234                ; Message 143: 'i hate ^'
  DEFB 237                ; Message 144: 'KIDNAPPED'
  DEFB 233                ; Message 145: 'ATE'
  DEFB 238                ; Message 146: 'SET FIRE TO'
  DEFB 238                ; Message 147: 'BLEW UP'
  DEFB 238                ; Message 148: 'IS MAKING RUDE PHONE CALLS TO'
  DEFB 238                ; Message 149: 'IS BLACKMAILING'
  DEFB 238                ; Message 150: 'SQUASHED'
  DEFB 238                ; Message 151: 'POISONED'
  DEFB 238                ; Message 152: 'GOLDFISH'
  DEFB 239                ; Message 153: 'SCHOOL CAT'
  DEFB 239                ; Message 154: 'LATIN MASTER'
  DEFB 239                ; Message 155: 'LOLLIPOP LADY'
  DEFB 239                ; Message 156: 'PTA'
  DEFB 239                ; Message 157: 'CARETAKER'S BUDGIE'
  DEFB 239                ; Message 158: 'MILK MONITOR'
  DEFB 239                ; Message 159: 'HEAD BOY'
  DEFB 252                ; Message 160: 'KILIMANJARO'
  DEFB 252                ; Message 161: 'KOSCIUSKO'
  DEFB 252                ; Message 162: 'McKINLEY'
  DEFB 252                ; Message 163: 'KOMMUNISMA'
  DEFB 252                ; Message 164: 'HUASCARAN'
  DEFB 252                ; Message 165: 'SAJAMA'
  DEFB 252                ; Message 166: 'COOK'
  DEFB 252                ; Message 167: 'SNOWDON'
  DEFB 252                ; Message 168: 'TANZANIA'
  DEFB 252                ; Message 169: 'AUSTRALIA'
  DEFB 252                ; Message 170: 'ALASKA'
  DEFB 252                ; Message 171: 'THE USSR'
  DEFB 252                ; Message 172: 'PERU'
  DEFB 252                ; Message 173: 'BOLIVIA'
  DEFB 253                ; Message 174: 'NEW ZEALAND'
  DEFB 253                ; Message 175: 'WALES'
  DEFB 253                ; Message 176: ' BADGER'
  DEFB 253                ; Message 177: ' SQUIRREL'
  DEFB 253                ; Message 178: ' BEAVER'
  DEFB 253                ; Message 179: ' HARE'
  DEFB 253                ; Message 180: 'N EAGLE'
  DEFB 253                ; Message 181: 'N OTTER'
  DEFB 253                ; Message 182: ' BEE'
  DEFB 253                ; Message 183: ' BUG'
  DEFB 253                ; Message 184: ' SET'
  DEFB 253                ; Message 185: ' DREY'
  DEFB 253                ; Message 186: ' LODGE'
  DEFB 253                ; Message 187: ' FORM'
  DEFB 253                ; Message 188: 'N EYRIE'
  DEFB 253                ; Message 189: ' HOLT'
  DEFB 253                ; Message 190: ' HIVE'
  DEFB 253                ; Message 191: ' BAD PROGRAM'
  DEFB 253                ; Message 192: 'JOHN'
  DEFB 253                ; Message 193: 'STEPHEN'
  DEFB 253                ; Message 194: 'JAMES I'
  DEFB 253                ; Message 195: 'GEORGE V'
  DEFB 253                ; Message 196: 'HENRY V'
  DEFB 253                ; Message 197: 'HENRY VII'
  DEFB 253                ; Message 198: 'GEORGE I'
  DEFB 253                ; Message 199: 'CHARLES I'
  DEFB 253                ; Message 200: '199'
  DEFB 253                ; Message 201: '135'
  DEFB 253                ; Message 202: '603'
  DEFB 253                ; Message 203: '910'
  DEFB 253                ; Message 204: '413'
  DEFB 253                ; Message 205: '485'
  DEFB 253                ; Message 206: '714'
  DEFB 253                ; Message 207: '625'

; Message 84: 'WHO BECAME KING IN 1[11]?{9sp}'
;
; Used by the routine at DOCLASS.
MSG084:
  DEFM "WHO BECAME KING IN 1"
  DEFB 11                 ; 11: '{year}'
  DEFM "?"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 80: 'WHAT LIVES IN A[11]?{9sp}'
;
; Used by the routine at DOCLASS.
MSG080:
  DEFM "WHAT LIVES IN A"
  DEFB 11                 ; 11: '{habitat}'
  DEFM "?"
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

; Message 79: '{{Please Sir I cannot tell a lie . . }it is }A[10]{9sp}'
;
; Used by the routine at DOCLASS.
MSG079:
  DEFB 137                ; 137: '{Please Sir I cannot tell a lie . . }it is '
  DEFM "A"
  DEFB 10                 ; 10: '{animal}'
  DEFB 4                  ; 4: '         ' (9 spaces)
  DEFB 0                  ; End marker

